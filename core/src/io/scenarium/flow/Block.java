/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow;

import java.beans.EventSetDescriptor;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javax.swing.event.EventListenerList;

import io.beanmanager.BeanDesc;
import io.beanmanager.BeanRenameListener;
import io.beanmanager.editors.DynamicEnableBean;
import io.beanmanager.editors.DynamicVisibleBean;
import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.TransientProperty;
import io.beanmanager.editors.container.BeanEditor;
import io.beanmanager.editors.primitive.number.ControlType;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.beanmanager.tools.WrapperTools;
import io.scenarium.core.filemanager.scenariomanager.StructChangeListener;
import io.scenarium.core.tools.Unit;
import io.scenarium.flow.internal.Log;
import io.scenarium.flow.operator.EvolvedVarArgsOperator;
import io.scenarium.flow.operator.OperatorManager;
import io.scenarium.flow.operator.player.SubFlowDiagram;
import io.scenarium.flow.operator.rmi.RemoteOperator;
import io.scenarium.flow.scheduler.DiagramScheduler;

import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;

public class Block implements IOComponent, CpuUsageMeasurable, LocalisedObject, DynamicVisibleBean, DynamicEnableBean, StructChangeListener {
	public static final String METHOD_OUTPUT_PREFIX = "getOutput";
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	public final Object operator;
	private List<BlockInput> inputs = Collections.unmodifiableList(new ArrayList<>()); // Le nombre de patte peux avoir changé entre un getNbInput et l'accès...
	private List<BlockOutput> outputs = Collections.unmodifiableList(new ArrayList<>());
	private List<BlockOutput> triggerableOutputs = Collections.unmodifiableList(new ArrayList<>());
	private final ReentrantLock ioLock = new ReentrantLock();
	private Rectangle2D rectangle;
	@TransientProperty
	private String error;
	@TransientProperty
	private String warning;
	@TransientProperty
	private boolean alive = false;
	@PropertyInfo(index = 1, info = "Specify if the block is enable and is executable at runtime")
	private boolean enable;
	@PropertyInfo(index = 2, info = "Indicates whether all data should be consumed before the operator's death")
	private boolean consumesAllDataBeforeDying = false;
	@PropertyInfo(index = 3, info = "Block priority when starting a diagram. Blocks with a higher priority will be initialized before blocks with a lower priority")
	@NumberInfo(controlType = ControlType.SPINNER)
	private int startPriority;
	@PropertyInfo(index = 4, info = "Block priority when stopping a diagram. Blocks with a higher priority will be stopped before blocks with a lower priority")
	@NumberInfo(controlType = ControlType.SPINNER)
	private int stopPriority;
	@PropertyInfo(index = 5, info = "Priority of block's thread. Only used for diagram in multicore mode")
	@NumberInfo(min = Thread.MIN_PRIORITY, max = Thread.MAX_PRIORITY, controlType = ControlType.SPINNER)
	private int threadPriority = Thread.NORM_PRIORITY;

	@PropertyInfo(index = 13, info = "Specify the execution mode:\nLOCAL: The block is executed in the Scenarium process\nISOLATED: The block is executed in a separate process\nREMOTE: The block is executed on a remote machine")
	private ProcessMode processMode;
	@PropertyInfo(index = 14, info = "Socket port used for the communication between isolate process and Scenarium process")
	private int isolatePort;
	@PropertyInfo(index = 15, info = "Synchronize properties between the local and the remote operator")
	private boolean synchronizeProperties;
	@PropertyInfo(index = 16, info = "Ip and port of the remote machine used to process the block")
	private InetSocketAddress remoteIp;
	@PropertyInfo(index = 17, info = "Create an remote operator on the remote machine and request properties on this one.\nThis mode must be used if the choice of some properties depends of the remote machine (ex. serial communication port available).")
	private boolean remoteProperty;

	private final EventListenerList listeners = new EventListenerList();
	private int nbPropertyAsInput;
	private int nbStaticInput;
	private int nbDynamicInput;
	private int nbVarArgsInput = -1;
	private int nbStaticOutput;
	private int nbPropertyAsOutput;

	@TransientProperty
	private float cpuUsage = -1;
	// private LinkedList<IODatas> entrieConsumptionStack;
	private boolean processMethodReturnVoid;
	private long nbConsume;
	private SetterPropertyHandler[] setterPropertyMethodHandles;
	private GetterPropertyHandler[] getterPropertyMethodHandles;
	private MethodHandle processMethodHandler;
	private Object[] varArgsBuff;
	private Object[] outputBuff;
	private long[] outputBuffTs;
	private long[] outputBuffToi;
	private long[] inputTs;
	private long[] inputTOI;
	private IOLinks[] index;
	@TransientProperty
	private Thread thread;
	@TransientProperty
	private RemoteOperator remoteOperator;
	private PropertyChangeListener runTimePropertyChangeListener;
	// Function used for non-EvolvedOperator to change inputs or outputs without synchronization issue
	private Consumer<Runnable> runLaterFunction;
	private BiConsumer<String, Object> triggerPropertyFunction;
	private final BeanRenameListener brl;

	/** Create a block and linked them to the operator. To unlinked the operator, the method {@link #dispose() dispose} must be call
	 * @param operator
	 * @param location
	 * @param isMiddlePoint
	 * @throws IllegalInputArgument */
	public Block(Object operator, Point2D location, boolean isMiddlePoint) throws IllegalInputArgument {
		Objects.requireNonNull(operator);
		this.operator = operator;
		this.brl = (oldBeanDesc, beanDesc) -> {
			if (beanDesc.bean == operator)
				this.pcs.firePropertyChange("name", oldBeanDesc.name, beanDesc.name);
		};
		BeanEditor.addStrongRefBeanRenameListener(this.brl);
		if (!OperatorManager.isOperator(operator))
			throw new IllegalArgumentException("the object operator don't respect operator rules");
		if (location == null)
			location = new Point2D(0, 0);
		resetProperties();
		populate();
		// if (operator instanceof ScenarioPlayer)
		// ((ScenarioPlayer) operator).addPropertyChangeListener(e -> {
		// if (e.getPropertyName().equals("subManagement") && !(Boolean) e.getNewValue())
		// setProcessMode(ProcessMode.LOCAL);
		// });
		int width = 100;
		int height = computeHeight();
		this.rectangle = new Rectangle2D(isMiddlePoint ? location.getX() - width / 2 : location.getX(), isMiddlePoint ? location.getY() - height / 2 : location.getY(), width, height);
		// Log.info("create block: " + this);
	}

	// public boolean birthFailed() {
	// return this.birthFailed;
	// }

	@Override
	public boolean canTriggerOrBeTriggered() {
		return this.alive;// this.enable && !this.birthFailed;
	}

	public void clearTemporaryBuffer() {
		if (this.varArgsBuff != null)
			Arrays.fill(this.varArgsBuff, null);
		Arrays.fill(this.outputBuff, null);
		Arrays.fill(this.outputBuffTs, -1);
		Arrays.fill(this.outputBuffToi, -1);
	}

	private int computeHeight() {
		return Math.max(120, (this.outputs.size() + getNbInput()) * 20); // Re event si rechange
	}

	public void consume() {
		this.nbConsume++;
	}

	public long getConsume() {
		return this.nbConsume;
	}

	public float getCpuUsage() {
		return this.cpuUsage;
	}

	@Override
	public IOLinks[] getIndex() { // Double-checked Locking without Volatile. It's not a problem if the index is created several times.
		this.index = null;
		IOLinks[] index = this.index;
		if (index == null)
			synchronized (this) {
				index = this.index;
				if (index == null) {
					index = buildIndex(this.nbPropertyAsOutput);
					this.index = index;
				}
			}
		return index;
	}

	public int getInputIndex(String inputName) {
		for (int i = 0; i < this.inputs.size(); i++)
			if (this.inputs.get(i).getName().equals(inputName))
				return i - this.nbPropertyAsInput;
		throw new IllegalArgumentException(inputName + " is not an input of " + getName());
	}

	@Override
	public List<BlockInput> getInputs() {
		return this.inputs;
	}

	public ReentrantLock getIOLock() {
		return this.ioLock;
	}

	public int getIsolatePort() {
		return this.isolatePort;
	}

	/** Get the maximum timestamp of inputs or current time if there is no input.
	 *
	 * @param outputValue the output to trigger
	 *
	 * @return true if the output is successfully triggered
	 *
	 * @throws IllegalArgumentException if outputValue is null */
	public long getMaxTimeStamp() {
		if (this.inputTs == null)
			return System.currentTimeMillis();
		long maxTimeStamp = Long.MIN_VALUE;
		for (long ts : this.inputTs)
			if (ts > maxTimeStamp)
				maxTimeStamp = ts;
		return maxTimeStamp;
	}

	@PropertyInfo(index = 0, info = "Name of the block")
	public String getName() {
		if (this.operator != null)
			return BeanEditor.getBeanDesc(this.operator).name;
		return "ERROR";
	}

	public int getNbDynamicInput() {
		return this.nbDynamicInput;
	}

	public int getNbInput() {
		return this.inputs.size();
	}

	public int getNbTriggerableInput() {
		return this.inputs.size() - this.nbPropertyAsInput;
	}

	public int getNbTriggerableOutput() {
		return this.outputs.size() - this.nbPropertyAsOutput;
	}

	public int getNbPropertyAsInput() {
		return this.nbPropertyAsInput;
	}

	public int getNbStaticInput() {
		return this.nbStaticInput;
	}

	public int getNbStaticOutput() {
		return this.nbStaticOutput;
	}

	public int getNbPropertyAsOutput() {
		return this.nbPropertyAsOutput;
	}

	public int getNbVarArgsInput() {
		return this.nbVarArgsInput;
	}

	public Object getOperator() {
		return this.operator;
	}

	@Override
	public Object[] getOutputBuff() {
		return this.outputBuff;
	}

	public long[] getOutputBuffToi() {
		return this.outputBuffToi;
	}

	@Override
	public long[] getOutputBuffTs() {
		return this.outputBuffTs;
	}

	public int getOutputIndex(String outputName) {
		for (int i = 0; i < this.outputs.size(); i++)
			if (this.outputs.get(i).getName().equals(outputName))
				return i - this.nbPropertyAsOutput;
		throw new IllegalArgumentException(outputName + " is not an output of " + getName());
	}

	@Override
	public List<BlockOutput> getOutputs() {
		return this.outputs;
	}

	@Override
	public List<BlockOutput> getTriggerableOutputs() {
		return this.triggerableOutputs;
	}

	@Override
	public Point2D getPosition() {
		return new Point2D((float) (this.rectangle.getMinX() + this.rectangle.getWidth() / 2.0), (float) (this.rectangle.getMinY() + this.rectangle.getHeight() / 2.0));
	}

	public MethodHandle getProcessHandler() {
		return this.processMethodHandler;
	}

	public ProcessMode getProcessMode() {
		return this.processMode;
	}

	public SetterPropertyHandler getPropertyMethodHandles(int inputIndex) {
		return this.setterPropertyMethodHandles[inputIndex];
	}

	public Rectangle2D getRectangle() {
		return this.rectangle;
	}

	public InetSocketAddress getRemoteIp() {
		return this.remoteIp;
	}

	public RemoteOperator getRemoteOperator() {
		return this.remoteOperator;
	}

	public Thread getThread() {
		return this.thread;
	}

	public long getTimeOfIssue(int i) {
		return this.inputTOI[i + this.nbPropertyAsInput];
	}

	public long getTimeStamp(int i) {
		return this.inputTs[i + this.nbPropertyAsInput];
	}

	public Object[] getVarArgsBuff() {
		return this.varArgsBuff;
	}

	public String getWarning() {
		return this.warning;
	}

	public int indexOfInput(BlockInput input) {
		return this.inputs.indexOf(input);
	}

	public String getError() {
		return this.error;
	}

	public boolean isEnable() {
		return this.enable;
	}

	public boolean launchable() {
		return this.enable && !this.conflict;
	}

	public boolean isProcessMethodReturnVoid() {
		return this.processMethodReturnVoid;
	}

	public boolean isPropertyAsInput(String propertyName) {
		if (propertyName == null)
			throw new IllegalArgumentException("PropertyName cannot be null");
		for (int i = 0; i < this.nbPropertyAsInput; i++)
			if (this.inputs.get(i).getName().equals(propertyName))
				return true;
		return false;
	}

	public boolean isPropertyAsOutput(String propertyName) {
		if (propertyName == null)
			throw new IllegalArgumentException("PropertyName cannot be null");
		for (int i = 0; i < this.nbPropertyAsOutput; i++)
			if (this.outputs.get(i).getName().equals(propertyName))
				return true;
		return false;
	}

	@Override
	public boolean isReady() {
		for (int k = 0; k < this.inputs.size(); k++)
			if (!this.inputs.get(k).getBuffer().isEmpty())
				return true;
		return false;
	}

	public boolean isRemoteProperty() {
		return this.remoteProperty;
	}

	public boolean isRunTimePropertyChangeListener() {
		return this.runTimePropertyChangeListener != null;
	}

	public boolean isSynchronizeProperties() {
		return this.synchronizeProperties;
	}

	private void populate() throws IllegalInputArgument {
		this.inputs = populateInputs();
		this.outputs = populateOutputs();
		this.triggerableOutputs = this.outputs;
		this.processMethodReturnVoid = true;
		if (this.outputs != null)
			for (BlockOutput output : this.outputs)
				if (output.getMethodHandle() == null)
					this.processMethodReturnVoid = false;
		this.nbStaticOutput = this.outputs != null ? this.outputs.size() : 0;
		this.outputBuff = new Object[this.nbStaticOutput];
		this.outputBuffTs = new long[this.outputBuff.length];
		this.outputBuffToi = new long[this.outputBuffTs.length];
		if (this.nbVarArgsInput != -1)
			this.varArgsBuff = (Object[]) Array.newInstance(this.inputs.get(this.inputs.size() - 1).getType(), this.nbVarArgsInput);
		// bindBlock();
	}

	private List<BlockInput> populateInputs() throws IllegalInputArgument {
		if (this.operator == null)
			return null;
		this.nbVarArgsInput = -1;
		ArrayList<BlockInput> inputs = new ArrayList<>();
		HashSet<String> inputsName = new HashSet<>();
		for (Method method : this.operator.getClass().getMethods())
			if (method.getName().equals("process")) {
				method.setAccessible(true);
				try {
					this.processMethodHandler = MethodHandles.lookup().unreflect(method).bindTo(this.operator);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				String[] names = null;
				for (Annotation anno : method.getAnnotations())
					if (anno instanceof ParamInfo) {
						names = ((ParamInfo) anno).in();
						break;
					}
				int i = 0;
				Parameter[] parameters = method.getParameters();
				int nbParam = parameters.length;
				String varArgInputName = null;
				for (int j = 0; j < nbParam; j++) {
					Parameter param = parameters[j];
					Class<?> type = param.getType();
					if (type.isPrimitive())
						throw new IllegalInputArgument(
								"Process method of the block: " + this + " contains forbidden primitive type: " + type + ". It must be replace by " + WrapperTools.toWrapper(type).getSimpleName());
					boolean isVarArgs = method.isVarArgs() && j == parameters.length - 1;
					String inputName = names != null && i < names.length && names[i] != null ? names[i++] : param.isNamePresent() ? param.getName() : type.getSimpleName();
					if (isVarArgs) {
						this.nbVarArgsInput = 0;
						varArgInputName = inputName;
						type = type.getComponentType();
						inputName += "-0";
					}
					if (!inputsName.contains(inputName)) {
						inputs.add(new BlockInput(this, type, inputName, isVarArgs));
						inputsName.add(inputName);
					} else
						throw new IllegalInputArgument("The block: " + this + " contains multiple intputs " + inputName + "\nAdd or change input name of input to avoid confusion");
				}
				if (varArgInputName != null)
					for (BlockInput input : inputs)
						if (!input.isVarArgs() && isInConflictWithVariadicInputName(varArgInputName, input.getName()))
							throw new IllegalInputArgument("The static input name: " + input.getName() + " is in conflict with the variadic arguments pattern: " + varArgInputName + "-x");
				break;
			}
		this.nbStaticInput = inputs.size();
		if (this.nbVarArgsInput == 0)
			this.nbStaticInput--;
		return Collections.unmodifiableList(inputs);
	}

	private List<BlockOutput> populateOutputs() {
		if (this.operator == null)
			return List.of();
		ArrayList<BlockOutput> outputs = new ArrayList<>();
		Method[] methods = this.operator.getClass().getMethods();
		HashSet<String> outputsName = new HashSet<>();
		for (Method method : methods)
			if (method.getDeclaringClass() != EvolvedOperator.class && method.getName().startsWith(METHOD_OUTPUT_PREFIX) && method.getParameterTypes().length == 0
					&& !method.getReturnType().equals(Void.TYPE))
				try {
					String outputName = method.getName().substring(METHOD_OUTPUT_PREFIX.length());
					method.setAccessible(true);
					outputs.add(new BlockOutput(this, MethodHandles.lookup().unreflect(method).bindTo(this.operator), method.getReturnType(), outputName));
					outputsName.add(outputName);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
		Collections.sort(outputs, (o1, o2) -> o1.getName().compareTo(o2.getName()));
		Method youngestProcessMethod = null; // PK???
		for (Method method : methods)
			if (method.getName().equals("process") && !method.getReturnType().equals(Void.TYPE)
					&& (youngestProcessMethod == null || !method.getReturnType().isAssignableFrom(youngestProcessMethod.getReturnType())))
				youngestProcessMethod = method;
		if (youngestProcessMethod != null) {
			String outputName = null;
			for (Annotation anno : youngestProcessMethod.getAnnotations())
				if (anno instanceof ParamInfo) {
					outputName = ((ParamInfo) anno).out();
					break;
				}
			Class<?> returnType = youngestProcessMethod.getReturnType();
			if (DiagramScheduler.isCloneable(WrapperTools.toWrapper(returnType))) {
				if (outputName == null)
					outputName = returnType.getSimpleName();
				if (!outputsName.contains(outputName)) {
					BlockOutput output = new BlockOutput(this, null, returnType, outputName);
					outputs.add(0, output);
					outputsName.add(outputName);
				} else
					fireError("The block: " + this + " contains multiple outputs" + outputName);
			} else
				fireError("The block: " + this + " contains output" + outputName + " of type: " + returnType.getSimpleName() + " that is not cloneable");
		}
		return Collections.unmodifiableList(outputs);
	}

	public void reset() {
		this.cpuUsage = -1;
		this.nbConsume = 0;
		if (this.operator instanceof EvolvedOperator && !isConflict())
			((EvolvedOperator) this.operator).setAdditionalInputs(null);
		for (BlockInput input : this.inputs) {
			input.getBuffer().clear();
			Link link = input.getLink();
			if (link != null)
				link.resetConsume();
		}
		resetWarning();
	}

	public void resetIndex() {
		this.index = null;
	}

	public void resetProperties() {
		setEnable(true);
		setConsumesAllDataBeforeDying(false);
		setStartPriority(0);
		setStopPriority(0);
		setThreadPriority(Thread.NORM_PRIORITY);
		setProcessMode(ProcessMode.LOCAL);
		setIsolatePort(1099);
		setSynchronizeProperties(true);
		setRemoteIp(new InetSocketAddress(0));
		setRemoteProperty(false);
	}

	public boolean isAlive() {
		return this.alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	@Override
	public void setCpuUsage(float cpuUsage) {
		if (cpuUsage > 1)
			cpuUsage = 1;
		this.cpuUsage = cpuUsage;
	}

	public void setError(String error) {
		if (!Objects.equals(this.error, error)) {
			this.error = error;
			fireRunningStateChange();
		}
	}

	public void setException(Throwable e) {
		StringBuilder errorMessage = new StringBuilder(e.getClass().getSimpleName());
		String message = e.getMessage();
		if (message != null)
			errorMessage.append(". Due to: " + message);
		Throwable cause = e.getCause();
		if (cause != null)
			errorMessage.append(System.lineSeparator() + ". Caused by: " + cause.getMessage());
		setError(errorMessage.toString());
	}

	public void setEnable(boolean enable) {
		boolean oldEnable = this.enable;
		boolean oldLaunchable = launchable();
		this.enable = enable;
		this.pcs.firePropertyChange("enable", oldEnable, enable);
		this.pcs.firePropertyChange("launchable", oldLaunchable, launchable());
	}

	public boolean isConsumesAllDataBeforeDying() {
		return this.consumesAllDataBeforeDying;
	}

	public void setConsumesAllDataBeforeDying(boolean consumesAllDataBeforeDying) {
		boolean oldConsumesAllDataBeforeDying = this.consumesAllDataBeforeDying;
		this.consumesAllDataBeforeDying = consumesAllDataBeforeDying;
		this.pcs.firePropertyChange("consumesAllDataBeforeDying", oldConsumesAllDataBeforeDying, consumesAllDataBeforeDying);
	}

	public int getStartPriority() {
		return this.startPriority;
	}

	public void setStartPriority(int startPriority) {
		int oldStartPriority = this.startPriority;
		this.startPriority = startPriority;
		this.pcs.firePropertyChange("startPriority", oldStartPriority, startPriority);
	}

	public int getStopPriority() {
		return this.stopPriority;
	}

	public void setStopPriority(int stopPriority) {
		int oldStopPriority = this.stopPriority;
		this.stopPriority = stopPriority;
		this.pcs.firePropertyChange("stopPriority", oldStopPriority, stopPriority);
	}

	public int getThreadPriority() {
		return this.threadPriority;
	}

	public void setThreadPriority(int threadPriority) {
		int oldValue = this.threadPriority;
		this.threadPriority = threadPriority;
		this.pcs.firePropertyChange("threadPriority", oldValue, threadPriority);
	}

	public void setInputTs(long[] ts) {
		this.inputTs = ts;
	}

	public void setIsolatePort(int isolatePort) {
		int oldIsolatePort = this.isolatePort;
		this.isolatePort = isolatePort;
		this.pcs.firePropertyChange("isolatePort", oldIsolatePort, isolatePort);
	}

	public void setName(String name) {
		String oldName = BeanEditor.getBeanDesc(this.operator).name;
		BeanEditor.renameBean(BeanEditor.getBeanDesc(this.operator), name);
		this.pcs.firePropertyChange("name", oldName, name);
	}

	@Override
	public void setPosition(double x, double y) {
		this.rectangle = new Rectangle2D((int) (x - this.rectangle.getWidth() / 2.0), (int) (y - this.rectangle.getHeight() / 2.0), this.rectangle.getWidth(), this.rectangle.getHeight());
	}

	public void setProcessMode(ProcessMode processMode) {
		if (this.operator instanceof SubFlowDiagram && !((SubFlowDiagram) this.operator).getSubDiagram().isSubManagement())
			processMode = ProcessMode.LOCAL;
		if (this.processMode != processMode) {
			ProcessMode oldProcessMode = this.processMode;
			this.processMode = processMode;
			this.pcs.firePropertyChange("processMode", oldProcessMode, processMode);
			setVisible();
		}
		if (processMode != ProcessMode.REMOTE)
			setRemoteProperty(false);
	}

	/** Add a property as an input of the block.
	 *
	 * @param propertyName the name of the property to be addded
	 * @return true if successfully add the property as input, false if the property is already as input
	 * @throws IllegalArgumentException if it's not possible to add the given property as input */
	public boolean addPropertyAsInput(String propertyName) {
		if (propertyName == null)
			throw new IllegalArgumentException("PropertyName cannot be null");
		var result = new Object() {
			IllegalArgumentException e;
			boolean success = true;
		};
		runTaskLater(() -> {
			try {
				// Check if already present
				for (int i = 0; i < getNbInput(); i++)
					if (this.inputs.get(i).getName().equals(propertyName)) {
						result.success = false;
						if (i >= this.nbPropertyAsInput)
							result.e = new IllegalArgumentException("Another input already has the name: " + propertyName);
						return;
					}
				// Set as property input
				String[] propertiesNameSequence = propertyName.split("-");
				Object[] propertiesobjectSequence = new Object[propertiesNameSequence.length - 1];
				GetterPropertyHandler[] propertiesGetterHandlerSequence = new GetterPropertyHandler[propertiesobjectSequence.length];
				Object po = this.operator;
				for (int j = 0; j < propertiesNameSequence.length; j++) {
					String pn = propertiesNameSequence[j];
					PropertyDescriptor ppd = findProperty(po.getClass(), pn);
					if (ppd == null) { // Cannot find the property...
						result.success = false;
						result.e = new IllegalArgumentException("Cannot find property: " + pn + " in the class: " + po.getClass());
						return;
					}
					if (j == propertiesNameSequence.length - 1) {
						ArrayList<BlockInput> inputs = new ArrayList<>(this.inputs);
						inputs.add(this.nbPropertyAsInput, new BlockInput(this, ppd.getPropertyType(), propertyName, false));
						try {
							SetterPropertyHandler[] newPropertyMethodHandles = new SetterPropertyHandler[this.nbPropertyAsInput + 1];
							if (this.setterPropertyMethodHandles != null)
								for (int i = 0; i < this.setterPropertyMethodHandles.length; i++)
									newPropertyMethodHandles[i] = this.setterPropertyMethodHandles[i];
							var sphc = new Object() {
								SetterPropertyHandler sph;
							};
							SetterPropertyHandler sph = new SetterPropertyHandler(po, ppd.getWriteMethod(), j == 0 ? null : () -> {
								for (int i = 0; i < propertiesGetterHandlerSequence.length; i++) {
									GetterPropertyHandler propertiesGetterHandler = propertiesGetterHandlerSequence[i];
									try {
										Object currentProperty = propertiesGetterHandler.get();
										Object registerProperty = propertiesobjectSequence[i];
										if (registerProperty == null)
											fireError("Error in SetterPropertyHandler");
										if (currentProperty != registerProperty)
											if (currentProperty == null)
												throw new BrokerPropertyException(propertyName, "The property: " + propertiesNameSequence[i] + " of the "
														+ (i == 0 ? "block : " + getName() : "sub bean: " + BeanEditor.getBeanDesc(propertiesobjectSequence[i - 1]).name) + " is null ");
											else if (currentProperty.getClass() == registerProperty.getClass()) {
												propertiesobjectSequence[i] = currentProperty;
												if (i + 1 < propertiesGetterHandlerSequence.length)
													propertiesGetterHandlerSequence[i + 1].reBindTo(currentProperty);
												else
													sphc.sph.reBindTo(propertiesobjectSequence[propertiesobjectSequence.length - 1]);
											} else {
												int k = i;
												while (true) {
													String pnn = propertiesNameSequence[k + 1];
													// Recherche si une propriété à le même nom
													PropertyDescriptor pd = findProperty(currentProperty.getClass(), pnn);
													if (pd == null)
														throw new BrokerPropertyException(propertyName, "The class: " + currentProperty.getClass() + " of the property: " + currentProperty
																+ " does not contains the property with the name: " + pn);
													propertiesobjectSequence[k] = currentProperty;
													k++;
													if (k == propertiesGetterHandlerSequence.length) {
														sphc.sph.setMethodHandler(pd.getWriteMethod(), currentProperty);
														break;
													}
													GetterPropertyHandler rmh = new GetterPropertyHandler(currentProperty, pd.getReadMethod(), null, null);
													propertiesGetterHandlerSequence[k] = rmh;
													currentProperty = rmh.get();
													if (currentProperty == null)
														throw new BrokerPropertyException(propertyName, "The property: " + propertiesNameSequence[i] + " of the "
																+ (i == 0 ? "block : " + getName() : "sub bean: " + BeanEditor.getBeanDesc(propertiesobjectSequence[k - 1]).name) + " is null ");
													if (currentProperty == propertiesobjectSequence[k])
														break;
												}
											}
									} catch (Throwable e) {
										// the user getter have a big mistake
										if (e instanceof BrokerPropertyException)
											throw e;
										e.printStackTrace();
									}
								}
							});
							sphc.sph = sph;
							newPropertyMethodHandles[newPropertyMethodHandles.length - 1] = sph;
							this.setterPropertyMethodHandles = newPropertyMethodHandles;
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						}
						this.nbPropertyAsInput++;
						this.inputs = Collections.unmodifiableList(inputs);
						fireStructChange();
						return;
					}
					try {
						Object oldPo = po;
						GetterPropertyHandler rmh = new GetterPropertyHandler(po, ppd.getReadMethod(), null, null);
						po = rmh.get();
						if (po == null) {
							result.success = false;
							result.e = new IllegalArgumentException("The property " + ppd.getName() + " of the bean: " + oldPo + "is null");
							return;
						}
						propertiesobjectSequence[j] = po;
						propertiesGetterHandlerSequence[j] = rmh;
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			} catch (Throwable e) {
				result.success = false;
				result.e = new IllegalArgumentException("An error occured while trying to add a property as input: ", e);
			}
		});
		if (!result.success && result.e != null)
			throw result.e;
		return result.success;
	}

	/** Remove a property as an input of the block.
	 *
	 * @param propertyName the name of the property to be removed
	 * @return true if successfully remove the property as input, false if the property is already as input */
	public boolean removePropertyAsInput(String propertyName) {
		if (propertyName == null)
			throw new IllegalArgumentException("PropertyName cannot be null");
		Unit<Boolean> success = new Unit<>(true);
		runTaskLater(() -> {
			for (int i = 0; i < this.nbPropertyAsInput; i++) {
				BlockInput input = this.inputs.get(i);
				if (input.getName().equals(propertyName)) {
					input.setLink(null, false);
					ArrayList<BlockInput> inputs = new ArrayList<>(this.inputs);
					inputs.remove(i);
					this.nbPropertyAsInput--;
					if (this.nbPropertyAsInput != 0) {
						SetterPropertyHandler[] newPropertyMethodHandles = new SetterPropertyHandler[this.nbPropertyAsInput];
						int index = 0;
						for (int j = 0; j < this.setterPropertyMethodHandles.length; j++)
							if (i != j)
								newPropertyMethodHandles[index++] = this.setterPropertyMethodHandles[j];
						this.setterPropertyMethodHandles = newPropertyMethodHandles;
					} else
						this.setterPropertyMethodHandles = null;
					this.inputs = Collections.unmodifiableList(inputs);
					fireStructChange();
					return;
				}
			}
			success.element = false; // Cannot find the property to removed
		});
		return success.element;
	}

	/** Add a property as an output of the block.
	 *
	 * @param propertyName the name of the property to be addded
	 * @return true if successfully add the property as output, false if the property is already as output
	 * @throws IllegalArgumentException if it's not possible to add the given property as output */
	public boolean addPropertyAsOutput(String propertyName) {
		if (propertyName == null)
			throw new IllegalArgumentException("PropertyName cannot be null");
		PropertyAsIoResult result = new PropertyAsIoResult();
		String[] propertiesNameSequence = propertyName.split("-");
		Object[] propertiesObjectSequence = new Object[propertiesNameSequence.length - 1];
		PropertyChangeListener[] propertiesListenerSequence = new PropertyChangeListener[propertiesNameSequence.length];
		GetterPropertyHandler[] propertiesGetterHandlerSequence = new GetterPropertyHandler[propertiesObjectSequence.length];
		runTaskLater(() -> {
			// Check if already present
			for (int i = 0; i < this.outputs.size(); i++)
				if (this.outputs.get(i).getName().equals(propertyName)) {
					result.success = false;
					if (i >= this.nbPropertyAsOutput)
						result.e = new IllegalArgumentException("Another ouput already has the name: " + propertyName);
					return;
				}
			// Set as property output
			repairPropertyAsOutput(this.operator, propertyName, propertiesNameSequence, propertiesObjectSequence, propertiesListenerSequence, propertiesGetterHandlerSequence, 0, result);
		});
		if (!result.success && result.e != null) {
			removePropertyAsOutputListeners(0, propertiesObjectSequence, propertiesListenerSequence, propertiesNameSequence, result);
			throw result.e;
		}
		return result.success;
	}

	private void repairPropertyAsOutput(Object po, String propertyName, String[] propertiesNameSequence, Object[] propertiesObjectSequence, PropertyChangeListener[] propertiesListenerSequence,
			GetterPropertyHandler[] propertiesGetterHandlerSequence, int depth, PropertyAsIoResult result) {
		for (int j = depth; j < propertiesNameSequence.length; j++) {
			String pn = propertiesNameSequence[j];
			Log.info("DYNAMICIO: repair from: " + po + " the property: " + pn);
			PropertyDescriptor ppd;
			try {
				ppd = findProperty(po.getClass(), pn);
			} catch (IntrospectionException e) {
				result.e = new IllegalArgumentException("Error while finding property: " + pn + " in the class: " + po.getClass());
				return;
			}
			if (ppd == null) {
				// Cannot find the property...
				result.e = new IllegalArgumentException("Cannot find property: " + pn + " in the class: " + po.getClass());
				return;
			}
			if (j == propertiesNameSequence.length - 1) {
				try {
					GetterPropertyHandler[] newPropertyMethodHandles = new GetterPropertyHandler[this.nbPropertyAsOutput + 1];
					if (this.getterPropertyMethodHandles != null)
						for (int i = 0; i < this.getterPropertyMethodHandles.length; i++)
							newPropertyMethodHandles[i] = this.getterPropertyMethodHandles[i];
					newPropertyMethodHandles[newPropertyMethodHandles.length - 1] = new GetterPropertyHandler(po, ppd.getReadMethod(), null,
							() -> removePropertyAsOutputListeners(0, propertiesObjectSequence, propertiesListenerSequence, propertiesNameSequence, result));
					this.getterPropertyMethodHandles = newPropertyMethodHandles;
					// Add a property listener to show if the property change

					PropertyChangeListener pcl = new PropertyChangeListener() {
						private ThreadPoolExecutor tpe;

						@Override
						public void propertyChange(PropertyChangeEvent evt) {
							if (evt.getPropertyName().equals(pn)) {
								Runnable task = () -> {
									Log.info("DYNAMICIO: " + evt.getPropertyName() + ": " + evt.getNewValue());
									triggerPropertyAsOutput(propertyName, evt.getNewValue());
								};
								if (Platform.isFxApplicationThread()) {
									if (this.tpe == null)
										this.tpe = new ThreadPoolExecutor(0, 1, 1L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
									this.tpe.execute(task); // Do not run triggerOutputProperty from javaFX Thread, can be bloqued..
								} else
									task.run();
							}
						}
					};
					BeanDesc<Object> beanDesc = BeanEditor.getBeanDesc(po);
					Log.info("DYNAMICIO: add final property change listener " + pcl + " on bean: " + beanDesc != null ? beanDesc.name : "unregister bean");
					try {
						addOrRemovePropertyListener(po, pcl, true);
					} catch (IntrospectionException e) {
						e.printStackTrace();
						result.e = new IllegalArgumentException("Cannot add property listener for the property: " + ppd.getName() + " of the bean of type: " + po.getClass(), e);
					}
					propertiesListenerSequence[j] = pcl;
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					result.e = new IllegalArgumentException("Cannot access to getter method of the property: " + ppd.getName() + " of the bean of type: " + po.getClass(), e);
				}
				if (!result.success) {
					ArrayList<BlockOutput> outputs = new ArrayList<>(this.outputs);
					outputs.add(this.nbPropertyAsOutput, new BlockOutput(this, null, ppd.getPropertyType(), propertyName));
					this.nbPropertyAsOutput++;
					setOutputs(outputs);
					fireStructChange();
					result.success = true;
				}
				return;
			}
			GetterPropertyHandler rmh;
			try {
				rmh = new GetterPropertyHandler(po, ppd.getReadMethod(), null, null);
			} catch (IllegalAccessException e) {
				result.e = new IllegalArgumentException("Error while accessing the property: " + ppd.getName() + " of the bean of type: " + po.getClass(), e);
				return;
			}
			propertiesGetterHandlerSequence[j] = rmh;
			int newDepth = j + 1;
			PropertyChangeListener pcl = evt -> {
				if (evt.getPropertyName().equals(pn))
					runTaskLater(() -> {
						Log.info("DYNAMICIO: Repair property from depth: " + newDepth);
						Object newPo = evt.getNewValue();
						Log.info("DYNAMICIO: " + evt.getPropertyName() + ": " + newPo);
						removePropertyAsOutputListeners(newDepth, propertiesObjectSequence, propertiesListenerSequence, propertiesNameSequence, result);
						result.e = null;
						if (newPo == null)
							result.e = new IllegalArgumentException("The property " + pn + " of the bean: " + BeanEditor.getBeanDesc(evt.getSource()).name + " is null");
						else
							repairPropertyAsOutput(newPo, propertyName, propertiesNameSequence, propertiesObjectSequence, propertiesListenerSequence, propertiesGetterHandlerSequence, newDepth,
									result);
						if (result.e != null)
							fireError("Error while repairing the property as output method reference due to: " + result.e.getMessage());
					});
			};
			Log.info("DYNAMICIO: add listener " + pcl + " on: " + po);
			try {
				addOrRemovePropertyListener(po, pcl, true);
			} catch (IllegalArgumentException | IntrospectionException e) {
				result.success = false;
				result.e = new IllegalArgumentException("Cannot add property listener for the property: " + ppd.getName() + " of the bean of type: " + po.getClass(), e);
				return;
			}
			propertiesListenerSequence[j] = pcl;
			Object oldPo = po;
			try {
				po = rmh.get();
			} catch (Throwable e) {
				result.e = new IllegalArgumentException("Error while getting the property: " + ppd.getName() + " of the bean of type: " + po.getClass(), e);
				return;
			}
			Log.info("DYNAMICIO: get the value: " + po);
			if (po == null) {
				result.e = new IllegalArgumentException("The property " + ppd.getName() + " of the bean: " + oldPo + " is null");
				return;
			}
			propertiesObjectSequence[j] = po;
		}
	}

	protected void triggerPropertyAsOutput(String propertyName, Object value) {
		if (isConflict())
			return;
		if (this.operator instanceof EvolvedOperator)
			((EvolvedOperator) this.operator).triggerProperty(propertyName, value);
		else
			this.triggerPropertyFunction.accept(propertyName, value);
	}

	protected void removePropertyAsOutputListeners(int beginIndex, Object[] propertiesObjectSequence, PropertyChangeListener[] propertiesListenerSequence, String[] propertiesNameSequence,
			PropertyAsIoResult result) {
		for (int i = beginIndex; i < propertiesListenerSequence.length; i++)
			try {
				Object po = i == 0 ? this.operator : propertiesObjectSequence[i - 1];
				if (po != null) {
					Log.info("DYNAMICIO: remove property change listener " + propertiesListenerSequence[i] + " on bean: " + BeanEditor.getBeanDesc(po).name);
					addOrRemovePropertyListener(po, propertiesListenerSequence[i], false);
				}
			} catch (IllegalArgumentException | IntrospectionException e) {
				result.e = new IllegalArgumentException("Cannot remove property listener for the property: " + propertiesNameSequence[i] + " of the bean of type: " + propertiesObjectSequence[i], e);
				return;
			}
	}

	class PropertyAsIoResult {
		IllegalArgumentException e;
		boolean success = false;
	}

	/** Remove a property as an output of the block.
	 *
	 * @param propertyName the name of the property to be removed
	 * @return true if successfully remove the property as output, false if the property is already as output */
	public boolean removePropertyAsOutput(String propertyName) {
		if (propertyName == null)
			throw new IllegalArgumentException("PropertyName cannot be null");
		Unit<Boolean> success = new Unit<>(true);
		runTaskLater(() -> {
			for (int i = 0; i < this.nbPropertyAsOutput; i++) {
				BlockOutput output = this.outputs.get(i);
				if (output.getName().equals(propertyName)) {
					disconnectOutput(output);
					ArrayList<BlockOutput> outputs = new ArrayList<>(this.outputs);
					outputs.remove(i);
					this.nbPropertyAsOutput--;
					if (this.nbPropertyAsOutput != 0) {
						GetterPropertyHandler[] newPropertyMethodHandles = new GetterPropertyHandler[this.nbPropertyAsOutput];
						int index = 0;
						for (int j = 0; j < this.getterPropertyMethodHandles.length; j++)
							if (i != j)
								newPropertyMethodHandles[index++] = this.getterPropertyMethodHandles[j];
							else
								this.getterPropertyMethodHandles[j].destroy();
						this.getterPropertyMethodHandles = newPropertyMethodHandles;
					} else {
						this.getterPropertyMethodHandles[0].destroy();
						this.getterPropertyMethodHandles = null;
					}
					setOutputs(outputs);
					fireStructChange();
					return;// true;
				}
			}
			success.element = false; // Cannot find the property to removed
		});
		return success.element;
	}

	private static void addOrRemovePropertyListener(Object po, PropertyChangeListener pcl, boolean add) throws IntrospectionException {
		for (EventSetDescriptor esd : Introspector.getBeanInfo(po.getClass()).getEventSetDescriptors())
			if (esd.getName().equals("propertyChange")) {
				try {
					if (add)
						esd.getAddListenerMethod().invoke(po, pcl);
					else
						esd.getRemoveListenerMethod().invoke(po, pcl);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
					throw new IllegalArgumentException("Cannot add listener to: " + po + " of type: " + po.getClass() + " due to: ", ex);
				}
				return;
			}
		throw new IllegalArgumentException("Cannot find property change listener methods in: " + po.getClass());
	}

	public void triggerPropertyAsOutput() {
		if (isConflict())
			return;
		nextPropertyAsOutput: for (int i = 0; i < this.nbPropertyAsOutput; i++) {
			String propertyName = this.outputs.get(i).getName();
			String[] propertiesNameSequence = propertyName.split("-");
			Log.info("DYNAMICIO: Trigger output of: " + Arrays.toString(propertiesNameSequence) + " from: " + this);
			Object po = this.operator;
			for (int j = 0; j < propertiesNameSequence.length; j++) {
				String pn = propertiesNameSequence[j];
				PropertyDescriptor ppd;
				try {
					ppd = findProperty(po.getClass(), pn);
				} catch (IntrospectionException e) {
					fireError("Error while finding property: " + pn + " in the class: " + po.getClass());
					continue nextPropertyAsOutput;
				}
				if (ppd == null) { // Cannot find the property...
					fireError("Cannot find property: " + pn + " in the class: " + po.getClass());
					continue nextPropertyAsOutput;
				}
				try {
					po = ppd.getReadMethod().invoke(po);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					fireError("Cannot get property: " + pn + " in the class: " + po.getClass());
					continue nextPropertyAsOutput;
				}
			}
			triggerPropertyAsOutput(propertyName, po);
		}
	}

	private void runTaskLater(Runnable task) {
		if (this.operator instanceof EvolvedOperator && !this.conflict) // An operator in conflict mode may not call the runlater linked to the operator
			((EvolvedOperator) this.operator).runLater(task);
		else {
			Consumer<Runnable> runLaterFunction = this.runLaterFunction;
			if (runLaterFunction != null)
				runLaterFunction.accept(task);
			else
				task.run();
		}
	}

	private static PropertyDescriptor findProperty(Class<? extends Object> type, String propernyName) throws IntrospectionException {
		for (PropertyDescriptor pd : Introspector.getBeanInfo(type).getPropertyDescriptors()) // Search property...
			if (pd.getWriteMethod() != null && pd.getName().equals(propernyName))
				return pd;
		return null;
	}

	public void setRemoteIp(InetSocketAddress remoteIp) {
		InetSocketAddress oldRemoteIp = this.remoteIp;
		this.remoteIp = remoteIp;
		this.pcs.firePropertyChange("remoteIp", oldRemoteIp, remoteIp);
	}

	public void setRemoteOperator(RemoteOperator remoteOperator) {
		this.remoteOperator = remoteOperator;
	}

	public void setRemoteProperty(boolean remoteProperty) {
		if (this.processMode != ProcessMode.REMOTE)
			remoteProperty = false;
		boolean oldRemoteProperty = this.remoteProperty;
		this.remoteProperty = remoteProperty;
		this.pcs.firePropertyChange("remoteProperty", oldRemoteProperty, remoteProperty);
	}

	public void setRunTimePropertyChangeListener(PropertyChangeListener listener) {
		if (this.runTimePropertyChangeListener != null)
			removePropertyChangeListener(this.runTimePropertyChangeListener);
		if (listener != null)
			addPropertyChangeListener(listener);
		this.runTimePropertyChangeListener = listener;
	}

	public void setSynchronizeProperties(boolean synchronizeProperties) {
		boolean oldSynchronizeProperties = this.synchronizeProperties;
		this.synchronizeProperties = synchronizeProperties;
		this.pcs.firePropertyChange("synchronizeProperties", oldSynchronizeProperties, synchronizeProperties);
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}

	public void setTimeOfIssue(long[] toi) {
		this.inputTOI = toi;
	}

	@Override
	public void setVisible() {
		fireSetPropertyVisible(this, "isolatePort", this.processMode == ProcessMode.ISOLATED);
		fireSetPropertyVisible(this, "synchronizeProperties", this.processMode != ProcessMode.LOCAL);
		fireSetPropertyVisible(this, "remoteIp", this.processMode == ProcessMode.REMOTE);
		fireSetPropertyVisible(this, "remoteProperty", this.processMode == ProcessMode.REMOTE);
	}

	@Override
	public void setEnable() {
		boolean subManagement = !(this.operator instanceof SubFlowDiagram) || ((SubFlowDiagram) this.operator).getSubDiagram().isSubManagement();
		fireSetPropertyEnable(this, "consumesAllDataBeforeDying", subManagement);
		fireSetPropertyEnable(this, "startPriority", subManagement);
		fireSetPropertyEnable(this, "stopPriority", subManagement);
		fireSetPropertyEnable(this, "threadPriority", subManagement);
		fireSetPropertyEnable(this, "processMode", subManagement);
		fireSetPropertyEnable(this, "isolatePort", subManagement);
		fireSetPropertyEnable(this, "synchronizeProperties", subManagement);
		fireSetPropertyEnable(this, "remoteIp", subManagement);
		fireSetPropertyEnable(this, "remoteProperty", subManagement);
	}

	public void setWarning(String warning) {
		if (!Objects.equals(this.warning, warning)) {
			this.warning = warning;
			fireRunningStateChange();
		}
	}

	public void resetWarning() {
		if (this.operator instanceof EvolvedOperator && ((EvolvedOperator) this.operator).canResetWarning())
			setWarning(null);
	}

	@Override
	public String toString() {
		return BeanEditor.getBeanDesc(this.operator).name;
	}

	public void updateBounds() {
		for (BlockInput input : this.inputs)
			input.setPosition(null);
		for (BlockOutput output : this.outputs)
			output.setPosition(null);
		if (this.rectangle != null)
			this.rectangle = new Rectangle2D((int) this.rectangle.getMinX(), (int) this.rectangle.getMinY(), 100, computeHeight());
	}

	boolean updateDynamicInputs(String[] names, Class<?>[] types) {
		if (names == null || types == null)
			throw new IllegalArgumentException("names and types musts be non null");
		if (names.length != types.length)
			throw new IllegalArgumentException("names and types must have the same size");
		for (int i = 0; i < types.length; i++)
			if (names[i] == null || types[i] == null)
				throw new IllegalArgumentException("All elements in names and types must be non null");
		if (!(this.operator instanceof EvolvedOperator))
			throw new IllegalArgumentException("This feature is only available for block linked to an EvolvedOperator");
		Unit<Boolean> evolved = new Unit<>(true);
		runTaskLater(() -> {
			LinkedList<BlockInput> checkInputs = new LinkedList<>();
			int indexOfDynamicInputs = this.nbPropertyAsInput + this.nbStaticInput;
			int end = indexOfDynamicInputs + this.nbDynamicInput;
			for (int j = indexOfDynamicInputs; j < end; j++)
				checkInputs.add(this.inputs.get(j));
			boolean hasChanged = false;
			ArrayList<Integer> toAdd = new ArrayList<>();
			nextInput: for (int i = 0; i < names.length; i++) {
				String inName = names[i];
				Class<?> inType = types[i];
				for (int j = 0; j < end; j++) {
					BlockInput input = this.inputs.get(j);
					if (input.getName().equals(inName) && input.getType() == inType) {
						checkInputs.remove(input);
						continue nextInput;
					}
				}
				toAdd.add(i);
			}
			if (checkInputs.isEmpty() && toAdd.isEmpty()) {
				evolved.element = false;
				return;// false;
			}
			ArrayList<BlockInput> inputs = new ArrayList<>(this.inputs);
			for (BlockInput blockInput : checkInputs) {
				Link link = blockInput.getLink();
				if (link != null)
					blockInput.setLink(null);
				if (inputs.remove(blockInput))
					this.nbDynamicInput--;
			}
			String varArgInputName = getVarArgsInputName();
			nextInputToAdd: for (Integer i : toAdd) {
				String inputName = names[i];
				for (BlockInput input : inputs)
					if (input.getName().equals(inputName)) {
						fireError("Another input already has the name: " + inputName);
						continue nextInputToAdd;
					}
				if (isInConflictWithVariadicInputName(varArgInputName, inputName)) {
					fireError("The dynamic input name: " + inputName + " is in conflict with the variadic arguments pattern: " + varArgInputName + "-x");
					continue nextInputToAdd;
				}
				inputs.add(indexOfDynamicInputs + this.nbDynamicInput, new BlockInput(this, types[i], inputName, false));
				hasChanged = true;
				this.nbDynamicInput++;
			}
			evolved.element = hasChanged || !checkInputs.isEmpty();
			if (evolved.element)
				this.inputs = Collections.unmodifiableList(inputs);
		});
		return evolved.element;
	}

	private boolean isInConflictWithVariadicInputName(String varArgInputName, String inputName) {
		// String varArgInputName = getVarArgsInputName();
		if (varArgInputName != null && inputName.startsWith(varArgInputName))
			try {
				Integer.parseInt(inputName.substring(varArgInputName.length() + 1));
				return true;
			} catch (NumberFormatException e) {} // It's not an number, it's ok
		return false;
	}

	private String getVarArgsInputName() {
		if (getNbVarArgsInput() != -1) { // The process method is variadic
			BlockInput varArgInput = this.inputs.get(this.inputs.size() - 1);
			String varArgInputName = varArgInput.getName();
			return varArgInputName.substring(0, varArgInputName.lastIndexOf("-"));
		}
		return null;
	}

	private void fireError(String error) {
		Log.error(getName() + ": " + error);
	}

	boolean updateDynamicOuputs(String[] names, Class<?>[] types) {
		if (names == null || types == null)
			throw new IllegalArgumentException("names and types must be non null");
		if (names.length != types.length)
			throw new IllegalArgumentException("names and types must have the same size");
		for (int i = 0; i < types.length; i++)
			if (names[i] == null || types[i] == null)
				throw new IllegalArgumentException("All elements in names and types must be non null");
		if (new HashSet<>(Arrays.asList(names)).size() != names.length)
			throw new IllegalArgumentException("All elements in names must be different");
		if (!(this.operator instanceof EvolvedOperator))
			throw new IllegalArgumentException("This feature is only available for block linked to an EvolvedOperator");
		Unit<Boolean> evolved = new Unit<>(true);
		runTaskLater(() -> {
			LinkedList<BlockOutput> checkedOutputs = new LinkedList<>();
			int indexOfDynamicOutputs = this.nbPropertyAsOutput + this.nbStaticOutput;
			int nbOutput = this.outputs.size();
			for (int j = indexOfDynamicOutputs; j < nbOutput; j++)
				checkedOutputs.add(this.outputs.get(j));
			boolean hasChanged = false;
			ArrayList<Integer> toAdd = new ArrayList<>();
			nextOuput: for (int i = 0; i < names.length; i++) {
				String outName = names[i];
				Class<?> outType = types[i];
				if (outType.isPrimitive())
					outType = WrapperTools.toWrapper(outType);
				for (int j = 0; j < nbOutput; j++) {
					BlockOutput output = this.outputs.get(j);
					if (output.getName().equals(outName)) {
						if (output.getType() != outType) { // Can keep this output be must change its type, if it's not an property as input
							Input[] linkinputs = output.getLinkInputs();
							ArrayList<Input> updatedInputs = null;
							if (linkinputs != null)
								for (Input input : linkinputs) {
									Link oldLink = input.getLink();
									if (!oldLink.getInput().getType().isAssignableFrom(outType))
										input.setLink(null);
									else {
										if (updatedInputs == null)
											updatedInputs = new ArrayList<>();
										updatedInputs.add(input);
									}
								}
							output.setType(outType);
							if (updatedInputs != null)
								updatedInputs.forEach(in -> in.setLink(in.getLink())); // The link does not change but the output type change, need to recheck type and fire events
						}
						checkedOutputs.remove(output);
						continue nextOuput;
					}
				}
				toAdd.add(i);
			}

			ArrayList<BlockOutput> outputs = new ArrayList<>(this.outputs);
			for (BlockOutput blockOutput : checkedOutputs) {
				disconnectOutput(blockOutput);
				outputs.remove(blockOutput);
				hasChanged = true;
			}
			nextOuputToAdd: for (Integer i : toAdd) {
				String name = names[i];
				Class<?> type = types[i];
				for (BlockOutput output : outputs)
					if (output.getName().equals(name)) {
						fireError("Another output already has the name: " + name);
						continue nextOuputToAdd;
					}
				outputs.add(indexOfDynamicOutputs + i, new BlockOutput(this, null, type, name));
				hasChanged = true;
			}
			for (int i = 0; i < names.length - 1; i++) {
				String outputName = outputs.get(indexOfDynamicOutputs + i).getName();
				if (!outputName.equals(names[i])) {
					hasChanged = true;
					for (int j = 0; j < names.length; j++)
						if (outputs.get(j).getName().equals(names[i])) {
							BlockOutput tempOutput = outputs.get(this.nbStaticOutput + i);
							outputs.set(indexOfDynamicOutputs + i, outputs.get(j));
							outputs.set(indexOfDynamicOutputs + j, tempOutput);
							break;
						}
				}
			}
			evolved.element = hasChanged;
			if (evolved.element)
				setOutputs(outputs);
		});
		return evolved.element;
	}

	private void setOutputs(List<BlockOutput> outputs) {
		this.outputs = Collections.unmodifiableList(outputs);
		this.triggerableOutputs = Collections.unmodifiableList(outputs.subList(this.nbPropertyAsOutput, outputs.size()));
		this.outputBuff = new Object[this.triggerableOutputs.size()];
		this.outputBuffTs = new long[this.outputBuff.length];
		this.outputBuffToi = new long[this.outputBuff.length];
	}

	private static void disconnectOutput(BlockOutput blockOutput) {
		nextLink: while (true) {
			Input[] linkinputs = blockOutput.getLinkInputs();
			if (linkinputs != null)
				for (Input input : linkinputs) {
					input.setLink(null);
					continue nextLink;
				}
			break;
		}
	}

	public boolean varArgsInputChanged(BlockInput input) {
		Unit<Boolean> shifted = new Unit<>(false);
		runTaskLater(() -> {
			ArrayList<BlockInput> inputs = new ArrayList<>(this.inputs);
			int fireVarArgsInputChangeTypeOfChange = -1;
			if (input.getLink() == null) { // A link was removed
				int indexOfVarArgs = this.nbPropertyAsInput + this.nbStaticInput + this.nbDynamicInput;
				if (indexOfVarArgs == inputs.size() - 1)
					return;
				if (!(this.operator instanceof EvolvedVarArgsOperator && inputs.indexOf(input) == inputs.size() - 1)) {// Je suis sur le dernier d'un drawer
					if (inputs.get(inputs.size() - 1).getLink() == null) { // Il faut pas retirer le canaddinput
						inputs.remove(inputs.size() - 1);
						this.nbVarArgsInput--;
						this.varArgsBuff = (Object[]) Array.newInstance(inputs.get(inputs.size() - 1).getType(), this.nbVarArgsInput);
					}
					for (; indexOfVarArgs < inputs.size(); indexOfVarArgs++)
						if (inputs.get(indexOfVarArgs).getLink() == null)
							break;
					BlockInput currInput = input;
					for (; indexOfVarArgs < inputs.size() - 1; indexOfVarArgs++) {
						currInput = inputs.get(indexOfVarArgs);
						Link link = inputs.get(indexOfVarArgs + 1).getLink();
						Link newLink = link == null ? null : new Link(currInput, link.getOutput());
						inputs.get(indexOfVarArgs).setLink(newLink, false); // Link shift
						shifted.element = true;
					}
					if (indexOfVarArgs < inputs.size())
						inputs.get(indexOfVarArgs).setLink(null, false); // Set to null last input
					fireVarArgsInputChangeTypeOfChange = VarArgsInputChangeListener.REMOVE;
				}
			} else if (inputs.get(inputs.size() - 1) == input) { // A link was added
				this.nbVarArgsInput = inputs.size() - this.nbPropertyAsInput - this.nbStaticInput - this.nbDynamicInput;
				if (this.varArgsBuff.length != this.nbVarArgsInput)
					this.varArgsBuff = (Object[]) Array.newInstance(inputs.get(inputs.size() - 1).getType(), this.nbVarArgsInput);
				if (this.operator instanceof EvolvedVarArgsOperator) {
					Class<?>[] inputsType = new Class[inputs.size() - this.nbPropertyAsInput];
					for (int i = this.nbPropertyAsInput; i < inputs.size(); i++) {
						Link link = inputs.get(i).getLink();
						inputsType[i - this.nbPropertyAsInput] = link == null ? null : link.getOutput().getType();
					}
					if (!((EvolvedVarArgsOperator) this.operator).canAddInput(inputsType))
						return;
				}
				String inputName = input.getName();
				int indexOf = inputName.lastIndexOf("-");
				int index = Integer.parseInt(inputName.substring(indexOf + 1)) + 1;
				BlockInput newInput = new BlockInput(this, input.getType(), inputName.substring(0, indexOf) + "-" + Integer.toString(index), input.isVarArgs());
				for (InputLinkChangeListener lcl : input.getLinkChangeListener())
					newInput.addLinkChangeListener(lcl);
				inputs.add(newInput);
				fireVarArgsInputChangeTypeOfChange = VarArgsInputChangeListener.NEW;
			} else { // A link was changed
				fireVarArgsInputChangeTypeOfChange = VarArgsInputChangeListener.CHANGED;
				fireVarArgsInputChange(inputs.indexOf(input), VarArgsInputChangeListener.CHANGED);
				return;
			}
			this.inputs = Collections.unmodifiableList(inputs);
			fireVarArgsInputChange(inputs.indexOf(input), fireVarArgsInputChangeTypeOfChange);
		});
		return shifted.element.booleanValue();
	}

	public void addStructChangeListener(StructChangeListener listener) {
		this.listeners.add(StructChangeListener.class, listener);
	}

	public void removeStructChangeListener(StructChangeListener listener) {
		this.listeners.remove(StructChangeListener.class, listener);
	}

	public void fireStructChange() {
		updateBounds();
		for (StructChangeListener listener : this.listeners.getListeners(StructChangeListener.class))
			listener.structChanged();
	}

	public void addInputLinksChangeListener(InputLinksChangeListener listener) {
		this.listeners.add(InputLinksChangeListener.class, listener);
	}

	public void removeInputLinksChangeListener(InputLinksChangeListener listener) {
		this.listeners.remove(InputLinksChangeListener.class, listener);
	}

	void fireInputLinksChangeListener(Input input) {
		this.inputs.indexOf(input);
		for (InputLinksChangeListener listener : this.listeners.getListeners(InputLinksChangeListener.class))
			listener.inputLinkChanged(this.inputs.indexOf(input));
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public void addPropertyChangeListenerIfNotPresent(PropertyChangeListener listener) {
		for (PropertyChangeListener list : this.pcs.getPropertyChangeListeners())
			if (listener == list)
				return;
		addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	public void addRunningStateChangeListener(RunningStateChangeListener listener) {
		this.listeners.add(RunningStateChangeListener.class, listener);
	}

	public void removeRunningStateChangeListener(RunningStateChangeListener listener) {
		this.listeners.remove(RunningStateChangeListener.class, listener);
	}

	public void fireRunningStateChange() {
		for (RunningStateChangeListener listener : this.listeners.getListeners(RunningStateChangeListener.class))
			listener.stateChange();
	}

	public void addVarArgsInputChangeListener(VarArgsInputChangeListener listener) {
		this.listeners.add(VarArgsInputChangeListener.class, listener);
	}

	public void removeVarArgsInputChangeListener(VarArgsInputChangeListener listener) {
		this.listeners.remove(VarArgsInputChangeListener.class, listener);
	}

	private void fireVarArgsInputChange(int indexOfInput, int typeOfChange) {
		updateBounds();
		for (VarArgsInputChangeListener listener : this.listeners.getListeners(VarArgsInputChangeListener.class))
			listener.varArgsInputChanged(indexOfInput, typeOfChange);
	}

	public static Rectangle2D getBlockMinimumBounds(Point2D location, boolean isMiddlePoint) {
		int width = 100;
		int height = 120;
		return new Rectangle2D(isMiddlePoint ? location.getX() - width / 2 : location.getX(), isMiddlePoint ? location.getY() - height / 2 : location.getY(), width, height);
	}

	public void setInputError(int ip, String message) {
		this.inputs.get(ip).setError(message);
	}

	public void setRunLaterFunction(Consumer<Runnable> runLaterFunction) {
		this.runLaterFunction = runLaterFunction;
	}

	public void setTriggerPropertyFunction(BiConsumer<String, Object> triggerPropertyFunction) {
		this.triggerPropertyFunction = triggerPropertyFunction;
	}

	public void dispose() {
		BeanEditor.removeStrongRefBeanRenameListener(this.brl);
		for (int i = 0; i < this.nbPropertyAsOutput; i++) // Need to destroy listeners on property linked to output
			removePropertyAsOutput(this.outputs.get(i).getName());
		unbindBlock();
		if (this.operator instanceof EvolvedOperator && !isConflict())
			((EvolvedOperator) this.operator).setTrigger(null);
	}

	private void unbindBlock() {
		if (this.operator instanceof EvolvedOperator) {
			EvolvedOperator eop = (EvolvedOperator) this.operator;
			eop.removeStructChangeListener(this);
			if (eop.getBindedBlock() == this)
				eop.bindBlock(null);
		}
	}

	private void bindBlock() {
		if (this.operator instanceof EvolvedOperator) {
			EvolvedOperator eop = (EvolvedOperator) this.operator;
			eop.bindBlock(this);
			eop.removeStructChangeListener(this); // Ensure that only one listener is registered
			eop.addStructChangeListener(this);
			try {
				eop.updateIOStructure();
			} catch (Exception e) {
				fireError("Error while updating the structure of the block: " + this + " and the operator: " + this.operator + "\n" + e.getClass().getSimpleName() + ": " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	private boolean conflict = false;

	public boolean isConflict() {
		return this.conflict;
	}

	void setConflict(boolean conflict) {
		boolean oldValue = this.conflict;
		boolean oldLaunchable = launchable();
		this.conflict = conflict;
		if (conflict) {
			unbindBlock();
			if (this.operator instanceof EvolvedOperator)
				((EvolvedOperator) this.operator).setTrigger(null);
		} else
			bindBlock();
		this.pcs.firePropertyChange("conflict", oldValue, conflict);
		this.pcs.firePropertyChange("launchable", oldLaunchable, launchable());
	}

	@Override
	public void structChanged() {
		fireStructChange();
	}
}
