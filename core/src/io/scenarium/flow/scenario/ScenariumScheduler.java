/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.scenario;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Objects;

import io.beanmanager.editors.DynamicVisibleBean;
import io.scenarium.core.filemanager.scenariomanager.LocalScenario;
import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.filemanager.scenariomanager.ScenarioException;
import io.scenarium.core.filemanager.scenariomanager.ScenarioManager;
import io.scenarium.core.filemanager.scenariomanager.ScenarioTrigger;
import io.scenarium.core.filemanager.scenariomanager.TimedScenario;
import io.scenarium.core.timescheduler.Schedulable;
import io.scenarium.core.timescheduler.ScheduleTask;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.flow.internal.Log;

// TODO REFACTo MUST be a deprecated class...
public class ScenariumScheduler extends TimedScenario implements /* MetaScenario, Trigger, */DynamicVisibleBean {
	private ArrayList<TimedScenario> scenarios;

	// private ArrayList<ScheduleTask> schedulableTasks;
	private long beginTime = -1;
	private long endTime = -1;
	private boolean[] valideScenario;

	private int[] scenarioIds;

	@Override
	public boolean canCreateDefault() {
		return false;
	}

	@Override
	protected synchronized boolean close() {
		if (this.scenarios != null) // Attention, bien garder les scénarios, juste les fermer
			for (LocalScenario scenario : this.scenarios) {
				scenario.setTrigger(null);
				scenario.death();
			}
		return true;
	}

	@Override
	public long getBeginningTime() {
		return this.beginTime;
	}

	@Override
	public long getNbFrame() {
		return -1;
	}

	@Override
	public Class<?> getDataType() {
		return null;
	}

	@Override
	public long getEndTime() {
		return this.endTime;
	}

	@Override
	public String[] getReaderFormatNames() {
		return new String[] { "psf" };
	}

	public String[] getScenariosName() {
		return this.scenarios == null ? null : this.scenarios.stream().map(s -> getScenarioName(s.getFile().getName())).toArray(String[]::new);
	}

	@Override
	public int getSchedulerType() {
		return Scheduler.EVENT_SCHEDULER;
	}

	@Override
	public Schedulable getTaskFromId(int id) {
		return this.scenarios.get(id);
	}

	@Override
	public int getTaskId(Schedulable task) {
		return this.scenarios.indexOf(task);
	}

	@Override
	public void initOrdo() {
		if (this.file == null)	
			return;
		ArrayList<ScheduleTask> schedulableTasks = new ArrayList<>();
		schedulableTasks = readScheduleFile();
		// Déplacement en amont, et si le set scheduler ajoute des taches, beginTime et endTime vont changer
		setSchedulerInterfaceToSubScenario();	
		if (schedulableTasks != null) {
			this.schedulerInterface.addTasks(schedulableTasks, (beginTime, endTime) -> setTimeInterval(beginTime, endTime));
			schedulableTasks = null; // Pour vider les objets
		} else
			setTimeInterval(-1, -1);
	}
	
	private void setTimeInterval(long beginTime, long endTime) {
		this.beginTime = beginTime;
		this.endTime = endTime;
		fireAnnotationChanged(this, "startTime");
		fireAnnotationChanged(this, "stopTime");
	}

	@Override
	public void updateIOStructure() {
		if (this.scenarios == null)
			readScheduleFileHeader();
		ArrayList<String> names = new ArrayList<>();
		ArrayList<Class<?>> types = new ArrayList<>();
		if (this.scenarios != null)
			for (LocalScenario scenario : this.scenarios) {
				File lfile = scenario.getFile();
				if (lfile != null) {
					Class<?> type = scenario.getDataType();
					if (type != null) {
						names.add(getScenarioName(lfile.getName()));
						types.add(scenario.getDataType());
					}
				}
			}
		fireOutputChanged(names.toArray(String[]::new), types.toArray(Class[]::new));
	}

	@Override
	public synchronized void load(File scenarioFile, boolean backgroundLoading) throws IOException, ScenarioException {
		this.file = scenarioFile;
		if (this.scenarios == null) {
			readScheduleFileHeader();
			setSchedulerInterfaceToSubScenario();
		}
		for (int i = 0; i < this.scenarios.size(); i++) {
			LocalScenario scenario = this.scenarios.get(i);
			try {
				scenario.birth();
				final int scenarioIndex = i;
				scenario.setTrigger(new ScenarioTrigger() {
					@Override
					public boolean isActive() {
						return ScenariumScheduler.this.st.isActive();
					}

					@Override
					public boolean triggerOutput(Object outputValue, long timeStamp) {
						if (ScenariumScheduler.this.st != null && ScenariumScheduler.this.st.isActive())
							return ScenariumScheduler.this.st.triggerOutput(outputValue, timeStamp);
						return false;
					}

					@Override
					public boolean triggerOutput(Object[] outputValues, long timeStamp) {
						Object[] ouputsVector = ScenariumScheduler.this.st.generateOuputsVector();
						if (ouputsVector == null)
							return false;
						ouputsVector[scenarioIndex] = outputValues[0];
						if (ScenariumScheduler.this.st != null && ScenariumScheduler.this.st.isActive())
							return ScenariumScheduler.this.st.triggerOutput(ouputsVector, timeStamp);
						return false;
					}

					@Override
					public boolean triggerOutput(Object[] outputValues, long[] timeStamps) {
						Object[] ouputsVector = ScenariumScheduler.this.st.generateOuputsVector();
						if (ouputsVector == null)
							return false;
						ouputsVector[scenarioIndex] = outputValues[0];
						if (ouputsVector.length != timeStamps.length) {
							long[] newTimeStamps = new long[ouputsVector.length];
							newTimeStamps[scenarioIndex] = timeStamps[0];
							timeStamps = newTimeStamps;
						}
						if (ScenariumScheduler.this.st != null && ScenariumScheduler.this.st.isActive())
							return ScenariumScheduler.this.st.triggerOutput(ouputsVector, timeStamps);
						return false;
					}

					@Override
					public Object[] generateOuputsVector() {
						return new Object[1];
					}

					@Override
					public void setWarning(String warning) {
						ScenariumScheduler.this.st.setWarning(warning);
					}

					@Override
					public boolean holdLock() {
						return ScenariumScheduler.this.st.holdLock();
					}

					@Override
					public String getName() {
						return scenario.toString();
					}

					@Override
					public void runLater(Runnable task) {
						ScenariumScheduler.this.st.runLater(task);
					}

					@Override
					public Object[] getAdditionalInputs() {
						return ScenariumScheduler.this.st.getAdditionalInputs();
					}

					@Override
					public long getTimeStamp(int i) {
						return ScenariumScheduler.this.st.getTimeStamp(i);
					}

					@Override
					public boolean isAlive() {
						return ScenariumScheduler.this.st.isAlive();
					}
				});
			} catch (Exception e) {
				Log.error("Cannot load the scenario: " + scenario);
				e.printStackTrace();
			}
		}
		if (this.scenarios.size() != 0)
			this.scenarioData = this.scenarios.get(0).getScenarioData();
		fireLoadChanged();
	}

	@Override
	public void populateInfo(LinkedHashMap<String, String> info) throws IOException {

	}

	@Override
	public void process(Long timePointer) throws Exception {
		throw new IllegalAccessError(getClass().getSimpleName() + " can never be call");
	}

	private void readCompactHeader(MappedByteBuffer buffer, String scenarioPath, HashSet<String> scenarioNames) {
		buffer.get();
		this.scenarios = new ArrayList<>();
		int nbInput = buffer.getInt();
		this.valideScenario = new boolean[nbInput];
		this.scenarioIds = new int[nbInput];
		for (int i = 0; i < nbInput; i++) {
			String filePath = getString(buffer);
			String scenarioName = getScenarioName(filePath);
			if (scenarioNames.contains(scenarioName)) {
				Log.error("This scenario already include the outputs: " + scenarioName);
				continue;
			}
			scenarioNames.add(scenarioName);
			filePath = scenarioPath + File.separator + filePath;
			if (!new File(filePath).exists())
				continue;
			Class<? extends Scenario> scenarioType = ScenarioManager.getScenarioType(new File(filePath));
			if (scenarioType == null) {
				Log.error("No loader for the file: " + filePath);
				continue;
			} else if (!LocalScenario.class.isAssignableFrom(scenarioType)) {
				Log.error("Not a LocalScenario type");
				continue;
			}
			try {
				TimedScenario scenario = (TimedScenario) scenarioType.getConstructor().newInstance();
				scenario.setFile(new File(filePath));
				if (scenario.getDataType() == null)
					continue;
				scenario.setStartTime(getStartTime());
				scenario.setStopTime(getStopTime());
				scenario.setPeriod(1);
				this.scenarios.add(scenario);
				this.scenarioIds[i] = this.scenarios.size() - 1;
			} catch (IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				Log.error("Cannot create the scenario: " + scenarioType.getSimpleName());
				continue;
			}
			this.valideScenario[i] = true;
		}
	}

	private void readHeader(BufferedReader br, String scenarioPath, HashSet<String> scenarioNames) throws NumberFormatException, IOException {
		this.scenarios = new ArrayList<>();
		int nbInput = Integer.parseInt(br.readLine());
		this.valideScenario = new boolean[nbInput];
		this.scenarioIds = new int[nbInput];
		for (int i = 0; i < nbInput; i++) {
			String filePath = br.readLine();
			filePath = filePath.substring(0, filePath.lastIndexOf(":"));
			String scenarioName = getScenarioName(filePath);
			if (scenarioNames.contains(scenarioName)) {
				Log.error("This scenario already include the outputs: " + scenarioName);
				continue;
			}
			scenarioNames.add(scenarioName);
			filePath = scenarioPath + File.separator + filePath;
			if (!new File(filePath).exists())
				continue;
			Class<? extends Scenario> scenarioType = ScenarioManager.getScenarioType(new File(filePath));
			if (scenarioType == null) {
				Log.error("no loader for the file: " + filePath);
				continue;
			} else if (!LocalScenario.class.isAssignableFrom(scenarioType)) {
				Log.error("Not a LocalScenario type");
				continue;
			}
			try {
				TimedScenario scenario = (TimedScenario) scenarioType.getConstructor().newInstance();
				scenario.setFile(new File(filePath));
				if (scenario.getDataType() == null)
					continue;
				scenario.setStartTime(getStartTime());
				scenario.setStopTime(getStopTime());
				scenario.setPeriod(1);
				this.scenarios.add(scenario);
				this.scenarioIds[i] = this.scenarios.size() - 1;
			} catch (IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				Log.error("cannot create the scenario: " + scenarioType.getSimpleName());
				continue;
			}
			this.valideScenario[i] = true;
		}
	}

	private ArrayList<ScheduleTask> readScheduleFile() {
		// Log.error("readScheduleFile: " + Thread.currentThread().getId());
		if (this.file == null || !this.file.getName().endsWith(getReaderFormatNames()[0]) || !this.file.exists())
			return null;
		String ligne = null;
		String scenarioPath = this.file.getParent();
		ArrayList<ScheduleTask> schedulableTasks = new ArrayList<>();
		HashSet<String> scenarioNames = new HashSet<>();
		try (FileInputStream fis = new FileInputStream(this.file)) {
			if (fis.read() == 0)
				try (BufferedReader br = new BufferedReader(new InputStreamReader(fis))) {
					if (this.scenarios == null)
						readHeader(br, scenarioPath, scenarioNames);
					else
						skipHeader(br);
					Scenario[] scenariosArray = new Scenario[this.scenarios.size()];
					for (int i = 0; i < scenariosArray.length; i++)
						scenariosArray[i] = this.scenarios.get(i);
					int valideScenarioLength = this.valideScenario.length;
					while ((ligne = br.readLine()) != null) {
						int i1 = ligne.indexOf(":");
						int i2 = ligne.indexOf(":", i1 + 1);
						int i3 = ligne.indexOf(":", i2 + 1);
						int scenarioIndex = Integer.parseInt(ligne.substring(i2 + 1, i3));
						if (scenarioIndex < valideScenarioLength && this.valideScenario[scenarioIndex])
							schedulableTasks.add(new ScheduleTask(Long.parseLong(ligne.substring(0, i1)), Long.parseLong(ligne.substring(i1 + 1, i2)), scenariosArray[this.scenarioIds[scenarioIndex]],
									Integer.parseInt(ligne.substring(i3 + 1))));
					}
				}
			else {
				FileChannel inChannel = fis.getChannel();
				MappedByteBuffer buffer = inChannel.map(FileChannel.MapMode.READ_ONLY, 0, inChannel.size());
				buffer.load();
				if (this.scenarios == null)
					readCompactHeader(buffer, scenarioPath, scenarioNames);
				else
					skipCompactHeader(buffer);
				Scenario[] scenariosArray = new Scenario[this.scenarios.size()];
				for (int i = 0; i < scenariosArray.length; i++)
					scenariosArray[i] = this.scenarios.get(i);
				int valideScenarioLength = this.valideScenario.length;
				try {
					while (buffer.hasRemaining()) {
						long toi = buffer.getLong();
						long ts = buffer.getLong();
						int scenarioIndex = buffer.getInt();
						long seekIndex = buffer.getLong();
						if (scenarioIndex >= 0 && scenarioIndex < valideScenarioLength && this.valideScenario[scenarioIndex])
							schedulableTasks.add(new ScheduleTask(toi, ts, scenariosArray[this.scenarioIds[scenarioIndex]], seekIndex));
					}
				} catch (BufferUnderflowException e) {
					Log.error("The end of the schedule file: " + this.file + " is corrupted, some data may be missing");
				}
			}
		} catch (NumberFormatException | StringIndexOutOfBoundsException | IOException e) {
			Log.error("Erreur while reading Schedule File: " + e.getClass().getSimpleName() + ": " + e.getMessage());
		}
		return schedulableTasks;
	}

	private void readScheduleFileHeader() {
		if (this.file == null || !this.file.getName().endsWith(getReaderFormatNames()[0]) || !this.file.exists()) {
			this.scenarios = null;
			return;
		}
		this.scenarios = new ArrayList<>();
		String scenarioPath = this.file.getParent();
		HashSet<String> scenarioNames = new HashSet<>();
		try (FileInputStream fis = new FileInputStream(this.file);) {
			if (fis.read() == 0)
				try (BufferedReader br = new BufferedReader(new InputStreamReader(fis))) {
					readHeader(br, scenarioPath, scenarioNames);
				}
			else {
				FileChannel inChannel = fis.getChannel();
				MappedByteBuffer buffer = inChannel.map(FileChannel.MapMode.READ_ONLY, 0, inChannel.size());
				buffer.load();
				readCompactHeader(buffer, scenarioPath, scenarioNames);
			}
		} catch (NumberFormatException | IOException e) {
			Log.error("Erreur while reading Schedule File: " + e.getClass().getSimpleName() + ": " + e.getMessage());
		}
	}

	@Override
	public void setStartTime(Date startTime) {
		super.setStartTime(startTime);
		if (this.scenarios != null)
			for (TimedScenario scenario : this.scenarios)
				scenario.setStartTime(startTime);
	}

	@Override
	public void setStopTime(Date stopTime) {
		super.setStopTime(stopTime);
		if (this.scenarios != null)
			for (TimedScenario scenario : this.scenarios)
				scenario.setStopTime(stopTime);
	}

	@Override
	public void save(File file) throws IOException {}

	@Override
	public void setFile(File file) {
		if (Objects.equals(file, this.file))
			return;
		this.scenarios = null;
		super.setFile(file);
		if (this.st != null)
			try {
				updateIOStructure();
			} catch (Exception e) {
				e.printStackTrace();
				throw new IllegalArgumentException("Cannot init the struct, " + file.getName());
			}
	}

	private void setSchedulerInterfaceToSubScenario() {
		if (this.scenarios != null)
			for (Scenario scenario : this.scenarios)
				scenario.setScheduler(this.schedulerInterface);
	}

	// @Override
	// public void setTheaterPanel(TheaterPanel theaterPanel) {
	// super.setTheaterPanel(theaterPanel);
	// this.scenarios.get(0).setTheaterPanel(theaterPanel);
	// }

	@Override
	public void setTrigger(ScenarioTrigger trigger) {
		super.setTrigger(trigger);
		if (trigger != null && this.scenarios == null) // Ajout pour ne pas lire le fichier quand on arrête le diagramme
			readScheduleFileHeader(); // 14/09/17 -> changement de readScheduleFile vers readScheduleFileHeader. On a besoin que des scenarios...
		if (this.scenarios == null)
			return;
	}

	private static void skipCompactHeader(MappedByteBuffer buffer) throws NumberFormatException {
		buffer.get();
		int nbInput = buffer.getInt();
		for (int i = 0; i < nbInput; i++)
			getString(buffer);
	}

	private static void skipHeader(BufferedReader br) throws NumberFormatException, IOException {
		int nbInput = Integer.parseInt(br.readLine());
		for (int i = 0; i < nbInput; i++)
			br.readLine();
	}

	@Override
	public void setVisible() {
		super.setVisible();
		fireSetPropertyVisible(this, "period", false);
	}

	public static String getScenarioName(String filePath) {
		return filePath.substring(0, filePath.lastIndexOf("."))/* .substring(filePath.indexOf("_") + 1) */;
	}

	private static String getString(ByteBuffer bb) {
		byte[] stringBytes = new byte[bb.getInt()];
		bb.get(stringBytes);
		return new String(stringBytes, StandardCharsets.UTF_8);
	}
}