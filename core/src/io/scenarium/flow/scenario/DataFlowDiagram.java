/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.scenario;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import io.beanmanager.BeanDesc;
import io.beanmanager.BeanManager;
import io.beanmanager.BeanRenameListener;
import io.beanmanager.BeanUnregisterListener;
import io.beanmanager.SaveListener;
import io.beanmanager.editors.DynamicEnableBean;
import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.basic.EnumEditor;
import io.beanmanager.editors.basic.InetSocketAddressEditor;
import io.beanmanager.editors.container.BeanEditor;
import io.beanmanager.tools.Tuple;
import io.scenarium.core.editors.NotChangeableAtRuntime;
import io.scenarium.core.filemanager.scenariomanager.LocalScenario;
import io.scenarium.core.filemanager.scenariomanager.MetaScenario;
import io.scenarium.core.filemanager.scenariomanager.PeriodListener;
import io.scenarium.core.filemanager.scenariomanager.RecordingListener;
import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.filemanager.scenariomanager.ScenarioException;
import io.scenarium.core.filemanager.scenariomanager.ScenarioTrigger;
import io.scenarium.core.filemanager.scenariomanager.SourceChangeListener;
import io.scenarium.core.filemanager.scenariomanager.StartStopChangeListener;
import io.scenarium.core.filemanager.scenariomanager.TimedScenario;
import io.scenarium.core.timescheduler.ScheduleTask;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.core.timescheduler.SchedulerInterface;
import io.scenarium.core.timescheduler.SchedulerPropertyChangeListener;
import io.scenarium.core.timescheduler.SchedulerState;
import io.scenarium.core.timescheduler.TriggerMode;
import io.scenarium.core.timescheduler.VisuableSchedulable;
import io.scenarium.core.tools.AnimationTimerConsumer;
import io.scenarium.core.tools.ObservableValue;
import io.scenarium.core.tools.VisuableSchedulableContainer;
import io.scenarium.flow.Block;
import io.scenarium.flow.BlockInfo;
import io.scenarium.flow.BlockInput;
import io.scenarium.flow.BlockOutput;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.flow.FlowDiagram;
import io.scenarium.flow.FlowDiagramChangeListener;
import io.scenarium.flow.FlowDiagramIO;
import io.scenarium.flow.FlowDiagramInput;
import io.scenarium.flow.FlowDiagramOutput;
import io.scenarium.flow.IllegalInputArgument;
import io.scenarium.flow.Input;
import io.scenarium.flow.InputRingBuffer;
import io.scenarium.flow.Link;
import io.scenarium.flow.ModificationType;
import io.scenarium.flow.Output;
import io.scenarium.flow.ProcessMode;
import io.scenarium.flow.ReaderType;
import io.scenarium.flow.RunningMode;
import io.scenarium.flow.SubDataFlowDiagramTrigger;
import io.scenarium.flow.VarArgsInputChangeListener;
import io.scenarium.flow.internal.Log;
import io.scenarium.flow.operator.OperatorManager;
import io.scenarium.flow.operator.SchedulerInterfaceRemote;
import io.scenarium.flow.operator.player.ScenarioPlayer;
import io.scenarium.flow.operator.rmi.RemoteOperator;
import io.scenarium.flow.operator.rmi.VisuableSchedulableRemoteOperator;
import io.scenarium.flow.scheduler.DiagramScheduler;
import io.scenarium.flow.scheduler.MonoCoreDiagramScheduler;
import io.scenarium.flow.scheduler.MultiCoreDiagramScheduler;
import io.scenarium.flow.scheduler.NoThreadedDiagramScheduler;
import io.scenarium.pluginManager.LoadModuleListener;
import io.scenarium.pluginManager.ModuleManager;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;

// Strategies for sub-diagram :
// problem:
//	3 canals (D: diagram, S: sub-diagram, F: JavaFX Thread):
//		1- trigger input to sub-diagram:	D::process -> D::triggeroutput lock S and wait S available
//		2- trigger output to diagram:		S::stackOutputsAndTrigger -> S::runFOTaskLater lock D and wait D available
//		3- structure change:				F::Add/Remove -> runLater lock D -> runLater lock S
// If 1 and then 2 while processing -> deadlock
//
// Best:	RunLater	 				No object instantiation			Existing mode		No additional thread
// Middle:	OfferTask					Object instantiation			Exclusive mode		No additional thread
// Worst:	MonoOutputInterfaceThread	Object instantiation			No mode				One additional thread for outputs
//
// Possibilities:
//	Diagram mode		Sub diagram mode		Running						Editing
//	MULTI_CORE			MULTI_CORE				RunLater					MonoOutputInterfaceThread
//	MULTI_CORE			MONO_CORE				OfferTask					MonoOutputInterfaceThread
//	MULTI_CORE			NO_THREADED				OfferTask					MonoOutputInterfaceThread
//	MONO_CORE			MULTI_CORE				RunLater					MonoOutputInterfaceThread
//	MONO_CORE			MONO_CORE				OfferTask					MonoOutputInterfaceThread
//	MONO_CORE			NO_THREADED				OfferTask					MonoOutputInterfaceThread
//	NO_THREADED			MULTI_CORE				RunLater					MonoOutputInterfaceThread
//	NO_THREADED			MONO_CORE				MonoOutputInterfaceThread	MonoOutputInterfaceThread
//	NO_THREADED			NO_THREADED				MonoOutputInterfaceThread	MonoOutputInterfaceThread	(dead lock sinon, pas le même verrou pour synchronized... interdire ? (même chose que mode non managé)

@BlockInfo(info = "Allows the scheduling of a flow diagram. Can be added within another flow diagram as a sub-flow diagram. In case of a conflict between blocks, the blocks will be marked as conflicting and cannot be executed until the resolution of the conflict. Indeed, two blocks cannot be executed concurrently in the same flow diagram.")
public class DataFlowDiagram extends LocalScenario implements FlowDiagramChangeListener, SourceChangeListener, PeriodListener, BeanUnregisterListener, BeanRenameListener, VisuableSchedulableContainer,
		SchedulerPropertyChangeListener, PropertyChangeListener, StartStopChangeListener, MetaScenario, RecordingListener, LoadModuleListener, SaveListener, DynamicEnableBean {
	private static final String RUNNING_MODE_TAG = "rm";
	private static final String TRIGGER_MODE_TAG = "tm";
	private static final String BLOCKS_TAG = "Blocks";
	private static final String X_COORDINATE_TAG = "x";
	private static final String Y_COORDINATE_TAG = "y";
	private static final String LINKS_TAG = "Links";
	private static final String LINK_OUTPUT_OPERATOR_NAME_TAG = "oo";
	private static final String LINK_OUTPUT_NAME_TAG = "on";
	private static final String LINK_INPUT_OPERATOR_NAME_TAG = "io";
	private static final String LINK_INPUT_NAME_TAG = "in";
	private static final String INPUTS_TAG = "Inputs";
	private static final String OUTPUTS_TAG = "Outputs";
	private static final String IO_TYPE_TAG = "t";
	private static final String IO_NAME_TAG = "n";
	private static final String IO_MAX_STACK_SIZE_TAG = "s";
	public static final String READER_FORMAT_NAME = "pdfd";
	private long nbGap;
	private ArrayList<Block> schedulableBlocksWithSomeThingToPlan = new ArrayList<>();
	private final ArrayList<Block> schedulableBlocksWithoutSomeThingsToPlan = new ArrayList<>();
	@PropertyInfo(index = 1, info = "Determines whether this flow diagram should have its own running mode policy or should follow the parents' running mode policy.")
	@NotChangeableAtRuntime
	private boolean subManagement = false;
	@PropertyInfo(index = 2, nullable = false, savable = false, info = "Specify the running mode of the diagram.\nNo_Threaded: Run the diagram without any dedicated thread. This implies that the thread triggering data will execute all subsequent tasks.\nMonoCore: Run all blocks in a single dedicated thread and can only be triggered from this thread.\nMultiCore: Each block is triggered from its own dedicated thread.")
	@NotChangeableAtRuntime
	private RunningMode runningMode = RunningMode.MULTI_CORE;
	@PropertyInfo(index = 3, nullable = false, savable = false, info = "Specify if the diagram is scheduled with the time stamp of record or with the time of issue.\nTimeStamp: replay the record based on the time when the data is available by recorder.\nTimeOfIssue: replay the record based on the time stamp of the data received by the recorder.")
	@NotChangeableAtRuntime
	private TriggerMode triggerMode = TriggerMode.TIME_OF_ISSUE;

	private DiagramScheduler diagramScheduler;
	private AnimationTimerConsumer animationTimerConsumer;
	private SchedulerInterface oldSchedulerInterface;
	// private boolean started;
	private CountDownLatch processModeCountDownLatch;
	private double periodFromSubScenario;
	private RunningMode inFileRunningMode = null;
	private TriggerMode inFileTriggerMode = null;

	@Override
	public void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		if (OperatorManager.isOperator(beanDesc.bean))
			((FlowDiagram) this.scenarioData).beanRename(beanDesc.bean);
	}

	@Override
	public void beanUnregister(BeanDesc<?> beanDesc, boolean delete) {
		if (OperatorManager.isOperator(beanDesc.bean)) {
			FlowDiagram fd = (FlowDiagram) this.scenarioData;
			for (Block block : fd.getBlocks())
				if (block.operator == beanDesc.bean) {
					ArrayList<Object> h = new ArrayList<>();
					h.add(block);
					fd.removeElements(h/* , false */, false);
					break;
				}
		}
	}

	@Override
	public boolean canCreateDefault() {
		return true;
	}

	@Override
	public synchronized boolean close() {
		if (this.schedulerInterface != null)
			// if (hasBlock() && hasSomeThingsToPlan()) // Do not interrupt the scheduler if I am a sub diagram
			// schedulerInterface.stop(); // stop vers stopAndWait, j'attend la fin avant de continuer
			this.schedulerInterface.removePropertyChangeListener(this);
		if (this.diagramScheduler != null) {
			this.diagramScheduler.stop();
			if (this.scenarioData != null)
				((FlowDiagram) this.scenarioData).setRunTaskLaterInSubDiagramFunction(null);
			this.diagramScheduler = null;
		}
		// Only if file changed now
		// if (scenarioData != null) {
		// ((FlowDiagram) scenarioData).removeFlowDiagramChangeListener(this);
		// ((FlowDiagram) scenarioData).removeRecordingListener(this);
		// ((FlowDiagram) scenarioData).death(); // détruit tous les blocks...
		// }
		// if (!hasBlock())
		// scenarioData = null;
		return true;
	}

	@Override
	public boolean dispose() {
		if (this.schedulerInterface != null && /* st.hasBlock() */ this.st != null && hasSomeThingsToPlan())
			this.schedulerInterface.stop();
		death();
		BeanEditor.removeStrongRefBeanUnregisterListener(this);
		BeanEditor.removeStrongRefBeanRenameListener(this);
		ModuleManager.removeLoadModuleListener(this);
		FlowDiagram fd = (FlowDiagram) this.scenarioData;
		fd.setOutputsSupplier(null);
		fd.removeFlowDiagramChangeListener(this);
		fd.removeRecordingListener(this);
		fd.death(); // détruit tous les blocks...
		this.scenarioData = null;
		return true;
	}

	@Override
	public void death() {
		// this.started = false;
		if (this.scenarioData != null) {
			FlowDiagram fd = (FlowDiagram) this.scenarioData;
			fd.setTriggerOutputFunction(null);
			// fd.setRunLaterFunction(null);
		}
		if (this.outputInterfaceThread != null)
			synchronized (this) {
				this.pendingsDatas = null;
				this.outputInterfaceThread.interrupt();
				this.outputInterfaceThread = null;
			}
		stateChanged(SchedulerState.STOPPING);
		close();
	}

	@Override
	public boolean hasSomeThingsToPlan() {
		if (this.scenarioData == null) {
			try {
				load(getFile(), false);
			} catch (ScenarioException | IOException e) {}
			if (this.scenarioData == null)
				return false;
		}
		for (Block blockToManage : ((FlowDiagram) this.scenarioData).getManagedBlocks(b -> !b.isConflict()))
			if (blockToManage.operator instanceof ScenarioPlayer && ((ScenarioPlayer) blockToManage.operator).hasSomeThingsToPlan())
				return true;
		return false;
	}

	private static boolean createLink(FlowDiagram fd, String iName, String iOp, String oName, String oOp) {
		Input lInput = null;
		if (iOp.equals("i")) {
			for (FlowDiagramInput fdInput : fd.getInputs())
				if (fdInput.getName().equals(iName)) {
					lInput = fdInput;
					break;
				}
		} else
			for (Block block : fd.getBlocks()) {
				Object op = block.getOperator();
				if (op != null)
					if (BeanEditor.getBeanDesc(op).getSimpleDescriptor().equals(iOp))
						for (BlockInput input : block.getInputs())
							if (input.getName().equals(iName)) {
								lInput = input;
								break;
							}
				if (lInput != null)
					break;
			}
		if (lInput == null)
			return false;
		Output lOutput = null;
		if (oOp.equals("o")) {
			if (lInput instanceof FlowDiagramInput)
				return true;
			for (FlowDiagramOutput fdOutput : fd.getOutputs())
				if (fdOutput.getName().equals(oName)) {
					lOutput = fdOutput;
					break;
				}
		} else
			for (Block block : fd.getBlocks())
				if (BeanEditor.getBeanDesc(block.getOperator()).getSimpleDescriptor().equals(oOp)) {
					for (BlockOutput output : block.getOutputs())
						if (output.getName().equals(oName)) {
							lOutput = output;
							break;
						}
					if (lOutput != null)
						break;
				}
		if (lOutput == null)
			return false;
		return lInput.setLink(new Link(lInput, lOutput));
	}

	// private final Semaphore fileChangeDone = new Semaphore(1);
	// private ThreadPoolExecutor fileChangeExecutor;

	@Override
	public void setFile(File file) {
		if (Objects.equals(file, getFile()))
			return;
		super.setFile(file);
		if (this.scenarioData != null)
			dispose();
		updateIOStructure();
		updateWarningStatus();
	}

	public void updateWarningStatus() {
		if (this.scenarioData != null && this.updateParentDiagramWarningStatus != null) {
			ArrayList<Block> blocksInConflict = ((FlowDiagram) this.scenarioData).getBlocksInConflict();
			if (!blocksInConflict.isEmpty()) {
				this.st.setWarning("Blocks: " + blocksInConflict.stream().map(Block::getName).collect(Collectors.joining(",")) + " are in conflicts with other blocks");
				this.updateParentDiagramWarningStatus.run();
				return;
			}
			this.st.setWarning(null);
			this.updateParentDiagramWarningStatus.run();
		}
	}

	@Override
	public void flowDiagramChanged(FlowDiagram source, Object element, String property, ModificationType modificationType) {
		// Log.error("flowDiagramChanged: " + element + " " + property + " " + modificationType);
		if (element instanceof FlowDiagramIO || element instanceof Link && (((Link) element).getInput() instanceof FlowDiagramInput || ((Link) element).getOutput() instanceof FlowDiagramOutput))
			updateIOStructure();
		updateDiagramInfo(source, element, modificationType, "subChange".equals(property));
		fireDiagramChanged(source, element, property, modificationType);
	}

	public void addFlowDiagramChangeListener(FlowDiagramChangeListener listener) {
		this.listeners.add(FlowDiagramChangeListener.class, listener);
	}

	public void removeFlowDiagramChangeListener(FlowDiagramChangeListener listener) {
		this.listeners.remove(FlowDiagramChangeListener.class, listener);
	}

	private void fireDiagramChanged(FlowDiagram source, Object element, String property, ModificationType modificationType) {
		for (FlowDiagramChangeListener listener : this.listeners.getListeners(FlowDiagramChangeListener.class))
			listener.flowDiagramChanged(source, element, property, modificationType);
	}

	@Override
	public long getBeginningTime() {
		long minBeginTime = -1;
		for (Block block : this.schedulableBlocksWithSomeThingToPlan) {
			Object op = block.operator;
			if (op instanceof ScenarioPlayer) {
				Scenario scenario = ((ScenarioPlayer) op).getScenario();
				if (scenario != null) {
					long beginTime = scenario.getBeginningTime();
					if (beginTime >= 0 && (minBeginTime == -1 || beginTime < minBeginTime))
						minBeginTime = beginTime;
				}
			}
		}
		return minBeginTime;
	}

	@Override
	public Class<?> getDataType() {
		return DataFlowDiagram.class;
	}

	@Override
	public long getEndTime() {
		long maxEndTime = -1;
		for (Block block : this.schedulableBlocksWithSomeThingToPlan) {
			Object op = block.operator;
			if (op instanceof ScenarioPlayer) {
				Scenario scenario = ((ScenarioPlayer) op).getScenario();
				if (scenario != null) {
					long endTime = scenario.getEndTime();
					if (endTime > maxEndTime)
						maxEndTime = endTime;
				}
			}
		}
		return maxEndTime;
	}

	public long getNbGap() {
		return this.nbGap;
	}

	public void saveDiagram(File file) {
		if (file != null) {
			Log.info("save: " + DataFlowDiagram.class.getSimpleName() + ": " + file);
			try {
				save(file);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	@Override
	public String[] getReaderFormatNames() {
		return new String[] { READER_FORMAT_NAME };
	}

	public RunningMode getRunningMode() {
		return this.runningMode;
	}

	@Override
	public int getSchedulerType() {
		return Scheduler.EVENT_SCHEDULER;
	}

	public TriggerMode getTriggerMode() {
		return this.triggerMode;
	}

	@Override
	public void initOrdo() {
		// TODO REFACTO pb the listener is set before derived the interface class ???
		// TODO REFACTO the listener is attache to the scheduler and not with the interface ==> conception mistake
		if (this.oldSchedulerInterface != null)
			this.oldSchedulerInterface.removePropertyChangeListener(this);
		this.schedulerInterface.addPropertyChangeListener(this);
		this.oldSchedulerInterface = this.schedulerInterface;
		this.schedulerInterface.setTriggerMode(this.triggerMode);
		if (this.st == null)
			this.schedulerInterface = new SchedulerInterface(this.schedulerInterface);
		for (Block block : this.schedulableBlocksWithSomeThingToPlan) {
			ScenarioPlayer player = (ScenarioPlayer) block.operator;
			player.addPeriodChangeListenerIfAbsent(this); // TODO Remettre les listeners
			player.addStartStopChangeListenerIfAbsent(this);
			block.addPropertyChangeListenerIfNotPresent(this);
			if (block.getProcessMode() != ProcessMode.LOCAL)
				this.schedulerInterface = new SchedulerInterfaceRemote(this.schedulerInterface, block);

			Scenario scenario = player.getScenario();
			SchedulerInterface schedulerInterface = this.schedulerInterface;
			if (scenario instanceof TimedScenario) {
				TimedScenario ls = (TimedScenario) scenario;
				// ls.addPeriodListenerIfAbsent(this); // TODO Remettre les listeners
				// ls.addStartStopChangeListener(this);
				// block.addPropertyChangeListenerIfAbsent(this);
				// schedulerInterface = block.getProcessMode() == ProcessMode.LOCAL ? this.schedulerInterface
				// : this.schedulerInterface.derivateAsRemote(block.getProcessMode() != ProcessMode.LOCAL ? block : null);
				// ((LocalScenario) op).setColorIndex(colorIndex++);
				// if (colorIndex == COLORS.length)
				// colorIndex = 0;
				if (ls.getSchedulerType() == Scheduler.TIMER_SCHEDULER) {
					double lsPeriod = ls.getPeriod();
					int nbFrame = (int) ls.getNbFrame();
					if (lsPeriod > 0 && nbFrame > 0) {
						ArrayList<ScheduleTask> schedulableTasks = new ArrayList<>(nbFrame);
						long beginTime = schedulerInterface.getBeginTime();
						for (int i = 0; i < nbFrame; i++) {
							long time = beginTime + (long) (i * lsPeriod);
							schedulableTasks.add(new ScheduleTask(time, time, ls, time));
						}
						schedulerInterface.addTasks(schedulableTasks, null);
					}
				}
			}
			player.setScheduler(schedulerInterface);
			player.addSourceChangeListenerIfAbsent(this);

		}
		for (Block block : this.schedulableBlocksWithoutSomeThingsToPlan) {
			Object op = block.operator;
			if (op instanceof ScenarioPlayer) {
				Scenario scenario = ((ScenarioPlayer) op).getScenario();
				if (scenario != null) {
					scenario.setScheduler(this.schedulerInterface);
					scenario.addSourceChangeListenerIfAbsent(this);
				}
			}
		}
		if (this.st == null)
			setScenarioIndex(this, 0);
		startStopChanged();
	}

	private int setScenarioIndex(MetaScenario scenario, int scenarioIdIndex) {
		for (Scenario ss : scenario.getAdditionalScenario()) {
			if (ss instanceof MetaScenario)
				scenarioIdIndex = setScenarioIndex((MetaScenario) ss, scenarioIdIndex);
			ss.setId(scenarioIdIndex++);
		}
		return scenarioIdIndex;
	}

	@Override
	public void updateIOStructure() {
		if (this.st != null/* && st.hasBlock() */) {
			if (this.scenarioData == null) {
				try {
					load(getFile(), false);
				} catch (ScenarioException | IOException e) {}
				if (this.scenarioData == null)
					return;
			}
			FlowDiagram fd = (FlowDiagram) this.scenarioData;
			// ReentrantLock ioLock = fd.getLockIO();
			// ioLock.lock();
			// try {
			ArrayList<String> names = new ArrayList<>();
			ArrayList<Class<?>> types = new ArrayList<>();
			for (FlowDiagramInput fdInput : fd.getInputs()) {
				Class<?> type = fdInput.getType();
				if (type != null) {
					names.add(fdInput.getName());
					types.add(type);
				}
			}
			fireOutputChanged(names.toArray(String[]::new), types.toArray(Class[]::new));
			names.clear();
			types.clear();
			for (FlowDiagramOutput fdOutput : fd.getOutputs()) {
				Class<?> type = fdOutput.getType();
				if (type != null) {
					names.add(fdOutput.getName());
					types.add(type);
				}
			}
			fireInputChanged(names.toArray(String[]::new), types.toArray(Class[]::new));
			// } finally {
			// ioLock.unlock();
			// }
		}
	}

	@Override
	public boolean isPaintableProperties() {
		return true;
	}

	private volatile LinkedBlockingDeque<Runnable> pendingsDatas;
	private Thread outputInterfaceThread;
	private BiFunction<FlowDiagram, Object, Block> addBlockValidator;
	private BiPredicate<FlowDiagram, Object> operatorRemoved;
	private Runnable updateParentDiagramWarningStatus;
	private Runnable resetIndex;
	private final Object pendingsDatasLock = new Object();

	@Override
	public void birth() throws IOException, ScenarioException { // Call if exists as subDiagram
		super.birth();
		if (this.oldSchedulerInterface != null)
			this.oldSchedulerInterface.removePropertyChangeListener(this);
		if (this.schedulerInterface != null)
			this.schedulerInterface.addPropertyChangeListener(this);
		this.oldSchedulerInterface = this.schedulerInterface;
		if (this.scenarioData != null) {
			FlowDiagram fd = (FlowDiagram) this.scenarioData;
			if (this.subManagement) {
				stateChanged(SchedulerState.STARTING);
				fd.setTriggerOutputFunction((outputs, outputsTs, triggerOutputFunction) -> {
					/* if (super.getRunningMode() == RunningMode.MULTI_CORE && this.runningMode == RunningMode.MULTI_CORE) runLater(() -> triggerOutputFunction.accept(outputs, outputsTs,
					 * this::triggerOutput)); else */if (this.st.holdLock())
						triggerOutputFunction.accept(outputs, outputsTs, this.st::triggerOutput);
					else {
						Object[] outputsForOutputInterface = outputs == null ? null : outputs.clone();
						long[] outputsTsForOutputInterface = outputsTs == null ? null : outputsTs.clone();
						getPendingDatas().offer(() -> triggerOutputFunction.accept(outputsForOutputInterface, outputsTsForOutputInterface, this.st::triggerOutput)); // doit pas être bloquant!!!
					}
				});
				// fd.setRunLaterFunction(this::runLater);
			}
		}
	}

	private LinkedBlockingDeque<Runnable> getPendingDatas() { // Double-checked Locking using Volatile
		LinkedBlockingDeque<Runnable> localPendingsDatasRef = this.pendingsDatas;
		if (localPendingsDatasRef == null)
			synchronized (this.pendingsDatasLock) {
				localPendingsDatasRef = this.pendingsDatas;
				if (localPendingsDatasRef == null) {
					LinkedBlockingDeque<Runnable> pendingsDatas = new LinkedBlockingDeque<>();
					this.outputInterfaceThread = new Thread(this.st.getName() + " dispatching thread") {
						@Override
						public void run() {
							while (!Thread.currentThread().isInterrupted())
								try {
									Runnable task = pendingsDatas.take();
									DataFlowDiagram.this.st.runLater(task);
								} catch (InterruptedException e) {
									return;
								}
						}
					};
					this.pendingsDatas = localPendingsDatasRef = pendingsDatas;
					try {
						this.outputInterfaceThread.start();
					} catch (IllegalThreadStateException e) {
						// Stopped while starting
					}
				}
			}
		return localPendingsDatasRef;
	}

	@Override
	public synchronized void load(File scenarioFile, boolean backgroundLoading) throws IOException, ScenarioException {
		// Charge le diagramme
		if (backgroundLoading) { // backgroundLoading not possible, leads to concurrent access on blocks (e.g. setBlock)
			ObservableValue<Double> sdp = new ObservableValue<>(0.0);
			sdp.setValue(0.5);
			this.progressProperty = sdp;
			new Thread(() -> loadImmediately(scenarioFile)).start();
		} else
			loadImmediately(scenarioFile);
		// this.started = true; Log.info("birth of: " + getClass().getSimpleName());

	}

	private void loadImmediately(File scenarioFile) {
		if (this.scenarioData != null) {
			fireLoadChanged();
			return;
		}
		this.inFileRunningMode = null;
		this.inFileTriggerMode = null;
		FlowDiagram fd = new FlowDiagram();
		fd.setFlowDiagramGraphConsistencyMethods(this.addBlockValidator, this.operatorRemoved, this::updateWarningStatus, () -> this.schedulerInterface.stop(), this.resetIndex, this.runIOTaskLater);
		if (scenarioFile != null && scenarioFile.exists())
			try {
				Element racine = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(scenarioFile).getDocumentElement();
				String diagramRunningModeName = racine.getAttribute(RUNNING_MODE_TAG);
				if (!diagramRunningModeName.isEmpty()) {
					EnumEditor<RunningMode> rm = new EnumEditor<>(RunningMode.class);
					rm.setAsText(diagramRunningModeName);
					RunningMode value = rm.getValue();
					if (value != null) {
						this.inFileRunningMode = value;
						if (getRunningMode() != this.inFileRunningMode)
							setRunningMode(this.inFileRunningMode);
					}
				}
				String diagramTriggerModeName = racine.getAttribute(TRIGGER_MODE_TAG);
				if (!diagramTriggerModeName.isEmpty()) {
					EnumEditor<TriggerMode> rm = new EnumEditor<>(TriggerMode.class);
					rm.setAsText(diagramTriggerModeName);
					TriggerMode value = rm.getValue();
					if (value != null) {
						this.inFileTriggerMode = value;
						if (getTriggerMode() != this.inFileTriggerMode)
							setTriggerMode(this.inFileTriggerMode);
					}
				}
				ArrayList<Tuple<Block, String>> missedInputs = new ArrayList<>();
				HashMap<Block, ArrayList<VarArgsInputChangeListener>> varArgsInputChangeListenerBlockMap = new HashMap<>();
				// Load and create blocks
				Node blocksElement = racine.getElementsByTagName(BLOCKS_TAG).item(0);
				if (blocksElement != null) {
					NodeList bsNodeList = blocksElement.getChildNodes();
					for (int i = 0; i < bsNodeList.getLength(); i++) {
						Node item = bsNodeList.item(i);
						if (item instanceof Element) {
							Block block = getBlockFromElement((Element) item, missedInputs, varArgsInputChangeListenerBlockMap);
							if (block != null)
								fd.addBlock(block);
						}
					}
				}
				// Load and create flow diagram inputs
				Node outputsElement = racine.getElementsByTagName(OUTPUTS_TAG).item(0);
				if (outputsElement != null) {
					NodeList bsNodeList = outputsElement.getChildNodes();
					for (int l = 0; l < bsNodeList.getLength(); l++) {
						Node item = bsNodeList.item(l);
						if (item instanceof Element) {
							int x = (int) Double.parseDouble(((Element) item).getAttribute(X_COORDINATE_TAG));
							int y = (int) Double.parseDouble(((Element) item).getAttribute(Y_COORDINATE_TAG));
							String name = ((Element) item).getAttribute(IO_NAME_TAG);
							Class<?> type = getType(((Element) item).getAttribute(IO_TYPE_TAG));
							fd.addOutput(new FlowDiagramInput(fd, type, name, new Point2D(x, y)));
						}
					}
				}
				// Load and create flow diagram outputs
				Node inputsElement = racine.getElementsByTagName(INPUTS_TAG).item(0);
				if (inputsElement != null) {
					NodeList bsNodeList = inputsElement.getChildNodes();
					for (int l = 0; l < bsNodeList.getLength(); l++) {
						Node item = bsNodeList.item(l);
						if (item instanceof Element) {
							int x = (int) Double.parseDouble(((Element) item).getAttribute(X_COORDINATE_TAG));
							int y = (int) Double.parseDouble(((Element) item).getAttribute(Y_COORDINATE_TAG));
							String name = ((Element) item).getAttribute(IO_NAME_TAG);
							Class<?> type = getType(((Element) item).getAttribute(IO_TYPE_TAG));
							fd.addInput(new FlowDiagramOutput(type, name, new Point2D(x, y)));
						}
					}
				}
				// Load and create all links
				Node linksElement = racine.getElementsByTagName(LINKS_TAG).item(0);
				ArrayList<String[]> missedLink = new ArrayList<>();
				if (linksElement != null) {
					NodeList bsNodeList = linksElement.getChildNodes();
					for (int l = 0; l < bsNodeList.getLength(); l++) {
						Node item = bsNodeList.item(l);
						if (item instanceof Element)
							createLinkFromElement(fd, (Element) item, missedLink);
					}
				}
				setVarArgsLinks(fd, missedLink, varArgsInputChangeListenerBlockMap, missedInputs);
				fd.updateIOTypes(); // Pas top, plutot FlowDiagram.create().add(inputs).add(outputs).add(links).build();
			} catch (SAXException | ParserConfigurationException e) {} catch (IOException ex) {
				ex.printStackTrace();
			}
		fd.addFlowDiagramChangeListener(this);
		fd.addRecordingListener(this);
		BeanEditor.addStrongRefBeanUnregisterListener(this);
		BeanEditor.addStrongRefBeanRenameListener(this);
		ModuleManager.addLoadModuleListener(this);
		this.file = scenarioFile;
		if (this.st != null && this.st instanceof SubDataFlowDiagramTrigger)
			fd.setOutputsSupplier(((SubDataFlowDiagramTrigger) this.st).getFatherOperator());
		this.scenarioData = fd;
		fd.setSubManagement(isSubManagement());
		fd.setPotentialConflictsInSubFlowDiagram(this);
		updateDiagramInfo(null, null, ModificationType.CHANGE, false); // Je recharge le diagram encore une fois sinon..., mais je rajoute à schedulableBlocks et donc initOrdo père et donc scheduler
		fireLoadChanged();
		fireRecordingChanged(isRecording());
	}

	@Override
	public void setTrigger(ScenarioTrigger st) {
		super.setTrigger(st);
		if (this.scenarioData != null)
			((FlowDiagram) this.scenarioData).setOutputsSupplier(st == null ? null : ((SubDataFlowDiagramTrigger) st).getFatherOperator());
	}

	private static Class<?> getType(String type) {
		try {
			switch (type) {
			case "null":
				return null;
			case "boolean":
				return boolean.class;
			case "byte":
				return byte.class;
			case "char":
				return char.class;
			case "short":
				return short.class;
			case "int":
				return int.class;
			case "long":
				return long.class;
			case "float":
				return float.class;
			case "double":
				return double.class;
			default:
				return BeanManager.getClassFromDescriptor(type);
			}
		} catch (ClassNotFoundException e) {
			Log.error("Cannot find class of type: " + type);
			return null;
		}
	}

	private static void setVarArgsLinks(FlowDiagram fd, ArrayList<String[]> missedLink, HashMap<Block, ArrayList<VarArgsInputChangeListener>> varArgsInputChangeListenerBlockMap,
			ArrayList<Tuple<Block, String>> missedInputs) {
		// Set links for varargs
		if (!missedLink.isEmpty()) {
			int oldSize;
			do {
				oldSize = missedLink.size();
				for (Iterator<String[]> iterator = missedLink.iterator(); iterator.hasNext();) {
					String[] att = iterator.next();
					if (createLink(fd, att[0], att[1], att[2], att[3]))
						iterator.remove();
				}
			} while (oldSize != missedLink.size());
		}
		varArgsInputChangeListenerBlockMap.forEach((block, listenList) -> listenList.forEach(listener -> block.removeVarArgsInputChangeListener(listener))); // Remove all listener even if
		for (String[] att : missedLink)
			Log.error("cannot set the link: " + att[3] + ": " + att[2] + " -> " + att[1] + ": " + att[0]);
	}

	private static void createLinkFromElement(FlowDiagram fd, Element item, ArrayList<String[]> missedLink) {
		String iName = item.getAttribute(LINK_INPUT_NAME_TAG);
		String iOp = item.getAttribute(LINK_INPUT_OPERATOR_NAME_TAG);
		String oName = item.getAttribute(LINK_OUTPUT_NAME_TAG);
		String oOp = item.getAttribute(LINK_OUTPUT_OPERATOR_NAME_TAG);
		if (!createLink(fd, iName, iOp, oName, oOp))
			missedLink.add(new String[] { iName, iOp, oName, oOp });
	}

	private static Block getBlockFromElement(Element element, ArrayList<Tuple<Block, String>> missedInputs, HashMap<Block, ArrayList<VarArgsInputChangeListener>> varArgsInputChangeListenerBlockMap) {
		Object operator = null;
		String operatorName = element.getAttribute("o");
		try {
			String operatorTypeName = operatorName.substring(0, operatorName.indexOf(BeanDesc.SEPARATOR));
			for (Class<?> operatorType : OperatorManager.getOperators())
				if (BeanManager.getDescriptorFromClassWithSimpleName(operatorType).equals(operatorTypeName)) {
					try {
						operatorName = operatorName.substring(operatorTypeName.length() + 1);
						BeanDesc<?> regBeanDesc = BeanEditor.getRegisterBean(operatorType, operatorName);
						if (regBeanDesc == null) {
							operator = operatorType.getConstructor().newInstance();
							BeanManager beanManager = new BeanManager(operator, BeanManager.defaultDir);
							BeanEditor.registerBean(operator, operatorName, BeanManager.defaultDir); // Les fils n'existe pas encore...
							if (!beanManager.load()) {
								BeanEditor.unregisterBean(operator, true);
								operator = null;
							}
						} else
							operator = regBeanDesc.bean;
					} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
						if (e instanceof IllegalArgumentException)
							Log.error(e.getMessage());
						operator = null;
					}
					break;
				}
			if (operator == null)
				Log.error("Cannot load the operator: " + operatorName);
		} catch (StringIndexOutOfBoundsException e) {
			Log.error("Error during initialisation of operator: " + operatorName + "Error: " + e.getMessage());
		}
		if (operator != null) {
			int x = (int) Double.parseDouble(element.getAttribute(X_COORDINATE_TAG));
			int y = (int) Double.parseDouble(element.getAttribute(Y_COORDINATE_TAG));
			try {
				Block block = new Block(operator, new Point2D(x, y), false);
				String a = element.getAttribute("enable");
				if (!a.isEmpty())
					block.setEnable(Boolean.parseBoolean(a));
				a = element.getAttribute("waitAllDataConsumedBeforeDying");
				if (!a.isEmpty())
					block.setConsumesAllDataBeforeDying(Boolean.parseBoolean(a));
				a = element.getAttribute("startPriority");
				if (!a.isEmpty())
					block.setStartPriority(Integer.parseInt(a));
				a = element.getAttribute("stopPriority");
				if (!a.isEmpty())
					block.setStopPriority(Integer.parseInt(a));
				a = element.getAttribute("threadPriority");
				if (!a.isEmpty())
					block.setThreadPriority(Integer.parseInt(a));
				a = element.getAttribute("processMode");
				if (!a.isEmpty())
					block.setProcessMode(ProcessMode.valueOf(a));
				a = element.getAttribute("isolatePort");
				if (!a.isEmpty())
					block.setIsolatePort(Integer.parseInt(a));
				a = element.getAttribute("synchronizeProperties");
				if (!a.isEmpty())
					block.setSynchronizeProperties(Boolean.parseBoolean(a));
				a = element.getAttribute("remoteIp");
				if (!a.isEmpty()) {
					InetSocketAddressEditor isae = new InetSocketAddressEditor();
					isae.setAsText(a);
					block.setRemoteIp(isae.getValue());
				}
				a = element.getAttribute("remoteProperty");
				if (!a.isEmpty())
					block.setRemoteProperty(Boolean.parseBoolean(a));
				// fd.addBlock(block);
				NodeList iNodeList = element.getChildNodes();
				for (int j = 0; j < iNodeList.getLength(); j++) {
					Node iItem = iNodeList.item(j);
					if (iItem instanceof Element) {
						String ioName = ((Element) iItem).getAttribute(IO_NAME_TAG);
						if (iItem.getNodeName().startsWith("i")) {
							boolean success = false;
							for (BlockInput input : block.getInputs())
								if (input.getName().equals(ioName)) {
									loadInputParameters(input, (Element) iItem);
									success = true;
									break;
								}
							if (!success) // Try to create the input as input property
								try {
									if (block.addPropertyAsInput(ioName)) {
										BlockInput input = block.getInputs().get(block.getNbPropertyAsInput() + block.getInputIndex(ioName));
										loadInputParameters(input, (Element) iItem);
										success = true;
									}
								} catch (IllegalArgumentException e) {
									Log.error("Cannot set property as output: " + ioName + " for block: " + block.getName());
									Log.error(e.getMessage());
								}
							if (!success) { // VarArgs cannot be set now, delayed after blocks loaded
								Tuple<Block, String> blockAndInputName = new Tuple<>(block, ioName);
								missedInputs.add(blockAndInputName);
								ArrayList<VarArgsInputChangeListener> varArgsInputChangeListenerList = varArgsInputChangeListenerBlockMap.get(block);
								if (varArgsInputChangeListenerList == null) { // If there is at least two VarArgs or property inputs that failed to load
									varArgsInputChangeListenerList = new ArrayList<>();
									varArgsInputChangeListenerBlockMap.put(block, varArgsInputChangeListenerList);
								}
								if (block.getNbInput() != 0 && block.getInputs().get(block.getNbInput() - 1).isVarArgs()) {
									ArrayList<Tuple<Block, String>> finalMissedInput = missedInputs;
									ArrayList<VarArgsInputChangeListener> finalVarArgsInputChangeListenerList = varArgsInputChangeListenerList;
									int listenIndex = varArgsInputChangeListenerList.size(); // index of the last one, the current one
									VarArgsInputChangeListener l = (index, type) -> { // Add listener witch load properties of input when created
										if (type == VarArgsInputChangeListener.NEW) {
											BlockInput input = block.getInputs().get(index);
											if (input.getName().equals(ioName)) {
												loadInputParameters(input, (Element) iItem);
												finalMissedInput.remove(blockAndInputName);
												// Remove Listener when properties set, remove from list because cannot make reference to l
												block.removeVarArgsInputChangeListener(finalVarArgsInputChangeListenerList.get(listenIndex));
												return;
											}
										}
									};
									block.addVarArgsInputChangeListener(l);
									varArgsInputChangeListenerList.add(l);
								}
							}
						} else
							try {
								block.addPropertyAsOutput(ioName);
							} catch (IllegalArgumentException e) {
								Log.error("Cannot set property as output: " + ioName + " for block: " + block.getName());
								Log.error(e.getMessage());
							}
					}
				}
				return block;
			} catch (IllegalInputArgument e) {
				Log.error("Cannot load block: " + operatorName + " due to: " + e.getMessage());
			}
		}
		return null;
	}

	// private ReentrantLock ioLock;
	//
	// private void setIOLock(ReentrantLock ioLock) {
	// this.ioLock = ioLock;
	// }

	private static void loadInputParameters(BlockInput input, Element iItem) {
		String rt = iItem.getAttribute(IO_TYPE_TAG);
		if (rt != null && !rt.isBlank()) {
			EnumEditor<ReaderType> ee = new EnumEditor<>(ReaderType.class);
			ee.setAsText(rt);
			input.setReaderType(ee.getValue());
		}
		String mss = iItem.getAttribute(IO_MAX_STACK_SIZE_TAG);
		if (mss != null && !mss.isBlank())
			input.setMaxStackSize(Integer.parseInt(mss));
	}

	private void firePeriodChanged() {
		for (PeriodListener listener : this.listeners.getListeners(PeriodListener.class))
			listener.periodChanged(this.periodFromSubScenario);
	}

	@Override
	public void periodChanged(double period) {
		this.periodFromSubScenario = period;
		firePeriodChanged();
		this.schedulerInterface.clean();
		initOrdo();
	}

	@Override
	public void populateInfo(LinkedHashMap<String, String> info) throws IOException {
		if (this.scenarioData == null)
			try {
				load(getFile(), false);
			} catch (ScenarioException e) {}
		info.put("Number of frames", Long.toString(this.nbGap));
		info.put("Schedulable block", Integer.toString(this.schedulableBlocksWithSomeThingToPlan.size()));
	}

	@Override
	public void process(Long timePointer) {
		Object[] addInputs = this.st.getAdditionalInputs();
		long[] ts = new long[addInputs.length]; // TODO Remove array creation
		for (int i = 0; i < ts.length; i++)
			ts[i] = this.st.getTimeStamp(i);
		DiagramScheduler ds = this.diagramScheduler;
		Object sd = this.scenarioData;
		// if(addInputs.length != ((FlowDiagram) sd).getOutputs().size()){
		// Log.info("pas possible: " + addInputs.length + " " + ((FlowDiagram) sd).getOutputs().size());
		// }
		if (ds != null && sd != null)
			// if (ds instanceof MonoCoreDiagramScheduler)
			// ((MonoCoreDiagramScheduler) ds).triggerOutput(sd, addInputs, ts); // triggerOutput pas bloquant
			// else
			// Log.info(Arrays.stream(addInputs).map(v -> v == null ? null : v.getClass().getSimpleName()).collect(Collectors.joining(",")));
			ds.triggerOutput(sd, addInputs, ts);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals("processMode")) {
			CountDownLatch processModeCountDownLatch = this.processModeCountDownLatch;
			if (processModeCountDownLatch != null)
				try {
					processModeCountDownLatch.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			this.processModeCountDownLatch = new CountDownLatch(1);
			new Thread(() -> {
				sourceChanged(hasSomeThingsToPlan());
				this.processModeCountDownLatch.countDown();
			}).start();
		}
	}

	private void registerVisuableSchedulable(Block block) {
		this.animationTimerConsumer.register(block.getRemoteOperator() != null ? (VisuableSchedulableRemoteOperator) block.getRemoteOperator() : (VisuableSchedulable) block.operator);
	}

	@Override
	public void save(File file) throws IOException {
		FlowDiagram fd = (FlowDiagram) this.scenarioData;
		List<Block> blocks = fd.getBlocks();
		try {
			Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			Element racine = document.createElement(FlowDiagram.class.getSimpleName());
			racine.setAttribute(RUNNING_MODE_TAG, this.runningMode.name());
			racine.setAttribute(TRIGGER_MODE_TAG, this.triggerMode.name());

			if (!blocks.isEmpty()) {
				Element blocksElement = document.createElement(BLOCKS_TAG);
				for (int i = 0; i < blocks.size(); i++)
					blocksElement.appendChild(getElementFromBlock(blocks.get(i), document, i));
				racine.appendChild(blocksElement);
			}

			ArrayList<Link> links = fd.getAllLinks();
			if (!links.isEmpty()) {
				Element linksElement = document.createElement(LINKS_TAG);
				for (int i = 0; i < links.size(); i++)
					linksElement.appendChild(getElementFromLink(links.get(i), document, i));
				racine.appendChild(linksElement);
			}

			List<FlowDiagramInput> outputs = fd.getInputs();
			if (!outputs.isEmpty()) {
				int k = 0;
				Element outputsElement = document.createElement(OUTPUTS_TAG);
				for (FlowDiagramInput fdOutput : outputs) {
					Element iElement = document.createElement("o" + k++);
					Point2D pos = fdOutput.getPosition();
					iElement.setAttribute(X_COORDINATE_TAG, Integer.toString((int) pos.getX()));
					iElement.setAttribute(Y_COORDINATE_TAG, Integer.toString((int) pos.getY()));
					iElement.setAttribute(IO_NAME_TAG, fdOutput.getName());
					Class<?> type = fdOutput.getType(); // Need to save type to avoid to depends to order of links
					iElement.setAttribute(IO_TYPE_TAG, type == null ? "null" : BeanManager.getDescriptorFromClass(type));
					outputsElement.appendChild(iElement);
				}
				racine.appendChild(outputsElement);
			}

			List<FlowDiagramOutput> inputs = fd.getOutputs();
			if (!inputs.isEmpty()) {
				int k = 0;
				Element inputsElement = document.createElement(INPUTS_TAG);
				for (FlowDiagramOutput fdInput : inputs) {
					Element oElement = document.createElement("i" + k++);
					Point2D pos = fdInput.getPosition();
					oElement.setAttribute(X_COORDINATE_TAG, Integer.toString((int) pos.getX()));
					oElement.setAttribute(Y_COORDINATE_TAG, Integer.toString((int) pos.getY()));
					oElement.setAttribute(IO_NAME_TAG, fdInput.getName());
					Class<?> type = fdInput.getType();
					oElement.setAttribute(IO_TYPE_TAG, type == null ? "null" : BeanManager.getDescriptorFromClass(type));
					inputsElement.appendChild(oElement);
				}
				racine.appendChild(inputsElement);
			}
			document.appendChild(racine);
			try (FileOutputStream os = new FileOutputStream(file)) {
				TransformerFactory.newInstance().newTransformer().transform(new DOMSource(document), new StreamResult(os));
			}
		} catch (ParserConfigurationException | TransformerException | TransformerFactoryConfigurationError e) {
			Log.error("error during saving data flow diagram");
		}
		// Save all blocks
		HashSet<Object> visitedOperators = new HashSet<>();
		for (Block block : blocks) {
			Object op = block.operator;
			if (!visitedOperators.contains(op) && op instanceof EvolvedOperator) {
				RemoteOperator remoteOp = block.getRemoteOperator();
				try {
					if (remoteOp != null ? remoteOp.needToBeSaved() : ((EvolvedOperator) op).needToBeSaved())
						new BeanManager(op, BeanManager.defaultDir).save(true);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				visitedOperators.add(op);
			}
		}
	}

	private static Element getElementFromBlock(Block block, Document document, int i) {
		Element bElement = document.createElement("b" + i);
		bElement.setAttribute("o", BeanEditor.getBeanDesc(block.getOperator()).getSimpleDescriptor());
		Rectangle2D rect = block.getRectangle();
		bElement.setAttribute(X_COORDINATE_TAG, Integer.toString((int) rect.getMinX()));
		bElement.setAttribute(Y_COORDINATE_TAG, Integer.toString((int) rect.getMinY()));
		if (!block.isEnable())
			bElement.setAttribute("enable", Boolean.toString(block.isEnable()));
		if (block.isConsumesAllDataBeforeDying())
			bElement.setAttribute("waitAllDataConsumedBeforeDying", Boolean.toString(block.isConsumesAllDataBeforeDying()));
		if (block.getStartPriority() != 0)
			bElement.setAttribute("startPriority", Integer.toString(block.getStartPriority()));
		if (block.getStopPriority() != 0)
			bElement.setAttribute("stopPriority", Integer.toString(block.getStopPriority()));
		if (block.getThreadPriority() != Thread.NORM_PRIORITY)
			bElement.setAttribute("threadPriority", Integer.toString(block.getThreadPriority()));
		if (block.getProcessMode() != ProcessMode.LOCAL)
			bElement.setAttribute("processMode", block.getProcessMode().toString());
		if (block.getIsolatePort() != 1099)
			bElement.setAttribute("isolatePort", Integer.toString(block.getIsolatePort()));
		if (!block.isSynchronizeProperties())
			bElement.setAttribute("synchronizeProperties", Boolean.toString(block.isSynchronizeProperties()));
		if (block.getRemoteIp() != null && !block.getRemoteIp().equals(new InetSocketAddress(0))) {
			InetSocketAddressEditor isae = new InetSocketAddressEditor();
			isae.setValue(block.getRemoteIp());
			bElement.setAttribute("remoteIp", isae.getAsText());
		}
		if (block.isRemoteProperty())
			bElement.setAttribute("remoteProperty", Boolean.toString(block.isRemoteProperty()));
		List<BlockInput> inputs = block.getInputs();
		for (int j = 0; j < inputs.size(); j++) {
			BlockInput input = inputs.get(j);
			ReaderType rt = input.getReaderType();
			int mss = input.getMaxStackSize();
			if (rt != ReaderType.FIFO || mss != InputRingBuffer.DEFAULT_MAX_STACK_SIZE || block.getInputIndex(input.getName()) < 0) {
				Element iElement = document.createElement("i" + j);
				iElement.setAttribute(IO_NAME_TAG, input.getName());
				// int indexOfInput = input.getBlock().getInputIndex(input.getName());
				// iElement.setAttribute("p", Boolean.toString(indexOfInput < 0));
				if (rt != ReaderType.FIFO) {
					EnumEditor<ReaderType> ee = new EnumEditor<>(ReaderType.class);
					ee.setValue(input.getReaderType());
					iElement.setAttribute(IO_TYPE_TAG, ee.getAsText());
				}
				if (mss != InputRingBuffer.DEFAULT_MAX_STACK_SIZE)
					iElement.setAttribute(IO_MAX_STACK_SIZE_TAG, Integer.toString(input.getMaxStackSize()));
				bElement.appendChild(iElement);
			}
		}
		List<BlockOutput> outputs = block.getOutputs();
		int nbPropertyOutput = block.getNbPropertyAsOutput();
		for (int j = 0; j < nbPropertyOutput; j++) {
			Element oElement = document.createElement("o" + j);
			oElement.setAttribute(IO_NAME_TAG, outputs.get(j).getName());
			bElement.appendChild(oElement);
		}
		return bElement;
	}

	private static Element getElementFromLink(Link link, Document document, int i) {
		Element lElement = document.createElement("l" + i);
		Input input = link.getInput();
		lElement.setAttribute(LINK_INPUT_OPERATOR_NAME_TAG, input instanceof BlockInput ? BeanEditor.getBeanDesc(((BlockInput) input).getBlock().operator).getSimpleDescriptor() : "i");
		lElement.setAttribute(LINK_INPUT_NAME_TAG, input.getName());
		Output output = link.getOutput();
		lElement.setAttribute(LINK_OUTPUT_OPERATOR_NAME_TAG, output instanceof BlockOutput ? BeanEditor.getBeanDesc(((BlockOutput) output).getBlock().operator).getSimpleDescriptor() : "o");
		lElement.setAttribute(LINK_OUTPUT_NAME_TAG, output.getName());
		return lElement;
	}

	@Override
	public void setAnimationTimer(AnimationTimerConsumer animationTimerConsumer) {
		this.animationTimerConsumer = animationTimerConsumer;
		if (this.scenarioData != null) {
			FlowDiagram fd = (FlowDiagram) this.scenarioData;
			if (animationTimerConsumer != null)
				for (Link link : fd.getAllLinks()) {
					Input input = link.getInput();
					if (input instanceof BlockInput)
						if (((BlockInput) input).getBlock().operator instanceof VisuableSchedulable)
							registerVisuableSchedulable(((BlockInput) input).getBlock());
				}
			for (Block block : fd.getBlocks())
				if (block.operator instanceof VisuableSchedulableContainer)
					((VisuableSchedulableContainer) block.operator).setAnimationTimer(animationTimerConsumer);
		}
	}

	public void setRunningMode(RunningMode runningMode) {
		RunningMode oldRunningMode = this.runningMode;
		this.runningMode = runningMode;
		// stopIfWanted(() -> this.schedulerInterface.stop());
		this.pcs.firePropertyChange("runningMode", oldRunningMode, this.runningMode);
	}

	public void setTriggerMode(TriggerMode triggerMode) {
		TriggerMode oldTriggerMode = this.triggerMode;
		this.triggerMode = triggerMode;
		// stopIfWanted(() -> sourceChanged(hasSomeThingsToPlan()));
		this.pcs.firePropertyChange("triggerMode", oldTriggerMode, this.triggerMode);
	}

	// private void stopIfWanted(Runnable runnable) {
	// if (this.schedulerInterface != null && this.schedulerInterface.isRunning())
	// if (ScenariumGui.isJavaFXThreadStarted()) {
	// Alert alert = new Alert(AlertType.CONFIRMATION, "", new ButtonType("Yes", ButtonData.YES), new ButtonType("No", ButtonData.NO));
	// ((Stage) alert.getDialogPane().getScene().getWindow()).setAlwaysOnTop(true);
	// alert.setTitle("Scheduler stop");
	// alert.setHeaderText("Do you want to stop the diagram to take into account the modification?");
	// if (alert.showAndWait().get().getButtonData() == ButtonData.YES)
	// new Thread(new Task<>() {
	// @Override
	// protected Boolean call() throws Exception {
	// runnable.run();
	// return true;
	// }
	// }).start();
	// } else
	// runnable.run();
	// }

	@Override
	public void sourceChanged(boolean planChanged) {
		if (!planChanged || this.schedulerInterface == null)
			return;
		this.schedulerInterface.clean();
		initOrdo();
	}

	private BlockLaunchableListener blockLaunchableListener;
	private Consumer<Runnable> runIOTaskLater;

	class BlockLaunchableListener implements PropertyChangeListener {

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			if (evt.getPropertyName().equals("launchable"))
				addOrRemoveBlockToDiagramScheduler((Block) evt.getSource(), (Boolean) evt.getNewValue());
		}
	}

	private void addOrRemoveBlockToDiagramScheduler(Block block, boolean add) {
		// Log.info("addOrRemoveBlockToDiagramScheduler: " + block + " " + add + " to: " + this);
		DiagramScheduler ds = DataFlowDiagram.this.diagramScheduler;
		if (ds == null)
			return;
		Object operator = block.operator;
		boolean isListeningScheduler = operator instanceof SchedulerPropertyChangeListener;
		// offerTask -> does not block the JavaFx thread in birth or death methods of operator
		if (isListeningScheduler) {
			AtomicBoolean stateChanged = new AtomicBoolean(false);
			SchedulerPropertyChangeListener listener = e -> stateChanged.set(true);
			this.schedulerInterface.addPropertyChangeListener(listener);
			this.schedulerInterface.offerTask(() -> {
				if (add) {
					ds.addBlock(block, isListeningScheduler);
					if (isListeningScheduler) {
						this.schedulerInterface.removePropertyChangeListener(listener);
						if (!stateChanged.get()) // if not -> the state of the scheduler changed. We thus don't have to send the event.
							((SchedulerPropertyChangeListener) operator).stateChanged(SchedulerState.STARTED);
					}
				} else {
					if (isListeningScheduler) {
						this.schedulerInterface.removePropertyChangeListener(listener);
						if (!stateChanged.get()) // if not -> the state of the scheduler changed. We thus don't have to send the event.
							((SchedulerPropertyChangeListener) operator).stateChanged(SchedulerState.STOPPING);
					}
					ds.removeBlock(block);
				}
			});
		} else
			this.schedulerInterface.offerTask(() -> {
				if (add)
					ds.addBlock(block, isListeningScheduler);
				else
					ds.removeBlock(block);
			});
	}

	@Override
	public void stateChanged(SchedulerState state) {
		if (state == SchedulerState.STARTING || state == SchedulerState.STARTED || state == SchedulerState.SUSPENDED || state == SchedulerState.UNSUSPENDED || state == SchedulerState.PRESTOP
				|| state == SchedulerState.STOPPING)
			synchronized (this) {
				if (state == SchedulerState.STARTING) {
					if (this.diagramScheduler == null && this.scenarioData != null && (this.st == null /* || !st.hasBlock() */ || this.st.isAlive() && this.subManagement)) {
						FlowDiagram fd = (FlowDiagram) this.scenarioData;
						this.blockLaunchableListener = new BlockLaunchableListener();
						for (Block block : fd.getManagedBlocks(b -> true))
							block.addPropertyChangeListener(this.blockLaunchableListener);
						if (this.runningMode == RunningMode.MULTI_CORE)
							this.diagramScheduler = new MultiCoreDiagramScheduler(fd);
						else if (this.runningMode == RunningMode.MONO_CORE) {
							CountDownLatch startLock = new CountDownLatch(1);
							DiagramScheduler ds = new MonoCoreDiagramScheduler(fd, startLock, this.st == null ? toString() : this.st.getName());
							this.schedulerInterface.setStartLock(startLock);
							this.diagramScheduler = ds;
						} else
							this.diagramScheduler = new NoThreadedDiagramScheduler(fd);
						fd.setRunTaskLaterInSubDiagramFunction(this.st == null ? Runnable::run : this.st::runLater);
					}
				} else if (state == SchedulerState.STARTED) {
					if (this.diagramScheduler != null)
						this.diagramScheduler.start();
				} else if (state == SchedulerState.SUSPENDED) {
					if (this.diagramScheduler != null)
						this.diagramScheduler.pause();
				} else if (state == SchedulerState.UNSUSPENDED) {
					if (this.diagramScheduler != null)
						this.diagramScheduler.resume();
				} else if (state == SchedulerState.PRESTOP) {
					if (this.diagramScheduler != null)
						this.diagramScheduler.preStop();
				} else if (state == SchedulerState.STOPPING)
					if (this.diagramScheduler != null && (this.st == null/* || !st.hasBlock() */ || this.subManagement)) {
						FlowDiagram fd = (FlowDiagram) this.scenarioData;
						for (Block block : fd.getManagedBlocks(b -> true))
							block.removePropertyChangeListener(this.blockLaunchableListener);
						this.blockLaunchableListener = null;
						if (this.st == null/* || !st.hasBlock() */) {
							this.diagramScheduler.stop();
							((FlowDiagram) this.scenarioData).setRunTaskLaterInSubDiagramFunction(null);
							this.diagramScheduler = null;
						}
					}
			}
	}

	private void unRegisterVisuableSchedulable(Block block) {
		this.animationTimerConsumer.unRegister(block.getRemoteOperator() != null ? (VisuableSchedulableRemoteOperator) block.getRemoteOperator() : (VisuableSchedulable) block.operator);
	}

	private void updateDiagramInfo(FlowDiagram source, Object element, ModificationType modificationType, boolean subChange) {
		// Log.info(source + " " + element + " " + modificationType + " " + this + " " + subChange);
		FlowDiagram fd = (FlowDiagram) this.scenarioData;
		if (fd == null)
			return;
		boolean schedulingChange = false;
		if (modificationType != ModificationType.SCHEDULINGCHANGE) {
			ArrayList<Block> newSchedulableBlocksWithSomeThingToPlan = new ArrayList<>();
			this.schedulableBlocksWithoutSomeThingsToPlan.clear();
			for (Block block : fd.getBlocks()) {
				Scenario scenario;
				if (block.operator instanceof ScenarioPlayer && (scenario = ((ScenarioPlayer) block.operator).getScenario()) != null)
					if (scenario != null && scenario.hasSomeThingsToPlan())
						newSchedulableBlocksWithSomeThingToPlan.add(block);
					else
						this.schedulableBlocksWithoutSomeThingsToPlan.add(block);
				schedulingChange = !this.schedulableBlocksWithSomeThingToPlan.equals(newSchedulableBlocksWithSomeThingToPlan);
			}
			if (schedulingChange)
				// Log.info("scheduling change: " + this.schedulableBlocksWithSomeThingToPlan + " to: " + newSchedulableBlocksWithSomeThingToPlan);
				this.schedulableBlocksWithSomeThingToPlan = newSchedulableBlocksWithSomeThingToPlan;
		} else
			schedulingChange = true;
		if (schedulingChange)
			fireScheduleChanged();
		long newNbGap = 0;
		for (Block block : this.schedulableBlocksWithSomeThingToPlan)
			if (block.operator instanceof LocalScenario) {
				long nbGapOp = ((LocalScenario) block.operator).getEndTime();
				if (nbGapOp > newNbGap)
					newNbGap = nbGapOp;
			}
		if (newNbGap != this.nbGap)
			this.nbGap = newNbGap;
		if (!subChange && this.animationTimerConsumer != null)
			if (element instanceof Link)
				if (modificationType == ModificationType.NEW) {
					Input input = ((Link) element).getInput();
					if (input instanceof BlockInput && ((BlockInput) input).getBlock().operator instanceof VisuableSchedulable)
						registerVisuableSchedulable(((BlockInput) input).getBlock());
				} else if (modificationType == ModificationType.DELETE) {
					Input input = ((Link) element).getInput();
					if (input instanceof BlockInput && ((BlockInput) input).getBlock().operator instanceof VisuableSchedulable) {
						Block block = ((BlockInput) input).getBlock();
						boolean needToUnregister = true;
						for (BlockInput bi : block.getInputs())
							if (bi.getLink() != null) {
								needToUnregister = false;
								break;
							}
						if (needToUnregister)
							unRegisterVisuableSchedulable(((BlockInput) input).getBlock());
					}
				}
		if (element instanceof Block && this.diagramScheduler != null && (!subChange || ((FlowDiagram) this.scenarioData).isManagedFlowDiagram(source))) {
			Block block = (Block) element;
			if (modificationType == ModificationType.NEW) {
				block.addPropertyChangeListener(this.blockLaunchableListener);
				addOrRemoveBlockToDiagramScheduler(block, true);
			} else if (modificationType == ModificationType.DELETE) {
				block.removePropertyChangeListener(this.blockLaunchableListener);
				addOrRemoveBlockToDiagramScheduler(block, false);
			}
		}
	}

	@Override
	public void startStopChanged() {
		long min = Long.MAX_VALUE;
		long max = Long.MIN_VALUE;
		for (Block sb : this.schedulableBlocksWithSomeThingToPlan) {
			Object op = sb.operator;
			if (op instanceof ScenarioPlayer) {
				Scenario scenario = ((ScenarioPlayer) op).getScenario();
				Date std = scenario.getStartTime();
				long beginningTime = std != null ? std.getTime() : scenario.getBeginningTime();
				if (beginningTime < min)
					min = beginningTime;
				std = scenario.getStopTime();
				long endTime = std != null ? std.getTime() : scenario.getEndTime();
				if (endTime > max)
					max = endTime;
			}
		}
		if (this.schedulerInterface != null) {
			this.schedulerInterface.setStartTime(min);
			this.schedulerInterface.setStopTime(max);
		}
		fireStartStopTimeChanged();
	}

	@Override
	public Scenario[] getAdditionalScenario() {
		return this.schedulableBlocksWithSomeThingToPlan.stream().map(b -> ((ScenarioPlayer) b.operator).getScenario()).filter(Objects::nonNull).toArray(Scenario[]::new);
	}

	@Override
	public Date getStartTime() {
		return null;
	}

	@Override
	public Date getStopTime() {
		return null;
	}

	@Override
	public double getPeriod() {
		return this.periodFromSubScenario;
	}

	@Override
	public boolean canRecord() {
		return true;
	}

	@PropertyInfo(index = 3, savable = false, info = "Activates or deactivates all recorders included in the flow diagram")
	@Override
	public boolean isRecording() {
		FlowDiagram fd = (FlowDiagram) getScenarioData();
		return fd == null ? false : fd.isRecording();
	}

	@Override
	public void setRecording(boolean recording) {
		FlowDiagram sd = (FlowDiagram) getScenarioData();
		if (sd != null) {
			boolean oldValue = sd.isRecording();
			sd.setRecording(recording);
			this.pcs.firePropertyChange("recording", oldValue, recording);
		}
	}

	@Override
	public void recordingPropertyChanged(boolean recording) {
		fireRecordingChanged(recording);
	}

	@Override
	public void loaded(Module module) {}

	@Override
	public void unloaded(Module module) {}

	@Override
	public Runnable modified(Module module) { // Save old diagrams info and reload them after the reload of the module
		FlowDiagram fd = (FlowDiagram) this.scenarioData;
		ArrayList<Element> blockElements = new ArrayList<>();
		ArrayList<Element> linkElements = new ArrayList<>();
		if (fd != null) {
			List<Block> blocks = fd.getBlocks();
			Block[] blocksToRemove = fd.getBlocks().stream().filter(block -> block.operator.getClass().getModule().equals(module)).toArray(Block[]::new);
			for (Block blockToRemove : blocksToRemove) {
				// Log.info("Reload operator of block: " + blockToRemove.getName());
				Document document;
				try {
					document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
					// Save Block properties
					blockElements.add(getElementFromBlock(blockToRemove, document, 0));
					// Save Links properties
					int linkIndex = 0;
					for (Block block : blocks)
						for (BlockInput input : block.getInputs()) {
							Link link = input.getLink();
							if (link != null && link.getOutput() instanceof BlockOutput && ((BlockOutput) link.getOutput()).getBlock() == blockToRemove)
								// Log.info("Save output link: " + link);
								linkElements.add(getElementFromLink(link, document, linkIndex++));
						}
					for (BlockInput input : blockToRemove.getInputs()) {
						Link link = input.getLink();
						if (link != null)
							// Log.info("Save input link: " + link);
							linkElements.add(getElementFromLink(link, document, linkIndex++));
					}
				} catch (ParserConfigurationException e) {
					e.printStackTrace();
				}
			}
		}
		return () -> {
			ArrayList<Tuple<Block, String>> missedInputs = new ArrayList<>();
			HashMap<Block, ArrayList<VarArgsInputChangeListener>> varArgsInputChangeListenerBlockMap = new HashMap<>();
			// Reload block properties
			for (Element blockElement : blockElements) {
				Block block = getBlockFromElement(blockElement, missedInputs, varArgsInputChangeListenerBlockMap);
				if (block != null)
					fd.addBlock(block);
			}
			// Reload links properties
			ArrayList<String[]> missedLink = new ArrayList<>();
			for (Element linkElement : linkElements)
				createLinkFromElement(fd, linkElement, missedLink);
			setVarArgsLinks(fd, missedLink, varArgsInputChangeListenerBlockMap, missedInputs);
		};
	}

	@Override
	public void onSaveRequested(boolean performed) {
		if (this.inFileRunningMode != this.runningMode || this.inFileTriggerMode != this.triggerMode)
			synchronized (this) {
				RunningMode rm = this.runningMode;
				TriggerMode tm = this.triggerMode;
				boolean needToLoad = this.scenarioData == null;
				if (needToLoad) {
					loadImmediately(getFile());
					this.runningMode = rm;
					this.triggerMode = tm;
				}
				File file = getFile();
				if (file != null && file.exists())
					saveDiagram(getFile());
				this.inFileRunningMode = this.runningMode;
				this.inFileTriggerMode = this.triggerMode;
			}
	}

	public boolean isSubManagement() {
		return this.subManagement;
	}

	public void setSubManagement(boolean subManagement) {
		boolean oldSubManagement = this.subManagement;
		this.subManagement = subManagement;
		if (isManagedScenario()) {
			FlowDiagram sd = (FlowDiagram) getScenarioData();
			if (sd != null)
				sd.setSubManagement(this.subManagement);
			setEnable();
		}
		this.pcs.firePropertyChange("subManagement", oldSubManagement, this.subManagement);
	}

	@Override
	public void setVisible() {
		super.setVisible();
		fireSetPropertyVisible(this, "recording", canRecord());
		fireSetPropertyVisible(this, "subManagement", isManagedScenario());
		fireSetPropertyVisible(this, "triggerMode", !isManagedScenario());
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "runningMode", !isManagedScenario() || this.subManagement);
	}

	public void setFlowDiagramGraphConsistencyMethods(BiFunction<FlowDiagram, Object, Block> addBlockValidator, BiPredicate<FlowDiagram, Object> operatorRemoved, Runnable updateWarningStatus,
			Runnable resetIndex, Consumer<Runnable> runIOTaskLater) {
		this.addBlockValidator = addBlockValidator;
		this.operatorRemoved = operatorRemoved;
		this.updateParentDiagramWarningStatus = updateWarningStatus;
		this.resetIndex = resetIndex;
		this.runIOTaskLater = runIOTaskLater;
		if (this.scenarioData != null)
			((FlowDiagram) this.scenarioData).setFlowDiagramGraphConsistencyMethods(addBlockValidator, operatorRemoved, this::updateWarningStatus, () -> this.schedulerInterface.stop(), resetIndex,
					runIOTaskLater);
	}

}