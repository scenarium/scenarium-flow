/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow;

public class InputDescriptor {
	public final String inputIndex;
	public final String connectedOutputName;
	public final Class<?> connectedOutputType;

	public InputDescriptor(String inputIndex, String connectedOutputName, Class<?> connectedOutputType) {
		this.inputIndex = inputIndex;
		this.connectedOutputName = connectedOutputName;
		this.connectedOutputType = connectedOutputType;
	}
}
