/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import javax.swing.event.EventListenerList;

import io.beanmanager.BeanManager;
import io.beanmanager.tools.TriConsumer;
import io.scenarium.core.filemanager.scenariomanager.RecordingListener;
import io.scenarium.core.filemanager.scenariomanager.ScheduleChangeListener;
import io.scenarium.core.filemanager.scenariomanager.SourceChangeListener;
import io.scenarium.core.filemanager.scenariomanager.StructChangeListener;
import io.scenarium.flow.operator.player.ScenarioPlayer;
import io.scenarium.flow.operator.player.SubFlowDiagram;
import io.scenarium.flow.scenario.DataFlowDiagram;

public class FlowDiagram implements Serializable, InputLinkChangeListener, SourceChangeListener, CpuUsageMeasurable, IOComponent, IONameChangeListener, PropertyChangeListener, ScheduleChangeListener,
		FlowDiagramChangeListener {
	private static final long serialVersionUID = 1L;
	private List<Block> blocks = Collections.unmodifiableList(new ArrayList<>());
	private List<FlowDiagramInput> outputs = Collections.unmodifiableList(new ArrayList<>());
	private List<FlowDiagramOutput> inputs = Collections.unmodifiableList(new ArrayList<>());
	private List<FlowDiagramOutput> triggerableOutputs = Collections.unmodifiableList(new ArrayList<>());
	private final EventListenerList listeners = new EventListenerList();
	private boolean managingCpuUsage;
	private float cpuUsage = -2;
	private BiConsumer<List<Block>, Runnable> timeOutTask;
	private final HashMap<Block, StructChangeListener> structChangeListenerMap = new HashMap<>();
	// private HashMap<Block, NameChangeListener> nameChangeListenerMap = new HashMap<>();
	private TriConsumer<Object[], long[], TriConsumer<Object[], long[], BiConsumer<Object[], long[]>>> triggerOutputFunction;
	private Consumer<Runnable> runIOTaskLaterFunction;
	// private Consumer<Runnable> offerTaskFunction;
	// private Supplier<Boolean> holdLockFunction;
	private Consumer<Runnable> runTaskLaterInSubDiagramFunction;
	private boolean isSubManagement;
	private Object[] outputBuff;
	private long[] outputBuffTs;
	private IOLinks[] index;
	private final HashMap<FlowDiagramInput, Integer> outputMap = new HashMap<>();
	private Runnable preschedulingChange;
	private Runnable resetParentIndex;

	private Supplier<List<BlockOutput>> outputSupplier;
	private BiFunction<FlowDiagram, Object, Block> addBlockValidator;
	private BiPredicate<FlowDiagram, Object> operatorRemoved;
	private Runnable updateWarningStatus;

	public FlowDiagram() {}

	public boolean addBlock(Block block) {
		Object operator = block.operator;
		Block blockWithSameOperator = getNonConflictingBlockInCurrentOrSubFlowDiagram(operator, null);
		if (blockWithSameOperator != null)
			for (ArrayList<Block> blockPath : getBlocksPath(blockWithSameOperator, false))
				if (blockPath.size() == 0)
					// A block with the same operator exists in the current diagram, add operation cancelled
					return false;
				else {
					// A block with the same operator exists in a sub diagram. Set these block in conflict
					blockWithSameOperator.setConflict(true);
					blockPath.get(blockPath.size() - 1).setConflict(false);
					// blockPath.forEach(b -> ((FlowDiagram) ((DataFlowDiagram) b.operator).getScenarioData()).updateWarningStatus());
				}
		// Set block in conflict if a parent flow diagram have a block with the same operator
		Block notInConflictBlock = getNonConflictingBlockInParentFlowDiagram(operator);
		if (notInConflictBlock != null) {
			block.setConflict(true);
			notInConflictBlock.setConflict(false);
		} else
			block.setConflict(false);
		ArrayList<Block> blocks = new ArrayList<>(this.blocks);
		blocks.add(block);
		this.blocks = blocks;
		if (operator instanceof ScenarioPlayer) {
			((ScenarioPlayer) operator).addSourceChangeListener(this);
			if (operator instanceof SubFlowDiagram) {
				DataFlowDiagram subDFD = ((SubFlowDiagram) operator).getSubDiagram();
				subDFD.setFlowDiagramGraphConsistencyMethods((flowDiagramSource, operatorToAdd) -> {
					// Search in contained blocks and sub blocks
					Block existingBlock = searchBlock(flowDiagramSource, FlowDiagram.this, operatorToAdd, false);
					return existingBlock != null ? existingBlock : getNonConflictingBlockInParentFlowDiagram(operatorToAdd); // Search in father flowDiagram if exists
				}, (flowDiagramSource, operatorRemoved) -> {
					Block existingBlock = searchBlock(flowDiagramSource, FlowDiagram.this, operatorRemoved, true);
					if (existingBlock != null) {
						existingBlock.setConflict(false);
						return true;
					}
					return FlowDiagram.this.operatorRemoved == null ? false : FlowDiagram.this.operatorRemoved.test(FlowDiagram.this, operatorRemoved);
				}, this::updateWarningStatus, () -> this.index = null, this::runIOTaskLater);
				subDFD.addScheduleChangeListener(this);
				// if (!isSubManagement())
				subDFD.addFlowDiagramChangeListener(this);
				// Sets in conflict mode all blocks of the sub flow diagram with operators in conflict with existing blocks
				setPotentialConflictsInSubFlowDiagram(subDFD);
				subDFD.updateWarningStatus();
			}
		}
		block.getInputs().forEach(input -> input.addLinkChangeListener(this));
		StructChangeListener scl = () -> {
			block.getInputs().forEach(input -> input.addLinkChangeListenerIfNotPresent(this));
			fireDiagramChanged(this, block, "struct", ModificationType.CHANGE);
			for (Block otherBlock : blocks) {
				List<BlockInput> oinputs = otherBlock.getInputs();
				for (int i = 0; i < oinputs.size(); i++) {
					BlockInput input = oinputs.get(i);
					Link link = input.getLink();
					if (link != null && link.getOutput() instanceof BlockOutput && ((BlockOutput) link.getOutput()).getBlock() == otherBlock) {
						List<BlockOutput> oOutput = otherBlock.getOutputs();
						int j = 0;
						for (; j < oOutput.size(); j++)
							if (oOutput.get(j).getName().equals(link.getOutput().getName())) { // si même nom, je recrer lien
								// if(link.getOutput() != blockChanged.getOutput(j)) //Ajout du 23/08/07 pas besoin de remettre le lien si c'est le même...
								input.setLink(new Link(link.getInput(), oOutput.get(j))); // mon block n'a peut être pas encore la patte
								break;
							}
						if (j == oOutput.size()) {
							input.setLink(null); // et si je décalle les var args, je rate une patte
							i = -1;
						}
					}
				}
			}
			for (FlowDiagramInput fdi : this.outputs) {
				Link link = fdi.getLink();
				if (link != null && link.getOutput() instanceof BlockOutput && ((BlockOutput) link.getOutput()).getBlock() == block) {
					List<BlockOutput> outputs = block.getOutputs();
					int j = 0;
					for (; j < outputs.size(); j++)
						if (outputs.get(j).getName().equals(link.getOutput().getName())) { // si même nom, je recrer lien
							fdi.setLink(new Link(link.getInput(), outputs.get(j))); // mon block n'a peut être pas encore la patte
							break;
						}
					if (j == outputs.size())
						fdi.setLink(null);
				}
			}
		};
		this.structChangeListenerMap.put(block, scl);
		block.addStructChangeListener(scl);
		block.addPropertyChangeListener(this);
		if (block.isConflict())
			this.updateWarningStatus.run();
		if (operator instanceof AbstractRecorder) {
			AbstractRecorder ar = (AbstractRecorder) operator;
			ar.addPropertyChangeListener(this);
			boolean isPreviouslyRecording = this.blocks.stream().filter(b -> b != block && b.getOperator() instanceof AbstractRecorder)
					.anyMatch(b -> ((AbstractRecorder) b.getOperator()).isRecording());
			if (ar.isRecording() && !isPreviouslyRecording)
				fireRecordingChange(true);
		}
		fireDiagramChanged(this, block, null, ModificationType.NEW);
		return true;
	}

	private void updateWarningStatus() {
		if (this.updateWarningStatus != null)
			this.updateWarningStatus.run();
	}

	private Block searchBlock(FlowDiagram flowDiagramSource, FlowDiagram flowDiagram, Object operatorToAdd, boolean inClonflict) {
		for (Block b : flowDiagram.getBlocks()) {
			Object subOperator = b.operator;
			if (subOperator instanceof SubFlowDiagram) {
				FlowDiagram fd = (FlowDiagram) ((SubFlowDiagram) subOperator).getSubDiagram().getScenarioData();
				if (fd == flowDiagramSource)
					continue;
				if (fd != null) {
					Block res = searchBlock(flowDiagram, fd, operatorToAdd, inClonflict);
					if (res != null)
						return res;
				}
			}
			if (subOperator != flowDiagramSource && b.isConflict() == inClonflict && subOperator == operatorToAdd)
				return b;
		}
		return null;
	}

	public void setPotentialConflictsInSubFlowDiagram(DataFlowDiagram dfd) {
		FlowDiagram fd = (FlowDiagram) dfd.getScenarioData();
		if (fd != null)
			for (Block subBlock : fd.getBlocks()) {
				Object subBlockOp = subBlock.operator;
				// Search if block already exists and is not our block
				Block notInConflictBlock = getBlock(subBlockOp, subBlock);
				if (notInConflictBlock != null) {
					subBlock.setConflict(true);
					notInConflictBlock.setConflict(false);
				}
				if (subBlockOp instanceof SubFlowDiagram)
					setPotentialConflictsInSubFlowDiagram(((SubFlowDiagram) subBlockOp).getSubDiagram());
			}
	}

	public ArrayList<Block> getBlocksInConflict() {
		ArrayList<Block> blocks = new ArrayList<>();
		populateBlocksInConflict(this, blocks);
		return blocks;
	}

	private void populateBlocksInConflict(FlowDiagram flowDiagram, ArrayList<Block> blocks) {
		for (Block b : flowDiagram.getBlocks()) {
			if (b.isConflict())
				blocks.add(b);
			Object subOperator = b.operator;
			if (subOperator instanceof SubFlowDiagram) {
				FlowDiagram fd = (FlowDiagram) ((SubFlowDiagram) subOperator).getSubDiagram().getScenarioData();
				if (fd != null)
					populateBlocksInConflict(fd, blocks);
			}
		}
	}

	public boolean removeBlock(Block block/* , boolean needToKillOperator */) {
		ArrayList<Block> blocks = new ArrayList<>(this.blocks);
		if (!blocks.remove(block))
			return false;
		this.blocks = blocks;
		block.getInputs().forEach(input -> input.removeLinkChangeListener(this));
		Object operator = block.operator;
		block.removePropertyChangeListener(this);
		if (operator instanceof EvolvedOperator) {
			// ((EvolvedOperator) operator).setStructChangeCallBack(this.structChangeListenerMap.remove(block));
			if (operator instanceof ScenarioPlayer) {
				// ((Player) operator).dispose();
				((ScenarioPlayer) operator).removeSourceChangeListener(this);
				if (operator instanceof SubFlowDiagram) {
					DataFlowDiagram subDiagram = ((SubFlowDiagram) operator).getSubDiagram();
					subDiagram.setFlowDiagramGraphConsistencyMethods(null, null, null, null, null);
					subDiagram.removeScheduleChangeListener(this);
					subDiagram.removeFlowDiagramChangeListener(this);
				}
			}
			if (operator instanceof AbstractRecorder) {
				AbstractRecorder ar = (AbstractRecorder) operator;
				ar.removePropertyChangeListener(this);
				if (ar.isRecording() && !isRecording())
					fireRecordingChange(false);
			}
		} else
			block.setRunLaterFunction(null);
		block.removeStructChangeListener(this.structChangeListenerMap.remove(block));
		block.dispose();
		fireDiagramChanged(this, block, null, ModificationType.DELETE);
		if (!block.isConflict()) {
			// Search the first conflicting block to set it as no conflict
			boolean blockSetAsNotInConflit = false;
			if (this.operatorRemoved != null)
				// Search in parent flow diagram
				blockSetAsNotInConflit = this.operatorRemoved.test(this, operator);
			if (!blockSetAsNotInConflit)
				// Search in sub flow diagram
				for (Block b : this.blocks)
					if (b.operator instanceof SubFlowDiagram) {
						FlowDiagram fd = (FlowDiagram) ((SubFlowDiagram) b.operator).getSubDiagram().getScenarioData();
						if (fd != null && tryToSetBlockAsNotInConflit(fd, operator))
							break;
					}
		} else
			this.updateWarningStatus.run();
		return true;
	}

	private boolean tryToSetBlockAsNotInConflit(FlowDiagram flowDiagram, Object removedOperator) {
		for (Block block : flowDiagram.getBlocks()) {
			Object operator = block.operator;
			if (operator == removedOperator) {
				block.setConflict(false);
				return true;
			}
			if (operator instanceof DataFlowDiagram) {
				FlowDiagram subFlowDiagram = (FlowDiagram) ((DataFlowDiagram) operator).getScenarioData();
				if (subFlowDiagram != null && tryToSetBlockAsNotInConflit(subFlowDiagram, removedOperator))
					return true;
			}
		}
		return false;
	}

	public void addFlowDiagramChangeListener(FlowDiagramChangeListener listener) {
		this.listeners.add(FlowDiagramChangeListener.class, listener);
	}

	public void addOutput(FlowDiagramInput output) {
		runIOTaskLater(() -> {
			ArrayList<FlowDiagramInput> outputs = new ArrayList<>(this.outputs);
			outputs.add(output);
			setInputs(outputs);
			output.addLinkChangeListener(this);
			output.addIONameChangeListener(this);
			fireDiagramChanged(this, output, null, ModificationType.NEW);
		});
	}

	private void setInputs(ArrayList<FlowDiagramInput> inputs) {
		this.outputs = inputs;
	}

	private void updateOutputBuffer() {
		int nbInput = 0;
		for (FlowDiagramInput fdi : this.outputs)
			if (fdi.getType() != null)
				nbInput++;
		if (this.outputBuff == null || this.outputBuff.length != nbInput) {
			this.outputBuff = new Object[nbInput];
			this.outputBuffTs = new long[nbInput];
		}
		this.outputMap.clear();
		int index = 0;
		for (int i = 0; i < this.outputs.size(); i++) {
			FlowDiagramInput input = this.outputs.get(i);
			if (input.getType() != null)
				this.outputMap.put(input, index++);
		}
	}

	public void addManagingCpuUsage(ManagingCpuUsageListener listener) {
		this.listeners.add(ManagingCpuUsageListener.class, listener);
	}

	public void addInput(FlowDiagramOutput output) {
		runIOTaskLater(() -> {
			ArrayList<FlowDiagramOutput> outputs = new ArrayList<>(this.inputs);
			outputs.add(output);
			this.inputs = outputs;
			updateTriggerableOutputs();
			output.addIONameChangeListener(this);
			fireDiagramChanged(this, output, null, ModificationType.NEW);
		});

	}

	public void beanRename(Object bean) {
		for (Block block : this.blocks)
			if (block.operator == bean) {
				fireDiagramChanged(this, block, "name", ModificationType.CHANGE);
				break;
			}
	}

	public void blockModeChanged(Block block, ModificationType modificationType) {
		fireDiagramChanged(this, block, null, modificationType);
	}

	@Override
	public boolean canTriggerOrBeTriggered() {
		return true;
	}

	@Override
	public Object clone() {
		return this;
	}

	public Block getBlock(Object operator) {
		return getBlock(operator, null);
	}

	public Block getBlock(Object operator, Block filteredBlock) {
		Block block = getNonConflictingBlockInCurrentOrSubFlowDiagram(operator, filteredBlock);
		return block != null ? block : getNonConflictingBlockInParentFlowDiagram(operator); // Search in father flowDiagram if exists
	}

	// Search in contained blocks and in sub FlowDiagram
	private Block getNonConflictingBlockInCurrentOrSubFlowDiagram(Object operator, Block filteredBlock) {
		for (Block block : this.blocks)
			if (!block.isConflict()) {
				Object op = block.operator;
				if (op == operator && filteredBlock != block)
					return block;
				if (op instanceof SubFlowDiagram) {
					FlowDiagram subFlowDiagram = (FlowDiagram) ((SubFlowDiagram) op).getSubDiagram().getScenarioData();
					if (subFlowDiagram != null) {
						Block subBlock = subFlowDiagram.getNonConflictingBlockInCurrentOrSubFlowDiagram(operator, filteredBlock);
						if (subBlock != null)
							return subBlock;
					}
				}
			}
		return null;
	}

	private Block getNonConflictingBlockInParentFlowDiagram(Object operator) {
		return this.addBlockValidator == null ? null : this.addBlockValidator.apply(this, operator); // Search in father flowDiagram if exists
	}

	public ArrayList<ArrayList<Block>> getBlocksPath(Block block, boolean onlyNonConflictingBlock) {
		ArrayList<ArrayList<Block>> res = new ArrayList<>();
		getBlockPath(block.operator, onlyNonConflictingBlock, blockPath -> res.add(blockPath));
		return res;
	}

	private void getBlockPath(Object operator, boolean onlyNonConflictingBlock, Consumer<ArrayList<Block>> mc) {
		List<Block> blocks = this.blocks;
		for (Block block : blocks) {
			Object op = block.operator;
			if (op == operator && (!onlyNonConflictingBlock || !block.isConflict()))
				mc.accept(new ArrayList<>());
			if (op instanceof SubFlowDiagram) {
				FlowDiagram subFlowDiagram = (FlowDiagram) ((SubFlowDiagram) op).getSubDiagram().getScenarioData();
				subFlowDiagram.getBlockPath(operator, onlyNonConflictingBlock, blockPath -> {
					blockPath.add(0, block);
					mc.accept(blockPath);
				});
			}
		}
	}

	public void death() {
		while (!this.blocks.isEmpty())
			removeBlock(this.blocks.get(0));
		this.blocks = new ArrayList<>();
	}

	private void fireDiagramChanged(FlowDiagram source, Object element, String property, ModificationType modificationType) {
		if (modificationType != ModificationType.SCHEDULINGCHANGE)
			resetIndex();
		this.index = null;
		for (FlowDiagramChangeListener listener : this.listeners.getListeners(FlowDiagramChangeListener.class))
			listener.flowDiagramChanged(source, element, property, modificationType);
	}

	private void resetIndex() {
		this.index = null;
		if (this.resetParentIndex != null)
			this.resetParentIndex.run();
		for (Block block : this.blocks) {
			block.resetIndex();
			if (block.operator instanceof SubFlowDiagram) {
				FlowDiagram sd = (FlowDiagram) ((SubFlowDiagram) block.operator).getSubDiagram().getScenarioData();
				if (sd != null)
					sd.resetIndex();
			}
		}
	}

	public void fireManagingCpuUsageChanged() {
		for (ManagingCpuUsageListener listener : this.listeners.getListeners(ManagingCpuUsageListener.class))
			listener.managingCpuChanged(this.managingCpuUsage);
	}

	public ArrayList<Link> getAllLinks() {
		ArrayList<Link> links = new ArrayList<>();
		for (Block block : this.blocks)
			for (BlockInput input : block.getInputs()) {
				Link link = input.getLink();
				if (link != null)
					links.add(link);
			}
		for (FlowDiagramInput input : this.outputs) {
			Link link = input.getLink();
			if (link != null)
				links.add(link);
		}
		return links;
	}

	public List<Block> getBlocks() {
		return this.blocks;
	}

	public Block getBlock(String name) {
		for (Block block : this.blocks)
			if (block.getName().equals(name))
				return block;
		return null;
	}

	public List<Block> getManagedBlocks(Predicate<Block> condition) {
		ArrayList<Block> nonConflictingBlocks = new ArrayList<>();
		populateManagedBlocks(nonConflictingBlocks, condition, this);
		return nonConflictingBlocks;
	}

	private void populateManagedBlocks(ArrayList<Block> nonConflictingBlocks, Predicate<Block> condition, FlowDiagram fd) {
		for (Block block : fd.getBlocks())
			if (condition.test(block)) {
				if (block.operator instanceof SubFlowDiagram) {
					DataFlowDiagram dfd = ((SubFlowDiagram) block.operator).getSubDiagram();
					if (!dfd.isSubManagement()) {
						FlowDiagram subFd = (FlowDiagram) dfd.getScenarioData();
						if (subFd != null)
							populateManagedBlocks(nonConflictingBlocks, condition, subFd);
					}
				}
				nonConflictingBlocks.add(block);
			}
	}

	public boolean isManagedFlowDiagram(FlowDiagram fd) {
		for (Block block : getBlocks())
			if (block.operator instanceof SubFlowDiagram) {
				DataFlowDiagram dfd = ((SubFlowDiagram) block.operator).getSubDiagram();
				if (!dfd.isSubManagement()) {
					FlowDiagram subFd = (FlowDiagram) dfd.getScenarioData();
					if (subFd != null)
						if (subFd == fd)
							return true;
						else if (!subFd.isSubManagement())
							if (subFd.isManagedFlowDiagram(fd))
								return true;
				}
			}
		return false;
	}

	public float getCpuUsage() {
		return this.cpuUsage;
	}

	@Override
	public IOLinks[] getIndex() { // Double-checked Locking without Volatile. It's not a problem if the index is created several times.
		this.index = null;
		IOLinks[] index = this.index;
		if (index == null)
			synchronized (this) {
				index = this.index;
				if (index == null) {
					index = buildIndex(0);
					this.index = index;
				}
			}
		return index;
	}

	public FlowDiagramInput getInput(int index) {
		return this.outputs.get(index);
	}

	@Override
	public List<FlowDiagramInput> getInputs() {
		return this.outputs;
	}

	@Override
	public Object[] getOutputBuff() {
		return this.outputBuff;
	}

	@Override
	public long[] getOutputBuffTs() {
		return this.outputBuffTs;
	}

	@Override
	public List<FlowDiagramOutput> getOutputs() {
		return this.inputs;
	}

	public int getOutputIndex(FlowDiagramInput input) {
		return this.outputMap.get(input);
	}

	@Override
	public List<? extends Output> getTriggerableOutputs() {
		return this.triggerableOutputs;
	}

	public BiConsumer<List<Block>, Runnable> getTimeOutTask() {
		return this.timeOutTask;
	}

	public boolean isManagingCpuUsage() {
		return this.managingCpuUsage;
	}

	@Override
	public boolean isReady() {
		return true;
	}

	@Override
	public void linkChange(Link link, ModificationType modificationType) {
		runIOTaskLater(() -> {
			if (link.getInput() instanceof FlowDiagramInput)
				updateOutputBuffer();
			if (link.getOutput() instanceof FlowDiagramOutput)
				updateTriggerableOutputs();
			fireDiagramChanged(this, link, null, modificationType);
		});
	}

	@Override
	public void nameChanged(IO io) {
		fireDiagramChanged(this, io, "name", ModificationType.CHANGE);
	}

	public ArrayList<Object> removeElements(ArrayList<Object> selectedElements/* , boolean needToKillOperator */, boolean purgeBean) {
		// Log.info("selectedElements: " + selectedElements);
		ArrayList<Object> removedElements = new ArrayList<>();
		for (Object selectedElement : selectedElements)
			if (selectedElement instanceof Block) {
				Block selectedBlock = (Block) selectedElement;
				if (selectedBlock.operator instanceof ScenarioPlayer && this.preschedulingChange != null && ((ScenarioPlayer) selectedBlock.operator).hasSomeThingsToPlan())
					this.preschedulingChange.run(); // Need to stop the diagram before removing a schedulable block because it cannot stop itself in a runLater function
				runIOTaskLater(() -> {
					for (Block block : this.blocks) {
						boolean hasRemovedLink = true;
						while (hasRemovedLink) {
							hasRemovedLink = false;
							for (BlockInput input : block.getInputs()) {
								Link link = input.getLink();
								if (link != null && link.getOutput() instanceof BlockOutput && ((BlockOutput) link.getOutput()).getBlock() == selectedElement) {
									input.setLink(null);
									hasRemovedLink = true;
									break;
								}
							}
						}
					}
					boolean hasRemovedLink = false;
					do {
						hasRemovedLink = false;
						for (BlockInput input : ((Block) selectedElement).getInputs())
							if (input.getLink() != null) {
								input.setLink(null); // Met à jour DFD
								hasRemovedLink = true;
								break;
							}
						if (!hasRemovedLink)
							break;
					} while (hasRemovedLink);
					for (Input input : this.outputs) {
						Link link = input.getLink();
						if (link != null && link.getOutput() instanceof BlockOutput && ((BlockOutput) link.getOutput()).getBlock() == selectedElement)
							input.setLink(null);
						// updateOutputBuffer();
					}
					if (removeBlock((Block) selectedElement/* , needToKillOperator */))
						removedElements.add(selectedElement);
					if (purgeBean)
						BeanManager.purgeBean(((Block) selectedElement).operator, true, purgeBean);
				});
			} else if (selectedElement instanceof Link)
				runIOTaskLater(() -> {
					boolean isRemoved = false;
					for (Input input : this.outputs)
						if (selectedElement == input.getLink()) {
							input.setLink(null);
							isRemoved = true;
							// updateOutputBuffer();
							break;
						}
					if (!isRemoved)
						linkRemoved: for (Block block : this.blocks)
							for (BlockInput input : block.getInputs())
								if (selectedElement == input.getLink()) {
									input.setLink(null);
									isRemoved = true;
									break linkRemoved;
								}
					if (isRemoved)
						removedElements.add(selectedElement);
				});
			else if (selectedElement instanceof FlowDiagramInput)
				runIOTaskLater(() -> {
					ArrayList<FlowDiagramInput> inputs = new ArrayList<>(this.outputs);
					if (inputs.remove(selectedElement))
						removedElements.add(selectedElement);
					setInputs(inputs);
					((FlowDiagramInput) selectedElement).setLink(null);
					((FlowDiagramInput) selectedElement).removeLinkChangeListener(this);
					((FlowDiagramInput) selectedElement).removeIONameChangeListener(this);
					fireDiagramChanged(this, selectedElement, null, ModificationType.DELETE);
				});
			else if (selectedElement instanceof FlowDiagramOutput)
				runIOTaskLater(() -> {
					boolean hasRemovedLink = false;
					do {
						hasRemovedLink = false;
						for (Block block : this.blocks)
							for (BlockInput input : block.getInputs()) {
								Link link = input.getLink();
								if (link != null && selectedElement == link.getOutput()) {
									input.setLink(null);
									hasRemovedLink = true;
									break;
								}
							}
						if (!hasRemovedLink)
							break;
					} while (hasRemovedLink);
					ArrayList<FlowDiagramOutput> outputs = new ArrayList<>(this.inputs);
					if (outputs.remove(selectedElement))
						removedElements.add(selectedElement);
					this.inputs = outputs;
					updateTriggerableOutputs();
					((FlowDiagramOutput) selectedElement).removeIONameChangeListener(this);
					fireDiagramChanged(this, selectedElement, null, ModificationType.DELETE);
				});
			else if (selectedElement instanceof BlockInput)
				runIOTaskLater(() -> {
					BlockInput bi = (BlockInput) selectedElement;
					if (bi.block.operator instanceof EvolvedOperator)
						if (((EvolvedOperator) bi.block.operator).removePropertyAsInput(bi.getName()))
							removedElements.add(selectedElement);
						else if (bi.block.removePropertyAsInput(bi.getName()))
							removedElements.add(selectedElement);
				});
		return removedElements;
	}

	private void updateTriggerableOutputs() {
		ArrayList<FlowDiagramOutput> to = new ArrayList<>();
		for (FlowDiagramOutput fdo : this.inputs)
			if (fdo.getType() != null)
				to.add(fdo);
		this.triggerableOutputs = Collections.unmodifiableList(to);
	}

	// TODO diagramDrawerFx does not remove his listener, we will have event after the reload.
	public void removeFlowDiagramChangeListener(FlowDiagramChangeListener listener) {
		// It do it only if setDrawableElement or on death, BUT it is too late....
		this.listeners.remove(FlowDiagramChangeListener.class, listener);
	}

	public void removeManagingCpuUsageListener(ManagingCpuUsageListener listener) {
		this.listeners.remove(ManagingCpuUsageListener.class, listener);
	}

	@Override
	public void setCpuUsage(float cpuUsage) {
		if (cpuUsage > 1)
			cpuUsage = 1;
		this.cpuUsage = cpuUsage;
	}

	public void setManagingCpuUsage(boolean managingCpuUsage) {
		this.managingCpuUsage = managingCpuUsage;
		fireManagingCpuUsageChanged();
	}

	public void setStopTimeOutTask(BiConsumer<List<Block>, Runnable> timeOutTask) {
		this.timeOutTask = timeOutTask;
	}

	@Override
	public void sourceChanged(boolean planChanged) {
		fireDiagramChanged(this, null, null, ModificationType.CHANGE);
	}

	@Override
	public String toString() {
		return "Blocks: " + this.blocks.toString() + " NbInputs: " + this.outputs.size() + " NbOutputs: " + this.inputs.size();
	}

	public boolean isRecording() {
		return this.blocks.stream().filter(b -> b.getOperator() instanceof AbstractRecorder).anyMatch(b -> ((AbstractRecorder) b.getOperator()).isRecording());
	}

	public void setRecording(boolean recording) {
		this.blocks.stream().filter(b -> b.getOperator() instanceof AbstractRecorder).forEach(b -> ((AbstractRecorder) b.getOperator()).setRecording(recording));
	}

	public void addRecordingListener(RecordingListener listener) {
		this.listeners.add(RecordingListener.class, listener);
	}

	public void removeRecordingListener(RecordingListener listener) {
		this.listeners.remove(RecordingListener.class, listener);
	}

	private void fireRecordingChange(boolean recording) {
		for (RecordingListener listener : this.listeners.getListeners(RecordingListener.class))
			listener.recordingPropertyChanged(recording);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String pn = evt.getPropertyName();
		if (pn.equals("recording"))
			fireRecordingChange(isRecording());
		if (pn.equals("conflict"))
			this.updateWarningStatus.run();
	}

	public void setTriggerOutputFunction(TriConsumer<Object[], long[], TriConsumer<Object[], long[], BiConsumer<Object[], long[]>>> triggerOutputFunction) {
		this.triggerOutputFunction = triggerOutputFunction;
	}

	public void setRunIOTaskLaterFunction(Consumer<Runnable> runIOTaskLaterFunction) {
		this.runIOTaskLaterFunction = runIOTaskLaterFunction;
	}

	public void setRunTaskLaterInSubDiagramFunction(Consumer<Runnable> runLaterFunction) {
		this.runTaskLaterInSubDiagramFunction = runLaterFunction;
	}

	/** For all structure changes, it is first necessary to lock the sub-diagram to suspend triggering outputs calls that can lock the diagram and then lock the diagram.
	 * @param task the task containing structural changes for this instance of FlowDiagram */
	private void runIOTaskLater(Runnable task) {
		Consumer<Runnable> runLaterFunction = this.runIOTaskLaterFunction;
		if (runLaterFunction != null)
			runLaterFunction.accept(() -> runSubTaskLater(task));
		else
			runSubTaskLater(task);
	}

	private void runSubTaskLater(Runnable task) {
		Consumer<Runnable> runTaskLaterInSubDiagramFunction = this.runTaskLaterInSubDiagramFunction;
		if (runTaskLaterInSubDiagramFunction != null)
			runTaskLaterInSubDiagramFunction.accept(task);
		else
			// Not a subDiagram
			task.run();
	}

	/** Before triggering an output of a sub-diagram, it is necessary to lock the block containing the sub-diagram so that these outputs do not change.
	 * @param task */
	public void runFlowDiagramTriggerOutputTaskLater(Object[] outputs, long[] outputsTs, TriConsumer<Object[], long[], BiConsumer<Object[], long[]>> task) {
		if (this.triggerOutputFunction != null)
			this.triggerOutputFunction.accept(outputs, outputsTs, task);
	}

	public void setSubManagement(boolean isSubManagement) {
		if (this.isSubManagement == isSubManagement)
			return;
		this.isSubManagement = isSubManagement;
		if (!isSubManagement)
			this.cpuUsage = -2;
		// for (Block block : this.blocks)
		// if (block.operator instanceof DataFlowDiagram)
		// if (isSubManagement)
		// ((DataFlowDiagram) block.operator).removeFlowDiagramChangeListener(this);
		// else
		// ((DataFlowDiagram) block.operator).addFlowDiagramChangeListener(this);
		resetIndex();
		fireDiagramChanged(this, this, "subChange", ModificationType.CHANGE);
	}

	@Override
	public void scheduleChange() {
		fireDiagramChanged(this, null, null, ModificationType.SCHEDULINGCHANGE);
	}

	public void updateIOTypes() {
		for (FlowDiagramOutput output : this.inputs)
			if (output.getType() != null && output.inputs == null)
				output.setType(null);
		boolean inputChanged = false;
		for (FlowDiagramInput input : this.outputs)
			if (input.getType() != null && input.getLink() == null) {
				input.setType(null);
				inputChanged = true;
			}
		if (inputChanged)
			updateOutputBuffer();
	}

	Input[] getCorrespondingIntputs(String inputName) {
		for (FlowDiagramOutput output : this.inputs)
			if (output.getName().equals(inputName))
				return output.getLinkInputs();
		return null;
	}

	public boolean isSubManagement() {
		return this.isSubManagement;
	}

	public Input[] getCorrespondingOutputs(String outputName) {
		List<BlockOutput> blockOutputs = this.outputSupplier.get();
		if (blockOutputs == null)
			return null;
		for (BlockOutput output : blockOutputs)
			if (output.getName().equals(outputName))
				return output.getLinkInputs();
		throw new IllegalArgumentException("No output corresponding to: " + outputName);
	}

	public void setOutputsSupplier(EvolvedOperator operator) {
		this.outputSupplier = operator == null ? null : operator::getBlockOutputs;
	}

	@Override
	public void flowDiagramChanged(FlowDiagram source, Object element, String property, ModificationType modificationType) {
		fireDiagramChanged(source, element, "subChange", modificationType);
	}

	public void setFlowDiagramGraphConsistencyMethods(BiFunction<FlowDiagram, Object, Block> addBlockValidator, BiPredicate<FlowDiagram, Object> operatorRemoved, Runnable updateWarningStatus,
			Runnable preschedulingChange, Runnable resetParentIndex, Consumer<Runnable> runIOTaskLater) {
		this.addBlockValidator = addBlockValidator;
		this.operatorRemoved = operatorRemoved;
		this.updateWarningStatus = updateWarningStatus;
		this.preschedulingChange = preschedulingChange;
		this.resetParentIndex = resetParentIndex;
		this.runIOTaskLaterFunction = runIOTaskLater;
	}

	public void reset() {
		// setCpuUsage(-1);
		// Reset flow diagram input
		for (FlowDiagramInput fdi : this.outputs)
			if (fdi.link != null)
				fdi.link.resetConsume();
		// Reset sub flow diagram input
		for (Block block : this.blocks) {
			Object operator = block.operator;
			if (operator instanceof SubFlowDiagram) {
				FlowDiagram fd = (FlowDiagram) ((SubFlowDiagram) operator).getSubDiagram().getScenarioData();
				if (fd != null && !fd.isSubManagement())
					fd.reset();
			}
		}
	}
}
