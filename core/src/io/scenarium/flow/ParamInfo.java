/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ParamInfo {
	/**
	 * List of names of the inputs for the process operator
	 * @return list of string of the inputs (camel-case)
	 */
	String[] in() default {};

	String[] inUnit() default {};

	/**
	 * Single main output names of the process operator
	 * @apiNote Do not set return, better use of:
	 * <code>
	 * <pre>
	 * public TypeReturn getOutputNameOfYourOutput() {
	 *     return null;
	 * }
	 * public process(xxx) {
	 *     triggerOutput(...);
	 * }
	 * </pre>
	 * </code>
	 * @return Name of the output (camel-case)
	 */
	String out() default "";

	String[] outUnit() default {};
}