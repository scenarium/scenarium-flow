/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.scheduler;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.TimerTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import io.scenarium.core.tools.SchedulerUtils;
import io.scenarium.flow.CpuUsageMeasurable;

public class CPUUsageTask extends TimerTask {
	private final ThreadMXBean bean = ManagementFactory.getThreadMXBean();
	private final ArrayList<CPUUsageTaskElement> cpuUsageTaskElements = new ArrayList<>();
	private ScheduledExecutorService cpuTimer;

	private CPUUsageTask() {}

	public void close() {
		if (this.cpuTimer != null) {
			this.cpuTimer.shutdown();
			this.cpuTimer = null;
		}
	}

	@Override
	public void run() {
		for (CPUUsageTaskElement cpuUsageTaskElement : this.cpuUsageTaskElements)
			cpuUsageTaskElement.update(this.bean);
	}

	public static CPUUsageTask createCPUUsageTask() throws UnsupportedOperationException {
		ThreadMXBean bean = ManagementFactory.getThreadMXBean();
		if (!bean.isThreadCpuTimeSupported() || !bean.isCurrentThreadCpuTimeSupported() || !bean.isThreadCpuTimeEnabled())
			throw new UnsupportedOperationException("Monitoring threads is not supported by the platform");
		return new CPUUsageTask();
	}

	public static CPUUsageTask createCPUUsageTask(long idThread, CpuUsageMeasurable cum) {
		CPUUsageTask cpuUsageTask = createCPUUsageTask();
		cpuUsageTask.addTask(idThread, cum);
		return cpuUsageTask;
	}

	public void addTask(long idThread, CpuUsageMeasurable cum) {
		cum.setCpuUsage(0);
		this.cpuUsageTaskElements.add(new CPUUsageTaskElement(idThread, cum, this.bean.getThreadCpuTime(idThread)));
		if (this.cpuTimer == null) {
			this.cpuTimer = SchedulerUtils.getTimer("CPUUsageTask");
			this.cpuTimer.scheduleWithFixedDelay(this, 1, 1, TimeUnit.SECONDS);
		}
	}

	public boolean removeTask(long idThread) {
		this.cpuUsageTaskElements.removeIf(e -> e.id == idThread);
		if (this.cpuUsageTaskElements.isEmpty()) {
			close();
			return true;
		}
		return false;
	}
}

class CPUUsageTaskElement {
	public final long id;
	public final CpuUsageMeasurable cum;
	public long cpuStartTime;
	public long start;

	public CPUUsageTaskElement(long id, CpuUsageMeasurable cum, long cpuStartTime) {
		this.id = id;
		this.cum = cum;
		this.cpuStartTime = cpuStartTime;
		this.start = System.nanoTime();
	}

	public void update(ThreadMXBean bean) {
		this.cum.setCpuUsage((float) (bean.getThreadCpuTime(this.id) - this.cpuStartTime) / (System.nanoTime() - this.start));
		this.start = System.nanoTime();
		this.cpuStartTime = bean.getThreadCpuTime(this.id);
	}
}
