/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.scheduler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import io.scenarium.core.tools.Triple;
import io.scenarium.flow.Block;
import io.scenarium.flow.BlockInput;
import io.scenarium.flow.FlowDiagram;
import io.scenarium.flow.IOComponent;
import io.scenarium.flow.IOData;
import io.scenarium.flow.IOLinks;

public class NoThreadedDiagramScheduler extends DiagramScheduler {
	private final List<Block> blocks;
	private HashMap<Object, ArrayList<Runnable>> onStartTasks;
	private HashMap<Object, ArrayList<Runnable>> onPauseTasks;
	private HashMap<Object, ArrayList<Runnable>> onResumeTasks;
	private HashMap<Object, ArrayList<Runnable>> onStopTasks;
	private final BlockRingBuffer todoBlocks = new BlockRingBuffer();
	private final HashMap<IOComponent, Triple<Object[], long[], long[]>> blockBuffs = new HashMap<>();
	private boolean isAlive = true;
	private boolean isStarted = false;

	public NoThreadedDiagramScheduler(FlowDiagram fd) {
		super(fd);
		fd.setCpuUsage(-2);
		this.blocks = getLaunchableBlocksAndSetCpuUsage(-2);
		for (Block block : this.blocks)
			createBlock(block);
		this.isStarted = true;
	}

	public void createBlock(Block block) {
		DiagramScheduler.initBlock(block, this);
		initOperator(block);
	}

	@Override
	public void removeManagedBlock(Block block) {
		this.blocks.remove(block);
		DiagramScheduler.destroyBlock(block);
		if (this.onStartTasks != null)
			this.onStartTasks.remove(block);
		if (this.onPauseTasks != null)
			this.onPauseTasks.remove(block);
		if (this.onResumeTasks != null)
			this.onResumeTasks.remove(block);
		if (this.onStopTasks != null)
			this.onStopTasks.remove(block);
	}

	@Override
	public void addManagedBlock(Block block, boolean waitBlockStart) {
		this.blocks.add(block);
		createBlock(block);
	}

	@Override
	public boolean isAlive() {
		return this.isAlive;
	}

	private boolean runBlock(Block block) {
		if (!block.isReady())
			return false;
		Triple<Object[], long[], long[]> blockBuff = this.blockBuffs.get(block);
		Object[] inputs;
		long[] ts;
		long[] toi;
		if (blockBuff == null || blockBuff.getFirst().length != block.getNbInput()) {
			inputs = new Object[block.getNbInput()];
			ts = new long[block.getNbInput()];
			toi = new long[block.getNbInput()];
			this.blockBuffs.put(block, new Triple<>(inputs, ts, toi));
		} else {
			inputs = blockBuff.getFirst();
			ts = blockBuff.getSecond();
			toi = blockBuff.getThird();
		}
		List<BlockInput> blockInputs = block.getInputs();
		for (int i = 0; i < inputs.length; i++) {
			IOData ioData = blockInputs.get(i).pop();
			if (ioData != null) {
				inputs[i] = ioData.getValue();
				ts[i] = ioData.getTs();
				toi[i] = ioData.getToi();
			} else {
				inputs[i] = null;
				ts[i] = -1;
				toi[i] = -1;
			}
		}
		return DiagramScheduler.processBlock(block, inputs, ts, toi);
	}

	private void stackOutputsAndTrigger(Block block) {
		stackOutputsAndTrigger(block, block.getOutputBuff(), block.getOutputBuffTs());
	}

	@Override
	public synchronized void onStart(Object source, Runnable runnable) {
		checkIfNotDead();
		if (this.isStarted)
			runnable.run();
		pushTask(this.onStartTasks, taskMap -> this.onStartTasks = taskMap, source, runnable);
	}

	@Override
	public synchronized void onResume(Object source, Runnable runnable) {
		checkIfNotDead();
		pushTask(this.onResumeTasks, taskMap -> this.onResumeTasks = taskMap, source, runnable);
	}

	@Override
	public synchronized void onPause(Object source, Runnable runnable) {
		checkIfNotDead();
		pushTask(this.onPauseTasks, taskMap -> this.onPauseTasks = taskMap, source, runnable);
	}

	@Override
	public synchronized void onStop(Block source, Runnable runnable) {
		checkIfNotDead();
		pushTask(this.onStopTasks, taskMap -> this.onStopTasks = taskMap, source, runnable);
	}

	private static void pushTask(HashMap<Object, ArrayList<Runnable>> onEventTasks, Consumer<HashMap<Object, ArrayList<Runnable>>> mapProvider, Object source, Runnable task) {
		if (onEventTasks == null) {
			onEventTasks = new HashMap<>();
			mapProvider.accept(onEventTasks);
		}
		ArrayList<Runnable> taskList = onEventTasks.get(source);
		if (taskList == null) {
			taskList = new ArrayList<>();
			onEventTasks.put(source, taskList);
		}
		taskList.add(task);
	}

	private void checkIfNotDead() {
		if (!this.isAlive)
			new IllegalAccessError("The scheduler is dead. Scheduler events cannot therefore occur");
	}

	@Override
	public void start() {
		this.blocks.forEach(Block::triggerPropertyAsOutput);
		consumeOnEventTasks(this.onStartTasks);
	}

	@Override
	public void resume() {
		super.resume();
		consumeOnEventTasks(this.onResumeTasks);
	}

	@Override
	public void pause() {
		super.pause();
		consumeOnEventTasks(this.onPauseTasks);
	}

	@Override
	public void preStop() {
		consumeOnEventTasks(this.onStopTasks);
	}

	private void consumeOnEventTasks(HashMap<Object, ArrayList<Runnable>> tasks) {
		if (tasks != null)
			tasks.forEach((source, taskList) -> taskList.forEach(task -> triggerTask(source, task)));
	}

	@Override
	public void stop() {
		this.isAlive = false;
		List<Block> blockingBlock = Collections.synchronizedList(new ArrayList<>());
		BiConsumer<List<Block>, Runnable> timeOutTask = this.fd.getTimeOutTask();
		Thread stopTimeOutThread = null;
		if (timeOutTask != null) {
			stopTimeOutThread = new Thread(() -> {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					return;
				}
				timeOutTask.accept(blockingBlock, () -> {});
			});
			stopTimeOutThread.start();
		}
		for (Block block : this.blocks) {
			if (blockingBlock.isEmpty())
				blockingBlock.add(block);
			else
				blockingBlock.set(0, block);
			DiagramScheduler.destroyBlock(block);
		}
		if (stopTimeOutThread != null)
			stopTimeOutThread.interrupt();
		this.blocks.clear();
		this.onStartTasks = null;
		this.onPauseTasks = null;
		this.onResumeTasks = null;
		this.onStopTasks = null;
		this.fd.reset();
	}

	@Override
	public synchronized void triggerProperty(Object source, String outputPropertyName, Object outputValue, long timeStamp) {
		stackPropertyAndTrigger((IOComponent) source, outputPropertyName, outputValue, timeStamp);
		while (!this.todoBlocks.isEmpty() && this.isAlive) {
			Block block = this.todoBlocks.popFirst();
			if ((this.isAlive || block.isConsumesAllDataBeforeDying()) && runBlock(block))
				stackOutputsAndTrigger(block);
		}
	}

	@Override
	public void triggerOutput(Object source, Object outputValue, long timeStamp) {
		triggerOutput(source, new Object[] { outputValue }, new long[] { timeStamp });
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long timeStamp) {
		long[] timeStamps = new long[outputValues.length];
		for (int i = 0; i < timeStamps.length; i++)
			timeStamps[i] = timeStamp;
		triggerOutput(source, outputValues, timeStamps);
	}

	@Override
	public synchronized void triggerOutput(Object source, Object[] outputValues, long[] timeStamps) {
		stackOutputsAndTrigger((IOComponent) source, outputValues, timeStamps);
		while (!this.todoBlocks.isEmpty() && this.isAlive) {
			Block block = this.todoBlocks.popFirst();
			if ((this.isAlive || block.isConsumesAllDataBeforeDying()) && runBlock(block))
				stackOutputsAndTrigger(block);
		}
	}

	@Override
	public boolean holdLock(Object source) {
		return Thread.holdsLock(this);
	}

	@Override
	public synchronized void triggerTask(Object source, Runnable runnable) {
		runnable.run();
	}

	// @Override
	// public synchronized void offerTask(Object source, Runnable runnable) {
	// runnable.run();
	// }

	@Override
	protected void blockNameChanged(Block block) {}

	@Override
	protected void stackOutputAndTriggerBlock(Block block, IOLinks ioLinks, Object[] outputs, long[] outputsTs, long timeOfIssue) {
		if (DiagramScheduler.stackOutputs(block, ioLinks, outputs, outputsTs, timeOfIssue))
			this.todoBlocks.push(block);
	}

	@Override
	protected void stackPropertyAndTriggerBlock(Block comp, IOLinks ioLinks, String outputPropertyName, Object outputValue, long timeStamp, long timeOfIssue) {
		if (DiagramScheduler.stackProperty(comp, ioLinks, outputPropertyName, outputValue, timeStamp, timeOfIssue))
			this.todoBlocks.push((Block) ioLinks.getComponent());
	}
}
