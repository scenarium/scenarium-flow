/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.scheduler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import io.beanmanager.tools.Tuple;
import io.scenarium.flow.Block;
import io.scenarium.flow.BlockInput;
import io.scenarium.flow.FlowDiagram;
import io.scenarium.flow.IOComponent;
import io.scenarium.flow.IOData;
import io.scenarium.flow.IOLinks;
import io.scenarium.flow.ManagingCpuUsageListener;
import io.scenarium.flow.Output;
import io.scenarium.flow.ProcessMode;
import io.scenarium.flow.internal.Log;
import io.scenarium.flow.operator.player.SubFlowDiagram;

public class MultiCoreDiagramScheduler extends DiagramScheduler implements ManagingCpuUsageListener {
	private ThreadedBlock[] threadedBlocks;
	private final HashMap<Block, ThreadedBlock> threadedBlocksMap = new HashMap<>();
	private final HashMap<Block, Condition> incomingDataOrTaskSignals = new HashMap<>();
	private volatile boolean dead = false;
	private CPUUsageTask cpuUsageTask;
	private final Object cpuUsageTaskLock = new Object();
	private final boolean started;

	public MultiCoreDiagramScheduler(FlowDiagram fd) {
		super(fd);
		fd.setCpuUsage(-1);
		List<Block> blocks = getLaunchableBlocksAndSetCpuUsage(-1);
		AtomicInteger nbThreadedBlocksLeft = new AtomicInteger(blocks.size());
		fd.addManagingCpuUsage(this);
		this.threadedBlocks = new ThreadedBlock[blocks.size()];
		for (int i = 0; i < blocks.size(); i++) {
			Block block = blocks.get(i);
			DiagramScheduler.initBlock(block, this);
			ThreadedBlock blockThread = new ThreadedBlock(block, nbThreadedBlocksLeft, this);
			this.incomingDataOrTaskSignals.put(block, blockThread.incomingDataOrTask);
			if (this.fd.isManagingCpuUsage())
				blockThread.initCpuUsageTask();
			this.threadedBlocksMap.put(block, blockThread);
			this.threadedBlocks[i] = blockThread;
		}
		Arrays.sort(this.threadedBlocks, (a, b) -> Integer.compare(b.block.getStartPriority(), a.block.getStartPriority()));
		int i = 0;
		ArrayList<ThreadedBlock> launchedThreadedBlocks = new ArrayList<>(this.threadedBlocks.length);
		while (i != this.threadedBlocks.length) {
			int priority = this.threadedBlocks[i].block.getStartPriority();
			while (i != this.threadedBlocks.length && this.threadedBlocks[i].block.getStartPriority() == priority) {
				ThreadedBlock t = this.threadedBlocks[i++];
				t.start();
				launchedThreadedBlocks.add(t);
			}
			waitThreadedBlock(launchedThreadedBlocks, tb -> tb.isStarted);
			launchedThreadedBlocks.clear();
		}
		this.started = true;
	}

	@Override
	protected void removeManagedBlock(Block block) {
		if (this.dead)
			return;
		synchronized (this) {
			if (this.threadedBlocks.length - 1 == -1)
				Log.error("remove: " + block + " available: " + Arrays.toString(this.threadedBlocks));
			ThreadedBlock[] newThreadedBlocks = new ThreadedBlock[this.threadedBlocks.length - 1]; // TODO NegativeArraySizeException
			int newIndex = 0;
			try {
				for (int i = 0; i < this.threadedBlocks.length; i++) {
					ThreadedBlock threadedBlock = this.threadedBlocks[i];
					if (block == threadedBlock.getBlock()) {
						if (!threadedBlock.isStarted) // If not yet started, wait the end of the start before interrupt them
							waitThreadedBlock(List.of(threadedBlock), tb -> tb.isStarted);
						stopThreadedBlock(threadedBlock);
					} else
						newThreadedBlocks[newIndex++] = threadedBlock;
				}
				this.threadedBlocks = newThreadedBlocks;
			} catch (ArrayIndexOutOfBoundsException e) {
				// Cannot find this block
			}
		}
	}

	private void stopThreadedBlock(ThreadedBlock threadedBlock) {
		Block block = threadedBlock.getBlock();
		if (!block.isAlive()) // Birth failed
			block.reset();
		else {
			ReentrantLock lock = block.getIOLock();
			lock.lock();
			try {
				Condition signal = this.incomingDataOrTaskSignals.get(block);
				threadedBlock.dead = true;
				signal.signal();
			} finally {
				lock.unlock();
			}
		}
	}

	@Override
	protected void addManagedBlock(Block block, boolean waitBlockStart) {
		if (this.dead)
			return;
		synchronized (this) {
			ThreadedBlock tb = MultiCoreDiagramScheduler.this.threadedBlocksMap.get(block);
			if (tb != null)
				try {
					tb.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			DiagramScheduler.initBlock(block, this);
			ThreadedBlock threadedBlock = new ThreadedBlock(block, null, this);
			this.threadedBlocksMap.put(block, threadedBlock);
			this.incomingDataOrTaskSignals.put(block, threadedBlock.incomingDataOrTask);
			ThreadedBlock[] newThreadedBlocks = new ThreadedBlock[this.threadedBlocks.length + 1];
			System.arraycopy(this.threadedBlocks, 0, newThreadedBlocks, 0, this.threadedBlocks.length);
			newThreadedBlocks[this.threadedBlocks.length] = threadedBlock;
			this.threadedBlocks = newThreadedBlocks;
			if (this.fd.isManagingCpuUsage())
				threadedBlock.initCpuUsageTask();
			threadedBlock.start();
			if (waitBlockStart)
				waitThreadedBlock(List.of(threadedBlock), t -> t.isStarted);
		}
	}

	@Override
	public boolean isStarted() {
		return this.started;
	}

	@Override
	public boolean isAlive() {
		return this.started && !this.dead;
	}

	@Override
	public boolean isDead() {
		return this.dead;
	}

	@Override
	protected void blockNameChanged(Block block) {
		ThreadedBlock threadedBlock = this.threadedBlocksMap.get(block);
		if (threadedBlock != null)
			triggerTask(block, () -> threadedBlock.setName(block.getName()));
	}

	@Override
	public void managingCpuChanged(boolean managingCpu) {
		if (this.dead)
			return;
		if (managingCpu)
			for (ThreadedBlock threadedBlock : this.threadedBlocks)
				threadedBlock.initCpuUsageTask();
		else
			for (ThreadedBlock threadedBlock : this.threadedBlocks)
				threadedBlock.closeCpuUsageTask();
	}

	@Override
	public void onStart(Object source, Runnable runnable) {
		if (this.started)
			runnable.run();
		else
			addOnEvent(source, tb -> tb.onStart(runnable));
	}

	@Override
	public void onResume(Object source, Runnable runnable) {
		addOnEvent(source, tb -> tb.onResume(runnable));
	}

	@Override
	public void onPause(Object source, Runnable runnable) {
		addOnEvent(source, tb -> tb.onPause(runnable));
	}

	@Override
	public void onStop(Block source, Runnable runnable) {
		addOnEvent(source, tb -> tb.onStop(runnable));
	}

	private void addOnEvent(Object source, Consumer<ThreadedBlock> consumer) {
		if (this.dead)
			throw new IllegalAccessError("The scheduler is dead. The stopped event cannot therefore occur");
		ThreadedBlock threadedBlock = this.threadedBlocksMap.get(source);
		if (threadedBlock != null)
			consumer.accept(threadedBlock);
		else
			throw new IllegalArgumentException("The source: " + source + " is not managed by this scheduler");
	}

	@Override
	public void start() {
		for (ThreadedBlock tb : this.threadedBlocks)
			tb.getBlock().triggerPropertyAsOutput();
		for (ThreadedBlock tb : this.threadedBlocks)
			triggerTask(tb.getBlock(), tb::consumeOnStartTasks);
	}

	@Override
	public void resume() {
		super.resume();
		for (ThreadedBlock tb : this.threadedBlocks)
			tb.consumeOnResumeTasks();
	}

	@Override
	public void pause() {
		super.pause();
		for (ThreadedBlock tb : this.threadedBlocks)
			tb.consumeOnPauseTasks();
	}

	@Override
	public void preStop() {
		for (ThreadedBlock tb : this.threadedBlocks)
			tb.consumeOnStopTasks();
	}

	@Override
	protected void stackPropertyAndTriggerBlock(Block comp, IOLinks ioLinks, String outputPropertyName, Object outputValue, long timeStamp, long timeOfIssue) {
		IOComponent linkedComp = ioLinks.getComponent();
		Condition incomingDataSignal = this.incomingDataOrTaskSignals.get(linkedComp);
		if (incomingDataSignal != null) {
			ReentrantLock ioLock = ((Block) linkedComp).getIOLock();
			ioLock.lock();
			try {
				if (DiagramScheduler.stackProperty(comp, ioLinks, outputPropertyName, outputValue, timeStamp, timeOfIssue))
					incomingDataSignal.signal();
			} finally {
				ioLock.unlock();
			}
		} else
			Log.error(comp + ": trigger a property after the death of: " + linkedComp);
	}

	@Override
	protected void stackOutputAndTriggerBlock(Block block, IOLinks ioLinks, Object[] outputs, long[] outputsTs, long timeOfIssue) {
		Condition incomingDataSignal = this.incomingDataOrTaskSignals.get(block);
		if (incomingDataSignal != null) {
			ReentrantLock ioLock = block.getIOLock();
			ioLock.lock();
			try {
				if (DiagramScheduler.stackOutputs(block, ioLinks, outputs, outputsTs, timeOfIssue))
					incomingDataSignal.signal();
			} finally {
				ioLock.unlock();
			}
		} else
			Log.error(block + ": trigger outputs after the death of block: " + block); // Sortie de diagram de flux
	}

	@Override
	public void stop() {
		if (this.dead)
			return;
		synchronized (this) {
			this.dead = true;
			ThreadedBlock[] threadedBlocks = this.threadedBlocks.clone();
			Arrays.sort(threadedBlocks, (a, b) -> Integer.compare(b.block.getStopPriority(), a.block.getStopPriority()));
			int i = 0;
			ArrayList<ThreadedBlock> stoppedThreadedBlocks = new ArrayList<>(threadedBlocks.length);
			while (i != threadedBlocks.length) {
				int priority = threadedBlocks[i].block.getStopPriority();
				while (i != threadedBlocks.length && threadedBlocks[i].block.getStopPriority() == priority) {
					ThreadedBlock tb = threadedBlocks[i++];
					stopThreadedBlock(tb);
					stoppedThreadedBlocks.add(tb);
				}
				waitThreadedBlock(stoppedThreadedBlocks, tb -> !tb.isAlive());
				stoppedThreadedBlocks.clear();
			}
			this.threadedBlocks = null;
			this.incomingDataOrTaskSignals.clear();
			this.fd.removeManagingCpuUsageListener(this);
			this.fd.reset();
		}
	}

	@Override
	public void triggerProperty(Object source, String outputPropertyName, Object outputValue, long timeStamp) {
		if (isAlive())
			stackPropertyAndTrigger((IOComponent) source, outputPropertyName, outputValue, timeStamp);
	}

	@Override
	public void triggerOutput(Object source, Object outputValue, long timeStamp) {
		if (isAlive())
			if (!this.started)
				showEarlyTriggerError(source);
			else
				stackOutputsAndTrigger((IOComponent) source, new Object[] { outputValue }, new long[] { timeStamp });
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long timeStamp) {
		if (isAlive()) {
			if (!this.started)
				showEarlyTriggerError(source);
			boolean isNonNullEntrie = false;
			List<? extends Output> outputs = ((IOComponent) source).getTriggerableOutputs();
			for (int i = 0; i < outputValues.length; i++)
				if (outputs.get(i).getLinkInputs() == null)
					outputValues[i] = null;
				else if (outputValues[i] != null)
					// outputValues[i] = clone(outputValues[i]);
					isNonNullEntrie = true;
			long[] timeStamps = new long[outputValues.length];
			for (int i = 0; i < timeStamps.length; i++)
				timeStamps[i] = timeStamp;
			if (isNonNullEntrie)
				stackOutputsAndTrigger((IOComponent) source, outputValues, timeStamps);
		}
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long[] timeStamps) {
		if (isAlive()) {
			if (!this.started)
				showEarlyTriggerError(source);
			boolean isNonNullEntrie = false;
			List<? extends Output> outputs = ((IOComponent) source).getTriggerableOutputs();
			for (int i = 0; i < outputValues.length; i++)
				if (outputs.get(i).getLinkInputs() == null) {
					outputValues[i] = null;
					timeStamps[i] = -1;
				} else if (outputValues[i] != null)
					// outputValues[i] = clone(outputValues[i]);
					isNonNullEntrie = true;
			if (isNonNullEntrie)
				stackOutputsAndTrigger((IOComponent) source, outputValues, timeStamps);
		}
	}

	private void showEarlyTriggerError(Object source) {
		Log.error("Early trigger of: " + source + " data removed");
	}

	@Override
	public void triggerTask(Object source, Runnable task) {
		ThreadedBlock threadedBlock = this.threadedBlocksMap.get(source);
		if (threadedBlock != null) {
			if (Thread.currentThread() == threadedBlock
					|| threadedBlock.getBlock().getRemoteOperator() != null && threadedBlock.getBlock().getRemoteOperator().isFromThreadedBlock(threadedBlock.getId()))
				task.run();
			else if (!threadedBlock.getBlock().isAlive()) {
				if (!(threadedBlock.block.operator instanceof SubFlowDiagram))
					try {
						threadedBlock.join();
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
					}
				task.run();
			} else {
				ReentrantLock lock = ((Block) source).getIOLock();
				if (lock.isHeldByCurrentThread())
					task.run();
				else {
					boolean isDead;
					lock.lock();
					try {
						isDead = threadedBlock.dead;
						if (!isDead) {
							Condition schedulerAvailable = lock.newCondition();
							Condition taskCompleted = lock.newCondition();
							Tuple<Condition, Condition> taskToOffer = new Tuple<>(schedulerAvailable, taskCompleted);
							threadedBlock.tasks.add(taskToOffer);
							this.incomingDataOrTaskSignals.get(source).signal();
							try {
								schedulerAvailable.await();
								task.run();
								taskCompleted.signal();
							} catch (InterruptedException e) {
								if (threadedBlock.tasks.remove(taskToOffer))
									task.run();
								taskCompleted.signal();
								Thread.currentThread().interrupt();
							}
						}
					} finally {
						lock.unlock();
					}
					if (isDead) {
						// Wait the end of the thread and execute the task after, but release the lock
						if (!(threadedBlock.block.operator instanceof SubFlowDiagram))
							try {
								threadedBlock.join();
							} catch (InterruptedException e) {
								Thread.currentThread().interrupt();
							}
						task.run();
					}
				}
			}
		} else {
			Log.error("Cannot find corresponding threadedBlock");
			task.run(); // Cannot find corresponding threadedBlock
		}
	}

	// public void offerTask(Object source, Runnable task) {
	// ThreadedBlock threadedBlock = this.threadedBlocksMap.get(source);
	// if (threadedBlock != null) {
	// if (holdLock(threadedBlock, source))
	// task.run();
	// else if (!threadedBlock.getBlock().isAlive()) {
	// if (!(threadedBlock.block.operator instanceof SubFlowDiagram))
	// try {
	// threadedBlock.join();
	// } catch (InterruptedException e) {
	// Thread.currentThread().interrupt();
	// }
	// task.run();
	// } else {
	// ReentrantLock lock = ((Block) source).getIOLock();
	// if (lock.isHeldByCurrentThread())
	// task.run();
	// else {
	// boolean isDead;
	// lock.lock();
	// try {
	// isDead = threadedBlock.dead;
	// if (!isDead) {
	// Condition taskCompleted = lock.newCondition();
	// threadedBlock.tasksCompleted.add(taskCompleted);
	// threadedBlock.tasks.add(task);
	// this.incomingDataOrTaskSignals.get(source).signal();
	// // Log.info("Thread: " + Thread.currentThread() + " take lock: " + lock + " and is waiting");
	// try {
	// taskCompleted.await();
	// } catch (InterruptedException e) { // Not really important. The task was already triggered and the ThreadedBlock will execute all tasks before dying.
	// Thread.currentThread().interrupt();
	// }
	// }
	// } finally {
	// lock.unlock();
	// }
	// if (isDead) {
	// // Wait the end of the thread and execute the task after, but release the lock
	// if (!(threadedBlock.block.operator instanceof SubFlowDiagram))
	// try {
	// threadedBlock.join();
	// } catch (InterruptedException e) {
	// Thread.currentThread().interrupt();
	// }
	// task.run();
	// }
	// }
	// }
	// return;
	// }
	// task.run(); // Cannot find corresponding threadedBlock
	// }

	@Override
	public boolean holdLock(Object source) {
		ThreadedBlock threadedBlock = this.threadedBlocksMap.get(source);
		return threadedBlock == null ? true : holdLock(threadedBlock, source);
	}

	private boolean holdLock(ThreadedBlock threadedBlock, Object source) {
		return Thread.currentThread() == threadedBlock
				|| threadedBlock.getBlock().getRemoteOperator() != null && threadedBlock.getBlock().getRemoteOperator().isFromThreadedBlock(threadedBlock.getId());
	}

	private void waitThreadedBlock(List<ThreadedBlock> threadedBlocks, Predicate<ThreadedBlock> blockCondition) {
		int timeOutTime = 1000;
		for (ThreadedBlock tb : threadedBlocks) {
			ProcessMode pm = tb.block.getProcessMode();
			if (pm == ProcessMode.ISOLATED) {
				timeOutTime = 2000;
				break;
			} else if (pm == ProcessMode.REMOTE)
				timeOutTime = 1500;
		}
		long endOfTimeout = System.currentTimeMillis() + timeOutTime;
		boolean timeOut = false;
		LinkedList<ThreadedBlock> blockingThreads = new LinkedList<>(threadedBlocks);
		while (!blockingThreads.isEmpty()) {
			long millis = endOfTimeout - System.currentTimeMillis();
			if (millis < 0) {
				timeOut = true;
				break;
			}
			try {
				Thread.sleep(Math.min(50, millis));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			blockingThreads.removeIf(blockCondition);
		}
		if (timeOut) {
			List<Block> aliveSynchroThreads = Collections.synchronizedList(blockingThreads.stream().map(t -> t.getBlock()).collect(Collectors.toList()));
			BiConsumer<List<Block>, Runnable> timeOutTask = this.fd.getTimeOutTask();
			Thread stopTimeOutThread = null;
			if (timeOutTask != null) {
				stopTimeOutThread = new Thread(() -> timeOutTask.accept(aliveSynchroThreads, () -> blockingThreads.forEach(tb -> tb.interrupt())), "Stopping warning thread for: " + this.fd);
				stopTimeOutThread.start();
			}
			while (!blockingThreads.isEmpty()) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for (Iterator<ThreadedBlock> iterator = blockingThreads.iterator(); iterator.hasNext();) {
					ThreadedBlock tb = iterator.next();
					if (blockCondition.test(tb)) {
						iterator.remove();
						aliveSynchroThreads.remove(tb.getBlock());
					}
				}
			}
			if (stopTimeOutThread != null)
				stopTimeOutThread.interrupt();
		}
	}

	class ThreadedBlock extends Thread {
		private final Object onTaskLock = new Object();
		private ArrayList<Runnable> onStartTasks;
		private ArrayList<Runnable> onPauseTasks;
		private ArrayList<Runnable> onResumeTasks;
		private ArrayList<Runnable> onStopTasks;
		private final Block block;
		private final MultiCoreDiagramScheduler scheduler;
		private AtomicInteger nbThreadedBlocksLeft;
		private Object[] inputs = new Object[0];
		private long[] ts;
		private long[] toi;
		private boolean cpuUsageMonitored;
		private final ReentrantLock ioLock;
		private final Condition incomingDataOrTask;
		// public ArrayList<Condition> tasksCompleted = new ArrayList<>();
		public ArrayList<Tuple<Condition, Condition>> tasks = new ArrayList<>();
		// private LinkedList<Runnable> pendingsTasks;
		public boolean isStarted;
		public boolean dead;

		public ThreadedBlock(Block block, AtomicInteger nbThreadedBlocksLeft, MultiCoreDiagramScheduler scheduler) {
			this.block = block;
			this.ioLock = block.getIOLock();
			this.incomingDataOrTask = this.ioLock.newCondition();
			this.scheduler = scheduler;
			this.nbThreadedBlocksLeft = nbThreadedBlocksLeft;
			setPriority(block.getThreadPriority());
			setName(block.getName());
		}

		public void onStart(Runnable runnable) {
			synchronized (this.onTaskLock) {
				ArrayList<Runnable> onStartTasks = this.onStartTasks;
				if (onStartTasks == null)
					onStartTasks = new ArrayList<>();
				onStartTasks.add(runnable);
				this.onStartTasks = onStartTasks;
			}
		}

		public void onResume(Runnable runnable) {
			synchronized (this.onTaskLock) {
				ArrayList<Runnable> onResumeTasks = this.onResumeTasks;
				if (onResumeTasks == null)
					onResumeTasks = new ArrayList<>();
				onResumeTasks.add(runnable);
				this.onResumeTasks = onResumeTasks;
			}
		}

		public void onPause(Runnable runnable) {
			synchronized (this.onTaskLock) {
				ArrayList<Runnable> onPauseTasks = this.onPauseTasks;
				if (onPauseTasks == null)
					onPauseTasks = new ArrayList<>();
				onPauseTasks.add(runnable);
				this.onPauseTasks = onPauseTasks;
			}
		}

		public void onStop(Runnable runnable) {
			synchronized (this.onTaskLock) {
				ArrayList<Runnable> onStopTasks = this.onStopTasks;
				if (onStopTasks == null)
					onStopTasks = new ArrayList<>();
				onStopTasks.add(runnable);
				this.onStopTasks = onStopTasks;
			}
		}

		public void consumeOnStartTasks() {
			consumeOnEventTasks(this.onStartTasks);
		}

		public void consumeOnResumeTasks() {
			consumeOnEventTasks(this.onResumeTasks);
		}

		public void consumeOnPauseTasks() {
			consumeOnEventTasks(this.onPauseTasks);
		}

		public void consumeOnStopTasks() {
			consumeOnEventTasks(this.onStopTasks);
		}

		private void consumeOnEventTasks(ArrayList<Runnable> tasks) {
			if (tasks != null)
				tasks.forEach(t -> triggerTask(this.block, t));
		}

		private void consumeTasks() {
			while (!this.tasks.isEmpty()) {
				Tuple<Condition, Condition> task = this.tasks.remove(0);
				try {
					task.getFirst().signal();
					task.getSecond().await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		public Block getBlock() {
			return this.block;
		}

		public void initCpuUsageTask() {
			if (!this.cpuUsageMonitored) {
				synchronized (this.scheduler.cpuUsageTaskLock) {
					if (MultiCoreDiagramScheduler.this.cpuUsageTask == null)
						try {
							MultiCoreDiagramScheduler.this.cpuUsageTask = CPUUsageTask.createCPUUsageTask();
						} catch (UnsupportedOperationException e) {
							Log.error(e.getMessage());
							return;
						}
					MultiCoreDiagramScheduler.this.cpuUsageTask.addTask(getId(), this.block);
				}
				this.cpuUsageMonitored = true;
			}
		}

		public void closeCpuUsageTask() {
			if (this.cpuUsageMonitored)
				synchronized (this.scheduler.cpuUsageTaskLock) {
					if (MultiCoreDiagramScheduler.this.cpuUsageTask.removeTask(getId()))
						MultiCoreDiagramScheduler.this.cpuUsageTask = null;
					this.cpuUsageMonitored = false;
				}
		}

		@Override
		public /* synchronized */ void run() { // Why synchronized ? with what ? Can be in conflict with Thread::setName
			this.scheduler.initOperator(this.block);
			if (this.nbThreadedBlocksLeft != null) {
				this.nbThreadedBlocksLeft.decrementAndGet();
				this.nbThreadedBlocksLeft = null;
			}
			this.isStarted = true;
			if (!this.block.isAlive()) // Birth failed
				return;
			this.block.setThread(Thread.currentThread());
			dead: while (!canBeStopped()) {
				this.ioLock.lock();
				try {
					while (true) {
						// while (this.pendingsTasks != null && !this.pendingsTasks.isEmpty())
						// this.pendingsTasks.pop().run();
						if (!this.tasks.isEmpty())
							consumeTasks();
						if (this.block.isReady())
							break;
						if (canBeStopped())
							break dead;
						this.incomingDataOrTask.await();
					}
					int nbInput = this.block.getNbInput();
					if (this.inputs.length != nbInput) {
						this.inputs = new Object[nbInput];
						this.ts = new long[nbInput];
						this.toi = new long[nbInput];
					}
					List<BlockInput> blockInputs = this.block.getInputs();
					for (int i = 0; i < blockInputs.size(); i++) {
						IOData ioData = blockInputs.get(i).pop();
						if (ioData != null) {
							this.inputs[i] = ioData.getValue();
							this.ts[i] = ioData.getTs();
							this.toi[i] = ioData.getToi();
						} else {
							this.inputs[i] = null;
							this.ts[i] = -1;
							this.toi[i] = -1;
						}
					}
				} catch (InterruptedException e) {
					break;
				} finally {
					this.ioLock.unlock();
				}
				if (DiagramScheduler.processBlock(this.block, this.inputs, this.ts, this.toi))
					stackOutputsAndTrigger(this.block, this.block.getOutputBuff(), this.block.getOutputBuffTs());
			}
			this.ioLock.lock();
			try {
				consumeTasks();
			} finally {
				this.ioLock.unlock();
			}
			DiagramScheduler.destroyBlock(this.block);
			MultiCoreDiagramScheduler.this.threadedBlocksMap.remove(this.block);
			MultiCoreDiagramScheduler.this.incomingDataOrTaskSignals.remove(this.block);
			if (this.cpuUsageMonitored)
				closeCpuUsageTask();
		}

		private boolean canBeStopped() {
			return this.dead && (!this.block.isConsumesAllDataBeforeDying() || !this.block.isReady() && this.tasks.isEmpty());
		}

		@Override
		public String toString() {
			return "Thread of: " + this.block;
		}
	}
}