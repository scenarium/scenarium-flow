/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.scheduler;

import java.lang.invoke.MethodHandle;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;
import java.util.function.Function;

import io.beanmanager.BeanAndSubBeanPropertyChangeListener;
import io.beanmanager.BeanManager;
import io.beanmanager.editors.container.BeanEditor;
import io.beanmanager.rmi.RMIBeanManager;
import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.filemanager.scenariomanager.ScenarioManager;
import io.scenarium.flow.Block;
import io.scenarium.flow.BlockInput;
import io.scenarium.flow.BlockOutput;
import io.scenarium.flow.BrokerPropertyException;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.flow.FlowDiagram;
import io.scenarium.flow.FlowDiagramInput;
import io.scenarium.flow.IOComponent;
import io.scenarium.flow.IOLink;
import io.scenarium.flow.IOLinks;
import io.scenarium.flow.Link;
import io.scenarium.flow.ModificationType;
import io.scenarium.flow.ProcessMode;
import io.scenarium.flow.internal.Log;
import io.scenarium.flow.operator.OperatorManager;
import io.scenarium.flow.operator.player.SubFlowDiagram;
import io.scenarium.flow.operator.rmi.RemoteOperator;
import io.scenarium.flow.operator.rmi.VisuableSchedulableRemoteOperator;

public abstract class DiagramScheduler implements Trigger {
	private static final ConcurrentHashMap<Class<?>, Function<Object, Object>> CLONER_FACTORY = new ConcurrentHashMap<>();
	protected final FlowDiagram fd;
	private boolean running;

	public DiagramScheduler(FlowDiagram fd) {
		this.fd = fd;
		this.running = true;
	}

	protected List<Block> getLaunchableBlocksAndSetCpuUsage(int subManagedFlowDiagramCPUMode) {
		return this.fd.getManagedBlocks(b -> {
			if (b.operator instanceof SubFlowDiagram && !((SubFlowDiagram) b.operator).getSubDiagram().isSubManagement()) {
				FlowDiagram subFd = (FlowDiagram) ((SubFlowDiagram) b.operator).getSubDiagram().getScenarioData();
				if (subFd != null)
					subFd.setCpuUsage(subManagedFlowDiagramCPUMode);
			}
			return b.launchable();
		});
	}

	private static void addSynchronization(BeanManager[] beanManagers, BeanAndSubBeanPropertyChangeListener[] synchronizationListeners) {
		beanManagers[0].addBeanAndSubBeanPropertyChangeListener(synchronizationListeners[0]);
		beanManagers[1].addBeanAndSubBeanPropertyChangeListener(synchronizationListeners[1]);
	}

	public final void removeBlock(Block block) {
		if (block.operator instanceof SubFlowDiagram) {
			FlowDiagram fd = (FlowDiagram) ((SubFlowDiagram) block.operator).getSubDiagram().getScenarioData();
			if (fd != null && !fd.isSubManagement())
				fd.getManagedBlocks(Block::launchable).forEach(this::removeManagedBlock);
		}
		removeManagedBlock(block);
	}

	protected abstract void removeManagedBlock(Block block);

	public final void addBlock(Block block, boolean waitBlockStart) {
		if (block.operator instanceof SubFlowDiagram) {
			FlowDiagram fd = (FlowDiagram) ((SubFlowDiagram) block.operator).getSubDiagram().getScenarioData(); // Set cpu usage
			if (fd != null && !fd.isSubManagement())
				fd.getManagedBlocks(Block::launchable).forEach(b -> addManagedBlock(b, false));
		}
		addManagedBlock(block, waitBlockStart);
	}

	protected abstract void addManagedBlock(Block block, boolean waitBlockStart);

	protected abstract void blockNameChanged(Block block);

	@Override
	public abstract void onStart(Object source, Runnable runnable);

	@Override
	public abstract void onResume(Object source, Runnable runnable);

	@Override
	public abstract void onPause(Object source, Runnable runnable);

	@Override
	public abstract void onStop(Block source, Runnable runnable);

	public void resume() {
		this.running = true;
	}

	@Override
	public boolean isRunning() {
		return this.running;
	}

	public abstract void start();

	public void pause() {
		this.running = false;
	}

	public abstract void preStop();

	public abstract void stop();

	private static void populateSynchronization(Block block, BeanManager[] beanManagers, BeanAndSubBeanPropertyChangeListener[] synchronizationListeners) {
		RMIBeanManager rbm = new RMIBeanManager(block.getRemoteOperator(), BeanManager.defaultDir);
		BeanAndSubBeanPropertyChangeListener remoteListener = (beanType, beanName, propertyName) -> {
			Object value = rbm.getSubBeanValue(beanType, beanName, propertyName);
			Log.info("RMI: Remote property changed for block " + block + ":  bean type: " + beanType + ", beanName: " + beanName + ", property name: " + propertyName + ", value: " + value
					+ " transfer to local");
			new BeanManager(BeanEditor.getRegisterBean(beanType, beanName).bean, "").setValue(propertyName, value);
		};
		BeanAndSubBeanPropertyChangeListener localListener = (beanType, beanName, propertyName) -> {
			Object value = new BeanManager(BeanEditor.getRegisterBean(beanType, beanName).bean, "").getValue(propertyName);
			Log.info("RMI: Local property changed for block " + block + ": bean type: " + beanType + ", beanName: " + beanName + ", property name: " + propertyName + ", value: " + value
					+ " transfer to remote");
			rbm.setSubBeanValue(beanType, beanName, propertyName, value);
		};
		BeanManager lbm = new BeanManager(block.operator, BeanManager.defaultDir);
		synchronizationListeners[0] = localListener;
		synchronizationListeners[1] = remoteListener;
		beanManagers[0] = lbm;
		beanManagers[1] = rbm;
	}

	// @Override
	// public synchronized void onPause(Object source, Runnable runnable) {
	//// ArrayList<Runnable> _onPauseTasks = new ArrayList<>(onPauseTasks);
	//// _onPauseTasks.add(runnable);
	//// onPauseTasks = _onPauseTasks;
	// }
	//
	// public void pause() {
	// ArrayList<Runnable> _onPauseTasks = onPauseTasks;
	// if(_onPauseTasks != null)
	// _onPauseTasks.forEach(Runnable::run);
	// }
	//
	// @Override
	// public synchronized void onResume(Object source, Runnable runnable) {
	// ArrayList<Runnable> _onResumeTasks = new ArrayList<>(onResumeTasks);
	// _onResumeTasks.add(runnable);
	// onResumeTasks = _onResumeTasks;
	// }
	//
	// public void resume() {
	// ArrayList<Runnable> _onResumeTasks = onResumeTasks;
	// if(_onResumeTasks != null)
	// _onResumeTasks.forEach(Runnable::run);
	// }

	@SuppressWarnings("unchecked")
	public static <T extends Object> boolean addCloner(Class<T> type, Function<T, T> cloneFunction) {
		return CLONER_FACTORY.putIfAbsent(type, (Function<Object, Object>) cloneFunction) == null;
	}

	@SuppressWarnings("unchecked")
	public static <T extends Object> void replaceCloner(Class<T> type, Function<T, T> cloneFunction) {
		CLONER_FACTORY.put(type, (Function<Object, Object>) cloneFunction);
	}

	public static void purgeCloners(Module module) {
		CLONER_FACTORY.keySet().removeIf(c -> c.getModule().equals(module));
	}

	protected void stackPropertyAndTrigger(IOComponent component, String outputPropertyName, Object outputValue, long timeStamp) {
		if (!component.canTriggerOrBeTriggered())
			return;
		long timeOfIssue = System.currentTimeMillis();
		IOLinks[] index = component.getIndex();
		for (int i = 0; i < index.length; i++) {
			IOLinks ioLinks = index[i];
			IOComponent linkedComp = ioLinks.getComponent();
			if (linkedComp.canTriggerOrBeTriggered())
				if (linkedComp instanceof Block)
					stackPropertyAndTriggerBlock((Block) linkedComp, ioLinks, outputPropertyName, outputValue, timeStamp, timeOfIssue);
				else {
					FlowDiagram flowDiagram = (FlowDiagram) linkedComp;
					if (flowDiagram.isSubManagement()) {
						int id = i;
						flowDiagram.runFlowDiagramTriggerOutputTaskLater(null, null, (noutputs, noutputsTs, triggerOutput) -> { // The structure of the subDiagram can no longer change
							IOLinks[] newIndex = component.getIndex();
							IOLink ioLink = newIndex[id].getLinks(outputPropertyName).get(0); // Only one possible link between an block output and an flowdiagram output
							// The structure of the subDiagram changed between the last index and if the destination changed, the data cannot be send
							if (newIndex != index && (newIndex.length <= id || ioLink.getInput() != index[id].getLinks(outputPropertyName).get(0).getInput()))
								return;
							int iIndex = flowDiagram.getOutputIndex((FlowDiagramInput) ioLink.getInput());
							Object[] obj = flowDiagram.getOutputBuff();
							long[] ts = flowDiagram.getOutputBuffTs();
							ts[iIndex] = timeStamp;
							obj[iIndex] = /* l.isNeedToCopy() ? DiagramScheduler.clone(outputValue) : */outputValue; // Already a copy
							triggerOutput.accept(obj, ts);
							obj[iIndex] = null;
							consumeIOLink(ioLink);
						});
					} else
						for (IOLink ioLink : ioLinks.getLinks(outputPropertyName))
							consumeIOLink(ioLink);
				}
		}
	}

	protected abstract void stackPropertyAndTriggerBlock(Block comp, IOLinks ioLinks, String outputPropertyName, Object outputValue, long timeStamp, long timeOfIssue);

	protected void stackOutputsAndTrigger(IOComponent component, Object[] outputs, long[] outputsTs) {
		if (!component.canTriggerOrBeTriggered())
			return;
		long timeOfIssue = System.currentTimeMillis();
		IOLinks[] index = component.getIndex();
		for (int i = 0; i < index.length; i++) {
			IOLinks ioLinks = index[i];
			IOComponent linkedComp = ioLinks.getComponent();
			if (linkedComp.canTriggerOrBeTriggered())
				if (linkedComp instanceof Block)
					stackOutputAndTriggerBlock((Block) linkedComp, ioLinks, outputs, outputsTs, timeOfIssue);
				else {
					FlowDiagram flowDiagram = (FlowDiagram) linkedComp;
					if (flowDiagram.isSubManagement()) {
						int id = i;
						// The structure of the subDiagram can no longer change. MAJ must not be in the process. Always the cost of a lock...
						flowDiagram.runFlowDiagramTriggerOutputTaskLater(outputs, outputsTs, (noutputs, noutputsTs, triggerOutput) -> {
							IOLinks[] newIndex = component.getIndex();
							// The structure of the subDiagram changed between the last index and if the destination changed, the data cannot be send
							if (newIndex != index && (newIndex.length <= id || newIndex[id].getLinks()[0].getInput() != index[id].getLinks()[0].getInput()))
								return;
							IOLinks ioL = newIndex[id];
							Object[] obj = flowDiagram.getOutputBuff();
							long[] ts = flowDiagram.getOutputBuffTs();
							if (pushOutputData(ioL, noutputs, (ioLink, outputObject) -> {
								int iIndex = flowDiagram.getOutputIndex((FlowDiagramInput) ioLink.getInput());
								obj[iIndex] = outputObject;
								ts[iIndex] = noutputsTs[ioLink.getOutputIndex()];
							}))
								triggerOutput.accept(obj, ts);
							Arrays.fill(obj, null);
						});
					} else
						for (IOLink ioLink : ioLinks.getLinks())
							consumeIOLink(ioLink);
				}
		}
	}

	protected abstract void stackOutputAndTriggerBlock(Block component, IOLinks ioLinks, Object[] outputs, long[] outputsTs, long timeOfIssue);

	public static boolean stackOutputs(IOComponent component, IOLinks ioLinks, Object[] outputs, long[] outputsTs, long timeOfIssue) {
		return DiagramScheduler.pushOutputData(ioLinks, outputs, (ioLink, outputObject) -> {
			if (((BlockInput) ioLink.getInput()).getBuffer().push(outputsTs[ioLink.getOutputIndex()], timeOfIssue, outputObject))
				DiagramScheduler.showBufferOverflow(component, (BlockInput) ioLink.getInput());
		});
	}

	public static boolean stackProperty(Block comp, IOLinks ioLinks, String outputPropertyName, Object outputValue, long timeStamp, long timeOfIssue) {
		boolean blockReady = false;
		ArrayList<IOLink> links = ioLinks.getLinks(outputPropertyName);
		if (links != null)
			for (IOLink ioLink : links) {
				blockReady = true;
				if (((BlockInput) ioLink.getInput()).getBuffer().push(timeStamp, timeOfIssue, ioLink.isNeedToCopy() ? DiagramScheduler.clone(outputValue) : outputValue))
					DiagramScheduler.showBufferOverflow(comp, (BlockInput) ioLink.getInput());
				consumeIOLink(ioLink);
			}
		return blockReady;
	}

	private static boolean pushOutputData(IOLinks ioLinks, Object[] outputs, BiConsumer<IOLink, Object> dataConsumer) {
		boolean blockReady = false;
		for (IOLink ioLink : ioLinks.getLinks()) {
			Object outputObject = outputs[ioLink.getOutputIndex()];
			if (outputObject == null)
				continue;
			blockReady = true;
			dataConsumer.accept(ioLink, ioLink.isNeedToCopy() ? clone(outputObject) : outputObject);
			consumeIOLink(ioLink);
		}
		return blockReady;
	}

	private static void consumeIOLink(IOLink ioLink) {
		Link link = ioLink.getInput().getLink();
		if (link != null)
			link.consume();
	}

	public static Object clone(Object object) {
		if (object == null)
			return null;
		Class<? extends Object> c = object.getClass();
		Function<Object, Object> clonemethod = CLONER_FACTORY.get(c);
		Object val = null;
		if (clonemethod != null)
			val = clonemethod.apply(object);
		else if (c.isArray()) {
			Class<?> type = c.getComponentType();
			if (type.isArray()) {
				Class<?> subType = type.getComponentType();
				Function<Object, Object> method = CLONER_FACTORY.get(subType);
				if (method == null) {
					method = loadCloneMethod(type);
					if (method == null) {
						Log.error("Cannot clone: " + object);
						return val;
					}
				}
				Object[][] in = (Object[][]) object;
				Object[][] out = (Object[][]) Array.newInstance(type, in.length);
				for (int i = 0; i < out.length; i++) {
					Object[] subin = in[i];
					if (subin != null) {
						Object[] subout = (Object[]) Array.newInstance(subType, subin.length);
						for (int j = 0; j < subout.length; j++) {
							Object v = subin[j];
							subout[j] = v == null ? null : method.apply(subin[j]);
						}
						out[i] = subout;
					}
				}
				val = out;
			} else {
				Object[] in = (Object[]) object;
				Object[] out = (Object[]) Array.newInstance(type, in.length);
				Function<Object, Object> method = CLONER_FACTORY.get(type);
				if (method == null) {
					method = loadCloneMethod(type);
					if (method == null) {
						Log.error("Cannot clone: " + object);
						return val;
					}
				}
				for (int i = 0; i < out.length; i++) {
					Object v = in[i];
					out[i] = v == null ? null : method.apply(v);
				}
				val = out;
			}
		} else {
			clonemethod = loadCloneMethod(c);
			if (clonemethod != null)
				val = clonemethod.apply(object);
		}
		if (val == null) {
			Log.error("Cannot clone: " + object);
			return null;
		}
		return val;
	}

	public static void initBlock(Block block, Trigger trigger) {
		Object op = block.operator;
		if (op instanceof EvolvedOperator)
			((EvolvedOperator) op).setTrigger(trigger);
		else {
			block.setRunLaterFunction(runnable -> trigger.triggerTask(block, runnable));
			block.setTriggerPropertyFunction((propertyName, value) -> trigger.triggerProperty(block, propertyName, value, block.getMaxTimeStamp()));
		}
		block.resetWarning();
		block.setError(null);
	}

	public void initOperator(Block block) {
		BeanAndSubBeanPropertyChangeListener[] synchronizationListeners = new BeanAndSubBeanPropertyChangeListener[2];
		BeanManager[] beanManagers = new BeanManager[2];
		block.setRunTimePropertyChangeListener(evt -> {
			if (!block.isRunTimePropertyChangeListener())
				return;
			switch (evt.getPropertyName()) {
			case "consumesAllDataBeforeDying":
			case "startPriority":
			case "stopPriority":
			case "remoteProperty":
			case "enable":
			case "conflict":
			case "launchable":
				return;
			case "name":
				blockNameChanged(block);
				return;
			case "synchronizeProperties":
				if ((Boolean) evt.getNewValue()) {
					populateSynchronization(block, beanManagers, synchronizationListeners);
					addSynchronization(beanManagers, synchronizationListeners);
					beanManagers[0].copyTo(beanManagers[1]);
				} else {
					beanManagers[1].removeBeanAndSubBeanPropertyChangeListener(synchronizationListeners[1]);
					beanManagers[1] = null;
					synchronizationListeners[1] = null;
					beanManagers[0].removeBeanAndSubBeanPropertyChangeListener(synchronizationListeners[0]);
					beanManagers[0] = null;
					synchronizationListeners[0] = null;
				}
				break;
			default:
				Log.info("RUNTIME: Required restart of " + block + " due to run mode change");
				if (block.operator instanceof Scenario) // No other way... Scheduler remains blocked otherwise due to restTime if re-enable
					((Scenario) block.operator).stop();
				else
					triggerTask(block, () -> {
						Log.info("RUNTIME: Relaunch " + block + " due to run mode change");
						destroyBlock(block);
						initBlock(block, DiagramScheduler.this);
						initOperator(block);
						Log.info("RUNTIME: Relaunch done " + block);
						this.fd.blockModeChanged(block, ModificationType.CHANGE);
					});
			}
		});
		try {
			if (block.getProcessMode() == ProcessMode.ISOLATED || block.getProcessMode() == ProcessMode.REMOTE) {
				// Log.info("rmi operator, main thread pid: " + ProcessHandle.current().pid());
				RemoteOperator remoteOperator = RemoteOperator.createRemoteOperator(block, true);
				if (remoteOperator.connectionError()) {
					block.setWarning("Cannot connect to the RMI process. It could be a port already in use error. See general console for more information");
					block.setAlive(false);
				} else
					block.setAlive(true);
				block.setRemoteOperator(remoteOperator);
				if (block.isSynchronizeProperties()) {
					populateSynchronization(block, beanManagers, synchronizationListeners);
					addSynchronization(beanManagers, synchronizationListeners);
				}
				// remoteOperator.loadBeanProperties(ObjectEditor.getBeanDesc(op).name);
				remoteOperator.birth();
			} else {
				OperatorManager.invokeBirth(block.operator);
				block.setAlive(true);
			}
		} catch (Throwable e) {
			if (e instanceof InvocationTargetException) {
				Throwable te = ((InvocationTargetException) e).getTargetException();
				if (te instanceof Exception)
					e = te;
			}
			block.resetWarning();
			block.setException(e);
			block.setAlive(false);
			if (e instanceof InterruptedException)
				Log.error("The initialisation of " + block.getName() + " was interrupted");
			else {
				String em = ScenarioManager.getErrorMessage(e);
				if (em == null)
					e.printStackTrace();
				else
					Log.error(block.getName() + ": " + em);
				e.printStackTrace();
			}
			destroyBlock(block);
		}
	}

	static void destroyBlock(Block block) {
		// Log.error("destroy block: " + block);
		Object operator = block.operator;
		if (operator instanceof EvolvedOperator)
			((EvolvedOperator) operator).setTrigger(null);
		else {
			block.setRunLaterFunction(null);
			block.setTriggerPropertyFunction(null);
		}
		block.setAlive(false);
		block.setRunTimePropertyChangeListener(null);
		try {
			RemoteOperator remoteOperator = block.getRemoteOperator();
			if (remoteOperator != null) {
				remoteOperator.death();
				if (remoteOperator instanceof VisuableSchedulableRemoteOperator)
					((VisuableSchedulableRemoteOperator) remoteOperator).setAnimated(false);
				remoteOperator.destroy();
				block.setRemoteOperator(null);
			} else
				OperatorManager.invokeDeath(operator);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		block.clearTemporaryBuffer();
		block.reset();
	}

	public static boolean isCloneable(Class<?> type) {
		if (type.isArray()) {
			type = type.getComponentType();
			if (type.isArray())
				type = type.getComponentType();
		}
		Class<?> superClass = type;
		do {
			if (CLONER_FACTORY.get(superClass) != null)
				return true;
			Class<?>[] inter = superClass.getInterfaces();
			for (Class<?> class1 : inter)
				if (CLONER_FACTORY.get(class1) != null)
					break;
		} while ((superClass = superClass.getSuperclass()) != null);

		return true;
	}

	private static Function<Object, Object> loadCloneMethod(Class<? extends Object> c) {
		Class<?> superClass = c;
		Function<Object, Object> clonemethod = null;
		do {
			Class<?>[] inter = superClass.getInterfaces();
			clonemethod = CLONER_FACTORY.get(superClass);
			if (clonemethod != null)
				break;
			for (Class<?> class1 : inter) {
				clonemethod = CLONER_FACTORY.get(class1);
				if (clonemethod != null)
					break;
			}
		} while (clonemethod == null && (superClass = superClass.getSuperclass()) != null);
		if (clonemethod != null)
			CLONER_FACTORY.put(c, clonemethod);
		return clonemethod;
	}

	/** Process a block with the given inputs and their corresponding timeStamps and timeOfIssue
	 *
	 * @param block the block to process
	 * @param inputsData the inputs data to be processed
	 * @param timeStamps the timeStamps for each inputs datas
	 * @param toithe timeOfIssue for each inputs datas
	 * @return true if there is at least one output for this block */
	static boolean processBlock(Block block, Object[] inputsData, long[] timeStamps, long[] timeOsIssue) {
		try {
			long maxTimeStamp = Long.MIN_VALUE;
			for (long ts : timeStamps)
				if (ts > maxTimeStamp)
					maxTimeStamp = ts;
			block.setInputTs(timeStamps);
			block.setTimeOfIssue(timeOsIssue);
			int nbPropertyInput = block.getNbPropertyAsInput();
			int ip = 0;
			for (; ip < nbPropertyInput; ip++) {
				Object propertyValue = inputsData[ip];
				if (propertyValue != null)
					try {
						block.getPropertyMethodHandles(ip).invoke(propertyValue);
						block.setInputError(ip, null);
					} catch (Exception e) {
						String baseErrorMessage = block.getName() + ": error while setting the property as input: ";
						if (e instanceof BrokerPropertyException)
							Log.error(baseErrorMessage + e.getClass().getSimpleName() + ": " + e.getMessage());
						else {
							Log.error(baseErrorMessage);
							e.printStackTrace();
						}
						block.setInputError(ip, e.getMessage());
					}
			}
			int l = ip;
			for (; l < inputsData.length; l++)
				if (inputsData[l] != null) {
					l = 0;
					break;
				}
			if (l != 0)
				return false;
			int nbStaticInput = block.getNbStaticInput();
			int nbDynamicInput = block.getNbDynamicInput();
			int nbVarArgsInput = block.getNbVarArgsInput();
			ip += nbStaticInput;
			if (nbDynamicInput != 0) {
				Object[] additionalInputs = ((EvolvedOperator) block.operator).getAdditionalInputs();
				if (additionalInputs == null || additionalInputs.length != nbDynamicInput) {
					additionalInputs = new Object[nbDynamicInput]; // Ok que a l'init ou si changement de taille des entrées additionnelles
					((EvolvedOperator) block.operator).setAdditionalInputs(additionalInputs);
				}
				for (int i = 0; i < additionalInputs.length; i++)
					additionalInputs[i] = inputsData[ip++];
			}
			List<BlockOutput> blockOutputs = block.getTriggerableOutputs();
			RemoteOperator remoteOp = block.getRemoteOperator();
			Object val;
			if (nbVarArgsInput >= 0) { // Cas avec des varArgs
				Object[] varArgs = block.getVarArgsBuff();
				// Log.info("Block: " + block + " Varargs: " + Arrays.toString(varArgs));
				for (int j = 0; j < varArgs.length; j++)
					varArgs[j] = inputsData[ip++];
				ip -= nbStaticInput + nbDynamicInput + nbVarArgsInput;
				if (remoteOp != null) {
					Object[] staticInputs = new Object[nbStaticInput + 1];
					for (int index = 0; index < nbStaticInput; index++)
						staticInputs[index] = inputsData[ip++];
					staticInputs[staticInputs.length - 1] = varArgs;
					boolean[] linkedStaticOutput = new boolean[block.getNbStaticOutput()];
					for (int i = 0; i < linkedStaticOutput.length; i++)
						linkedStaticOutput[i] = blockOutputs.get(i).getLinkInputs() != null;
					val = remoteOp.process(staticInputs, nbDynamicInput != 0 ? ((EvolvedOperator) block.operator).getAdditionalInputs() : null, linkedStaticOutput);
				} else {
					MethodHandle ph = block.getProcessHandler();
					if (nbStaticInput == 0)
						val = ph.invoke(varArgs);
					else if (nbStaticInput == 1)
						val = ph.invoke(inputsData[ip++], varArgs); // id[0] avant
					else if (nbStaticInput == 2)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 3)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 4)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 5)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 6)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 7)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 8)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 9)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], varArgs);
					else if (nbStaticInput == 10)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 11)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 12)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 13)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 14)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else if (nbStaticInput == 15)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], varArgs);
					else {
						Object[] staticInputs = new Object[nbStaticInput + 1]; // Ok que si très gros block
						for (int index = 0; index < nbStaticInput; index++)
							staticInputs[index] = inputsData[ip++];
						staticInputs[staticInputs.length - 1] = varArgs;
						val = ph.invokeWithArguments(staticInputs);
					}
				}
			} else { // Cas sans varArgs
				ip -= nbStaticInput + nbDynamicInput;
				if (remoteOp != null) {
					Object[] staticInputs;
					if (nbStaticInput != block.getNbInput()) {
						staticInputs = new Object[nbStaticInput]; // Ok que si très gros block
						for (int index = 0; index < nbStaticInput; index++)
							staticInputs[index] = inputsData[ip++];
					} else
						staticInputs = inputsData;
					boolean[] linkedStaticOutput = new boolean[block.getNbStaticOutput()];
					for (int i = 0; i < linkedStaticOutput.length; i++)
						linkedStaticOutput[i] = blockOutputs.get(i).getLinkInputs() != null;
					val = remoteOp.process(staticInputs, nbDynamicInput != 0 ? ((EvolvedOperator) block.operator).getAdditionalInputs() : null, linkedStaticOutput);
				} else {
					MethodHandle ph = block.getProcessHandler();
					if (nbStaticInput == 0)
						val = ph.invoke();
					else if (nbStaticInput == 1)
						val = ph.invoke(inputsData[ip]);
					else if (nbStaticInput == 2)
						val = ph.invoke(inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 3)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 4)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 5)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 6)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 7)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 8)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 9)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 10)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 11)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 12)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 13)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 14)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else if (nbStaticInput == 15)
						val = ph.invoke(inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++],
								inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip++], inputsData[ip]);
					else {
						Object[] staticInputs;
						if (nbStaticInput != block.getNbInput()) {
							staticInputs = new Object[nbStaticInput]; // Ok que si très gros block
							for (int index = 0; index < nbStaticInput; index++)
								staticInputs[index] = inputsData[ip++];
						} else
							staticInputs = inputsData;
						val = ph.invokeWithArguments(staticInputs);
					}
				}
			}
			long timeOfIssue = System.currentTimeMillis();
			block.consume();
			// if(beginTime == -1 && block.getConsume() == 1) {
			// beginTime = System.currentTimeMillis();
			// }
			// int nbIt = 1000000;
			// if(block.getConsume() == nbIt) {
			// double seconde = (System.currentTimeMillis() - beginTime) / 1000.0;
			// Log.info("end Time: " + seconde + "s " + String.format("%.4f", seconde / 20.0 / nbIt * 1000000) + "μs/T");
			// }

			block.setError(null);

			Object[] outputValues = block.getOutputBuff();
			if (outputValues.length == 0)
				return false;
			int i = 0;
			long[] outputValuesTs = block.getOutputBuffTs();
			long[] outputValuesToi = block.getOutputBuffToi();
			int nbStaticOutput = block.getNbStaticOutput();
			boolean isAtLeastOnOutputs = false;
			if (remoteOp != null) {
				if (val == null)
					return false;
				Object[] outputs = (Object[]) val;
				if (outputs.length == 0)
					return false;
				if (blockOutputs.get(0).getLinkInputs() != null) {
					outputValues[0] = outputs[0];
					outputValuesTs[0] = maxTimeStamp;
					outputValuesToi[0] = timeOfIssue;
					isAtLeastOnOutputs = true;
					i++;
				} else if (!block.isProcessMethodReturnVoid()) {
					outputValues[0] = null;
					i++;
				}
				for (; i < nbStaticOutput; i++) {
					BlockOutput output = blockOutputs.get(i);
					if (output.getLinkInputs() != null && (val = outputs[i]) != null) {
						outputValues[i] = val;
						outputValuesTs[i] = maxTimeStamp;
						outputValuesToi[i] = timeOfIssue;
						isAtLeastOnOutputs = true;
					} else
						outputValues[i] = null;
				}
			} else {
				if (val != null && /* outputValues != null && */ blockOutputs.get(0).getLinkInputs() != null) {
					outputValues[0] = val;// clone(val);
					outputValuesTs[0] = maxTimeStamp;
					outputValuesToi[0] = timeOfIssue;
					isAtLeastOnOutputs = true;
					i++;
				} else if (!block.isProcessMethodReturnVoid()) {
					outputValues[0] = null;
					i++;
				}
				for (; i < nbStaticOutput; i++) {
					BlockOutput output = blockOutputs.get(i);
					if (output.getLinkInputs() != null && (val = output.getMethodHandle().invoke()) != null) {
						outputValues[i] = val;// clone(val);
						outputValuesTs[i] = maxTimeStamp;
						outputValuesToi[i] = timeOfIssue;
						isAtLeastOnOutputs = true;
					} else
						outputValues[i] = null;
				}
			}
			return isAtLeastOnOutputs;
		} catch (Throwable e) {
			block.setException(e);
			Log.error("failed to process: " + block.getName());
			e.printStackTrace();
			return false;
		}
	}

	public static void showBufferOverflow(IOComponent block, BlockInput input) {
		if (input.getBuffer().getNbMissedData() == 1)
			Log.error(block + ": BufferOverflow on input: " + input + ", max buffer size: " + input.getBuffer().getMaxSize());
	}
}
