/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.scheduler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import io.beanmanager.tools.Tuple;
import io.scenarium.core.tools.Triple;
import io.scenarium.flow.Block;
import io.scenarium.flow.BlockInput;
import io.scenarium.flow.FlowDiagram;
import io.scenarium.flow.IOComponent;
import io.scenarium.flow.IOData;
import io.scenarium.flow.IOLinks;
import io.scenarium.flow.ManagingCpuUsageListener;
import io.scenarium.flow.internal.Log;

public class MonoCoreDiagramScheduler extends DiagramScheduler implements ManagingCpuUsageListener {
	protected HashMap<Block, Object[]> blocksEntries = new HashMap<>();
	protected LinkedBlockingDeque<Long> timePointerConsumptionStack;
	private final MonoScheduler ms;
	private CPUUsageTask cpuUsageTask;

	public MonoCoreDiagramScheduler(FlowDiagram fd, CountDownLatch startLock, String name) {
		super(fd);
		this.timePointerConsumptionStack = new LinkedBlockingDeque<>();
		this.ms = new MonoScheduler(this, fd, startLock, name);
		launch(this.ms);
		managingCpuChanged(fd.isManagingCpuUsage());
		fd.addManagingCpuUsage(this);
		waitMonoSchedulerStart(startLock);
	}

	@Override
	public void removeManagedBlock(Block block) {
		MonoScheduler mds = this.ms;
		if (mds != null)
			mds.removeManagedBlock(block);
	}

	@Override
	public void addManagedBlock(Block block, boolean waitBlockStart) {
		MonoScheduler mds = this.ms;
		if (mds != null)
			mds.addManagedBlock(block);
	}

	@Override
	public boolean isStarted() {
		MonoScheduler ms = this.ms;
		return ms != null && ms.isStarted;
	}

	@Override
	public boolean isAlive() {
		MonoScheduler ms = this.ms;
		return ms != null && ms.isStarted && !ms.dead;
	}

	@Override
	public boolean isDead() {
		MonoScheduler ms = this.ms;
		return ms == null || ms.dead;
	}

	protected void launch(Thread t) {
		MonoScheduler ms = this.ms;
		if (ms != null)
			ms.start();
	}

	@Override
	public void managingCpuChanged(boolean managingCpu) {
		if (managingCpu && this.cpuUsageTask == null)
			try {
				this.cpuUsageTask = CPUUsageTask.createCPUUsageTask(this.ms.getId(), this.fd);
			} catch (UnsupportedOperationException e) {
				Log.error(e.getMessage());
				return;
			}
		else if (!managingCpu && this.cpuUsageTask != null) {
			this.cpuUsageTask.close();
			this.cpuUsageTask = null;
		}
	}

	@Override
	public void onStart(Object source, Runnable runnable) {
		addOnEvent(source, mds -> {
			if (mds.isStarted)
				runnable.run();
			else
				mds.onStart(source, runnable);
		});
	}

	@Override
	public void onResume(Object source, Runnable runnable) {
		addOnEvent(source, mds -> mds.onResume(source, runnable));
	}

	@Override
	public void onPause(Object source, Runnable runnable) {
		addOnEvent(source, mds -> mds.onPause(source, runnable));
	}

	@Override
	public void onStop(Block source, Runnable runnable) {
		addOnEvent(source, mds -> mds.onStop(source, runnable));
	}

	private void addOnEvent(Object source, Consumer<MonoScheduler> consumer) {
		MonoScheduler mds = this.ms;
		if (mds == null || mds.dead)
			throw new IllegalAccessError("The scheduler is dead. The started event cannot therefore occur");
		else
			consumer.accept(mds);
	}

	@Override
	public void start() {
		MonoScheduler mds = this.ms;
		if (mds != null && !mds.dead)
			mds.consumeOnStartTasksAndTriggerProperty();
	}

	@Override
	public void resume() {
		super.resume();
		MonoScheduler mds = this.ms;
		if (mds != null && !mds.dead)
			mds.consumeOnResumeTasks();
	}

	@Override
	public void pause() {
		super.pause();
		MonoScheduler mds = this.ms;
		if (mds != null && !mds.dead)
			mds.consumeOnPauseTasks();
	}

	@Override
	public void preStop() {
		MonoScheduler mds = this.ms;
		if (mds != null && !mds.dead)
			mds.consumeOnStopTasks();
	}

	@Override
	public void stop() {
		this.fd.removeManagingCpuUsageListener(this);
		if (this.cpuUsageTask != null) {
			this.cpuUsageTask.close();
			this.cpuUsageTask = null;
		}
		if (this.ms != null) {
			this.ms.lock.lock();
			try {
				this.ms.dead = true;
				this.ms.incomingDataOrTaskSignals.signalAll();
				this.ms.outputsConsumed.signalAll();
			} finally {
				this.ms.lock.unlock();
			}
			try {
				this.ms.join(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (this.ms.isAlive()) {
				BiConsumer<List<Block>, Runnable> timeOutTask = this.fd.getTimeOutTask();
				Thread stopTimeOutThread = null;
				if (timeOutTask != null) {
					stopTimeOutThread = new Thread(() -> timeOutTask.accept(this.ms.blockingBlock, () -> {
						this.ms.isInterrupted = true;
						this.ms.interrupt();
					}));
					stopTimeOutThread.start();
				}
				try {
					this.ms.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (stopTimeOutThread != null)
					stopTimeOutThread.interrupt();
			}
		}
		this.fd.reset();
	}

	@Override
	public void triggerProperty(Object source, String outputPropertyName, Object outputValue, long timeStamp) {
		MonoScheduler mds = this.ms;
		if (mds != null)
			mds.triggerProperty((IOComponent) source, outputPropertyName, outputValue, timeStamp);
	}

	@Override
	public void triggerOutput(Object source, Object outputValue, long timeStamp) {
		triggerPreparedOutput(source, new Object[] { outputValue }, new long[] { timeStamp });
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long timeStamp) {
		long[] timeStamps = new long[outputValues.length];
		for (int i = 0; i < timeStamps.length; i++)
			timeStamps[i] = timeStamp;
		triggerPreparedOutput(source, outputValues.clone(), timeStamps);
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long[] timeStamps) {
		triggerPreparedOutput(source, outputValues.clone(), timeStamps.clone());
	}

	private void triggerPreparedOutput(Object source, Object[] outputValues, long[] timeStamps) {
		MonoScheduler mds = this.ms;
		if (mds != null)
			mds.triggerOutput((IOComponent) source, outputValues, timeStamps);
	}

	private void waitMonoSchedulerStart(CountDownLatch startLock) {
		try {
			boolean isStarted = startLock.await(1, TimeUnit.SECONDS);
			if (!isStarted) {
				BiConsumer<List<Block>, Runnable> timeOutTask = this.fd.getTimeOutTask();
				Thread stopTimeOutThread = null;
				if (timeOutTask != null) {
					stopTimeOutThread = new Thread(() -> timeOutTask.accept(this.ms.blockingBlock, () -> {
						this.ms.isInterrupted = true;
						MonoCoreDiagramScheduler.this.ms.interrupt();
						try {
							MonoCoreDiagramScheduler.this.ms.join();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}));
					stopTimeOutThread.start();
				}
				startLock.await();
				if (stopTimeOutThread != null)
					stopTimeOutThread.interrupt();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void blockNameChanged(Block block) {}

	@Override
	protected void stackOutputAndTriggerBlock(Block component, IOLinks ioLinks, Object[] outputs, long[] outputsTs, long timeOfIssue) {
		MonoScheduler mds = this.ms;
		if (mds != null)
			mds.stackOutputAndTrigger(component, ioLinks, outputs, outputsTs, timeOfIssue);
	}

	@Override
	protected void stackPropertyAndTriggerBlock(Block comp, IOLinks ioLinks, String outputPropertyName, Object outputValue, long timeStamp, long timeOfIssue) {
		MonoScheduler mds = this.ms;
		if (mds != null)
			mds.stackPropertyAndTriggerBlock(comp, ioLinks, outputPropertyName, outputValue, timeStamp, timeOfIssue);
	}

	@Override
	public void triggerTask(Object source, Runnable task) {
		MonoScheduler mds = this.ms;
		if (mds != null)
			mds.triggerTask(source, task);
		else
			task.run();
	}

	// @Override
	// public void offerTask(Object source, Runnable task) {
	// MonoScheduler mds = this.ms;
	// if (mds != null)
	// mds.offerTask(source, task);
	// else
	// task.run();
	// }

	@Override
	public boolean holdLock(Object source) {
		MonoScheduler mds = this.ms;
		return mds == null ? true : mds.holdLock(source);
	}
}

class MonoScheduler extends Thread {
	private final Object onTaskLock = new Object();
	private HashMap<Object, ArrayList<Runnable>> onStartTasks;
	private HashMap<Object, ArrayList<Runnable>> onPauseTasks;
	private HashMap<Object, ArrayList<Runnable>> onResumeTasks;
	private HashMap<Object, ArrayList<Runnable>> onStopTasks;

	private final BlockRingBuffer todoBlocks = new BlockRingBuffer();
	private final HashMap<IOComponent, Triple<Object[], long[], long[]>> blockBuffs = new HashMap<>();
	private final MonoCoreDiagramScheduler scds;
	private final List<Block> blocks;
	private IOComponent todoOutputSource;
	private Object[] todoOutputValues;
	private long[] todoTimeStamps;

	private IOComponent todoPropertySource;
	private String todoPropertyName;
	private Object todoPropertyValue;
	private long todoPropertyTimeStamp;

	volatile boolean dataRead = false;
	protected boolean isStarted = false;

	protected boolean dead = false;
	final ReentrantLock lock;
	final Condition incomingDataOrTaskSignals;
	final Condition outputsConsumed;
	final Condition propertyConsumed;
	final Condition schedulerAvailable;
	private final CountDownLatch startLock;
	protected List<Block> blockingBlock;
	public boolean isInterrupted = false;
	private long callingThreadId = -1;
	private Thread monoRemoteThread;

	public List<Tuple<Condition, Condition>> tasks = Collections.synchronizedList(new ArrayList<>());

	public MonoScheduler(MonoCoreDiagramScheduler scds, FlowDiagram fd/* , ReentrantLock ioLock */, CountDownLatch startLock, String name) {
		this.scds = scds;
		this.blocks = scds.getLaunchableBlocksAndSetCpuUsage(-2);
		this.startLock = startLock;
		this.blockingBlock = Collections.synchronizedList(new LinkedList<>());
		this.lock = /* ioLock != null ? ioLock : */new ReentrantLock();
		this.incomingDataOrTaskSignals = this.lock.newCondition();
		this.outputsConsumed = this.lock.newCondition();
		this.propertyConsumed = this.lock.newCondition();
		this.schedulerAvailable = this.lock.newCondition();
		setName("MonoCoreDiagramScheduler" + (name != null ? " of " + name : ""));
	}

	public void addManagedBlock(Block block) {
		if (this.blocks.contains(block))
			Log.error("problem");
		this.blocks.add(block);
		createBlock(block);
	}

	private void createBlock(Block block) {
		DiagramScheduler.initBlock(block, this.scds);
		this.scds.initOperator(block);
	}

	public void removeManagedBlock(Block block) {
		this.blocks.remove(block);
		DiagramScheduler.destroyBlock(block);
		if (this.onStartTasks != null)
			this.onStartTasks.remove(block);
		if (this.onPauseTasks != null)
			this.onPauseTasks.remove(block);
		if (this.onResumeTasks != null)
			this.onResumeTasks.remove(block);
		if (this.onStopTasks != null)
			this.onStopTasks.remove(block);
	}

	public void onStart(Object source, Runnable runnable) {
		pushTask(this.onStartTasks, taskMap -> this.onStartTasks = taskMap, source, runnable);
	}

	public void onResume(Object source, Runnable runnable) {
		pushTask(this.onResumeTasks, taskMap -> this.onResumeTasks = taskMap, source, runnable);
	}

	public void onPause(Object source, Runnable runnable) {
		pushTask(this.onPauseTasks, taskMap -> this.onPauseTasks = taskMap, source, runnable);
	}

	public void onStop(Object source, Runnable runnable) {
		pushTask(this.onStopTasks, taskMap -> this.onStopTasks = taskMap, source, runnable);
	}

	private void pushTask(HashMap<Object, ArrayList<Runnable>> onEventTasks, Consumer<HashMap<Object, ArrayList<Runnable>>> mapProvider, Object source, Runnable task) {
		synchronized (this.onTaskLock) {
			if (onEventTasks == null) {
				onEventTasks = new HashMap<>();
				mapProvider.accept(onEventTasks);
			}
			ArrayList<Runnable> taskList = onEventTasks.get(source);
			if (taskList == null) {
				taskList = new ArrayList<>();
				onEventTasks.put(source, taskList);
			}
			taskList.add(task);
		}
	}

	public void consumeOnStartTasksAndTriggerProperty() {
		this.blocks.forEach(Block::triggerPropertyAsOutput);
		consumeOnEventTasks(this.onStartTasks);
	}

	public void consumeOnResumeTasks() {
		consumeOnEventTasks(this.onResumeTasks);
	}

	public void consumeOnPauseTasks() {
		consumeOnEventTasks(this.onPauseTasks);
	}

	public void consumeOnStopTasks() {
		consumeOnEventTasks(this.onStopTasks);
	}

	private void consumeOnEventTasks(HashMap<Object, ArrayList<Runnable>> tasks) {
		if (tasks != null)
			tasks.forEach((source, taskList) -> taskList.forEach(task -> triggerTask(source, task)));
	}

	public static void main(String[] args) throws InterruptedException {
		ReentrantLock lock = new ReentrantLock();
		Condition incomingDataOrTaskSignals = lock.newCondition();
		ArrayList<Tuple<Condition, Condition>> tasks = new ArrayList<>();
		new Thread(() -> {
			lock.lock();
			try {
				try {
					incomingDataOrTaskSignals.await();
				} catch (InterruptedException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

				while (!tasks.isEmpty()) {
					Tuple<Condition, Condition> task = tasks.remove(0);
					// lock.lock();
					// try {
					try {
						Log.info("signal ready for son");
						task.getFirst().signal();
						Log.info("await son");
						task.getSecond().await();
						Log.info("all done");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					// } finally {
					// lock.unlock();
					// }
				}

			} finally {
				lock.unlock();
			}
		}).start();
		new Thread(() -> {
			lock.lock();
			try {
				Condition schedulerAvailable = lock.newCondition();
				Condition taskCompleted = lock.newCondition();
				tasks.add(new Tuple<>(schedulerAvailable, taskCompleted));
				incomingDataOrTaskSignals.signal();
				try {
					Log.info("await father");
					schedulerAvailable.await();
					Log.info("father ready");
					Log.info("good");
					taskCompleted.signal();
					Log.info("signal done for father");
				} catch (InterruptedException e) {}

			} finally {
				lock.unlock();
			}
		}).start();
	}

	@Override
	public void run() {
		this.blocks.sort((a, b) -> Integer.compare(b.getStartPriority(), a.getStartPriority()));
		for (Block block : this.blocks) {
			this.blockingBlock.add(block);
			createBlock(block);
			this.blockingBlock.remove(block);
		}
		this.startLock.countDown();
		this.isStarted = true;
		dead: while (!this.dead) {
			IOComponent source = null;
			Object[] outputValues = null;
			long[] timeStamps = null;
			Object propertyValue = null;
			String propertyName = null;
			long propertyTimeStamp = 0;
			this.lock.lock();
			try {
				while (true) {
					while (!this.pendingsTasks.isEmpty())
						this.pendingsTasks.pop().run();
					while (!this.tasks.isEmpty()) {
						Tuple<Condition, Condition> task = this.tasks.remove(0);
						try {
							task.getFirst().signal();
							task.getSecond().await();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					if (this.todoOutputSource != null || this.todoPropertySource != null)
						break;
					if (this.dead) // Just before the await, because consumeTasks can unlock this part and the signal incomingDataOrTaskSignals.signalAll of can be missed
						break dead;
					this.incomingDataOrTaskSignals.await();
				}
				if (this.todoOutputSource != null) {
					source = this.todoOutputSource;
					this.todoOutputSource = null;
					outputValues = this.todoOutputValues;
					this.todoOutputValues = null;
					timeStamps = this.todoTimeStamps;
					this.outputsConsumed.signal();
				} else {
					source = this.todoPropertySource;
					this.todoPropertySource = null;
					propertyValue = this.todoPropertyValue;
					this.todoPropertyValue = null;
					propertyName = this.todoPropertyName;
					propertyTimeStamp = this.todoPropertyTimeStamp;
					this.propertyConsumed.signal();
				}
				this.schedulerAvailable.signal();
			} catch (InterruptedException e) {
				break;
			} finally {
				this.lock.unlock();
			}
			if (isInterrupted())
				break;
			if (outputValues != null) {
				if (consumeOutputs(source, outputValues, timeStamps))
					break;
			} else if (consumeProperty(source, propertyName, propertyValue, propertyTimeStamp))
				break;
		}
		this.lock.lock();
		try {
			this.outputsConsumed.signalAll();
			this.propertyConsumed.signalAll();
			this.incomingDataOrTaskSignals.signalAll();
			this.schedulerAvailable.signalAll();
		} finally {
			this.lock.unlock();
		}
		while (!this.tasks.isEmpty()) {
			Tuple<Condition, Condition> task = this.tasks.remove(0);
			this.lock.lock();
			try {
				task.getFirst().signal();
				task.getSecond().await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				this.lock.unlock();
			}
		}
		this.blocks.sort((a, b) -> Integer.compare(b.getStopPriority(), a.getStopPriority()));
		for (Block block : this.blocks) {
			this.blockingBlock.add(block);
			if (this.isInterrupted)
				Thread.currentThread().interrupt();
			DiagramScheduler.destroyBlock(block);
			this.blockingBlock.remove(block);
		}
		this.onStartTasks = null;
		this.onPauseTasks = null;
		this.onResumeTasks = null;
		this.onStopTasks = null;
	}

	public boolean consumeProperty(IOComponent source, String outputPropertyName, Object outputValue, long timeStamp) {
		this.scds.stackPropertyAndTrigger(source, outputPropertyName, outputValue, timeStamp);
		while (!this.todoBlocks.isEmpty()) {
			if (isInterrupted())
				return true;
			Block block = this.todoBlocks.popFirst();
			if ((!this.dead || block.isConsumesAllDataBeforeDying()) && runBlock(block))
				stackOutputsAndTrigger(block);
		}
		return false;
	}

	public boolean consumeOutputs(IOComponent source, Object[] outputValues, long[] timeStamps) {
		this.scds.stackOutputsAndTrigger(source, outputValues, timeStamps);
		while (!this.todoBlocks.isEmpty()) {
			if (isInterrupted())
				return true;
			Block block = this.todoBlocks.popFirst();
			if ((!this.dead || block.isConsumesAllDataBeforeDying()) && runBlock(block))
				stackOutputsAndTrigger(block);
		}
		return false;
	}

	private boolean runBlock(Block block) {
		Triple<Object[], long[], long[]> blockBuff = this.blockBuffs.get(block);
		Object[] inputs;
		long[] ts;
		long[] toi;
		if (blockBuff == null || blockBuff.getFirst().length != block.getNbInput()) {
			inputs = new Object[block.getNbInput()];
			ts = new long[block.getNbInput()];
			toi = new long[block.getNbInput()];
			this.blockBuffs.put(block, new Triple<>(inputs, ts, toi));
		} else {
			inputs = blockBuff.getFirst();
			ts = blockBuff.getSecond();
			toi = blockBuff.getThird();
		}
		List<BlockInput> blockInputs = block.getInputs();
		for (int i = 0; i < inputs.length; i++) {
			IOData ioData = blockInputs.get(i).pop();
			if (ioData != null) {
				inputs[i] = ioData.getValue();
				ts[i] = ioData.getTs();
				toi[i] = ioData.getToi();
			} else {
				inputs[i] = null;
				ts[i] = -1;
				toi[i] = -1;
			}
		}
		long oldCallingThreadId = this.callingThreadId;
		this.callingThreadId = Thread.currentThread().getId();
		boolean hasReturnValue = DiagramScheduler.processBlock(block, inputs, ts, toi);
		this.callingThreadId = oldCallingThreadId;
		return hasReturnValue;
	}

	private void stackOutputsAndTrigger(Block block) {
		this.scds.stackOutputsAndTrigger(block, block.getOutputBuff(), block.getOutputBuffTs());
	}

	protected void stackPropertyAndTriggerBlock(Block comp, IOLinks ioLinks, String outputPropertyName, Object outputValue, long timeStamp, long timeOfIssue) {
		if (DiagramScheduler.stackProperty(comp, ioLinks, outputPropertyName, outputValue, timeStamp, timeOfIssue))
			this.todoBlocks.push((Block) ioLinks.getComponent());
	}

	protected void stackOutputAndTrigger(Block block, IOLinks ioLinks, Object[] outputs, long[] outputsTs, long timeOfIssue) {
		if (DiagramScheduler.stackOutputs(block, ioLinks, outputs, outputsTs, timeOfIssue))
			this.todoBlocks.push(block);
	}

	private void triggerPropertyFromMono(IOComponent source, String outputPropertyName, Object outputValue, long timeStamp) {
		this.scds.stackPropertyAndTrigger(source, outputPropertyName, outputValue, timeStamp);
		while (!this.todoBlocks.isEmpty()) {
			Block block = this.todoBlocks.popFirst();
			if (runBlock(block))
				stackOutputsAndTrigger(block);
		}
	}

	public void triggerProperty(IOComponent source, String outputPropertyName, Object outputValue, long timeStamp) {
		Thread currentThread = Thread.currentThread();
		if (currentThread == this || this.monoRemoteThread == currentThread)
			triggerPropertyFromMono(source, outputPropertyName, outputValue, timeStamp);
		else if (source instanceof Block && ((Block) source).getRemoteOperator() != null && ((Block) source).getRemoteOperator().isFromThreadedBlock(this.callingThreadId)) {
			Thread oldMonoThread = this.monoRemoteThread;
			this.monoRemoteThread = Thread.currentThread();
			triggerPropertyFromMono(source, outputPropertyName, outputValue, timeStamp);
			this.monoRemoteThread = oldMonoThread;
		} else {
			if (currentThread.isInterrupted()) {
				Thread.currentThread().interrupt();
				return;
			}
			if (this.lock.isHeldByCurrentThread())
				consumeProperty(source, outputPropertyName, outputValue, timeStamp);
			else
				propertySend: while (!this.dead) {
					this.lock.lock();
					try {
						while (!isInterrupted()) {
							if (this.dead)
								return;
							if (this.todoPropertySource == null) {
								this.todoPropertySource = source;
								this.todoPropertyName = outputPropertyName;
								this.todoPropertyValue = outputValue;
								this.todoPropertyTimeStamp = timeStamp;
								this.incomingDataOrTaskSignals.signal();
								if (!this.propertyConsumed.await(1000, TimeUnit.MILLISECONDS))
									Log.error("fail to trigger property");

								break propertySend;
							}
							this.schedulerAvailable.await();
						}
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
						return;
					} finally {
						this.lock.unlock();
					}
				}
		}
	}

	public void triggerOutput(IOComponent source, Object[] outputValues, long[] timeStamps) {
		Thread currentThread = Thread.currentThread();
		if (currentThread == this || this.monoRemoteThread == currentThread)
			triggerOutputFromMono(source, outputValues, timeStamps);
		else if (source instanceof Block && ((Block) source).getRemoteOperator() != null && ((Block) source).getRemoteOperator().isFromThreadedBlock(this.callingThreadId)) {
			Thread oldMonoThread = this.monoRemoteThread;
			this.monoRemoteThread = Thread.currentThread();
			triggerOutputFromMono(source, outputValues, timeStamps);
			this.monoRemoteThread = oldMonoThread;
		} else {
			if (currentThread.isInterrupted()) {
				Thread.currentThread().interrupt();
				return;
			}
			if (this.lock.isHeldByCurrentThread())
				consumeOutputs(source, outputValues, timeStamps);
			else
				dataSend: while (!this.dead) {
					this.lock.lock();
					try {
						while (!isInterrupted()) {
							if (this.dead)
								return;
							if (this.todoOutputSource == null) {
								this.todoOutputSource = source;
								this.todoOutputValues = outputValues;
								this.todoTimeStamps = timeStamps;
								this.incomingDataOrTaskSignals.signal();
								this.outputsConsumed.await(); // Blocked until the property is take into account to ensure that an other task scheduled after is executed before
								break dataSend;
							}
							this.schedulerAvailable.await();
						}
					} catch (InterruptedException e) {
						Thread.currentThread().interrupt();
						return;
					} finally {
						this.lock.unlock();
					}
				}
		}
	}

	private void triggerOutputFromMono(IOComponent source, Object[] outputValues, long[] timeStamps) {
		this.scds.stackOutputsAndTrigger(source, outputValues, timeStamps);
		while (!this.todoBlocks.isEmpty()) {
			Block block = this.todoBlocks.popFirst();
			if (runBlock(block))
				stackOutputsAndTrigger(block);
		}
	}

	private final LinkedList<Runnable> pendingsTasks = new LinkedList<>();

	public void offerTask(Object source, Runnable task) {
		Thread currentThread = Thread.currentThread();
		if (currentThread == this || this.monoRemoteThread == currentThread || this.lock.isHeldByCurrentThread())
			task.run();
		else {
			this.lock.lock();
			try {
				this.pendingsTasks.push(task);
				this.incomingDataOrTaskSignals.signal();
			} finally {
				this.lock.unlock();
			}
		}
	}

	public void triggerTask(Object source, Runnable task) {
		Thread currentThread = Thread.currentThread();
		if (currentThread == this || this.monoRemoteThread == currentThread || this.lock.isHeldByCurrentThread())
			task.run();
		else if (source instanceof Block && ((Block) source).getRemoteOperator() != null && ((Block) source).getRemoteOperator().isFromThreadedBlock(this.callingThreadId)) {
			Thread oldMonoThread = this.monoRemoteThread;
			this.monoRemoteThread = Thread.currentThread();
			task.run();
			this.monoRemoteThread = oldMonoThread;
		} else {
			this.lock.lock();
			try {
				if (this.dead)
					task.run();
				else {
					Condition schedulerAvailable = this.lock.newCondition();
					Condition taskCompleted = this.lock.newCondition();
					Tuple<Condition, Condition> taskToOffer = new Tuple<>(schedulerAvailable, taskCompleted);
					this.tasks.add(taskToOffer);
					this.incomingDataOrTaskSignals.signal();
					try {
						schedulerAvailable.await();
						task.run();
						taskCompleted.signal();
					} catch (InterruptedException e) {
						if (!this.tasks.remove(taskToOffer))
							taskCompleted.signal();
						else
							task.run();
						Thread.currentThread().interrupt();
					}
				}
			} finally {
				this.lock.unlock();
			}
		}
	}

	public boolean holdLock(Object source) {
		Thread currentThread = Thread.currentThread();
		if (currentThread == this || this.monoRemoteThread == currentThread || this.lock.isHeldByCurrentThread())
			return true;
		else if (source instanceof Block && ((Block) source).getRemoteOperator() != null && ((Block) source).getRemoteOperator().isFromThreadedBlock(this.callingThreadId))
			return true;
		return false;
	}
}

class BlockRingBuffer {
	private int maxSize;
	private int front;
	private int rear;
	private int bufLen;
	private Block[] blocks;

	public BlockRingBuffer() {
		this.front = 0;
		this.rear = 0;
		this.bufLen = 0;
		this.maxSize = 20;
		this.blocks = new Block[this.maxSize];
	}

	public BlockRingBuffer(int maxStackSize, BlockRingBuffer buffer) {
		this.front = 0;
		this.rear = 0;
		this.bufLen = 0;
		this.maxSize = maxStackSize;
		this.blocks = new Block[this.maxSize];
		if (buffer != null) {
			Block block;
			while ((block = buffer.popFirst()) != null)
				push(block);
		}
	}

	public boolean isEmpty() {
		return this.bufLen == 0;
	}

	public Block popFirst() {
		if (this.bufLen == 0)
			return null;
		Block block = this.blocks[this.front];
		this.blocks[this.front] = null;
		this.front += 1;
		if (this.front == this.maxSize)
			this.front = 0;
		this.bufLen--;
		return block;
	}

	public void push(Block block) {
		if (this.bufLen == this.maxSize) {
			BlockRingBuffer newBlockRingBuffer = new BlockRingBuffer(this.maxSize * 2, this);
			this.maxSize = newBlockRingBuffer.maxSize;
			this.front = newBlockRingBuffer.front;
			this.rear = newBlockRingBuffer.rear;
			this.bufLen = newBlockRingBuffer.bufLen;
			this.blocks = newBlockRingBuffer.blocks;
		}
		this.bufLen++;
		this.blocks[this.rear] = block;
		this.rear += 1;
		if (this.rear == this.maxSize)
			this.rear = 0;
	}
}

//// Record
// class OnConsumeTask {
// public final Object source;
// public final Runnable runnable;
//
// public OnConsumeTask(Object source, Runnable runnable) {
// this.source = source;
// this.runnable = runnable;
// }
// }