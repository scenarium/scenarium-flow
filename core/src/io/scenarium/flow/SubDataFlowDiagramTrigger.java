package io.scenarium.flow;

import io.scenarium.core.filemanager.scenariomanager.ScenarioTrigger;

public interface SubDataFlowDiagramTrigger extends ScenarioTrigger {
	public EvolvedOperator getFatherOperator();
}
