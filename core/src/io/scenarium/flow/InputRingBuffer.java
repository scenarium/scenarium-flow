/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow;

import java.util.Arrays;

import io.scenarium.flow.internal.Log;

public class InputRingBuffer {
	public static final int DEFAULT_MAX_STACK_SIZE = 100;

	public static void main(String[] args) {
		InputRingBuffer cb = new InputRingBuffer(3, false);
		cb.push(2, 2, Integer.valueOf(5));
		cb.push(4, 2, Integer.valueOf(8));
		cb.push(6, 2, Integer.valueOf(10));
		cb.push(8, 2, Integer.valueOf(15));
		cb.push(9, 2, Integer.valueOf(19));
		Log.info("" + cb.popFirst());
		Log.info("" + cb.popFirst());
		Log.info("" + cb.popFirst());
		Log.info("" + cb.popFirst());
		Log.info("" + cb.popFirst());
		cb.push(17, 2, Integer.valueOf(17));
		Log.info("" + cb.popLast());
		Log.info("" + cb.popFirst());
		Log.info("" + cb.popLast());
		Log.info("" + cb.popFirst());
		cb.push(17, 2, Integer.valueOf(19));
		Log.info("" + cb.popLast());
		Log.info("" + cb.isEmpty());
	}

	private final int maxSize;
	private final boolean eraseDataIfFull;
	private int front = 0;
	private int rear = 0;
	private int bufLen = 0;
	private final Object[] datas;
	private final long[] ts;
	private final long[] toi;
	private final IOData ioData = new IOData();

	private long nbMissedData;

	public InputRingBuffer() {
		this(DEFAULT_MAX_STACK_SIZE, false);
	}

	public InputRingBuffer(int maxSize, boolean eraseDataIfFull) {
		this.front = 0;
		this.rear = 0;
		this.bufLen = 0;
		this.nbMissedData = 0;
		this.datas = new Object[maxSize];
		this.maxSize = maxSize;
		this.eraseDataIfFull = eraseDataIfFull;
		this.ts = new long[maxSize];
		this.toi = new long[maxSize];
	}

	public InputRingBuffer(int maxStackSize, boolean eraseDataIfFull, InputRingBuffer buffer, Class<?> type) {
		this.front = 0;
		this.rear = 0;
		this.bufLen = 0;
		this.nbMissedData = 0;
		this.maxSize = maxStackSize;
		this.eraseDataIfFull = eraseDataIfFull;
		this.datas = new Object[this.maxSize];
		this.ts = new long[this.maxSize];
		this.toi = new long[this.maxSize];
		if (buffer != null) {
			IOData data;
			while ((data = buffer.popFirst()) != null)
				push(data.getTs(), data.getToi(), data.getValue());
		}
	}

	public void clear() {
		this.front = 0;
		this.rear = 0;
		this.bufLen = 0;
		this.nbMissedData = 0;
		Arrays.fill(this.datas, null);
	}

	public int getMaxSize() {
		return this.maxSize;
	}

	public long getNbMissedData() {
		return this.nbMissedData;
	}

	public boolean isEmpty() {
		return this.bufLen == 0;
	}

	public boolean isFull() {
		return this.bufLen == this.maxSize;
	}

	public IOData popFirst() {
		if (this.bufLen == 0)
			return null;
		this.ioData.setValue(this.datas[this.front]);
		this.datas[this.front] = null;
		this.ioData.setTs(this.ts[this.front]);
		this.ioData.setToi(this.toi[this.front]);
		this.front += 1;
		if (this.front == this.maxSize)
			this.front = 0;
		this.bufLen--;
		return this.ioData;
	}

	public IOData popLast() {
		if (this.bufLen == 0)
			return null;
		this.bufLen--;
		this.rear = this.rear == 0 ? this.maxSize - 1 : this.rear - 1;
		this.ioData.setValue(this.datas[this.rear]);
		this.datas[this.rear] = null;
		this.ioData.setTs(this.ts[this.rear]);
		this.ioData.setToi(this.toi[this.rear]);
		return this.ioData;
	}

	public boolean push(long ts, long toi, Object data) {
		if (isFull()) {
			this.nbMissedData++;
			if (this.eraseDataIfFull) {
				this.datas[this.front] = null;
				this.front += 1;
				if (this.front == this.maxSize)
					this.front = 0;
				this.datas[this.rear] = data;
				this.ts[this.rear] = ts;
				this.toi[this.rear] = toi;
				this.rear += 1;
				if (this.rear == this.maxSize)
					this.rear = 0;
			}
			return true;
		}
		this.bufLen++;
		this.datas[this.rear] = data;
		this.ts[this.rear] = ts;
		this.toi[this.rear] = toi;
		this.rear += 1;
		if (this.rear == this.maxSize)
			this.rear = 0;
		return false;
	}

	public boolean setEraseDataIfFull() {
		return this.eraseDataIfFull;
	}

	public int size() {
		return this.bufLen;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < this.datas.length; i++)
			sb.append(this.datas[i] + " ");
		return sb.toString();
	}
}
