/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow;

import javax.swing.event.EventListenerList;

import io.beanmanager.BeanPropertiesInheritanceLimit;

@BeanPropertiesInheritanceLimit
public abstract class Input extends IO {
	private final EventListenerList listeners = new EventListenerList();
	protected Link link;

	public Input(Class<?> varType, String name) {
		super(varType, name);
	}

	public void addLinkChangeListener(InputLinkChangeListener listener) {
		this.listeners.add(InputLinkChangeListener.class, listener);
	}

	public void addLinkChangeListenerIfNotPresent(InputLinkChangeListener listener) {
		for (InputLinkChangeListener l : this.listeners.getListeners(InputLinkChangeListener.class))
			if (l == listener)
				return;
		addLinkChangeListener(listener);
	}

	public void removeLinkChangeListener(InputLinkChangeListener listener) {
		this.listeners.remove(InputLinkChangeListener.class, listener);
	}

	private void fireLinkChange(Link link, ModificationType modificationType) {
		for (InputLinkChangeListener listener : this.listeners.getListeners(InputLinkChangeListener.class))
			listener.linkChange(link, modificationType);
	}

	public abstract IOComponent getComponent();

	public Link getLink() {
		return this.link;
	}

	public InputLinkChangeListener[] getLinkChangeListener() {
		return this.listeners.getListeners(InputLinkChangeListener.class);
	}

	// TODO Attention en FlowIO, bien impacter le diagram au dessus si changement de lien
	public boolean setLink(Link link) {
		if (link != null) {
			Output output = link.getOutput();
			Input input = link.getInput();
			if (output instanceof FlowDiagramOutput && output.getType() == null)
				((FlowDiagramOutput) output).setType(input.getType());
			if (input instanceof FlowDiagramInput/* && input.getType() == null */)
				((FlowDiagramInput) input).setType(output.getType());
			if (!link.getInput().getType().isAssignableFrom(link.getOutput().getType()))
				return false; // Invalid link
		} else if (this.link != null) {
			Input input = this.link.getInput();
			if (input instanceof FlowDiagramInput)
				((FlowDiagramInput) input).setType(null);
			Output output = this.link.getOutput();
			if (output instanceof FlowDiagramOutput && output.inputs.length <= 1)
				((FlowDiagramOutput) output).setType(null);
		}
		Link previousLink = this.link;
		this.link = link;
		if (previousLink != null)
			previousLink.getOutput().removeLink(previousLink);
		if (link != null)
			link.getOutput().addLink(link);
		if (previousLink != null && link != null)
			fireLinkChange(link, ModificationType.CHANGE); // Pas bon, faut info sur les deux... -> Pas obligatoirement pour l'ihm
		else if (previousLink != null)
			fireLinkChange(previousLink, ModificationType.DELETE);
		else if (link != null)
			fireLinkChange(link, ModificationType.NEW);
		return true;
	}
}
