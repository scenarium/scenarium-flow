/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow;

public class IOLink {
	private final int outputIndex;
	private final Input input;
	private final boolean needToCopy;

	public IOLink(int outputIndex, Input input, boolean needToCopy) {
		this.outputIndex = outputIndex;
		this.input = input;
		this.needToCopy = needToCopy;
	}

	public Input getInput() {
		return this.input;
	}

	public int getOutputIndex() {
		return this.outputIndex;
	}

	public boolean isNeedToCopy() {
		return this.needToCopy;
	}

	@Override
	public String toString() {
		return "oi: " + this.outputIndex + " copy: " + this.needToCopy;
	}
}