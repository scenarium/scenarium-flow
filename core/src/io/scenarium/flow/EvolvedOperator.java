/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.swing.event.EventListenerList;

import io.beanmanager.BeanRenameListener;
import io.beanmanager.editors.container.BeanEditor;
import io.scenarium.core.filemanager.scenariomanager.StructChangeListener;
import io.scenarium.flow.internal.Log;
import io.scenarium.flow.operator.DeclaredInputChangeListener;
import io.scenarium.flow.operator.DeclaredOutputChangeListener;
import io.scenarium.flow.operator.RemoteBlock;
import io.scenarium.flow.scheduler.MonoCoreDiagramScheduler;
import io.scenarium.flow.scheduler.MultiCoreDiagramScheduler;
import io.scenarium.flow.scheduler.Trigger;

import javafx.scene.layout.Region;

/** EvolvedOperator is the base class for all scenarium operators who needs to perform advanced operations. It allow among other things to:
 * <ul>
 * <li>Manage inputs ans outputs</li>
 * <li>Get timestamp and time of issue information of datas</li>
 * <li>Trigger datas with custom timeStamp</li>
 * <li>Get information from the block link to this operator</li>
 * <li>Prevent any concurrent method call</li>
 * <li>Restart and reload the structure of the block</li>
 * <li>Customize the HMI of the block and allow human interaction</li>
 * <li>Listen events as structure change, block name change, etc...</li>
 * </ul>
 *
 * By default, except if user don't want depedency with Scenarium, all operators must extend this class. An operator must also have a process method to be a valid Scenarium operator. To check if an
 * operator is valid for Scenarium, you can test it with {@link operatorManager#isOperator(Object) this} method. To add an operator to Scenarium, you can use {@link operatorManager#addOperator(Class)
 * this} method */
public abstract class EvolvedOperator {
	protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	private final EventListenerList listeners = new EventListenerList();
	private Block block;
	private RemoteBlock remoteBlock;
	private int id = -1;
	private Trigger trigger;
	private Object[] additionalInputs;
	private int earlyTrigger;
	private int lateTrigger;
	private HashMap<BeanRenameListener, BeanRenameListener> beanRenameListenersMap;
	private Object[] dataOutputBuffer;
	private long[] timeStampOutputBuffer;

	/** Adds a BeanRenameListener which will be notified whenever the name of the block changes.
	 *
	 * @param listener the listener to register */
	public void addBlockNameChangeListener(BeanRenameListener listener) {
		RemoteBlock remoteBlock = this.remoteBlock;
		if (this.block != null) {
			BeanRenameListener brl = (oldBeanDesc, beanDesc) -> {
				Block block = this.block;
				if (block != null && beanDesc.bean == block.getOperator())
					listener.beanRename(oldBeanDesc, beanDesc);
			};
			HashMap<BeanRenameListener, BeanRenameListener> beanRenameListenersMap = this.beanRenameListenersMap;
			if (beanRenameListenersMap == null)
				beanRenameListenersMap = new HashMap<>();
			beanRenameListenersMap.put(listener, brl);
			this.beanRenameListenersMap = beanRenameListenersMap;
			BeanEditor.addStrongRefBeanRenameListener(brl);
		} else if (remoteBlock != null)
			remoteBlock.addBlockNameChangeListener(listener);
	}

	/** Adds a DeclaredInputChangeListener which will be notified whenever the structure inputs changes.
	 *
	 * @param listener the listener to register */
	public void addDeclaredInputChangeListener(DeclaredInputChangeListener listener) {
		RemoteBlock remoteBlock = this.remoteBlock;
		if (remoteBlock != null)
			remoteBlock.addDeclaredInputChangeListener(listener);
		else
			this.listeners.add(DeclaredInputChangeListener.class, listener);
	}

	/** Adds a DeclaredOutputChangeListener which will be notified whenever the structure outputs changes.
	 *
	 * @param listener the listener to register */
	public void addDeclaredOutputChangeListener(DeclaredOutputChangeListener listener) {
		RemoteBlock remoteBlock = this.remoteBlock;
		if (remoteBlock != null)
			remoteBlock.addDeclaredOutputChangeListener(listener);
		else
			this.listeners.add(DeclaredOutputChangeListener.class, listener);
	}

	/** Add listener for input links changed.
	 *
	 * @param listener the listener to register */
	public void addInputLinksChangeListener(InputLinksChangeListener listener) {
		Block block = this.block;
		if (block != null)
			block.addInputLinksChangeListener(listener);
		else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.addInputLinksChangeListener(listener);
			else
				throw new IllegalAccessError("This operator is not link to a block");
		}
	}

	/** Adds a StructChangeListener which will be notified whenever the structure of inputs and/or outputs changes.
	 *
	 * @param listener the listener to register */
	public void addStructChangeListener(StructChangeListener listener) {
		Block block = this.block;
		if (block != null)
			this.listeners.add(StructChangeListener.class, listener);
		else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.addStructChangeListener(listener);
			else
				throw new IllegalAccessError("This operator is not link to a block");
		}
	}

	/** Add listener for variadic input changed.
	 *
	 * @param listener the listener to register */
	public void addVarArgsInputChangeListener(VarArgsInputChangeListener listener) {
		Block block = this.block;
		if (block != null)
			block.addVarArgsInputChangeListener(listener);
		else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.addVarArgsInputChangeListener(listener);
			else
				throw new IllegalAccessError("This operator is not link to a block");
		}
	}

	/** Method call at startup */
	public abstract void birth() throws Exception;

	/** Method call at the end */
	public abstract void death() throws Exception;

	/** Fires declared input changed events with the array of input names and input types. This method is called in the call of {@link #updateInputs(String[], Class[]) updateInputs} and after the
	 * fires of structure changed events
	 *
	 * @param names the names array of new inputs
	 *
	 * @param types the types array of new outputs */
	public void fireDeclaredInputChanged(String[] names, Class<?>[] types) {
		RemoteBlock remoteBlock = this.remoteBlock;
		if (remoteBlock != null)
			remoteBlock.fireDeclaredInputChanged(names, types);
		else
			for (DeclaredInputChangeListener listener : this.listeners.getListeners(DeclaredInputChangeListener.class))
				listener.declaredInputChanged(names, types);
	}

	/** Fires declared output changed events with the array of output names and output types. This method is called in the call of {@link #updateOutputs(String[], Class[]) updateOutputs} and after the
	 * fires of structure changed events
	 *
	 * @param names the names array of new outputs
	 *
	 * @param types the types array of new outputs */
	public void fireDeclaredOutputChanged(String[] names, Class<?>[] types) {
		RemoteBlock remoteBlock = this.remoteBlock;
		if (remoteBlock != null)
			remoteBlock.fireDeclaredOutputChanged(names, types);
		else
			for (DeclaredOutputChangeListener listener : this.listeners.getListeners(DeclaredOutputChangeListener.class))
				listener.declaredOutputChanged(names, types);
	}

	/** Fires structure changed events with the reference to the block linked to this operator or null otherwise. It will also update the bounds of the block if it exists.
	 *
	 * @param blockName */
	public void fireStructChanged() {
		Block block = this.block;
		if (block != null)
			block.updateBounds();
		for (StructChangeListener listener : this.listeners.getListeners(StructChangeListener.class))
			listener.structChanged();
		RemoteBlock remoteBlock = this.remoteBlock;
		if (remoteBlock != null)
			remoteBlock.fireStructChanged();
	}

	/** Generate a new output array that can be used to trigger datas.
	 *
	 * @return an array of outputs. It size is equal to the number of output of this block */
	public Object[] generateOuputsVector() {
		int nbOutput = getNbTriggerableOutput();
		return nbOutput == -1 ? null : new Object[nbOutput];
	}

	/** Get the array of additional dynamic inputs. These inputs can be declared with the {@link #updateInputs(String[], Class[]) updateInputs} method. This array is always updated when entering in
	 * the process method of the operator.
	 *
	 * @return the array of additional dynamic inputs. It size is equal to the number of additional dynamic inputs declared and the i-th element corresponds to the data of the i-th input */
	public Object[] getAdditionalInputs() {
		return this.additionalInputs;
	}

	/** Get the name of the block.
	 *
	 * @return the block name or null if this operator is not embedded in a block */
	public String getBlockName() {
		Block block = this.block;
		if (block != null)
			return block.getName();
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.getBlockName() : null;
	}

	/** Get the index of an input with it name.
	 *
	 * @param inputName the name of the desired input
	 *
	 * @return the index of the input */
	public int getInputIndex(String inputName) {
		Block block = this.block;
		if (block != null)
			return block.getInputIndex(inputName);
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.getInputIndex(inputName) : -1;
	}

	/** Gets the input names of the block link to this operator.
	 *
	 * @return the input names array. It size is equal to the number of block inputs and the i-th element corresponds to the name of the i-th input */
	public String[] getInputNames() {
		Block block = this.block;
		if (block != null) {
			List<BlockInput> inputs = block.getInputs();
			String[] inputNames = new String[inputs.size()];
			for (int i = 0; i < inputs.size(); i++)
				inputNames[i] = inputs.get(i).getName();
			return inputNames;
		}
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.getInputsName() : null;
	}

	/** Get the maximum timestamp of all input datas. The timestamp correspond to the computer time of the original data. This information can be generated and is propaged by Scenarium but can be
	 * override using {@link #triggerOutput(Object[], long[]) triggerOutput} method (or one of these overloaded function).
	 *
	 * @return the maximum timeStamp of all input datas */
	public long getMaxTimeStamp() {
		Block block = this.block;
		if (block != null)
			return block.getMaxTimeStamp();
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.getMaxTimeStamp() : -1;
	}

	/** Gets the number of triggerable inputs.
	 *
	 * @return the number of triggerable inputs */
	public int getNbTriggerableInput() {
		Block block = this.block;
		if (block != null)
			return block.getNbTriggerableInput();
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.getNbTriggerableInput() : -1;
	}

	/** Gets the number of triggerable inputs.
	 * @deprecated Replaced by {@link #getNbTriggerableInput() getNbTriggerableInput} method.
	 *
	 * @return the number of triggerable inputs */
	@Deprecated(since = "0.0.5.18", forRemoval = true)
	public int getNbInput() {
		return getNbTriggerableInput();
	}

	/** Gets the number of triggerable outputs.
	 *
	 * @return the number of triggerable outputs */
	public int getNbTriggerableOutput() {
		Block block = this.block;
		if (block != null)
			return block.getNbTriggerableOutput();
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.getNbTriggerableOutput() : -1;
	}

	/** Gets the number of triggerable outputs.
	 * @deprecated Replaced by {@link #getNbTriggerableOutput() getNbTriggerableOutput} method.
	 *
	 * @return the number of triggerable outputs */
	@Deprecated(since = "0.0.5.18", forRemoval = true)
	public int getNbOutput() {
		return getNbTriggerableOutput();
	}

	/** Return the node property of this operator. This method return null by default and must be overidden in the operator implementation if the user wants to use an specific HMI for this block. It
	 * can be just a representation of the state of this the and/or controls to interract with him.
	 *
	 * @return the node property */
	public Region getNode() {
		return null;
	}

	/** Get the index of an output with it name.
	 *
	 * @param outputName the name of the desired output
	 *
	 * @return the index of the output */
	public int getOutputIndex(String outputName) {
		Block block = this.block;
		if (block != null)
			return block.getOutputIndex(outputName);
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.getOutputIndex(outputName) : -1;
	}

	/** Get the names of the outputs link to block inputs.
	 *
	 * @return the outputs names array or null if this operator is not embedded in a block. It size is equal to the number of block input and the i-th element corresponds to the output name link to
	 *         the i-th input of the block */
	public String[] getOutputLinkToInputNames() { // TODO Faire un cache pour regénérer que si nécessaire
		Block block = this.block;
		if (block != null) {
			List<BlockInput> inputs = block.getInputs();
			String[] outputLinkToInputName = new String[inputs.size()];
			for (int i = 0; i < inputs.size(); i++) {
				Link link = inputs.get(i).getLink();
				if (link != null)
					outputLinkToInputName[i] = link.getOutput().getName();
			}
			return outputLinkToInputName;
		}
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.getOutputLinkToInputName() : null;
	}

	/** Get the type of the outputs link to block inputs.
	 *
	 * @return the outputs types array or null if this operator is not embedded in a block. It size is equal to the number of block input and the i-th element corresponds to the output name link to
	 *         the i-th input of the block */
	public Class<?>[] getOutputLinkToInputTypes() {
		Block block = this.block;
		if (block != null) {
			List<BlockInput> inputs = block.getInputs();
			Class<?>[] outputLinkToInputType = new Class<?>[inputs.size()];
			for (int i = 0; i < inputs.size(); i++) {
				Link link = inputs.get(i).getLink();
				if (link != null)
					outputLinkToInputType[i] = link.getOutput().getType();
			}
			return outputLinkToInputType;
		}
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.getOutputLinkToInputType() : null;
	}

	/** Gets the triggerable outputs names of the block.
	 *
	 * @return the triggerable outputs names array. It size is equal to the number of triggerable block output and the i-th element corresponds to the name of the i-th triggerable output */
	public String[] getTriggerableOutputsName() {
		Block block = this.block;
		if (block != null) {
			List<BlockOutput> outputs = block.getTriggerableOutputs();
			String[] outputNames = new String[outputs.size()];
			for (int i = 0; i < outputs.size(); i++)
				outputNames[i] = outputs.get(i).getName();
			return outputNames;
		}
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.getTriggerableOutputsName() : null;
	}

	/** Get the time of issue of the input data. The time of issue correspond to the computer time when the input data was triggered. This information is generated by Scenarium and cannot be override.
	 *
	 * @param indexOfInput the index of the desired input
	 *
	 * @return the time of issue of the input data */
	public long getTimeOfIssue(int indexOfInput) {
		Block block = this.block;
		if (block != null)
			return block.getTimeOfIssue(indexOfInput);
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.getTimeOfIssue(indexOfInput) : -1;
	}

	/** Get the timestamp of the input data. The timestamp correspond to the computer time of the original data. This information can be generated and is propaged by Scenarium but can be override
	 * using {@link #triggerOutput(Object[], long[]) triggerOutput} method (or one of these overloaded function).
	 *
	 * @param indexOfInput the index of the desired input
	 *
	 * @return the timeStamp of the input data */
	public long getTimeStamp(int indexOfInput) {
		Block block = this.block;
		if (block != null)
			return block.getTimeStamp(indexOfInput);
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.getTimeStamp(indexOfInput) : -1;
	}

	/** Get warning property. Warning means that an event has occurred and requires warning the user.
	 *
	 * @return the description of the block warning or null if there is no warning */
	public String getWarning() {
		Block block = this.block;
		if (block != null)
			return block.getWarning();
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.getWarning() : null;
	}

	/** Returns whether this operator is embedded in a block
	 *
	 * @return true if this operator is embedded in a block */
	public boolean hasBlock() {
		return this.block != null || this.remoteBlock != null;
	}

	/** Initialize the dynamique inputs and outputs */
	protected void updateIOStructure() throws Exception {}

	/** Returns whether the block is defaulting. Defaulting means that an unexpected error has occurred.
	 *
	 * @return true if the block is defaulting */
	public String getError() {
		Block block = this.block;
		if (block != null)
			return block.getError();
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.getError() : null;
	}

	/** Return true if input inputObjects are exclusive to this block or false if they are shared by multiple blocks.
	 *
	 * @return true if input inputObjects are exclusive to this block */
	protected boolean isExclusiveObjectInput() {
		if (this.remoteBlock != null)
			return true;
		Trigger trigger = this.trigger;
		return trigger != null && trigger instanceof MultiCoreDiagramScheduler && trigger instanceof MonoCoreDiagramScheduler;
	}

	/** Returns whether the property is as input.
	 *
	 * @param propertyName the name of the desired input property
	 *
	 * @return true if the property is as input */
	public boolean isPropertyAsInput(String propertyName) {
		Block block = this.block;
		if (block != null)
			return block.isPropertyAsInput(propertyName);
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.isPropertyAsInput(propertyName) : false;
	}

	/** Returns whether the property is as output.
	 *
	 * @param propertyName the name of the desired output property
	 *
	 * @return true if the property is as output */
	public boolean isPropertyAsOutput(String propertyName) {
		Block block = this.block;
		if (block != null)
			return block.isPropertyAsOutput(propertyName);
		RemoteBlock remoteBlock = this.remoteBlock;
		return remoteBlock != null ? remoteBlock.isPropertyAsOutput(propertyName) : false;
	}

	/** Returns whether the block is alive. A block alive is a block with a trigger, which implies that it is started and not yet dead. That means that the {@link #birth() birth} method has been
	 * called but not yet the {@link #death() death} method.
	 *
	 * @return true if the block is alive */
	public boolean isAlive() {
		return this.trigger != null || this.remoteBlock != null;// this.trigger != null || this.remoteBlock != null;
	}

	/** Returns whether the block is active. An active block is a block that can trigger datas. That means that their trigger is started and alive.
	 *
	 * @return true if the block is alive */
	public boolean isActive() {
		Trigger trigger = this.trigger;
		return trigger == null ? false : trigger.isStarted() && trigger.isAlive();
	}

	/** Returns whether the block is running. A running block is defined by a block with a trigger already started but not yet dead and in a running mode (not paused).
	 *
	 * @return true if the block is paused and false if the block is resumed or not running */
	public boolean isRunning() {
		Block block = this.block;
		Trigger trigger = this.trigger;
		if (block == null || trigger == null) {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				return remoteBlock.isRunning();
			return false;
		}
		return trigger.isRunning();
	}

	/** Tell if the trigger is ready to trigger datas. If not, this method increment the earlyTrigger or lateTrigger count.
	 *
	 * @param trigger the trigger link to this operator
	 *
	 * @param block the block link to this operator
	 *
	 * @return true if the trigger is ready */
	private boolean isTriggerReady(Trigger trigger, Block block) {
		if (block != null && !block.canTriggerOrBeTriggered())
			return false;
		if (!trigger.isStarted()) {
			this.earlyTrigger++;
			if (this.earlyTrigger == 1 || this.earlyTrigger == 10 || this.earlyTrigger == 100 || this.earlyTrigger == 1000 || this.earlyTrigger == 10000 || this.earlyTrigger == 100000)
				Log.error("Early trigger of: " + block.getName() + " data removed. Occurence: " + this.earlyTrigger);
		} else if (!trigger.isAlive()) {
			if (this.lateTrigger == 1 || this.lateTrigger == 10 || this.lateTrigger == 100 || this.lateTrigger == 1000 || this.lateTrigger == 10000 || this.lateTrigger == 100000)
				Log.error("Late trigger of: " + block.getName() + " data removed. Occurence: " + this.lateTrigger);
		} else
			return true;
		return false;
	}

	/** Warns Scenarium that this block need to be saved if needed. By default, Scenarium saves an operators if it changes one of these properties. But some properties can changed even if scenarium
	 * don't changed them. User can then override this method to warn scenarium that this operator need to be saved. By default, this method return false.
	 *
	 * @return true if the block need to be saved */
	public boolean needToBeSaved() {
		return false;
	}

	/** An action to execute at the start of the diagram, after the end of the execution of all the birth methods of each block, or immediately if the diagram is already started. This method must be
	 * called from the birth method.
	 *
	 * @param runnable the Runnable whose run method will be executed at the start of the diagram
	 * @throws IllegalAccessError if this operator is not link to a block or if the block is already started or dead */
	public void onStart(Runnable runnable) {
		Block block = this.block;
		Trigger trigger = this.trigger;
		if (block == null || trigger == null) {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.onStart(runnable);
			else
				throw new IllegalAccessError("This operator is not link to a " + (trigger == null ? "scheduler" : "block") + ". The started event cannot therefore occur");
		} else
			trigger.onStart(block, runnable);
	}

	/** An action to execute each time the diagram is resumed.
	 *
	 * @param runnable the Runnable whose run method will be executed when the diagram is resumed */
	public void onResume(Runnable runnable) {
		Block block = this.block;
		Trigger trigger = this.trigger;
		if (block == null || trigger == null) {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.onResume(runnable);
			else
				throw new IllegalAccessError("This operator is not link to a " + (trigger == null ? "scheduler" : "block") + ". The resumed event cannot therefore occur");
		} else
			trigger.onResume(block, runnable);
	}

	/** An action to execute each time the diagram is paused.
	 *
	 * @param runnable the Runnable whose run method will be executed when the diagram is paused */
	public void onPause(Runnable runnable) {
		Block block = this.block;
		Trigger trigger = this.trigger;
		if (block == null || trigger == null) {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.onPause(runnable);
			else
				throw new IllegalAccessError("This operator is not link to a " + (trigger == null ? "scheduler" : "block") + ". The paused event cannot therefore occur");
		} else
			trigger.onPause(block, runnable);
	}

	/** An action to execute at the stop of the diagram, before the start of the execution of all the death methods of each block. This method must be called before the death of the operator.
	 *
	 * @param runnable the Runnable whose run method will be executed at the stop of the diagram
	 * @throws IllegalAccessError if this operator is not link to a block or if the block is already dead */
	public void onStop(Runnable runnable) {
		Block block = this.block;
		Trigger trigger = this.trigger;
		if (block == null || trigger == null) {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.onStop(runnable);
			else
				throw new IllegalAccessError("This operator is not link to a " + (trigger == null ? "scheduler" : "block") + ". The stopped event cannot therefore occur");
		} else
			trigger.onStop(block, runnable);
	}

	/** Removes the given listener from the list of listeners, that are notified whenever the name of the block changes.
	 *
	 * @param listener the listener to remove */
	public void removeBlockNameChangeListener(BeanRenameListener listener) {
		BeanRenameListener brl;
		HashMap<BeanRenameListener, BeanRenameListener> beanRenameListenersMap = this.beanRenameListenersMap;
		if (beanRenameListenersMap != null && (brl = beanRenameListenersMap.remove(listener)) != null) {
			BeanEditor.removeStrongRefBeanRenameListener(brl);
			if (beanRenameListenersMap.isEmpty())
				beanRenameListenersMap = null;
		} else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.removeBlockNameChangeListener(listener);
		}
	}

	/** Removes the given listener from the list of listeners, that are notified whenever the structure inputs changes.
	 *
	 * @param listener the listener to remove */
	public void removeDeclaredInputChangeListener(DeclaredInputChangeListener listener) {
		if (this.block != null)
			this.listeners.remove(DeclaredInputChangeListener.class, listener);
		else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.removeDeclaredInputChangeListener(listener);
		}
	}

	/** Removes the given listener from the list of listeners, that are notified whenever the structure outputs changes.
	 *
	 * @param listener the listener to remove */
	public void removeDeclaredOutputChangeListener(DeclaredOutputChangeListener listener) {
		if (this.block != null)
			this.listeners.remove(DeclaredOutputChangeListener.class, listener);
		else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.removeDeclaredOutputChangeListener(listener);
		}
	}

	/** Remove listener for input links changed.
	 *
	 * @param listener the listener to remove */
	public void removeInputLinksChangeListener(InputLinksChangeListener listener) {
		Block block = this.block;
		if (block != null)
			block.removeInputLinksChangeListener(listener);
		else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.removeInputLinksChangeListener(listener);
		}
	}

	/** Removes the given listener from the list of listeners, that are notified whenever the structure of inputs and/or outputs changes.
	 *
	 * @param listener the listener to remove */
	public void removeStructChangeListener(StructChangeListener listener) {
		Block block = this.block;
		if (block != null)
			this.listeners.remove(StructChangeListener.class, listener);
		else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.removeStructChangeListener(listener);
		}
	}

	/** Remove listener for variadic input changed.
	 *
	 * @param listener the listener to remove */
	public void removeVarArgsInputChangeListener(VarArgsInputChangeListener listener) {
		Block block = this.block;
		if (block != null)
			block.removeVarArgsInputChangeListener(listener);
		else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.removeVarArgsInputChangeListener(listener);
		}
	}

	/** This method restart this operator only if it is schedule. In that case, this method call the {@link #death() death} method and then the {@link #birth() birth} method. It is preferable to use
	 * the restartLater method if this operator is sensitive to concurrent method call of {@link #birth() birth}, process and {@link #death() death} method. */
	public void restart() {
		if (isAlive())
			try {
				death();
				birth();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	/** This method restart and reload the structure of this operator. The reload is always done but the restart is only done if this operator is schedule. The restart corresponds to the call of the
	 * {@link #death() death} method and then the {@link #birth() birth} method. The reload corresponds to the call of the {@link #updateIOStructure() initStruct}, done between the {@link #death()
	 * death} and the {@link #birth() birth} method in the case of a schedule operator or alone otherwise. It is preferable to use the {@link #restartAndReloadStructLater()
	 * restartAndReloadStructLater} method if this operator is sensitive to concurrent method call of {@link #birth() birth}, process and {@link #death() death} method. */
	public void restartAndReloadStruct() {
		try {
			if (isAlive()) {
				death();
				updateIOStructure();
				birth();
			} else
				updateIOStructure();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** This method call the {@link #restartAndReloadStruct() restartAndReloadStruct} in a {@link #runLater(Runnable) runLater} method to avoid any concurrent method call of {@link #birth() birth},
	 * process and {@link #death() death} method. */
	public void restartAndReloadStructLater() {
		if (isAlive())
			runLater(() -> restartAndReloadStruct());
		else
			restartAndReloadStruct();
	}

	/** This method call the {@link #restart() restart} method in a {@link #runLater(Runnable) runLater} method to avoid any concurrent method call of {@link #birth() birth}, process and
	 * {@link #death() death} method. */
	public void restartLater() {
		if (isAlive())
			runLater(() -> restart());
		else
			restart();
	}

	/** Run the specified runnable on the block's thread if it exists or immediately otherwise. This method will return only after the execution of the Runnable. It prevent any concurrent method call
	 * of {@link #birth() birth}, process, {@link #death() death} method and the specified runnable. The process is slightly different if this method is called from the scenarium process or from an
	 * isolated/remote process block. In the first case, if this method is called from the block's thread, the runnable will be call immediately. In the second case, if this method is called from the
	 * birth, process or death method, the Runnable will be call immediately. The result is very close except in the case where another thread than the Scenarium thread calls the birth, process or
	 * death method. But this case means that we are already in a concurrent method call context and therefore this method is useless.
	 *
	 * @param runnable the Runnable whose run method will be executed on the block's thread if it exists or immediately otherwise */
	public void runLater(Runnable runnable) {
		Block block = this.block;
		Trigger trigger = this.trigger;
		if (block == null || trigger == null) {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.runLater(runnable);
			else
				runnable.run();
		} else
			trigger.triggerTask(block, runnable);
	}

	// public void offerTask(Runnable runnable) {
	// Block block = this.block;
	// Trigger trigger = this.trigger;
	// if (block == null || trigger == null) {
	// RemoteBlock remoteBlock = this.remoteBlock;
	// if (remoteBlock != null)
	// remoteBlock.runLater(runnable);
	// else
	// runnable.run();
	// } else
	// trigger.offerTask(block, runnable);
	// }

	/** Returns whether this operator is holding their lock
	 *
	 * @return true if this operator is holding their lock */
	public boolean holdLock() {
		Block block = this.block;
		Trigger trigger = this.trigger;
		if (block == null || trigger == null) {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				return remoteBlock.holdLock();
			else
				return true;
		} else
			return trigger.holdLock(block);
	}

	/** Set the array of additional dynamic inputs. Take care to not set it if this operator is already managed by Scenarium.
	 *
	 * @param additionalInputs the array of additional dynamic inputs */
	public void setAdditionalInputs(Object[] additionalInputs) {
		this.additionalInputs = additionalInputs;
	}

	/** Bind this operator to the specified block.
	 *
	 * @param block the block to bind to this operator */
	void bindBlock(Block block) {
		if (Objects.equals(this.block, block))
			return;
		if (this.block != null)
			// Log.info("unbound: " + this);
			try {
				onBlockUnbound();
			} catch (Exception e) {
				e.printStackTrace();
			}
		this.block = block;
		if (this.block != null)
			// Log.info("bound: " + this);
			try {
				onBlockBound();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	Block getBindedBlock() {
		return this.block;
	}

	/** This methods is called when a block is linked to a block */
	protected void onBlockBound() throws Exception {}

	/** This methods is called when a block is unlinked to a block */
	protected void onBlockUnbound() throws Exception {}

	/** Set defaulting property. Defaulting means that an unexpected error has occurred.
	 *
	 * @param defaulting true if the block is defaulting */
	public void setError(String error) {
		Block block = this.block;
		if (block != null)
			block.setError(error);
		else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				this.remoteBlock.setError(error);
		}
	}

	/** Set the id of this operator. This id is used by the {@link #triggerOutput(Object[], long[]) triggerOutput} method (or one of these overloaded function) to identify the source of the data if
	 * this operator is not linked to a block.
	 *
	 * @param id the id of this operator. This identifier is useful to identify the source of a data. */
	public void setId(int id) {
		this.id = id;
	}

	/** Add the property as input.
	 *
	 * @param propertyName the name of the desired input property
	 *
	 * @return true if the change is made successfully */
	public boolean addPropertyAsInput(String propertyName) {
		boolean success = false;
		Block block = this.block;
		if (block != null) {
			if (block.addPropertyAsInput(propertyName)) {
				fireStructChanged();
				success = true;
			}
		} else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null && remoteBlock.addPropertyAsInput(propertyName)) {
				fireStructChanged();
				success = true;
			}
		}
		return success;
	}

	/** Remove a property as an input of the block.
	 *
	 * @param propertyName the name of the property to be removed
	 * @return true if successfully remove the property as input, false if the property is already as input */
	public boolean removePropertyAsInput(String propertyName) {
		boolean success = false;
		Block block = this.block;
		if (block != null) {
			if (block.removePropertyAsInput(propertyName)) {
				fireStructChanged();
				success = true;
			}
		} else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null && remoteBlock.removePropertyAsInput(propertyName)) {
				fireStructChanged();
				success = true;
			}
		}
		return success;
	}

	/** Add a property as an output of the block.
	 *
	 * @param propertyName the name of the property to be addded
	 * @return true if successfully add the property as output, false if the property is already as output
	 * @throws IllegalArgumentException if it's not possible to add the given property as output */
	public boolean addPropertyAsOutput(String propertyName) {
		boolean success = false;
		Block block = this.block;
		if (block != null) {
			if (block.addPropertyAsOutput(propertyName)) {
				fireStructChanged();
				success = true;
			}
		} else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null && remoteBlock.addPropertyAsOutput(propertyName)) {
				fireStructChanged();
				success = true;
			}
		}
		return success;
	}

	/** Remove a property as an output of the block.
	 *
	 * @param propertyName the name of the property to be removed
	 * @return true if successfully remove the property as output, false if the property is already as output */
	public boolean removePropertyAsOutput(String propertyName) {
		boolean success = false;
		Block block = this.block;
		if (block != null) {
			if (block.removePropertyAsOutput(propertyName)) {
				fireStructChanged();
				success = true;
			}
		} else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null && remoteBlock.removePropertyAsOutput(propertyName)) {
				fireStructChanged();
				success = true;
			}
		}
		return success;
	}

	/** Set the remote block reference. This property is used by Scenarium if the instance of the block link to this operator and the instance of this operator is not in the same process. In this
	 * case, this property is used to define its remote block reference. Thus, most calls made by this operator to the block will be done using RMI with this remote block reference. Take care to not
	 * set it if this operator is already managed by Scenarium.
	 *
	 * @param remoteBlock the remote block reference used by RMI to get or send information from the operator to the block */
	public void setRemoteBlock(RemoteBlock remoteBlock) {
		this.block = null;
		this.remoteBlock = remoteBlock;
	}

	/** Set the trigger for the block. Take care to not set it if this operator is already schedule by Scenarium.
	 *
	 * @param trigger the trigger for this operator. */
	public void setTrigger(Trigger trigger) {
		this.earlyTrigger = 0;
		this.lateTrigger = 0;
		this.trigger = trigger;
	}

	/** Set warning property. Warning means that an event has occurred and requires warning the user.
	 *
	 * @param warning a description of the warning or null if there is no warning */
	public void setWarning(String warning) {
		Block block = this.block;
		if (block != null)
			block.setWarning(warning);
		else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.setWarning(warning);
		}
	}

	/** Get the name of this operator. This method returns {@link #getBlockName() getBlockName} if non null or {@link Object#toString() toString} otherwise.
	 *
	 * @return a string representation of the operator */
	@Override
	public String toString() {
		String name = getBlockName();
		return name != null ? name : this.id != -1 ? Integer.toString(this.id) : super.toString();
	}

	void triggerProperty(String outputPropertyName, Object outputValue) {
		Trigger trigger = this.trigger;
		if (trigger != null) {
			Block block = this.block;
			long maxTimeStamp = block == null ? System.currentTimeMillis() : block.getMaxTimeStamp();
			long[] timeStamps = new long[1];
			for (int i = 0; i < timeStamps.length; i++)
				timeStamps[i] = maxTimeStamp;
			trigger.triggerProperty(block == null ? this.id : block, outputPropertyName, outputValue, maxTimeStamp);
		} else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.triggerProperty(outputPropertyName, outputValue);
		}
	}

	/** Internal method used by FlowDiagram to get block outputs
	 * @return block outputs */
	List<BlockOutput> getBlockOutputs() {
		return this.block == null ? null : this.block.getOutputs();
	}

	/** Trigger one output with one timeStamp which correspond to the maximum timestamp of inputs or current time if there is no input.
	 *
	 * @param outputValue the output to trigger
	 *
	 * @return true if the output is successfully triggered
	 *
	 * @throws IllegalArgumentException if outputValue is null */
	public boolean triggerOutput(Object outputValue) {
		if (outputValue == null)
			throw new IllegalArgumentException(getBlockName() + ": OutputValue cannot be null");
		if (this.block != null)
			checkTriggerOutputValueType(this.block, outputValue);
		Trigger trigger = this.trigger;
		if (trigger != null) {
			Block block = this.block;
			long maxTimeStamp = block == null ? System.currentTimeMillis() : block.getMaxTimeStamp();
			long[] timeStamps = new long[1];
			for (int i = 0; i < timeStamps.length; i++)
				timeStamps[i] = maxTimeStamp;
			if (isTriggerReady(trigger, block)) {
				trigger.triggerOutput(block == null ? this.id : block, outputValue, maxTimeStamp);
				return true;
			}
		} else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.triggerOutput(outputValue);
		}
		return false;
	}

	/** Trigger one output with one custom timestamp.
	 *
	 * @param outputValue the outputValue to trigger
	 *
	 * @return true if the outputValue is successfully triggered
	 *
	 * @throws IllegalArgumentException if outputValue is null */
	public boolean triggerOutput(Object outputValue, long timeStamp) {
		if (outputValue == null)
			throw new IllegalArgumentException(getBlockName() + ": OutputValue cannot be null");
		Block block = this.block;
		if (block != null)
			checkTriggerOutputValueType(block, outputValue);
		Trigger trigger = this.trigger;
		if (trigger != null) {
			if (isTriggerReady(trigger, block)) {
				trigger.triggerOutput(block == null ? this.id : block, outputValue, timeStamp);
				return true;
			}
		} else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.triggerOutput(outputValue, timeStamp);
		}
		return false;
	}

	/** Trigger outputs with one timeStamp which correspond to the maximum timestamp of inputs, or current time if there is no input.
	 *
	 * @param outputValues the outputValues to trigger
	 *
	 * @return true if outputValues is successfully triggered
	 *
	 * @throws IllegalArgumentException if outputValues is null */
	public boolean triggerOutput(Object[] outputValues) {
		if (outputValues == null)
			throw new IllegalArgumentException(getBlockName() + ": OutputValues cannot be null");
		Block block = this.block;
		if (block != null)
			checkTriggerOutputValuesType(block, outputValues);
		Trigger trigger = this.trigger;
		if (trigger != null) {
			if (isTriggerReady(trigger, block)) {
				long maxTimeStamp = block == null ? System.currentTimeMillis() : block.getMaxTimeStamp();
				long[] timeStamps = new long[outputValues.length];
				for (int i = 0; i < timeStamps.length; i++)
					timeStamps[i] = maxTimeStamp;
				trigger.triggerOutput(block == null ? this.id : block, outputValues, maxTimeStamp);
				return true;
			}
		} else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.triggerOutput(outputValues);
		}
		return false;
	}

	/** Trigger outputs with one custom timestamp.
	 *
	 * @param outputValues the outputValues to trigger
	 *
	 * @param timeStamp the timestamp of all outputValues
	 *
	 * @return true if outputValues is successfully triggered
	 *
	 * @throws IllegalArgumentException if outputValues is null */
	public boolean triggerOutput(Object[] outputValues, long timeStamp) {
		if (outputValues == null)
			throw new IllegalArgumentException(getBlockName() + ": OutputValues cannot be null");
		Block block = this.block;
		if (block != null)
			checkTriggerOutputValuesType(block, outputValues);
		Trigger trigger = this.trigger;
		if (trigger != null) {
			if (isTriggerReady(trigger, block)) {
				trigger.triggerOutput(block == null ? this.id : block, outputValues, timeStamp);
				return true;
			}
		} else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.triggerOutput(outputValues, timeStamp);
		}
		return false;
	}

	/** Trigger outputs with multiple custom timestamp. Prefer this method instead of other triggerOutput methods to avoid object instanciation. The outputValues and timeStamps array parameters
	 * corresponds to the array of outputs with their corresponding timestamps and they must therefore have the same size.
	 *
	 * @param outputValues the outputValues to trigger
	 *
	 * @param timeStamps the timeStamps array where the i-th element corresponds to the timestamp of the i-th element in the outputValues array
	 *
	 * @return true if outputValues is successfully triggered
	 *
	 * @throws IllegalArgumentException if outputValues or timeStamps is null or if outputValues and timeStamps does not have the same size */
	public boolean triggerOutput(Object[] outputValues, long[] timeStamps) {
		if (timeStamps == null)
			throw new IllegalArgumentException(getBlockName() + ": TimeStamps cannot be null");
		if (outputValues == null)
			throw new IllegalArgumentException(getBlockName() + ": OutputValues cannot be null");
		if (timeStamps.length != outputValues.length)
			throw new IllegalArgumentException(getBlockName() + ": TimeStamps and outputValues does not have the same size");
		Block block = this.block;
		if (block != null)
			checkTriggerOutputValuesType(block, outputValues);
		Trigger trigger = this.trigger;
		if (trigger != null) {
			if (isTriggerReady(trigger, block)) {
				trigger.triggerOutput(block == null ? this.id : block, outputValues, timeStamps);
				return true;
			}
		} else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.triggerOutput(outputValues, timeStamps);
		}
		return false;
	}

	private void checkTriggerOutputValueType(Block block, Object outputValue) {
		List<BlockOutput> triggerableOutputs = block.getTriggerableOutputs();
		if (triggerableOutputs.size() != 1)
			throw new IllegalArgumentException(getBlockName() + ": The number of triggerable outputs: " + triggerableOutputs.size() + " is not equals to 1");
		BlockOutput output = block.getTriggerableOutputs().get(0);
		if (outputValue != null && !output.getType().isAssignableFrom(outputValue.getClass()))
			throw new IllegalArgumentException(getBlockName() + ": Output of type " + outputValue.getClass() + " is not compatible with the output type " + output.getType());
	}

	private void checkTriggerOutputValuesType(Block block, Object[] outputValues) {
		List<BlockOutput> triggerableOutputs = block.getTriggerableOutputs();
		if (triggerableOutputs.size() != outputValues.length)
			throw new IllegalArgumentException(
					getBlockName() + ": The number of triggerable outputs: " + triggerableOutputs.size() + " does not correspond to the size of the output vector: " + outputValues.length);
		int i = 0;
		for (BlockOutput output : triggerableOutputs) {
			Object outputValue = outputValues[i];
			if (outputValue != null && !output.getType().isAssignableFrom(outputValue.getClass()))
				throw new IllegalArgumentException(getBlockName() + ": Output number: " + i + " of type " + outputValue.getClass() + " is not compatible with the output type " + output.getType());
			i++;
		}
	}

	/** Update dynamic inputs of the block. The names and types array parameters corresponds to the array of names and types of new inputs and they must therefore have the same size
	 *
	 * @param names the names array of new inputs
	 *
	 * @param types the types array of new inputs
	 *
	 * @return true if the dynamic inputs evolved
	 *
	 * @throws IllegalArgumentException if names or types is null or if one of their elements is null or if names and types does not have the same size */
	public boolean updateInputs(String[] names, Class<?>[] types) { // Pk synchronized avant???
		Block block = this.block;
		if (block != null) {
			if (block.updateDynamicInputs(names, types)) {
				fireStructChanged();
				fireDeclaredInputChanged(names, types);
				return true;
			}
		} else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				remoteBlock.updateInputs(names, types);
			else
				fireDeclaredOutputChanged(names, types);
		}
		return false;
	}

	/** Update dynamic outputs. The names and types array parameters corresponds to the array of names and types of new outputs and they must therefore have the same size
	 *
	 * @param names The names array of new outputs
	 *
	 * @param types The types array of new outputs
	 *
	 * @return true If the dynamic outputs evolved
	 *
	 * @throws IllegalArgumentException if names or types is null or if one of their elements is null or if names and types does not have the same size */
	public boolean updateOutputs(String[] names, Class<?>[] types) {
		Block block = this.block;
		if (block != null) {
			if (block.updateDynamicOuputs(names, types)) {
				fireStructChanged();
				fireDeclaredOutputChanged(names, types);
				return true;
			}
		} else {
			RemoteBlock remoteBlock = this.remoteBlock;
			if (remoteBlock != null)
				return remoteBlock.updateOutputs(names, types);
			else
				fireDeclaredOutputChanged(names, types);
		}
		return false;
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	/** Opens an output buffer to prepare for triggering output data. This method ensures that an output data buffer and timestamp is created whose size is equal to the number of triggerable outputs.
	 * @see #getNbTriggerableOutput() getNbTriggerableOutput
	 * @param clearBuffer true to clear the previous output data buffer
	 * @return the output data buffer of the previous call to this method */
	protected Object[] openOutputBuffer(boolean clearBuffer) {
		int nbTriggerableOutput = getNbTriggerableOutput();
		Object[] oldBuffer = this.dataOutputBuffer;
		if (this.dataOutputBuffer == null || this.dataOutputBuffer.length != nbTriggerableOutput) {
			this.dataOutputBuffer = new Object[nbTriggerableOutput];
			this.timeStampOutputBuffer = new long[nbTriggerableOutput];
		} else if (clearBuffer)
			Arrays.fill(this.dataOutputBuffer, null);
		return oldBuffer;
	}

	/** Feeds one output into the output data buffer created from the call of the {@link #openOutputBuffer(boolean) openOutputBuffer}. The timestamp for this data will be equals to
	 * {@link #getMaxTimeStamp() getMaxTimeStamp}.
	 * @param index output index of the data.
	 * @param data the data to be stored at the specified position.
	 * @exception NullPointerException if the {@link #openOutputBuffer(boolean) openOutputBuffer} was never call before
	 * @exception ArrayIndexOutOfBoundsException if the index of the output is greater than the size of the output data buffer. Occurs only if the method {@link #openOutputBuffer(boolean)
	 *                openOutputBuffer} has not been called since the last call of the method {@link #updateOutputs(String[], Class[]) updateOutputs} */
	protected void setOutputData(int index, Object data) {
		this.dataOutputBuffer[index] = data;
		this.timeStampOutputBuffer[index] = getMaxTimeStamp();
	}

	/** Feeds one output into the output data buffer created from the call of the {@link #openOutputBuffer(boolean) openOutputBuffer}.
	 * @param index output index for the data.
	 * @param data the data to be stored at the specified output.
	 * @param timeStamp the timestamp of the specified data
	 * @exception NullPointerException if the {@link #openOutputBuffer(boolean) openOutputBuffer} was never call before
	 * @exception ArrayIndexOutOfBoundsException if the index of the output is greater than the size of the output data buffer. Occurs only if the method {@link #openOutputBuffer(boolean)
	 *                openOutputBuffer} has not been called since the last call of the method {@link #updateOutputs(String[], Class[]) updateOutputs} */
	protected void setOutputData(int index, Object data, long timeStamp) {
		this.dataOutputBuffer[index] = data;
		this.timeStampOutputBuffer[index] = timeStamp;
	}

	/** Feeds one output into the output data buffer created from the call of the {@link #openOutputBuffer(boolean) openOutputBuffer}. The timestamp for this data will be equals to
	 * {@link #getMaxTimeStamp() getMaxTimeStamp}.
	 * @param name output name for the data.
	 * @param data the data to be stored for the specified output.
	 * @exception NullPointerException if the {@link #openOutputBuffer(boolean) openOutputBuffer} was never call before
	 * @exception IllegalArgumentException if the name of the output is not a previously declared static or dynamic output name. */
	protected void setOutputData(String name, Object data) {
		int index = getInputIndex(name);
		this.dataOutputBuffer[index] = data;
		this.timeStampOutputBuffer[index] = getMaxTimeStamp();
	}

	/** Feeds one output into the output data buffer created from the call of the {@link #openOutputBuffer(boolean) openOutputBuffer}.
	 * @param name output name for the data.
	 * @param data the data to be stored for the specified output.
	 * @param timeStamp the timestamp of the specified data
	 * @exception NullPointerException if the {@link #openOutputBuffer(boolean) openOutputBuffer} was never call before
	 * @exception IllegalArgumentException if the name of the output is not a previously declared static or dynamic output name. */
	protected void setOutputData(String name, Object data, long timeStamp) {
		int index = getInputIndex(name);
		this.dataOutputBuffer[index] = data;
		this.timeStampOutputBuffer[index] = timeStamp;
	}

	/** Triggers the output data from the output data buffer created from the call of {@link #openOutputBuffer(boolean) openOutputBuffer}.
	 * @see #triggerOutput(Object[], long[]) triggerOutput
	 * @exception IllegalArgumentException if the {@link #openOutputBuffer(boolean) openOutputBuffer} was never call before or if the method {@link #openOutputBuffer(boolean) openOutputBuffer} has not
	 *                been called since the last call of the method {@link #updateOutputs(String[], Class[]) updateOutputs} */
	protected void flushOutputBuffer() {
		triggerOutput(this.dataOutputBuffer, this.timeStampOutputBuffer);
	}

	public RunningMode getRunningMode() {
		if (this.trigger instanceof MultiCoreDiagramScheduler)
			return RunningMode.MULTI_CORE;
		else if (this.trigger instanceof MonoCoreDiagramScheduler)
			return RunningMode.MONO_CORE;
		else
			return RunningMode.NO_THREADED;
	}

	public boolean canResetWarning() {
		return true;
	}
}
