package io.scenarium.flow;

import java.rmi.RemoteException;
import java.util.HashMap;

import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.timescheduler.SchedulerPropertyChangeListener;
import io.scenarium.flow.operator.rmi.RMIScenarioOperatorCallBackImpl;
import io.scenarium.flow.operator.rmi.ScenarioRemoteOperator;

public class RMIScenarioOperatorCallBack extends RMIOperatorCallBack implements RMIScenarioOperatorCallBackImpl {
	private HashMap<Integer, SchedulerPropertyChangeListener> schedulerPropertyChangeListenersMap;
	private static final long serialVersionUID = 1L;

	public RMIScenarioOperatorCallBack(Block block) throws RemoteException {
		super(block);
	}

	@Override
	public void addPropertyChangeListener(int listenerId) throws RemoteException {
		SchedulerPropertyChangeListener brl = state -> {
			try {
				ScenarioRemoteOperator remoteOp = (ScenarioRemoteOperator) this.block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.firePropertyChanged(state);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (this.schedulerPropertyChangeListenersMap == null)
			this.schedulerPropertyChangeListenersMap = new HashMap<>();
		this.schedulerPropertyChangeListenersMap.put(listenerId, brl);
		((Scenario) this.block.operator).getSchedulerInterface().addPropertyChangeListener(brl);
	}

	@Override
	public void removePropertyChangeListener(int listenerId) throws RemoteException {
		// TODO Auto-generated method stub

	}

}
