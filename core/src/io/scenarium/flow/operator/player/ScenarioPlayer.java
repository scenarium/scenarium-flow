package io.scenarium.flow.operator.player;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

import javax.swing.event.EventListenerList;
import javax.vecmath.Color3f;

import io.beanmanager.BeanPropertiesInheritanceLimit;
import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.basic.DynamicPathInfo;
import io.beanmanager.editors.basic.PathInfo;
import io.beanmanager.editors.container.BeanInfo;
import io.beanmanager.editors.container.DynamicBeanInfo;
import io.scenarium.core.editors.NotChangeableAtRuntime;
import io.scenarium.core.filemanager.scenariomanager.IdChangeListener;
import io.scenarium.core.filemanager.scenariomanager.InputChangeListener;
import io.scenarium.core.filemanager.scenariomanager.LocalScenario;
import io.scenarium.core.filemanager.scenariomanager.OutputChangeListener;
import io.scenarium.core.filemanager.scenariomanager.PeriodListener;
import io.scenarium.core.filemanager.scenariomanager.Scenario;
import io.scenarium.core.filemanager.scenariomanager.ScenarioColorProvider;
import io.scenarium.core.filemanager.scenariomanager.ScenarioException;
import io.scenarium.core.filemanager.scenariomanager.ScenarioManager;
import io.scenarium.core.filemanager.scenariomanager.ScenarioTrigger;
import io.scenarium.core.filemanager.scenariomanager.SourceChangeListener;
import io.scenarium.core.filemanager.scenariomanager.StartStopChangeListener;
import io.scenarium.core.filemanager.scenariomanager.TimedScenario;
import io.scenarium.core.timescheduler.SchedulerInterface;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.flow.scenario.DataFlowDiagram;
import io.scenarium.flow.scenario.ScenariumScheduler;

import javafx.beans.InvalidationListener;
import javafx.collections.ObservableList;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;

@BeanPropertiesInheritanceLimit
public class ScenarioPlayer extends EvolvedOperator
		implements InputChangeListener, OutputChangeListener, ScenarioTrigger, SourceChangeListener, StartStopChangeListener, PeriodListener, IdChangeListener {
	@PropertyInfo(index = 0)
	@PathInfo(directory = false)
	@DynamicPathInfo(filtersMethodName = "getFilters")
	@NotChangeableAtRuntime
	protected File file;

	@PropertyInfo(index = 1, editable = false)
	@BeanInfo(alwaysExtend = true, inline = true)
	@DynamicBeanInfo(possibleSubclassesMethodName = "getPossibleScenario")
	private Scenario scenario; // scenario corresponding to the file

	private final EventListenerList listeners = new EventListenerList();
	private Polygon polygon;
	private SchedulerInterface schedulerInterface;

	@Override
	public void birth() throws IOException, ScenarioException {
		if (this.scenario != null) {
			this.scenario.load(this.file, false);
			this.scenario.birth();
		}
	}

	public void process() {}

	@Override
	public void death() {
		if (this.scenario != null)
			this.scenario.death();
	}

	public String[] getFilters() {
		return Arrays.stream(ScenarioManager.getReaderFormatNames()).filter(s -> !s.startsWith(DataFlowDiagram.class.getSimpleName()))
				.sorted((a, b) -> a.startsWith(ScenariumScheduler.class.getSimpleName()) ? -1 : 1).toArray(String[]::new);
	}

	public File getFile() {
		return this.file;
	}

	public void setFile(File file) {
		if (Objects.equals(file, getFile()))
			return;
		if (this.scenario != null) {
			setScenario(null);
			if (hasBlock())
				onBlockUnbound();
			else
				unloadScenario();
			this.scenario = null;
		}
		File oldFile = this.file;
		this.file = file;
		if (hasBlock())
			onBlockBound();
		this.pcs.firePropertyChange("file", oldFile, file);
	}

	protected void loadScenario() {
		File file = getFile();
		if (file != null)
			try {
				Class<? extends Scenario> scenarioType = ScenarioManager.getScenarioType(file);
				if (scenarioType != null) {
					if (this.scenario == null || !this.scenario.getClass().equals(scenarioType))
						setScenario(scenarioType.getConstructor().newInstance());
					this.scenario.setTrigger(this);
					this.scenario.addSourceChangeListener(this);
					if (this.scenario instanceof LocalScenario)
						((LocalScenario) this.scenario).addStartStopChangeListener(this);
					if (this.scenario instanceof TimedScenario)
						((LocalScenario) this.scenario).addPeriodChangeListener(this);
					this.scenario.setSource(file);
					if (this.schedulerInterface != null)
						this.scenario.setScheduler(this.schedulerInterface);
					sourceChanged(this.scenario.hasSomeThingsToPlan());
					return;
				}
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		updateOutputs(new String[0], new Class<?>[0]);
	}

	private void unloadScenario() {
		this.scenario.dispose();
		this.scenario.setTrigger(null);
		if (this.scenario instanceof LocalScenario)
			((LocalScenario) this.scenario).removeStartStopChangeListener(this);
		if (this.scenario instanceof TimedScenario)
			((LocalScenario) this.scenario).removePeriodChangeListener(this);
	}

	@Override
	public Region getNode() {
		this.polygon = new Polygon();
		this.polygon.setFill(Color.TRANSPARENT);
		this.polygon.setStrokeLineCap(StrokeLineCap.ROUND);
		this.polygon.setStrokeLineJoin(StrokeLineJoin.ROUND);
		StackPane sp = new StackPane(this.polygon);
		sp.setMouseTransparent(true);
		sp.setFocusTraversable(false);
		InvalidationListener il = e -> updatePolyPoints(this.polygon, sp.getWidth(), sp.getHeight());
		sp.heightProperty().addListener(il);
		sp.widthProperty().addListener(il);
		idChanged(this.scenario == null ? -1 : this.scenario.getId());
		return sp;
	}

	private static void updatePolyPoints(Polygon polygon, double width, double height) {
		ObservableList<Double> points = polygon.getPoints();
		double gapx = 0.85f;
		double gapy = 0.95f;
		double roundSize = width / 4;
		double minX = (1 - gapx) * width + roundSize;
		points.clear();
		points.addAll(minX, (1 - gapy) * width + roundSize, width - roundSize, width / 2, minX, gapy * width - roundSize);
		polygon.setStrokeWidth(roundSize);
	}

	@Override
	protected void onBlockBound() {
		loadScenario();
		if (this.scenario != null) {
			this.scenario.addInputChangeListener(this);
			this.scenario.addOutputChangeListener(this);
			this.scenario.addIdChangeListener(this);
			this.scenario.updateIOStructure();
			idChanged(this.scenario.getId());
		} else
			idChanged(-1);
	}

	@Override
	protected void onBlockUnbound() {
		if (this.scenario != null) {
			unloadScenario();
			this.scenario.removeInputChangeListener(this);
			this.scenario.removeOutputChangeListener(this);
			this.scenario.removeSourceChangeListener(this);
			this.scenario.removeIdChangeListener(this);
		}
	}

	@Override
	public void inputChanged(String[] names, Class<?>[] types) {
		updateInputs(names, types);
	}

	@Override
	public void outputChanged(String[] names, Class<?>[] types) {
		updateOutputs(names, types);
	}

	@Override
	public void sourceChanged(boolean planChanged) {
		fireSourceChanged(planChanged);
	}

	public Scenario getScenario() {
		return this.scenario;
	}

	public void setScenario(Scenario scenario) {
		Scenario oldScenario = this.scenario;
		this.scenario = scenario;
		this.pcs.firePropertyChange("scenario", oldScenario, scenario);
	}

	public void setScheduler(SchedulerInterface schedulerInterface) {
		this.schedulerInterface = schedulerInterface;
		if (this.scenario != null)
			this.scenario.setScheduler(schedulerInterface);
	}

	@Override
	public void idChanged(int id) {
		Color3f color = ScenarioColorProvider.getColor(id);
		if (this.polygon != null) {
			Stop[] stops = new Stop[] { new Stop(0, new Color(color.x, color.y, color.z, 130 / 255.0)), new Stop(1, new Color(color.x, color.y, color.z, 30 / 255.0)) };
			LinearGradient lg = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
			this.polygon.setStroke(lg);
		}
	}

	public void addSourceChangeListener(SourceChangeListener listener) {
		this.listeners.add(SourceChangeListener.class, listener);
	}

	public void addSourceChangeListenerIfAbsent(SourceChangeListener listener) {
		for (SourceChangeListener l : this.listeners.getListeners(SourceChangeListener.class))
			if (l == listener)
				return;
		this.listeners.add(SourceChangeListener.class, listener);
	}

	public void removeSourceChangeListener(SourceChangeListener listener) {
		this.listeners.remove(SourceChangeListener.class, listener);
	}

	protected void fireSourceChanged(boolean planChanged) {
		for (SourceChangeListener listener : this.listeners.getListeners(SourceChangeListener.class))
			listener.sourceChanged(planChanged);
	}

	public void addPeriodChangeListenerIfAbsent(PeriodListener listener) {
		for (PeriodListener l : this.listeners.getListeners(PeriodListener.class))
			if (l == listener)
				return;
		this.listeners.add(PeriodListener.class, listener);
	}

	private void firePeriodChanged(double period) {
		for (PeriodListener listener : this.listeners.getListeners(PeriodListener.class))
			listener.periodChanged(period);
	}

	public void addStartStopChangeListenerIfAbsent(StartStopChangeListener listener) {
		for (StartStopChangeListener l : this.listeners.getListeners(StartStopChangeListener.class))
			if (l == listener)
				return;
		this.listeners.add(StartStopChangeListener.class, listener);
	}

	protected void fireStartStopTimeChanged() {
		for (StartStopChangeListener listener : this.listeners.getListeners(StartStopChangeListener.class))
			listener.startStopChanged();
	}

	@Override
	public void periodChanged(double period) {
		firePeriodChanged(period);
	}

	@Override
	public void startStopChanged() {
		fireStartStopTimeChanged();
	}

	public boolean hasSomeThingsToPlan() {
		return this.scenario == null ? false : this.scenario.hasSomeThingsToPlan();
	}

	public Class<?>[] getPossibleScenario() {
		HashSet<Class<? extends Scenario>> rs = ScenarioManager.getRegisterScenario();
		rs.remove(DataFlowDiagram.class);
		return rs.toArray(Class[]::new);
	}

	@Override
	public String getName() {
		return getBlockName();
	}
}