package io.scenarium.flow.operator.player;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

import io.beanmanager.BeanManager;
import io.beanmanager.BeanPropertiesInheritanceLimit;
import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.basic.PathInfo;
import io.beanmanager.editors.container.BeanInfo;
import io.scenarium.core.editors.NotChangeableAtRuntime;
import io.scenarium.core.filemanager.scenariomanager.ScenarioException;
import io.scenarium.core.filemanager.scenariomanager.ScenarioManager;
import io.scenarium.core.timescheduler.VisuableSchedulable;
import io.scenarium.core.tools.AnimationTimerConsumer;
import io.scenarium.core.tools.VisuableSchedulableContainer;
import io.scenarium.flow.Block;
import io.scenarium.flow.BlockInput;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.flow.FlowDiagram;
import io.scenarium.flow.FlowDiagramChangeListener;
import io.scenarium.flow.Input;
import io.scenarium.flow.Link;
import io.scenarium.flow.ModificationType;
import io.scenarium.flow.SubDataFlowDiagramTrigger;
import io.scenarium.flow.operator.rmi.VisuableSchedulableRemoteOperator;
import io.scenarium.flow.scenario.DataFlowDiagram;

@BeanPropertiesInheritanceLimit
public class SubFlowDiagram extends ScenarioPlayer implements VisuableSchedulableContainer, FlowDiagramChangeListener, SubDataFlowDiagramTrigger {
	@PropertyInfo(index = 1, editable = false, nullable = false)
	@BeanInfo(alwaysExtend = true, inline = true)
	private final DataFlowDiagram subDiagram = new DataFlowDiagram();
	protected AnimationTimerConsumer animationTimerConsumer;
	protected boolean propertyChanged;

	public SubFlowDiagram() {
		this.subDiagram.setTrigger(this);
		setScenario(this.subDiagram);
	}

	@Override
	public void birth() throws IOException, ScenarioException {
		this.subDiagram.addFlowDiagramChangeListener(this);
		this.subDiagram.birth();
	}

	@Override
	public void process() {
		this.subDiagram.process(null);
	}

	@Override
	public void death() {
		this.subDiagram.removeFlowDiagramChangeListener(this);
		this.subDiagram.death();
	}

	@Override
	public void flowDiagramChanged(FlowDiagram source, Object element, String property, ModificationType modificationType) {
		if (!"subChange".equals(property) && element instanceof Link)
			if (modificationType == ModificationType.NEW) {
				Input input = ((Link) element).getInput();
				if (input instanceof BlockInput && ((BlockInput) input).getBlock().operator instanceof VisuableSchedulable)
					registerVisuableSchedulable(((BlockInput) input).getBlock());
			} else if (modificationType == ModificationType.DELETE) {
				Input input = ((Link) element).getInput();
				if (input instanceof BlockInput && ((BlockInput) input).getBlock().operator instanceof VisuableSchedulable) {
					Block block = ((BlockInput) input).getBlock();
					boolean needToUnregister = true;
					for (BlockInput bi : block.getInputs())
						if (bi.getLink() != null) {
							needToUnregister = false;
							break;
						}
					if (needToUnregister)
						unRegisterVisuableSchedulable(((BlockInput) input).getBlock());
				}
			}
	}

	@Override
	public String[] getFilters() {
		return Arrays.stream(ScenarioManager.getReaderFormatNames()).filter(s -> s.startsWith(DataFlowDiagram.class.getSimpleName())).toArray(String[]::new);
	}

	@Override
	public void setAnimationTimer(AnimationTimerConsumer animationTimerConsumer) {
		this.subDiagram.setAnimationTimer(animationTimerConsumer);
		this.animationTimerConsumer = animationTimerConsumer;
	}

	private void registerVisuableSchedulable(Block block) {
		this.animationTimerConsumer.register(block.getRemoteOperator() != null ? (VisuableSchedulableRemoteOperator) block.getRemoteOperator() : (VisuableSchedulable) block.operator);
	}

	private void unRegisterVisuableSchedulable(Block block) {
		this.animationTimerConsumer.unRegister(block.getRemoteOperator() != null ? (VisuableSchedulableRemoteOperator) block.getRemoteOperator() : (VisuableSchedulable) block.operator);
	}

	@Override
	@PropertyInfo(index = 0)
	@PathInfo(directory = false, filters = DataFlowDiagram.READER_FORMAT_NAME)
	@NotChangeableAtRuntime
	public File getFile() {
		return this.file;
	}

	@Override
	public void setFile(File file) {
		if (Objects.equals(file, this.file))
			return;
		File oldFile = this.file;
		this.subDiagram.setFile(file);
		this.file = file;
		if (hasBlock())
			loadScenario();
		this.pcs.firePropertyChange("file", oldFile, file);
	}

	@Override
	public boolean needToBeSaved() {
		if (this.propertyChanged) {
			this.propertyChanged = false;
			return true;
		}
		return false;
	}

	@Override
	public boolean canResetWarning() {
		return false;
	}

	@Override
	public EvolvedOperator getFatherOperator() {
		return this;
	}

	public DataFlowDiagram getSubDiagram() {
		return this.subDiagram;
	}

	public void setSubDiagram(DataFlowDiagram subDiagram) {
		new BeanManager(subDiagram, "").copyTo(new BeanManager(this.subDiagram, ""));
	}
}