
/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.operator.rmi;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;

import io.beanmanager.rmi.server.RMIBeanCallBackImpl;
import io.scenarium.core.timescheduler.VisuableSchedulable;
import io.scenarium.core.tools.ViewerAnimationTimer;
import io.scenarium.core.tools.ViewerAnimationTimerSupplier;

public class RMIVisuableSchedulableOperator<T extends VisuableSchedulable> extends RMIOperator<T> implements RMIVisuableSchedulableOperatorImpl {
	private ViewerAnimationTimer viewerAnimationTimer;

	public RMIVisuableSchedulableOperator(Class<T> type, String identifier, RMIBeanCallBackImpl callBack)
			throws RemoteException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		super(type, identifier, callBack);
	}

	@Override
	public void setAnimated(long callingThreadId, boolean animated) throws RemoteException {
		// Log.error("set animated: " + bean.toString());
		mapCallingThreadIdToRmiThread(callingThreadId);
		// Log.error("start of setAnimated: " + animated + " " + ProcessHandle.current().pid());
		this.bean.setAnimated(animated);
		if (animated && this.viewerAnimationTimer == null) {
			this.viewerAnimationTimer = ViewerAnimationTimerSupplier.create();
			this.viewerAnimationTimer.register(this.bean);
			this.viewerAnimationTimer.start();
		} else if (!animated && this.viewerAnimationTimer != null) {
			this.viewerAnimationTimer.stop();
			this.viewerAnimationTimer = null;
		}
		unMapCallingThreadIdToRmiThread();
		// Log.error("set animated: " + bean.toString() + " done");
	}
}