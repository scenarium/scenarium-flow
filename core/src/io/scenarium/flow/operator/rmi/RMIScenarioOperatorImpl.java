/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.operator.rmi;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;

import io.scenarium.core.timescheduler.SchedulerPropertyChangeListener;
import io.scenarium.core.timescheduler.SchedulerState;

public interface RMIScenarioOperatorImpl extends RMIOperatorImpl {
	public void update(long callingThreadId, int taskId, long timePointer, long schedulerTimePointer) throws RemoteException;

	public boolean canTrigger(long callingThreadId, long time) throws RemoteException;

	public void addPropertyChangeListener(SchedulerPropertyChangeListener listener) throws RemoteException;

	public void removePropertyChangeListener(SchedulerPropertyChangeListener listener) throws RemoteException;

	public void firePropertyChanged(long callingThreadId, SchedulerState state)
			throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException;
}
