/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.operator.rmi;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;

import io.beanmanager.rmi.server.RMIBeanCallBackImpl;
import io.scenarium.core.filemanager.scenariomanager.ScenarioTrigger;
import io.scenarium.core.timescheduler.SchedulerInterface;
import io.scenarium.core.timescheduler.SchedulerPropertyChangeListener;
import io.scenarium.core.timescheduler.SchedulerState;
import io.scenarium.flow.operator.player.ScenarioPlayer;

public class RMIScenarioOperator<T extends ScenarioPlayer> extends RMIOperator<T> implements RMIScenarioOperatorImpl, ScenarioTrigger {
	private long timeStamp;
	private final HashMap<Integer, SchedulerPropertyChangeListener> propertyChangeListenersMap = new HashMap<>();

	public RMIScenarioOperator(Class<T> type, String identifier, RMIBeanCallBackImpl callBack)
			throws RemoteException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		super(type, identifier, callBack);
		this.bean.setScheduler(new SchedulerInterface() {
			@Override
			public long getTimeStamp() {
				return RMIScenarioOperator.this.timeStamp;
			}

			@Override
			public void addPropertyChangeListener(SchedulerPropertyChangeListener schedulerPropertyChangeListener) {
				try {
					RMIScenarioOperator.this.addPropertyChangeListener(schedulerPropertyChangeListener);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void removePropertyChangeListener(SchedulerPropertyChangeListener schedulerPropertyChangeListener) {
				try {
					RMIScenarioOperator.this.removePropertyChangeListener(schedulerPropertyChangeListener);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void setStartTime(long startTime) {}

			@Override
			public void setStopTime(long stopTime) {}

			@Override
			public boolean isRunning() {
				return true;
			}

			@Override
			public boolean stop() {
				return false;
			}

			@Override
			public void setStartLock(CountDownLatch startLock) {}
		});
	}

	@Override
	public void birth(long callingThreadId) throws Throwable {
		super.birth(callingThreadId);
		this.bean.getScenario().setTrigger(this);
	}

	@Override
	public void update(long callingThreadId, int taskIdentifier, long timePointer, long timeStamp) throws RemoteException {
		mapCallingThreadIdToRmiThread(callingThreadId);
		this.timeStamp = timeStamp;
		try {
			this.bean.getScenario().getTaskFromId(taskIdentifier).update(timePointer);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public boolean canTrigger(long callingThreadId, long time) throws RemoteException {
		mapCallingThreadIdToRmiThread(callingThreadId);
		boolean canTrigger = this.bean.getScenario().canTrigger(time);
		unMapCallingThreadIdToRmiThread();
		return canTrigger;
	}

	@Override
	public void addPropertyChangeListener(SchedulerPropertyChangeListener listener) throws RemoteException {
		int id;
		synchronized (this.propertyChangeListenersMap) {
			id = this.propertyChangeListenersMap.size();
			this.propertyChangeListenersMap.put(id, listener);
		}
		try {
			((RMIScenarioOperatorCallBackImpl) this.callBack).addPropertyChangeListener(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void removePropertyChangeListener(SchedulerPropertyChangeListener listener) throws RemoteException {
		int listenerId;
		synchronized (this.propertyChangeListenersMap) {
			listenerId = getKeyFromValue(this.propertyChangeListenersMap, listener);
		}
		if (listenerId != -1)
			try {
				((RMIScenarioOperatorCallBackImpl) this.callBack).removePropertyChangeListener(listenerId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void firePropertyChanged(long callingThreadId, SchedulerState state) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<SchedulerPropertyChangeListener> values;
		synchronized (this.propertyChangeListenersMap) {
			values = new ArrayList<>(this.propertyChangeListenersMap.values());
		}
		for (SchedulerPropertyChangeListener listener : values)
			listener.stateChanged(state);
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public boolean isAlive() {
		return true;
	}

	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public Object[] generateOuputsVector() {
		int nbOutput = getNbTriggerableOutput();
		return nbOutput == -1 ? null : new Object[nbOutput];
	}

	@Override
	public String getName() {
		return getBlockName();
	}

	@Override
	public Object[] getAdditionalInputs() {
		return this.bean.getAdditionalInputs();
	}
}