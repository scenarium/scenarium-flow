/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.operator.rmi;

import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.rmi.RemoteException;

import io.beanmanager.rmi.server.RMIBean;
import io.beanmanager.rmi.server.RMIBeanCallBackImpl;
import io.beanmanager.rmi.server.RMIBeanServer;
import io.scenarium.core.NoGuiMainThread;
import io.scenarium.core.Scenarium;
import io.scenarium.core.timescheduler.VisuableSchedulable;
import io.scenarium.flow.internal.Log;
import io.scenarium.flow.operator.player.ScenarioPlayer;
import io.scenarium.pluginManager.ModuleManager;

public class RMIOperatorServer extends RMIBeanServer {

	public static void main(String[] args) throws RemoteException {
		if (args.length < 4) {
			Log.error(
					"RMIOperatorServer must be launch with at least 4 arguments:\n-port: the port for the remote bean\n-terminateIfParentTerminate: tell if the RMIBeanServer must terminate if the father process terminate\n-verbose: Explicitly displays in the console all operations performed\n-localDir: The local directory for Scenarium");
			System.exit(-1);
		}
		try {
			Scenarium.setWorkspace(args[3]);
			Scenarium.buildInstance(new NoGuiMainThread());
			// Scenarium.initScenarium(args[3]);
			ModuleManager.birth();
			if (args.length > 4 && args[4] != null)
				ModuleManager.registerExternalModules(Path.of(args[4]));
			RMIBeanServer.launch(new String[] { args[0], args[1], args[2] }, new RMIOperatorServer());
		} catch (InstantiationException e) {
			e.printStackTrace();
		}
	}

	protected RMIOperatorServer() throws RemoteException {
		super();
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" }) // Stackoverflow Question: 62901445
	protected <T> RMIBean<T> getRMIObject(Class<T> type, String identifier, RMIBeanCallBackImpl callBack)
			throws RemoteException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		if (VisuableSchedulable.class.isAssignableFrom(type))
			return new RMIVisuableSchedulableOperator(type, identifier, callBack);
		else if (ScenarioPlayer.class.isAssignableFrom(type))
			return new RMIScenarioOperator(type, identifier, callBack);
		else
			return new RMIOperator<>(type, identifier, callBack);
	}

	@Override
	protected RMIBeanServer getRMIServer() throws RemoteException {
		return new RMIOperatorServer();
	}
}
