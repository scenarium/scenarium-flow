package io.scenarium.flow.operator.rmi;

import java.rmi.RemoteException;

public interface RMIScenarioOperatorCallBackImpl extends RMIOperatorCallBackImpl {
	/**** Listeners methods *******/
	void addPropertyChangeListener(int listenerId) throws RemoteException;

	void removePropertyChangeListener(int listenerId) throws RemoteException;
}
