/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.operator.rmi;

import java.beans.IntrospectionException;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import io.beanmanager.BeanDesc;
import io.beanmanager.BeanManager;
import io.beanmanager.BeanRenameListener;
import io.beanmanager.rmi.server.RMIBean;
import io.beanmanager.rmi.server.RMIBeanCallBackImpl;
import io.scenarium.core.filemanager.scenariomanager.StructChangeListener;
import io.scenarium.flow.Block;
import io.scenarium.flow.BlockOutput;
import io.scenarium.flow.EvolvedOperator;
import io.scenarium.flow.IllegalInputArgument;
import io.scenarium.flow.InputLinksChangeListener;
import io.scenarium.flow.VarArgsInputChangeListener;
import io.scenarium.flow.operator.DeclaredInputChangeListener;
import io.scenarium.flow.operator.DeclaredOutputChangeListener;
import io.scenarium.flow.operator.OperatorManager;
import io.scenarium.flow.operator.RemoteBlock;

public class RMIOperator<T> extends RMIBean<T> implements RMIOperatorImpl, RemoteBlock {
	private MethodHandle processMethodHandle;
	private long mainThreadId;
	private Class<?>[] inputsClass = new Class<?>[0];
	private Class<?>[] additionalInputsClass = new Class<?>[0];
	private Object[] staticOutputsArray;
	private MethodHandle[] staticOutputMethodHandles;
	private boolean isProcessMethodReturnVoid;
	private final HashMap<Integer, BeanRenameListener> beanRenameListenersMap = new HashMap<>();
	private final HashMap<Integer, StructChangeListener> structChangeListenersMap = new HashMap<>();
	private final HashMap<Integer, VarArgsInputChangeListener> varArgsInputChangeListenersMap = new HashMap<>();
	private final HashMap<Integer, InputLinksChangeListener> inputLinksChangeListenersMap = new HashMap<>();
	private final HashMap<Integer, DeclaredInputChangeListener> declaredInputChangeListenersMap = new HashMap<>();
	private final HashMap<Integer, DeclaredOutputChangeListener> declaredOutputChangeListenersMap = new HashMap<>();
	private final ConcurrentHashMap<Integer, Runnable> runLaterRunnableMap = new ConcurrentHashMap<>();
	private final AtomicInteger runLaterTaskIdCpt = new AtomicInteger(0);
	private final HashMap<Integer, Runnable> onStartTaskMap = new HashMap<>();
	private final HashMap<Integer, Runnable> onResumeTaskMap = new HashMap<>();
	private final HashMap<Integer, Runnable> onPauseTaskMap = new HashMap<>();
	private final HashMap<Integer, Runnable> onStopTaskMap = new HashMap<>();

	private final ArrayList<SerializableWrapper> serializableWrapperPool = new ArrayList<>();

	private final ConcurrentHashMap<Thread, Long> callingThreadId = new ConcurrentHashMap<>();

	public RMIOperator(Class<T> type, String identifier, RMIBeanCallBackImpl callBack)
			throws RemoteException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		super(type, identifier, callBack);
		try {
			Block block = new Block(this.bean, null, false);
			this.staticOutputsArray = new Object[block.getNbStaticOutput()];
			this.staticOutputMethodHandles = new MethodHandle[block.getNbStaticOutput()];
			List<BlockOutput> outputs = block.getOutputs();
			for (int i = 0; i < this.staticOutputMethodHandles.length; i++)
				this.staticOutputMethodHandles[i] = outputs.get(i).getMethodHandle();
			this.isProcessMethodReturnVoid = block.isProcessMethodReturnVoid();
			// if (this.bean instanceof EvolvedOperator) add this.block = null; in setRemoteBlock
			// ((EvolvedOperator) this.bean).setBlock(null);
		} catch (IllegalInputArgument e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addBlockNameChangeListener(BeanRenameListener listener) {
		int id = this.beanRenameListenersMap.size();
		this.beanRenameListenersMap.put(id, listener);
		try {
			((RMIOperatorCallBackImpl) this.callBack).addBlockNameChangeListener(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addDeclaredInputChangeListener(DeclaredInputChangeListener listener) {
		int id;
		synchronized (this.declaredInputChangeListenersMap) {
			id = this.declaredInputChangeListenersMap.size();
			this.declaredInputChangeListenersMap.put(id, listener);
		}
		try {
			((RMIOperatorCallBackImpl) this.callBack).addDeclaredInputChangeListener(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addDeclaredOutputChangeListener(DeclaredOutputChangeListener listener) {
		int id;
		synchronized (this.declaredOutputChangeListenersMap) {
			id = this.declaredOutputChangeListenersMap.size();
			this.declaredOutputChangeListenersMap.put(id, listener);
		}
		try {
			((RMIOperatorCallBackImpl) this.callBack).addDeclaredOutputChangeListener(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addInputLinksChangeListener(InputLinksChangeListener listener) {
		int id;
		synchronized (this.inputLinksChangeListenersMap) {
			id = this.inputLinksChangeListenersMap.size();
			this.inputLinksChangeListenersMap.put(id, listener);
		}
		try {
			((RMIOperatorCallBackImpl) this.callBack).addInputLinksChangeListener(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addStructChangeListener(StructChangeListener listener) {
		int id;
		synchronized (this.structChangeListenersMap) {
			id = this.structChangeListenersMap.size();
			this.structChangeListenersMap.put(id, listener);
		}
		try {
			((RMIOperatorCallBackImpl) this.callBack).addStructChangeListener(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addVarArgsInputChangeListener(VarArgsInputChangeListener listener) {
		int id;
		synchronized (this.varArgsInputChangeListenersMap) {
			id = this.varArgsInputChangeListenersMap.size();
			this.varArgsInputChangeListenersMap.put(id, listener);
		}
		try {
			((RMIOperatorCallBackImpl) this.callBack).addVarArgsInputChangeListener(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void birth(long callingThreadId) throws Throwable {
		mapCallingThreadIdToRmiThread(callingThreadId);
		if (this.bean instanceof EvolvedOperator)
			((EvolvedOperator) this.bean).setRemoteBlock(this);
		for (Method method : this.bean.getClass().getMethods())
			if (method.getName().equals("process"))
				try {
					method.setAccessible(true);
					this.processMethodHandle = MethodHandles.lookup().unreflect(method).bindTo(this.bean);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
		OperatorManager.invokeBirth(this.bean);
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void death(long callingThreadId) throws Throwable {
		mapCallingThreadIdToRmiThread(callingThreadId);
		OperatorManager.invokeDeath(this.bean);
		if (this.bean instanceof EvolvedOperator)
			((EvolvedOperator) this.bean).setRemoteBlock(null);
		this.processMethodHandle = null;
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void fireBlockNameChanged(long callingThreadId, String name, String local, String oldName, int id) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		BeanRenameListener beanRenameListener;
		synchronized (this.beanRenameListenersMap) {
			beanRenameListener = this.beanRenameListenersMap.get(id);
		}
		if (beanRenameListener != null)
			beanRenameListener.beanRename(new BeanDesc<>(this.bean, name, local), new BeanDesc<>(this.bean, oldName, local));
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void fireDeclaredInputChanged(long callingThreadId, String[] names, Class<?>[] types) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<DeclaredInputChangeListener> values;
		synchronized (this.declaredInputChangeListenersMap) {
			values = new ArrayList<>(this.declaredInputChangeListenersMap.values());
		}
		for (DeclaredInputChangeListener listener : values)
			listener.declaredInputChanged(names, types);
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void fireDeclaredOutputChanged(long callingThreadId, String[] names, Class<?>[] types) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<DeclaredOutputChangeListener> values;
		synchronized (this.declaredOutputChangeListenersMap) {
			values = new ArrayList<>(this.declaredOutputChangeListenersMap.values());
		}
		for (DeclaredOutputChangeListener listener : values)
			listener.declaredOutputChanged(names, types);
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void fireInputLinksChanged(long callingThreadId, int indexOfInput) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<InputLinksChangeListener> values;
		synchronized (this.inputLinksChangeListenersMap) {
			values = new ArrayList<>(this.inputLinksChangeListenersMap.values());
		}
		for (InputLinksChangeListener listener : values)
			listener.inputLinkChanged(indexOfInput);
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void fireStructChanged(long callingThreadId) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<StructChangeListener> values;
		synchronized (this.structChangeListenersMap) {
			values = new ArrayList<>(this.structChangeListenersMap.values());
		}
		for (StructChangeListener listener : values)
			listener.structChanged();
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void fireVarArgsInputChanged(long callingThreadId, int indexOfInput, int typeOfChange) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<VarArgsInputChangeListener> values;
		synchronized (this.varArgsInputChangeListenersMap) {
			values = new ArrayList<>(this.varArgsInputChangeListenersMap.values());
		}
		for (VarArgsInputChangeListener listener : values)
			listener.varArgsInputChanged(indexOfInput, typeOfChange);
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public String getBlockName() {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).getBlockName();
		} catch (RemoteException e) {
			e.printStackTrace();
			return "";
		}
	}

	private long getCallingThreadId() {
		return this.callingThreadId.getOrDefault(Thread.currentThread(), -1L);
	}

	@Override
	public int getInputIndex(String inputName) {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).getInputIndex(inputName);
		} catch (RemoteException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public String[] getInputsName() {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).getInputsName();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	protected static Integer getKeyFromValue(Map<Integer, ?> map, EventListener listener) {
		if (map == null)
			return -1;
		int listenerId = -1;
		for (Integer lid : map.keySet())
			if (map.get(lid) == listener) {
				listenerId = lid;
				break;
			}
		return listenerId;
	}

	@Override
	public long getMaxTimeStamp() {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).getMaxTimeStamp();
		} catch (RemoteException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public int getNbTriggerableInput() {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).getNbTriggerableInput();
		} catch (RemoteException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public int getNbTriggerableOutput() {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).getNbTriggerableOutput();
		} catch (RemoteException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public int getOutputIndex(String outputName) {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).getOutputIndex(outputName);
		} catch (RemoteException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public String[] getOutputLinkToInputName() {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).getOutputLinkToInputName();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Class<?>[] getOutputLinkToInputType() {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).getOutputLinkToInputType();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String[] getTriggerableOutputsName() {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).getTriggerableOutputsName();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public long getTimeOfIssue(int indexOfInput) {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).getTimeOfIssue(indexOfInput);
		} catch (RemoteException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public long getTimeStamp(int indexOfInput) {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).getTimeStamp(indexOfInput);
		} catch (RemoteException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public String getWarning() {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).getWarning();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getError() {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).getError();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean isPropertyAsInput(String propertyName) {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).isPropertyAsInput(propertyName);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean isPropertyAsOutput(String propertyName) {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).isPropertyAsOutput(propertyName);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean isRunning() {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).isRunning();
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	protected void mapCallingThreadIdToRmiThread(long callingThreadId) {
		this.callingThreadId.put(Thread.currentThread(), callingThreadId);
	}

	@Override
	public boolean needToBeSaved(long callingThreadId) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		boolean returnVal = ((EvolvedOperator) this.bean).needToBeSaved();
		unMapCallingThreadIdToRmiThread();
		return returnVal;
	}

	@Override
	public void onStart(Runnable runnable) {
		int id;
		synchronized (this.onStartTaskMap) {
			id = this.onStartTaskMap.size();
			this.onStartTaskMap.put(id, runnable);
		}
		try {
			((RMIOperatorCallBackImpl) this.callBack).onStart(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStartCallBack(long callingThreadId) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<Runnable> values;
		synchronized (this.onStartTaskMap) {
			values = new ArrayList<>(this.onStartTaskMap.values());
		}
		for (Runnable runnable : values)
			runnable.run();
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void onResume(Runnable runnable) {
		int id;
		synchronized (this.onResumeTaskMap) {
			id = this.onResumeTaskMap.size();
			this.onResumeTaskMap.put(id, runnable);
		}
		try {
			((RMIOperatorCallBackImpl) this.callBack).onResume(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResumeCallBack(long callingThreadId) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<Runnable> values;
		synchronized (this.onResumeTaskMap) {
			values = new ArrayList<>(this.onResumeTaskMap.values());
		}
		for (Runnable runnable : values)
			runnable.run();
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void onPause(Runnable runnable) {
		int id;
		synchronized (this.onPauseTaskMap) {
			id = this.onPauseTaskMap.size();
			this.onPauseTaskMap.put(id, runnable);
		}
		try {
			((RMIOperatorCallBackImpl) this.callBack).onPause(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPauseCallBack(long callingThreadId) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<Runnable> values;
		synchronized (this.onPauseTaskMap) {
			values = new ArrayList<>(this.onPauseTaskMap.values());
		}
		for (Runnable runnable : values)
			runnable.run();
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void onStop(Runnable runnable) {
		int id;
		synchronized (this.onStopTaskMap) {
			id = this.onStopTaskMap.size();
			this.onStopTaskMap.put(id, runnable);
		}
		try {
			((RMIOperatorCallBackImpl) this.callBack).onStop(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStopCallBack(long callingThreadId) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<Runnable> values;
		synchronized (this.onStopTaskMap) {
			values = new ArrayList<>(this.onStopTaskMap.values());
		}
		for (Runnable runnable : values)
			runnable.run();
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public Object[] process(long callingThreadId, Object[] inputs, Object[] additionalInputs, boolean[] linkedStaticOutput) throws Throwable {
		mapCallingThreadIdToRmiThread(callingThreadId);
		if (additionalInputs != null) {
			SerializableWrapper.deserialize(additionalInputs);
			((EvolvedOperator) this.bean).setAdditionalInputs(additionalInputs);
		}
		SerializableWrapper.deserialize(inputs);
		Object val = this.processMethodHandle.invokeWithArguments(inputs);
		int i;
		if (this.isProcessMethodReturnVoid)
			i = 0;
		else {
			this.staticOutputsArray[0] = linkedStaticOutput[0] ? val : null;
			i = 1;
		}
		for (; i < this.staticOutputsArray.length; i++)
			this.staticOutputsArray[i] = linkedStaticOutput[i] ? this.staticOutputMethodHandles[i].invoke() : null;
		SerializableWrapper.serialize(this.staticOutputsArray, this.serializableWrapperPool);
		unMapCallingThreadIdToRmiThread();
		return this.staticOutputsArray;
	}

	@Override
	public Object[] processWithNewInputs(long callingThreadId, String[] inputsClassName, Object[] inputs, String[] additionalInputsClassName, Object[] additionalInputs, boolean[] linkedStaticOutput)
			throws Throwable {
		if (inputsClassName.length != this.inputsClass.length)
			this.inputsClass = new Class<?>[inputsClassName.length];
		for (int i = 0; i < this.inputsClass.length; i++)
			if (inputsClassName[i] != null)
				this.inputsClass[i] = BeanManager.getClassFromDescriptor(inputsClassName[i]);
		if (additionalInputsClassName.length != this.additionalInputsClass.length)
			this.additionalInputsClass = new Class<?>[additionalInputsClassName.length];
		for (int i = 0; i < this.additionalInputsClass.length; i++)
			if (additionalInputsClassName[i] != null)
				this.additionalInputsClass[i] = BeanManager.getClassFromDescriptor(additionalInputsClassName[i]);
		return process(callingThreadId, inputs, additionalInputs, linkedStaticOutput);
	}

	@Override
	public void removeBlockNameChangeListener(BeanRenameListener listener) {
		int listenerId;
		synchronized (this.beanRenameListenersMap) {
			listenerId = getKeyFromValue(this.beanRenameListenersMap, listener);
		}
		if (listenerId != -1)
			try {
				((RMIOperatorCallBackImpl) this.callBack).removeBlockNameChangeListener(listenerId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void removeDeclaredInputChangeListener(DeclaredInputChangeListener listener) {
		int listenerId;
		synchronized (this.declaredInputChangeListenersMap) {
			listenerId = getKeyFromValue(this.declaredInputChangeListenersMap, listener);
		}
		if (listenerId != -1)
			try {
				((RMIOperatorCallBackImpl) this.callBack).removeDeclaredInputChangeListener(listenerId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void removeDeclaredOutputChangeListener(DeclaredOutputChangeListener listener) {
		int listenerId;
		synchronized (this.declaredOutputChangeListenersMap) {
			listenerId = getKeyFromValue(this.declaredOutputChangeListenersMap, listener);
		}
		if (listenerId != -1)
			try {
				((RMIOperatorCallBackImpl) this.callBack).removeDeclaredOutputChangeListener(listenerId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void removeInputLinksChangeListener(InputLinksChangeListener listener) {
		int listenerId;
		synchronized (this.inputLinksChangeListenersMap) {
			listenerId = getKeyFromValue(this.inputLinksChangeListenersMap, listener);
		}
		if (listenerId != -1)
			try {
				((RMIOperatorCallBackImpl) this.callBack).removeInputLinksChangeListener(listenerId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void removeStructChangeListener(StructChangeListener listener) {
		int listenerId;
		synchronized (this.structChangeListenersMap) {
			listenerId = getKeyFromValue(this.structChangeListenersMap, listener);
		}
		if (listenerId != -1)
			try {
				((RMIOperatorCallBackImpl) this.callBack).removeStructChangeListener(listenerId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void removeVarArgsInputChangeListener(VarArgsInputChangeListener listener) {
		int listenerId;
		synchronized (this.varArgsInputChangeListenersMap) {
			listenerId = getKeyFromValue(this.varArgsInputChangeListenersMap, listener);
		}
		if (listenerId != -1)
			try {
				((RMIOperatorCallBackImpl) this.callBack).removeVarArgsInputChangeListener(listenerId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void runLater(Runnable runnable) {
		if (getCallingThreadId() == this.mainThreadId)
			runnable.run();
		else {
			int taskId = this.runLaterTaskIdCpt.getAndIncrement();
			this.runLaterRunnableMap.put(taskId, runnable);
			try {
				((RMIOperatorCallBackImpl) this.callBack).runLater(getCallingThreadId(), taskId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void runLaterCallBack(long callingThreadId, int taskId) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		this.runLaterRunnableMap.remove(taskId).run();
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void setError(String defaulting) {
		try {
			((RMIOperatorCallBackImpl) this.callBack).setError(defaulting);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setMainThreadId(long mainThreadId) throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		this.mainThreadId = mainThreadId;
	}

	@Override
	public boolean addPropertyAsInput(String propertyName) {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).addPropertyAsInput(getCallingThreadId(), propertyName);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean removePropertyAsInput(String propertyName) {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).removePropertyAsInput(getCallingThreadId(), propertyName);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean addPropertyAsOutput(String propertyName) {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).addPropertyAsOutput(getCallingThreadId(), propertyName);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean removePropertyAsOutput(String propertyName) {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).removePropertyAsOutput(getCallingThreadId(), propertyName);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void setWarning(String warning) {
		try {
			((RMIOperatorCallBackImpl) this.callBack).setWarning(warning);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void triggerProperty(String outputPropertyName, Object outputValue) {
		try {
			((RMIOperatorCallBackImpl) this.callBack).triggerProperty(getCallingThreadId(), outputPropertyName, SerializableWrapper.serialize(outputValue, this.serializableWrapperPool));
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean triggerOutput(Object outputValue) {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).triggerOutput(getCallingThreadId(), SerializableWrapper.serialize(outputValue, this.serializableWrapperPool));
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean triggerOutput(Object outputValue, long timeStamp) {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).triggerOutput(getCallingThreadId(), SerializableWrapper.serialize(outputValue, this.serializableWrapperPool), timeStamp);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean triggerOutput(Object[] outputValues) {
		try {
			SerializableWrapper.serialize(outputValues, this.serializableWrapperPool);
			return ((RMIOperatorCallBackImpl) this.callBack).triggerOutput(getCallingThreadId(), outputValues);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean triggerOutput(Object[] outputValues, long timeStamp) {
		try {
			SerializableWrapper.serialize(outputValues, this.serializableWrapperPool);
			return ((RMIOperatorCallBackImpl) this.callBack).triggerOutput(getCallingThreadId(), outputValues, timeStamp);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean triggerOutput(Object[] outputValues, long[] timeStamps) {
		try {
			SerializableWrapper.serialize(outputValues, this.serializableWrapperPool);
			return ((RMIOperatorCallBackImpl) this.callBack).triggerOutput(getCallingThreadId(), outputValues, timeStamps);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	protected void unMapCallingThreadIdToRmiThread() {
		this.callingThreadId.remove(Thread.currentThread());
	}

	@Override
	public boolean updateInputs(String[] names, Class<?>[] types) {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).updateInputs(getCallingThreadId(), names, types);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updateOutputs(String[] names, Class<?>[] types) {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).updateOutputs(getCallingThreadId(), names, types);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean holdLock() {
		try {
			return ((RMIOperatorCallBackImpl) this.callBack).holdLock(getCallingThreadId());
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}
}
