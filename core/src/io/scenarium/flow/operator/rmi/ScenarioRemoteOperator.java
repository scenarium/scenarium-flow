/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.operator.rmi;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;

import io.scenarium.core.timescheduler.SchedulerState;
import io.scenarium.flow.RMIOperatorCallBack;

public class ScenarioRemoteOperator extends RemoteOperator {
	public ScenarioRemoteOperator(Class<?> operatorClass, String identifier, String hostName, int port, boolean createRemoteOperator, RMIOperatorCallBack callBack, boolean eraseCallBackIfPresent)
			throws Exception {
		super(operatorClass, identifier, hostName, port, createRemoteOperator, callBack, eraseCallBackIfPresent);
	}

	public ScenarioRemoteOperator(Class<?> operatorClass, String identifier, String hostName, int port, boolean createRemoteOperator, RMIOperatorCallBack callBack, boolean eraseCallBackIfPresent,
			int timeout) throws Exception {
		super(operatorClass, identifier, hostName, port, createRemoteOperator, callBack, eraseCallBackIfPresent, timeout);
	}

	public void update(int taskId, long timePointer, long timeStamp) throws RemoteException {
		((RMIScenarioOperatorImpl) this.remoteBean).update(Thread.currentThread().getId(), taskId, timePointer, timeStamp);
	}

	public boolean canTrigger(long time) throws RemoteException {
		return ((RMIScenarioOperatorImpl) this.remoteBean).canTrigger(Thread.currentThread().getId(), time);
	}

	public void firePropertyChanged(SchedulerState state) throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		((RMIScenarioOperatorImpl) this.remoteBean).firePropertyChanged(Thread.currentThread().getId(), state);
	}
}
