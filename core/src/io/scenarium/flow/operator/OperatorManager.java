/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.operator;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.WeakHashMap;

import io.scenarium.flow.internal.Log;

public class OperatorManager {
	private static WeakHashMap<Class<?>, Boolean> operatorListCache = new WeakHashMap<>();
	private static final ArrayList<Class<?>> OPERATORS = new ArrayList<>();

	private OperatorManager() {}

	/** Add an operator to ScenariumFx. The operator must have a birth, a death method without parameters and without return value and a process method to be accepted.
	 *
	 * @param operator the operator to add to ScenariumFx */
	public static void addOperator(Class<?> operator) {
		if (OperatorManager.isOperator(operator)) {
			for (Class<?> op : OPERATORS)
				if (operator.getSimpleName().equals(op.getSimpleName()))
					if (operator.equals(op)) {
						Log.error(operator + " is already register, it cannot be registered twice");
						return;
					} else if (op.isAssignableFrom(operator)) {
						OPERATORS.remove(op);
						break;
					} else if (operator.isAssignableFrom(op))
						return;
					else {
						Log.error(operator + " cannot be registered, another operator: " + op.getName() + " has  the same class name");
						return;
					}
			OPERATORS.add(operator);
		} else
			Log.error(operator + " is not an operator, an operator have no-argument constructor, a public birth and death method without argument and with no return, and a public process method");
	}

	public static void purgeOperators(Module module) {
		OPERATORS.removeIf(op -> op.getModule().equals(module));
	}

	/** Return a copy of the list of all operators of ScenariumFx.
	 *
	 * @return a copy of the list of all operators */
	public static ArrayList<Class<?>> getOperators() {
		return new ArrayList<>(OPERATORS);
	}

	/** Tell if the operator class is an ScenariumFx valid operator. A valid operator must have a birth, a death method without parameters and without return value and a process method to be accepted.
	 *
	 * @param operatorClass the class to test
	 * @return true if the operatorClass is a valid ScenariumFx operator */
	public static boolean isOperator(Object operatorClass) {
		if (operatorClass == null)
			return false;
		Class<?> type = operatorClass instanceof Class<?> ? (Class<?>) operatorClass : operatorClass.getClass();
		Boolean isOperator = operatorListCache.get(type);
		if (isOperator != null)
			return isOperator;
		try {
			type.getConstructor();
			boolean isBirthMethod = false;
			boolean isProcessMethod = false;
			boolean isDeathMethod = false;
			for (Method method : type.getMethods()) {
				String methoName = method.getName();
				if (methoName.equals("birth") && method.getParameterTypes().length == 0)
					isBirthMethod = true;
				else if (methoName.equals("process"))
					isProcessMethod = true;
				else if (methoName.equals("death") && method.getParameterTypes().length == 0)
					isDeathMethod = true;
				if (isBirthMethod && isProcessMethod && isDeathMethod) {
					operatorListCache.put(type, true);
					return true;
				}
			}
		} catch (NoSuchMethodException | SecurityException e) {}
		operatorListCache.put(type, false);
		return false;
	}

	public static void invokeBirth(Object operator) throws IllegalAccessException, Throwable {
		invokeLifeMethod(operator, true);
	}

	public static void invokeDeath(Object operator) throws IllegalAccessException, Throwable {
		invokeLifeMethod(operator, false);
	}

	private static void invokeLifeMethod(Object operator, boolean birth) throws IllegalAccessException, Throwable {
		Method declaredMethod = operator.getClass().getMethod(birth ? "birth" : "death", new Class<?>[0]);
		declaredMethod.setAccessible(true);
		MethodHandles.lookup().unreflect(declaredMethod).invoke(operator);
	}
}
