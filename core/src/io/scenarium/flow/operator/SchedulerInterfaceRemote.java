package io.scenarium.flow.operator;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.function.BiConsumer;

import io.scenarium.core.filemanager.scenariomanager.LocalScenario;
import io.scenarium.core.timescheduler.Schedulable;
import io.scenarium.core.timescheduler.ScheduleTask;
import io.scenarium.core.timescheduler.SchedulerInterface;
import io.scenarium.flow.Block;
import io.scenarium.flow.operator.rmi.ScenarioRemoteOperator;

public class SchedulerInterfaceRemote extends SchedulerInterface {
	private final Block remoteBlock;

	public SchedulerInterfaceRemote(SchedulerInterface schedulerInterface, Block remoteBlock) {
		super(schedulerInterface);
		this.remoteBlock = remoteBlock;
	}

	@Override
	public void addTasks(ArrayList<ScheduleTask> schedulableTasks, BiConsumer<Long, Long> minMaxSupplier) {
		for (int i = 0; i < schedulableTasks.size(); i++) {
			ScheduleTask st = schedulableTasks.get(i);
			schedulableTasks.set(i, new ScheduleTask(st.timeofIssue, st.timeStamp, new Schedulable() {

				@Override
				public void update(long timePointer) {
					ScenarioRemoteOperator remoteOp = (ScenarioRemoteOperator) SchedulerInterfaceRemote.this.remoteBlock.getRemoteOperator();
					if (remoteOp != null)
						try {
							remoteOp.update(((LocalScenario) SchedulerInterfaceRemote.this.remoteBlock.getOperator()).getTaskId(st.task), timePointer, SchedulerInterfaceRemote.this.getTimeStamp());
						} catch (RemoteException e) {
							e.printStackTrace();
						}
				}

				@Override
				public boolean canTrigger(long time) {
					ScenarioRemoteOperator remoteOp = (ScenarioRemoteOperator) SchedulerInterfaceRemote.this.remoteBlock.getRemoteOperator();
					if (remoteOp != null)
						try {
							return remoteOp.canTrigger(time);
						} catch (RemoteException e) {
							e.printStackTrace();
						}
					return false;
				}
			}, st.seekIndex));
		}
		super.addTasks(schedulableTasks, minMaxSupplier);
	}
}
