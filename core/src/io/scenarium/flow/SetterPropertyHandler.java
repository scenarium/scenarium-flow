/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;

import io.scenarium.core.tools.UncheckedConsumer;
import io.scenarium.core.tools.UncheckedRunnable;

public class SetterPropertyHandler {
	private final UncheckedRunnable reparator;
	private UncheckedConsumer<Object> propertyConsumer;
	private MethodHandle methodHandler;

	public SetterPropertyHandler(Object propertyContainer, Method writeMethod, UncheckedRunnable reparator) throws IllegalAccessException {
		this.reparator = reparator;
		setMethodHandler(writeMethod, propertyContainer);
	}

	public void setMethodHandler(Method writeMethod, Object propertyContainer) throws IllegalAccessException {
		writeMethod.setAccessible(true);
		this.methodHandler = MethodHandles.lookup().unreflect(writeMethod);
		reBindTo(propertyContainer);
	}

	private void updatePropertyConsumer(MethodHandle methodHandler) {
		Class<?> type = methodHandler.type().parameterType(0);
		if (type.isPrimitive()) {
			if (type == int.class)
				this.propertyConsumer = obj -> {
					methodHandler.invokeExact(((Integer) obj).intValue());
				};
			else if (type == double.class)
				this.propertyConsumer = obj -> {
					methodHandler.invokeExact(((Double) obj).doubleValue());
				};
			else if (type == boolean.class)
				this.propertyConsumer = obj -> {
					methodHandler.invokeExact(((Boolean) obj).booleanValue());
				};
			else if (type == float.class)
				this.propertyConsumer = obj -> {
					methodHandler.invokeExact(((Float) obj).floatValue());
				};
			else if (type == long.class)
				this.propertyConsumer = obj -> {
					methodHandler.invokeExact(((Long) obj).longValue());
				};
			else if (type == byte.class)
				this.propertyConsumer = obj -> {
					methodHandler.invokeExact(((Byte) obj).byteValue());
				};
			else if (type == short.class)
				this.propertyConsumer = obj -> {
					methodHandler.invokeExact(((Short) obj).shortValue());
				};
			else if (type == char.class)
				this.propertyConsumer = obj -> {
					methodHandler.invokeExact(((Character) obj).charValue());
				};
			else {
				this.propertyConsumer = null;
				new IllegalArgumentException("Unknow primitive type");
			}
		} else
			this.propertyConsumer = obj -> {
				methodHandler.invoke(obj);
			};
	}

	public void invoke(Object propertyValue) throws Throwable {
		if (this.reparator != null)
			this.reparator.run();
		this.propertyConsumer.accept(propertyValue);
	}

	public void reBindTo(Object currentProperty) {
		updatePropertyConsumer(this.methodHandler.bindTo(currentProperty));
	}
}