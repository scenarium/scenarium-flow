package io.scenarium.flow;

import java.io.File;
import java.net.InetSocketAddress;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.BitSet;

import javax.vecmath.Color4f;
import javax.vecmath.GMatrix;
import javax.vecmath.GVector;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4d;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point2d;
import javax.vecmath.Point2f;
import javax.vecmath.Point2i;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Point3i;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4d;
import javax.vecmath.Vector4f;

import io.scenarium.core.PluginsCoreSupplier;
import io.scenarium.core.consumer.ScenarioConsumer;
import io.scenarium.core.struct.Mesh;
import io.scenarium.core.struct.OldObject3D;
import io.scenarium.core.struct.Text3D;
import io.scenarium.core.struct.Texture3D;
import io.scenarium.core.struct.curve.Curve;
import io.scenarium.core.struct.curve.CurveSeries;
import io.scenarium.core.struct.curve.EvolvedCurveSeries;
import io.scenarium.flow.consumer.ClonerConsumer;
import io.scenarium.flow.consumer.OperatorConsumer;
import io.scenarium.flow.consumer.PluginsFlowSupplier;
import io.scenarium.flow.operator.OperatorManager;
import io.scenarium.flow.operator.player.ScenarioPlayer;
import io.scenarium.flow.operator.player.SubFlowDiagram;
import io.scenarium.flow.scenario.DataFlowDiagram;
import io.scenarium.flow.scenario.ScenariumScheduler;
import io.scenarium.flow.scheduler.DiagramScheduler;
import io.scenarium.pluginManager.PluginsSupplier;

import javafx.scene.paint.Color;

public class Plugin implements PluginsSupplier, PluginsFlowSupplier, PluginsCoreSupplier {

	@Override
	public void birth() {
		// We must do this kind of things to be sure that no external dynamic module have create the class
		// in his path, and the instance in well managed by the plug-in.
		try {
			Class.forName(OperatorManager.class.getName());
			Class.forName(DiagramScheduler.class.getName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void registerModule(Module module) {}

	@Override
	public void unregisterModule(Module module) {
		OperatorManager.purgeOperators(module);
		DiagramScheduler.purgeCloners(module);
	}

	@Override
	public void populateOperators(OperatorConsumer operatorConsumer) {
		operatorConsumer.accept(ScenarioPlayer.class);
		operatorConsumer.accept(SubFlowDiagram.class);
	}

	@Override
	public void populateCloners(ClonerConsumer clonerConsumer) {
		clonerConsumer.accept(Boolean.class, o -> o);
		clonerConsumer.accept(Character.class, o -> o);
		clonerConsumer.accept(Byte.class, o -> o);
		clonerConsumer.accept(Double.class, o -> o);
		clonerConsumer.accept(Float.class, o -> o);
		clonerConsumer.accept(Integer.class, o -> o);
		clonerConsumer.accept(Short.class, o -> o);
		clonerConsumer.accept(Long.class, o -> o);
		clonerConsumer.accept(String.class, o -> o);
		clonerConsumer.accept(File.class, o -> o);
		clonerConsumer.accept(InetSocketAddress.class, o -> o);
		clonerConsumer.accept(Color.class, o -> o);
		clonerConsumer.accept(Curve.class, Curve::clone);
		clonerConsumer.accept(CurveSeries.class, CurveSeries::clone);
		clonerConsumer.accept(EvolvedCurveSeries.class, EvolvedCurveSeries::clone);
		clonerConsumer.accept(Point2d.class, o -> o.clone());
		clonerConsumer.accept(Point2f.class, o -> o.clone());
		clonerConsumer.accept(Point2i.class, o -> o.clone());
		clonerConsumer.accept(Vector2d.class, o -> o.clone());
		clonerConsumer.accept(Vector2f.class, o -> o.clone());
		clonerConsumer.accept(Point3d.class, o -> o.clone());
		clonerConsumer.accept(Point3f.class, o -> o.clone());
		clonerConsumer.accept(Point3i.class, o -> o.clone());
		clonerConsumer.accept(Vector3d.class, o -> o.clone());
		clonerConsumer.accept(Vector3f.class, o -> o.clone());
		clonerConsumer.accept(Vector4d.class, o -> o.clone());
		clonerConsumer.accept(Vector4f.class, o -> o.clone());
		clonerConsumer.accept(GVector.class, o -> o.clone());
		clonerConsumer.accept(Color4f.class, o -> o.clone());
		clonerConsumer.accept(Matrix3f.class, o -> o.clone());
		clonerConsumer.accept(Matrix3d.class, o -> o.clone());
		clonerConsumer.accept(Matrix4f.class, o -> o.clone());
		clonerConsumer.accept(Matrix4d.class, o -> o.clone());
		clonerConsumer.accept(GMatrix.class, o -> o.clone());
		clonerConsumer.accept(BitSet.class, o -> (BitSet) o.clone());
		clonerConsumer.accept(OldObject3D.class, OldObject3D::clone);
		clonerConsumer.accept(Text3D.class, Text3D::clone);
		clonerConsumer.accept(Texture3D.class, Texture3D::clone);
		clonerConsumer.accept(Mesh.class, Mesh::clone);
		clonerConsumer.accept(LocalTime.class, o -> o);
		clonerConsumer.accept(LocalDate.class, o -> o);
		clonerConsumer.accept(LocalDateTime.class, o -> o);
		clonerConsumer.accept(byte[].class, byte[]::clone);
		clonerConsumer.accept(short[].class, short[]::clone);
		clonerConsumer.accept(int[].class, int[]::clone);
		clonerConsumer.accept(long[].class, long[]::clone);
		clonerConsumer.accept(float[].class, float[]::clone);
		clonerConsumer.accept(double[].class, double[]::clone);
		clonerConsumer.accept(boolean[].class, boolean[]::clone);
		clonerConsumer.accept(char[].class, char[]::clone);
		clonerConsumer.accept(byte[][].class, o -> {
			byte[][] arr = o;
			byte[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
		clonerConsumer.accept(short[][].class, o -> {
			short[][] arr = o;
			short[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
		clonerConsumer.accept(int[][].class, o -> {
			int[][] arr = o;
			int[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
		clonerConsumer.accept(long[][].class, o -> {
			long[][] arr = o;
			long[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
		clonerConsumer.accept(float[][].class, o -> {
			float[][] arr = o;
			float[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
		clonerConsumer.accept(double[][].class, o -> {
			double[][] arr = o;
			double[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
		clonerConsumer.accept(boolean[][].class, o -> {
			boolean[][] arr = o;
			boolean[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
		clonerConsumer.accept(char[][].class, o -> {
			char[][] arr = o;
			char[][] temp = arr.clone();
			for (int i = 0; i < temp.length; i++)
				temp[i] = arr[i].clone();
			return temp;
		});
	}

	@Override
	public void populateScenarios(ScenarioConsumer scenarioConsumer) {
		scenarioConsumer.accept(DataFlowDiagram.class);
		scenarioConsumer.accept(ScenariumScheduler.class);
	}

	@Override
	public void loadPlugin(Object pluginInterface) {
		// check if the plug-in is supported locally:
		if (pluginInterface instanceof PluginsFlowSupplier) {
			PluginsFlowSupplier plugins = (PluginsFlowSupplier) pluginInterface;
			// Add first Cloner (in/out of operators) in the local system.
			plugins.populateCloners(new ClonerConsumer());
			// Add the Operators in the local system.
			plugins.populateOperators(new OperatorConsumer());
		}
	}
}
