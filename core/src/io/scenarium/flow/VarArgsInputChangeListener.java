/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow;

import java.util.EventListener;

@FunctionalInterface
public interface VarArgsInputChangeListener extends EventListener {
	public static final int CHANGED = 0;
	public static final int NEW = 1;
	public static final int REMOVE = 2;

	public void varArgsInputChanged(int indexOfInput, int typeOfChange);
}
