/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class IOLinks {
	private final IOComponent component;
	private final HashMap<String, ArrayList<IOLink>> proplinks;
	private final IOLink[] links;

	public IOLinks(IOComponent component, int iOSize) {
		this.component = component;
		this.proplinks = new HashMap<>();
		this.links = new IOLink[iOSize];
	}

	public IOComponent getComponent() {
		return this.component;
	}

	public IOLink[] getLinks() {
		return this.links;
	}

	@Override
	public String toString() {
		return this.component.toString() + this.proplinks + Arrays.toString(this.links);
	}

	public ArrayList<IOLink> getLinks(String outputPropertyName) {
		return this.proplinks.get(outputPropertyName);
	}

	public void setIOLink(int index, IOLink ioLink) {
		this.links[index] = ioLink;
	}

	public void setPropertyIOLink(String outputPropertyName, IOLink ioLink) {
		ArrayList<IOLink> ioLinks = this.proplinks.get(outputPropertyName);
		if (ioLinks == null) {
			ioLinks = new ArrayList<>();
			this.proplinks.put(outputPropertyName, ioLinks);
		}
		ioLinks.add(ioLink);
	}
}
