/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow;

import java.util.concurrent.locks.ReentrantLock;

import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.primitive.number.NumberInfo;

public class BlockInput extends Input implements BlockIO {
	@PropertyInfo(index = 0, info = "Mode for the pin:\n\t-FIFO (First In First Out)\n\t-LIFO (Last In Last Out)")
	private ReaderType readerType = ReaderType.FIFO;
	@PropertyInfo(index = 1, info = "Maximum size of the stack")
	@NumberInfo(min = 0)
	private int maxStackSize = 100;
	@PropertyInfo(index = 2, info = "Erase a data of the stack by an incoming one if the stack is full")
	private boolean eraseDataIfFull = false;

	protected final Block block;
	private InputRingBuffer buffer = new InputRingBuffer();
	public final boolean isVarArgs;

	public BlockInput(Block block, Class<?> type, String name, boolean isVarArgs) {
		super(type, name);
		this.block = block;
		this.isVarArgs = isVarArgs;
	}

	@Override
	public IOComponent getComponent() {
		return this.block;
	}

	@Override
	public Block getBlock() {
		return this.block;
	}

	@Override
	public Link getLink() {
		return this.link;
	}

	@Override
	public boolean setLink(Link link) {
		return setLink(link, true);
	}

	boolean setLink(Link link, boolean needToUpdateVarArgs) {
		ReentrantLock lock = this.block.getIOLock();
		lock.lock(); // Permet de bloquer le run pendant les modification de lien
		try {
			Input oldInput = this.link == null ? null : this.link.getInput();
			boolean success = super.setLink(link);
			if (success) {
				if (this.isVarArgs && needToUpdateVarArgs)
					if (this.block.varArgsInputChanged(this))
						return success;
				if (oldInput != null)
					this.block.fireInputLinksChangeListener(oldInput);
				if (link != null) {
					Input newInput = link.getInput();
					if (newInput != null && newInput != oldInput)
						this.block.fireInputLinksChangeListener(newInput);
				}
			}
			return success;
		} finally {
			lock.unlock();
		}
	}

	public InputRingBuffer getBuffer() {
		return this.buffer;
	}

	public IOData pop() {
		return this.readerType == ReaderType.LIFO ? this.buffer.popLast() : this.buffer.popFirst();
	}

	public boolean isVarArgs() {
		return this.isVarArgs;
	}

	public ReaderType getReaderType() {
		return this.readerType;
	}

	public void setReaderType(ReaderType readerType) {
		if (readerType != null)
			this.readerType = readerType;
	}

	public int getMaxStackSize() {
		return this.maxStackSize;
	}

	public void setMaxStackSize(int maxStackSize) {
		this.maxStackSize = maxStackSize;
		this.buffer = new InputRingBuffer(maxStackSize, this.eraseDataIfFull, this.buffer, this.type);
	}

	public boolean isEraseDataIfFull() {
		return this.eraseDataIfFull;
	}

	public void setEraseDataIfFull(boolean eraseDataIfFull) {
		this.eraseDataIfFull = eraseDataIfFull;
		this.buffer.setEraseDataIfFull();
	}
}
