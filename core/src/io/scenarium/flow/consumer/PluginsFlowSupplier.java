package io.scenarium.flow.consumer;

public interface PluginsFlowSupplier {
	public default void populateOperators(OperatorConsumer operatorConsumer) {}

	public default void populateCloners(ClonerConsumer clonerConsumer) {}

}
