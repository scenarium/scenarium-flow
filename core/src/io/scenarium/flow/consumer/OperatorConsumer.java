/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow.consumer;

import io.scenarium.flow.operator.OperatorManager;

public class OperatorConsumer {
	public <T extends Object> void accept(Class<T> classOperator) {
		OperatorManager.addOperator(classOperator);
	}
}
