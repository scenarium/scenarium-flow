/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow;

public class Link {
	private final Input input;
	private final Output output;
	private int nbConsume;
	private Runnable additionnalConsumedElement;

	public Link(Input input, Output output) {
		this.input = input;
		this.output = output;
	}

	public void consume() {
		this.nbConsume++;
		// Log.info("nbConsume of: " + this + " " + nbConsume);
		Runnable additionnalConsumedElement = this.additionnalConsumedElement;
		if (additionnalConsumedElement != null)
			additionnalConsumedElement.run();
	}

	public int getConsume() {
		return this.nbConsume;
	}

	public Input getInput() {
		return this.input;
	}

	public Output getOutput() {
		return this.output;
	}

	public void resetConsume() {
		this.nbConsume = 0;
	}

	@Override
	public String toString() {
		return (this.output == null ? String.valueOf((Object) null) : this.output.toString()) + " -> " + (this.input == null ? String.valueOf((Object) null) : this.input.toString());
	}

	/** Defines a task to execute when a link is consumed. Used if two links are interdependent as in the case of unmanaged sub-diagrams links.
	 * @param additionalElementToConsume the task to execute when a link is consumed */
	void setAdditionalElementToConsume(Runnable additionalElementToConsume) {
		// if(additionalElementToConsume == null)
		// Log.info("reset: " + this);
		this.additionnalConsumedElement = additionalElementToConsume;
	}
}
