/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.flow;

import java.util.ArrayList;
import java.util.List;

import io.scenarium.flow.operator.player.SubFlowDiagram;
import io.scenarium.flow.scenario.DataFlowDiagram;

public interface IOComponent {
	public List<? extends Input> getInputs();

	public Object[] getOutputBuff();

	public long[] getOutputBuffTs();

	public List<? extends Output> getOutputs();

	public List<? extends Output> getTriggerableOutputs();

	public boolean isReady();

	public boolean canTriggerOrBeTriggered();

	public IOLinks[] getIndex();

	public default IOLinks[] buildIndex(int outputOffset) {
		int[] iOList = new int[50]; // 50 est juste une taille de cache, il peut grossir tant que necessaire
		int indexIOList = 0;
		ArrayList<IOComponent> compoList = new ArrayList<>(50);
		List<? extends Output> outputs = getOutputs();
		int nbOutput = outputs.size();
		ArrayList<ArrayList<Input>> linkIbnputs = new ArrayList<>(nbOutput);
		for (Output output : outputs) {
			Input[] inputs = output.getLinkInputs();
			ArrayList<Input> inputList = new ArrayList<>();
			linkIbnputs.add(inputList);
			if (inputs == null)
				continue;
			for (Input nextInput : inputs) {
				IOComponent nextComp = nextInput.getComponent();
				nextInput.getLink().setAdditionalElementToConsume(null);
				boolean inputConsumed = false;
				if (nextInput instanceof FlowDiagramInput) {
					FlowDiagram dfd = (FlowDiagram) nextInput.getComponent();
					if (!dfd.isSubManagement()) {
						getCorrespondingOutput(inputList, nextInput, dfd);
						inputConsumed = true;
					}
				} else if (nextComp instanceof Block && ((Block) nextComp).operator instanceof SubFlowDiagram) {
					DataFlowDiagram dfd = ((SubFlowDiagram) ((Block) nextComp).operator).getSubDiagram();
					if (!dfd.isSubManagement()) {
						getCorrespondingInput(inputList, nextInput, dfd);
						inputConsumed = true;
					}
				}
				if (!inputConsumed)
					inputList.add(nextInput);
			}
		}
		for (int i = 0; i < nbOutput; i++) {
			ArrayList<Input> inputs = linkIbnputs.get(i);
			if (inputs == null)
				continue;
			for (int j = 0; j < inputs.size(); j++) {
				boolean success = false;
				Input nextInput = inputs.get(j);
				IOComponent nextComp = nextInput.getComponent();
				for (int k = 0; k < compoList.size(); k++) {
					IOComponent ioc = compoList.get(k);
					if (nextComp == ioc) {
						if (i >= outputOffset)
							iOList[k]++;
						success = true;
						break;
					}
				}
				if (!success) {
					if (i >= outputOffset) {
						if (indexIOList == iOList.length) {
							int[] iol = new int[iOList.length * 2];
							System.arraycopy(iOList, 0, iol, 0, iOList.length);
							iOList = iol;
						}
						iOList[indexIOList]++;
					}
					indexIOList++;
					compoList.add(nextComp);
				}
			}
		}
		IOLinks[] index = new IOLinks[compoList.size()];
		for (int i = 0; i < index.length; i++)
			index[i] = new IOLinks(compoList.get(i), iOList[i]);
		for (int i = 0; i < nbOutput; i++) {
			Output output = outputs.get(i);
			ArrayList<Input> inputs = linkIbnputs.get(i);
			if (inputs == null)
				continue;
			// boolean isFirst = true;
			for (Input nextInput : inputs) {
				IOLinks ioLink = null;
				IOComponent nextComp = nextInput.getComponent();
				for (IOLinks iol : index)
					if (nextComp == iol.getComponent()) {
						ioLink = iol;
						break;
					}
				if (i < outputOffset)
					ioLink.setPropertyIOLink(output.getName(), new IOLink(i, nextInput, true));
				else {
					IOLink[] blockLinks = ioLink.getLinks();
					int l = 0;
					while (blockLinks[l] != null)
						l++;
					ioLink.setIOLink(l, new IOLink(i - outputOffset, nextInput, true));
				}
			}
		}
		return index;
	}

	private void getCorrespondingOutput(ArrayList<Input> inputList, Input nextInput, FlowDiagram dfd) {
		Input[] correspondingOutputs = dfd.getCorrespondingOutputs(nextInput.getName());
		if (correspondingOutputs != null)
			for (Input correspondingOutput : correspondingOutputs) {
				IOComponent comp = correspondingOutput.getComponent();
				if (correspondingOutput instanceof FlowDiagramInput) {
					FlowDiagram subDfd = (FlowDiagram) comp;
					if (!subDfd.isSubManagement()) {
						getCorrespondingOutput(inputList, correspondingOutput, subDfd);
						continue;
					}
				} else if (comp instanceof Block && ((Block) comp).operator instanceof SubFlowDiagram) {
					comp.getCorrespondingInput(inputList, correspondingOutput, ((SubFlowDiagram) ((Block) comp).operator).getSubDiagram());
					continue;
				}
				inputList.add(correspondingOutput);
			}
		// Let this input to consume the link
		inputList.add(nextInput);
	}

	private void getCorrespondingInput(ArrayList<Input> inputList, Input nextInput, DataFlowDiagram dfd) {
		Input[] correspondingInputs = ((FlowDiagram) dfd.getScenarioData()).getCorrespondingIntputs(nextInput.getName());
		if (correspondingInputs != null) {
			boolean firstTime = true;
			for (Input correspondingIntput : correspondingInputs) {
				// bind the link consume with the first link connected to the sub flow diagram input
				correspondingIntput.getLink().setAdditionalElementToConsume(firstTime ? nextInput.getLink()::consume : null);
				IOComponent comp = correspondingIntput.getComponent();
				if (comp instanceof Block && ((Block) comp).operator instanceof SubFlowDiagram) {
					DataFlowDiagram subDfd = ((SubFlowDiagram) ((Block) comp).operator).getSubDiagram();
					if (!subDfd.isSubManagement())
						getCorrespondingInput(inputList, correspondingIntput, subDfd);
					else
						inputList.add(correspondingIntput);
				} else
					inputList.add(correspondingIntput);
				firstTime = false;
			}
		}
	}
}
