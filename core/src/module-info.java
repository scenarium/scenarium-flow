import io.scenarium.flow.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

open module io.scenarium.flow {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	exports io.scenarium.flow;
	exports io.scenarium.flow.operator;
	exports io.scenarium.flow.operator.rmi;
	exports io.scenarium.flow.operator.player;
	exports io.scenarium.flow.scenario;
	exports io.scenarium.flow.scheduler;
	exports io.scenarium.flow.consumer;

	requires transitive io.scenarium.core;
	requires transitive javafx.graphics;
	requires java.rmi;
	requires java.management;
	requires java.desktop;
	requires javafx.controls;
}
