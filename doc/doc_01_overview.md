scenarium-flow Overview
=======================

Senarium Flow is the main entry for the data Flow management scenarios.

This module will permit to manage a ```data flow diagram```


Tutorials:
==========

[global overview](doc/overview_system.md)
[contribution guideline](doc/contribution_guideline.md)

practices:
  - 01: [First run](doc/tutorial_01_first_run.md)
  - 02: [Dynamic parameter](doc/tutorial_02_dynamic_parameter.md)
  - 03: [Read Video and process data](doc/tutorial_03_video_processing.md)
  - 04: [Record and replay](doc/tutorial_04_replay.md)
  - 05: [Create a plug-in library](doc/tutorial_05_create_a_library_to_create_your_operators.md)
  - 06: [Write a block](doc/tutorial_06_write_a_new_block.md)
  - 07: [JNI Bridge](doc/tutorial_07_JNI_bridge.md)



