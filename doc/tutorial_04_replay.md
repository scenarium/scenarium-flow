Tutorial 4: Record and replay a flow
====================================

We consider now that you are able to create alone a Scenario.
Then create the next simple scenario.

![basic scenario|titleShow=true|align=center](tutorial_04/basic_flow.png)

```{NOTE}
We use the first ```Perlin noise``` to generate automatic data.
```

Record:
=======

Now we will record the flow:
  - Add operator: ```Basic/Recorder```
  - Connect the output of the block ```PerlinNoise-0```

![add recorder|titleShow=true|align=center](tutorial_04/add_record.png)

We need to select an output of the recorder (recorder property):

![configure recorder|titleShow=true|align=center](tutorial_04/configure_recorder.png)

Now your can save your scenario to open it later ```File->Save Scenario As...``` like "xxx_record.pdfd"

At every time you play the scenario a new entry of record is generated.

```{NOTE}
The record is in row, then pay attention when you record image stream.
```

Replay:
=======

Simply do:
  - Record your scenario as a new scenario file ==> permit to switch between record and replay. ```File->Save Scenario As...``` like "xxx_replay.pdfd"
  - Remove the recorder operator
  - Remove the Data generator operator
  - Remove the PerlinNoise-0 operator
  - Add the operator ```Scenario/ScenariumScheduler```

![regenerate scenario|titleShow=true|align=center](tutorial_04/regenerate.png)

```{NOTE}
The scheduler does not have output data ==> need to select the record to generate output elements
```

Configure your scheduler (operator property):

![configure scheduler|titleShow=true|align=center](tutorial_04/configure_scheduler.png)

Now link your recorded stream to the old operators

All is ready to replay.

  - Open the player console and enjoy

![player console|titleShow=true|align=center](tutorial_04/player.png)

  - ```1```: Play pause button
  - ```2```: Stop simulation
  - ```3```: Play reverse or forward
  - ```4```: Previous/next Record in the playlist
  - ```5```: Slowly/Faster: Change the speed of the simulation
  - ```6```: **In pause only:** Play the previous/next frame of data
  - ```7```: Automatic replay of the simulation
  - ```8```: Configure cycle for replay data (marker A/B can be change with a drag and drop)

