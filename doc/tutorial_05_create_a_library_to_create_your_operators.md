Create your library
===================

Create a new project in eclipse: ```file -> new -> Java Project```
  - Project name ```SampleLib```
  - Tick Use default JRE
![new project|titleShow=true|align=center](tutorial_05/new_project.png)

  - Click on ```next``` 
  - On tab ```Project``` select ```ModulePath```

![Add module path|titleShow=true|align=center](tutorial_05/add_module_path.png)

  - Click on ```Add...```
  - Tick ```beanmanager```
  - Tick ```scenaruim```

![Select scenarium|titleShow=true|align=center](tutorial_05/Select_scenarium.png)

  - Click on ```OK```
 
  - On tab ```Libraries``` select ```ModulePath```

![Add JARs|titleShow=true|align=center](tutorial_05/add_jars.png)

  - Click on ```Add JARs...```
  - Select all JARs in scenarium/lib/*

![Select All JARs|titleShow=true|align=center](tutorial_05/Select_all_jars_1.png)

  - Click on ```Ok```

  - On tab ```Libraries``` select ```ModulePath```
  - Click on ```Add JARs...```
  - Select ```vecmath.jar``` in beanmanager/lib/

![Select all JARs|titleShow=true|align=center](tutorial_05/Select_all_jars_2.png)

  - Click on ```Ok```

  - Click on ```Finish```
  - Click on ```Create``` for the module info file.


Create the plug-in elements:
---------------------------

Add the dependency of scenarium:

In the file ```module-info.java``` write:

```{.java}
module samplelib {
	// Require use of Scenarium (core)
	requires io.scenarium.core;
}
```

Create the package:

  - ```File -> new -> package...```
  - Name: ```SampleLib```
  - Click on ```Finish```
  - Select Package ```SampleLib```
  - Right click on it & ```new -> Class```
      - Name ```SamplePlugin```
      - Click on ```Finish```

Configure the plug-in in file ```SamplePlugin.java``` set ```implements PluginsSupplier```:

```{.java}
public class SamplePlugin implements PluginsSupplier  {
	
}
```

In ```module-info.java``` write:

```{.java}
import io.scenarium.core.PluginsSupplier;
import SampleLib.SamplePlugin;

module SampleLib {
	// Request use of the PluginsSupplier module
	uses PluginsSupplier;
	// Say that it privide an external API
	provides PluginsSupplier with SamplePlugin;
	// Require use of Scenarium (core)
	requires io.scenarium.core;
}
```

Now we provide a generic plug-in for scenarium.

We do not declare any element of the generic plug-in, For that, we need to override some function in ```SamplePlugin.java```

```{.java}
package SampleLib;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import io.scenarium.core.ClonerConsumer;
import io.scenarium.core.DataStreamConsumer;
import io.scenarium.core.PluginsSupplier;
import io.scenarium.core.display.drawer.TheaterPanel;
import io.scenarium.core.filemanager.filerecorder.FileStreamRecorder;
import io.scenarium.core.filemanager.scenariomanager.Scenario;

public class SamplePlugin implements PluginsSupplier  {
	@Override
	public void populateOperators(Consumer<Class<?>> operatorConsumer) {
		//operatorConsumer.accept(XxxxxxYyyyyy.class);
	}
	@Override
	public void populateCloners(ClonerConsumer clonerConsumer) {
		//clonerConsumer.accept(XxxxxxYyyyyy.class, XxxxxxYyyyyy::clone);
	}
	@Override
	public void populateDataStream(DataStreamConsumer dataStreamConsumer) {
		//dataStreamConsumer.accept(XxxxxxYyyyyy.class, XxxxxxYyyyyyInputStream.class, XxxxxxYyyyyyOutputStream.class);
	}
	@Override
	public void populateFileRecorder(BiConsumer<Class<?>, Class<? extends FileStreamRecorder>> fileRecorderConsumer) {
		//fileRecorderConsumer.accept(XxxxxxYyyyyy.class, XxxxxxYyyyyyStreamRecorder.class);
	}
	@Override
	public void populateScenarios(BiConsumer<Class<? extends Scenario>, String[]> scenarioConsumer) {
		//scenarioConsumer.accept(XxxxxxYyyyyy.class, new XxxxxxYyyyyy().getReaderFormatNames());
	}
	@Override
	public void populateDrawers(BiConsumer<Class<? extends Object>, Class<? extends TheaterPanel>> drawersConsumer) {
		//drawersConsumer.accept(XxxxxxYyyyyy.class, XxxxxxYyyyyyDrawer.class);
	}
}
```

All Is ready, but you are not ready to run ...

Integrate your library
======================

Development mode:
-----------------

To do this simply, we use an external repository to tun scenarium with external plugins: [scenarium-run](https://gitlab.com/HeeroYui/scenarium-run)

In this project: edit property:
  - On tab ```Project``` select ```ModulePath```

![Add project in path|titleShow=true|align=center](tutorial_05/add_project_in_path.png)

  - Click on ```Add...```
  - Tick ```pluginlib```
  - Click on ```OK```
  - Click on ```Apply and close```

Run the application ```ScenariumRun```

==> !!! enjoy !!!

Release Mode:
-------------

TODO: ...


