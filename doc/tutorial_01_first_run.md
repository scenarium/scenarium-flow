Tutorial 1: First Run of Scenarium
==================================

After starting Eclipse with the correct workspace.

  - Select/open the file ```scenarium/src/io.scenarium.core/Scenarium.java```
  - And run the program in menu: ```run -> Run```

**>>> Scenarium is now started <<< **


Create our first flow:
====================

First step is to add all needed blocks

  - Add the **data Generator**:
      - Right click on the mouse and ```Add Operator``` <br/> A menu will open with all the available element addable.
      - Drag and drop ```Dataprocessing/DataGenerator``` in the Scenarium main windows.
  - Add the **perlin noise** filter:
      - Jump in the ```Operator``` windows.
      - Drag and drop ```Dataprocessing/PerlinNoise``` in the Scenarium main windows (must have enough place to drop).
  - Add the **Oscilloscope** display:
      - Jump in the ```Operator``` windows.
      - Drag and drop ```basic/Oscilloscope``` in the Scenarium main windows.

![Simple blocks added on the scenarium flow|titleShow=true|align=center](tutorial_01/simple_blocks.png)


Link the generator to the other block

  - Click on the output of the ```generator``` block and drag it to the input of ```perlin noise``` block
  - Click on the output of the ```generator``` block and drag it to the input of ```Oscilloscope``` block
  - Click on the output of the ```perlin noise``` block and drag it to the second input of ```Oscilloscope``` block

![Linked blocks together|titleShow=true|align=center](tutorial_01/linked_flow.png)

Let now start the application and play with it

  - Open the player windows with the menu: ```View -> Player```
  - Press ```Play``` button

==> An oscilloscope will open and display the values generated

  - Double click on the Generator ```Gray area```
  - Set your mouse hover the Value area and scoll or change the value


![Full basic example|titleShow=true|align=center](tutorial_01/full_example.png)


Tips:
=====

Use the player controller without display it:

Select the flow windows.

  - ```SPACE``` : Start the scenario
  - ```SPACE``` : Suspend the scenario
  - ```CTRL``` + ```SPACE``` : Stop the scenario


