/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.flow.display.toolBar;

import java.io.File;
import java.lang.module.ModuleDescriptor;
import java.lang.module.ModuleDescriptor.Version;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import io.beanmanager.BeanDesc;
import io.beanmanager.BeanManager;
import io.beanmanager.BeanRegisterListener;
import io.beanmanager.BeanRenameListener;
import io.beanmanager.BeanUnregisterListener;
import io.beanmanager.editors.SubBeanChangeListener;
import io.beanmanager.editors.container.BeanEditor;
import io.beanmanager.ihmtest.FxTest;
import io.beanmanager.tools.FxUtils;
import io.scenarium.flow.operator.OperatorManager;
import io.scenarium.gui.core.display.AlertUtil;
import io.scenarium.gui.core.display.toolbarclass.ExternalTool;
import io.scenarium.gui.flow.internal.LoadPackageStream;
import io.scenarium.gui.flow.internal.Log;
import io.scenarium.pluginManager.LoadModuleListener;
import io.scenarium.pluginManager.ModuleManager;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class Operators extends ExternalTool implements SubBeanChangeListener, BeanRegisterListener, BeanUnregisterListener, BeanRenameListener, LoadModuleListener {
	public static final DataFormat BEAN_FORMAT = new DataFormat("ScenariumBean");
	public static final DataFormat OPERATOR_FORMAT = new DataFormat("ScenariumOperator");
	public static final DataFormat OPERATOR_CLASS_FORMAT = new DataFormat("ScenariumOperatorClass");
	private static final Image OPEN_FOLDER_ICON = new Image(LoadPackageStream.getStream("/open_folder.png"));
	private static final Image CLOSED_FOLDER_ICON = new Image(LoadPackageStream.getStream("/closed_folder.png"));
	public static final Image OPERATOR_INST_ICON = new Image(LoadPackageStream.getStream("/Operator.gif"));
	static final Image BEAN_ICON = new Image(LoadPackageStream.getStream("/bean.gif"));
	private static final String OPERATOR_PACKAGE_NAME = ".operator.";

	private TreeView<Object> tree;
	private BorderPane operatorPane;
	private BeanManager beanManager;
	private ObservableList<BeanDesc<?>> beanList;
	private ListView<BeanDesc<?>> beanListView;

	public static void main(String[] args) {
		Operators ofx = new Operators();
		FxTest.launchIHM(args, s -> ofx.getRegion(), e -> ofx.dispose());
	}

	@Override
	public void beanRegister(BeanDesc<?> beanDesc) {
		FxUtils.runLaterIfNeeded(() -> {
			if (OperatorManager.isOperator(beanDesc.bean)) {
				LinkedList<FilterableTreeItem<Object>> nodeToTest = new LinkedList<>();
				nodeToTest.add((FilterableTreeItem<Object>) this.tree.getRoot());
				while (!nodeToTest.isEmpty()) {
					FilterableTreeItem<Object> node = nodeToTest.pop();
					for (TreeItem<Object> treeItem : node.getInternalChildren()) {
						if (treeItem instanceof OperatorClassTreeItem)
							((OperatorClassTreeItem<?>) treeItem).reset();
						if (!treeItem.isLeaf())
							nodeToTest.push((FilterableTreeItem<Object>) treeItem);
					}
				}
			}
			this.beanList.add(beanDesc);
		});
	}

	@Override
	public void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		FxUtils.runLaterIfNeeded(() -> {
			beanUnregister(oldBeanDesc, true);
			beanRegister(beanDesc);
		});
	}

	@Override
	public void beanUnregister(BeanDesc<?> beanDesc, boolean delete) { // TODO on relache pas immédiatement alors...
		FxUtils.runLaterIfNeeded(() -> {
			LinkedList<FilterableTreeItem<Object>> nodeToTest = new LinkedList<>();
			nodeToTest.add((FilterableTreeItem<Object>) this.tree.getRoot());
			while (!nodeToTest.isEmpty()) {
				FilterableTreeItem<Object> node = nodeToTest.pop();
				ArrayList<TreeItem<?>> nodesToReset = new ArrayList<>();
				for (TreeItem<Object> treeItem : node.getInternalChildren()) { // TODO ConcurrentModificationException
					if (treeItem instanceof BeanTreeItem && ((BeanTreeItem) treeItem).beanDesc == beanDesc)
						nodesToReset.add(treeItem.getParent());
					if (!treeItem.isLeaf())
						nodeToTest.push((FilterableTreeItem<Object>) treeItem);
				}
				for (TreeItem<?> nodeToRest : nodesToReset)
					if (nodeToRest instanceof BeanTreeItem)
						((BeanTreeItem) nodeToRest).reset();
					else if (nodeToRest instanceof OperatorClassTreeItem)
						((OperatorClassTreeItem<?>) nodeToRest).reset();
			}
			this.beanList.remove(beanDesc);
		});
	}

	@Override
	public void dispose() {
		ModuleManager.removeLoadModuleListener(this);
		super.dispose();
		BeanEditor.removeStrongRefBeanRegisterListener(this);
		BeanEditor.removeStrongRefBeanUnregisterListener(this);
		BeanEditor.removeStrongRefBeanRenameListener(this);
		BeanManager.removeSubBeanChangeListener(this);
		if (this.beanManager != null)
			this.beanManager.saveIfChanged();
	}

	private static void expandTreeView(TreeItem<Object> node) {
		LinkedList<TreeItem<Object>> todo = new LinkedList<>();
		todo.add(node);
		while (!todo.isEmpty()) {
			TreeItem<Object> ti = todo.pop();
			ti.setExpanded(true);
			todo.addAll(ti.getChildren());
		}
	}

	@Override
	public SplitPane getRegion() {
		ModuleManager.addLoadModuleListener(this);
		SplitPane splitPane1 = new SplitPane();
		splitPane1.setOrientation(Orientation.VERTICAL);
		splitPane1.setDividerPositions(0.6);
		FilterableTreeItem<Object> rootNode = new FilterableTreeItem<>("rootNode");
		populateInternOperatorNode(rootNode);
		this.tree = new TreeView<>(rootNode);
		this.tree.setShowRoot(false);
		this.tree.getSelectionModel().selectedItemProperty().addListener((a, b, op) -> updatePanelOperator(op instanceof BeanTreeItem ? ((BeanTreeItem) op).beanDesc.bean : null));
		this.tree.setEditable(true);
		this.tree.setCellFactory(p -> {
			TreeCell<Object> cell = new TreeCell<>() {
				private void commitEdit(TextField textField, BeanDesc<?> beanDesc) {
					cancelEdit();
					if (changeBeanName(beanDesc.bean, textField.getText()))
						getTreeView().refresh();
				}

				@Override
				public void startEdit() {
					if (!isEditable() || !getTreeView().isEditable() || !(getItem() instanceof BeanDesc))
						return;
					BeanDesc<?> beanDesc = (BeanDesc<?>) getItem();
					setText(null);
					TextField textField = new TextField(beanDesc.name);
					textField.setPadding(new Insets(0));
					textField.setPrefHeight(0);
					textField.setStyle("-fx-background-color: -fx-control-inner-background;");
					textField.focusedProperty().addListener((a, b, c) -> {
						if (!c)
							commitEdit(textField, beanDesc);
					});
					textField.setOnKeyPressed(e -> {
						if (e.getCode() == KeyCode.ENTER)
							commitEdit(textField, beanDesc);
					});
					HBox.setHgrow(textField, Priority.ALWAYS);
					setGraphic(new HBox(3, new ImageView(OperatorManager.isOperator(beanDesc.bean) ? Operators.OPERATOR_INST_ICON : Operators.BEAN_ICON), textField));
					textField.requestFocus();
				}

				@Override
				protected void updateItem(Object item, boolean empty) {
					super.updateItem(item, empty);
					if (item != null) {
						TreeItem<Object> ti = getTreeItem();
						setGraphic(ti.getGraphic());
						setText(item.toString());
						String toolTipText = null;
						if (ti instanceof ModuleTreeItem) {
							Module module = ((ModuleTreeItem) ti).module;
							toolTipText = "Path: " + ModuleManager.getModulePath(((ModuleTreeItem) ti).module);
							ModuleDescriptor descriptor = module.getDescriptor();
							Optional<Version> version = descriptor.version();
							if (version.isPresent())
								toolTipText += System.lineSeparator() + "Version: " + version.get();
							String requiresText = descriptor.requires().stream().filter(r -> !ModuleManager.PARENT_MODULES_NAME.contains(r.name()))
									.map(r -> r.name() + (r.compiledVersion().isPresent() ? r.compiledVersion() : "")).collect(Collectors.joining(System.lineSeparator() + "\t"));
							if (!requiresText.isEmpty())
								toolTipText += System.lineSeparator() + "Requires: " + System.lineSeparator() + "\t" + requiresText;
						} else if (ti instanceof OperatorClassTreeItem)
							toolTipText = ((OperatorClassTreeItem<?>) ti).type.getName();
						if (toolTipText != null) {
							Tooltip tt = getTooltip();
							if (tt == null)
								setTooltip(new Tooltip(toolTipText));
							else
								tt.setText(toolTipText);
						} else
							setTooltip(null);

					} else {
						setGraphic(null);
						setText(null);
						setTooltip(null);
					}
				}
			};
			cell.setOnContextMenuRequested(e -> {
				if (cell.isEmpty()) {
					MenuItem rmmi = new MenuItem("Register module");
					rmmi.setOnAction(e1 -> {
						FileChooser fc = new FileChooser();
						fc.setSelectedExtensionFilter(new ExtensionFilter("Java module", "*.jar"));
						fc.setTitle("Register new module in Scenarium");
						File moduleFile = fc.showOpenDialog(this.stage);
						if (moduleFile != null)
							try {
								ModuleManager.registerExternalModules(moduleFile.toPath());
							} catch (IllegalArgumentException ex) {
								AlertUtil.show(ex, false);
							}
					});
					new ContextMenu(rmmi).show(cell, e.getScreenX(), e.getScreenY());
				} else {
					TreeItem<Object> treeItem = cell.getTreeItem();
					if (treeItem == null)
						return;
					ArrayList<MenuItem> menus = new ArrayList<>();
					if (treeItem instanceof BeanTreeItem) {
						MenuItem deleteMenu = new MenuItem("Delete");
						menus.add(deleteMenu);
						deleteMenu.setOnAction(e11 -> removeBean(((BeanTreeItem) treeItem).beanDesc, true, false));
						deleteMenu.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
						MenuItem deleteAndPurgeMenu = new MenuItem("Delete and delete sub beans");
						menus.add(deleteAndPurgeMenu);
						deleteAndPurgeMenu.setOnAction(e12 -> removeBean(((BeanTreeItem) treeItem).beanDesc, true, true));
						deleteAndPurgeMenu.setAccelerator(new KeyCodeCombination(KeyCode.DELETE, KeyCombination.SHIFT_DOWN));
					} else if (treeItem instanceof OperatorClassTreeItem) {
						MenuItem newInstMenu = new MenuItem("New instance...");
						newInstMenu.setAccelerator(new KeyCodeCombination(KeyCode.N));
						menus.add(newInstMenu);
						newInstMenu.setOnAction(e13 -> {
							try {
								BeanEditor.registerCreatedBeanAndCreateSubBean(((OperatorClassTreeItem<?>) treeItem).type.getConstructor().newInstance(), BeanManager.defaultDir);
							} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
								ex.printStackTrace();
							}
						});
					} else if (treeItem instanceof ModuleTreeItem && !ModuleManager.isInternalModule(((ModuleTreeItem) treeItem).module)) {
						MenuItem unregisterMenu = new MenuItem("Unregister");
						unregisterMenu.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
						menus.add(unregisterMenu);
						unregisterMenu.setOnAction(e13 -> {
							ModuleManager.unregisterExternalModule(((ModuleTreeItem) treeItem).module);
							System.gc();
							new Thread(() -> {
								while (true) {
									try {
										Thread.sleep(1000);
									} catch (InterruptedException ex) {
										ex.printStackTrace();
									}
									Log.info("gc");
									System.gc();
									break;
								}
							}).start();
						});
					}
					if (!treeItem.isLeaf()) {
						MenuItem eaMenu = new MenuItem("Expand All");
						eaMenu.setAccelerator(new KeyCodeCombination(KeyCode.E));
						eaMenu.setOnAction(e22 -> expandTreeView(treeItem));
						menus.add(eaMenu);
					}
					if (!menus.isEmpty())
						new ContextMenu(menus.toArray(MenuItem[]::new)).show(cell, e.getScreenX(), e.getScreenY());
				}
				e.consume();
			});
			return cell;
		});
		this.tree.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.DELETE) {
				TreeItem<Object> selectedItem = this.tree.getSelectionModel().getSelectedItem();
				if (selectedItem instanceof BeanTreeItem)
					removeBean(((BeanTreeItem) selectedItem).beanDesc, true, e.isShiftDown());
				else if (selectedItem instanceof ModuleTreeItem)
					ModuleManager.unregisterExternalModule(((ModuleTreeItem) selectedItem).module);
				// tree.getRoot().getChildren().remove(selectedItem);
			} else if (e.getCode() == KeyCode.N) {
				TreeItem<Object> selectedItem = this.tree.getSelectionModel().getSelectedItem();
				if (selectedItem instanceof OperatorClassTreeItem)
					try {
						BeanEditor.registerCreatedBeanAndCreateSubBean(((OperatorClassTreeItem<?>) selectedItem).type.getConstructor().newInstance(), BeanManager.defaultDir);
					} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
						ex.printStackTrace();
					}
			} else if (e.getCode() == KeyCode.E) {
				TreeItem<Object> selectedItem = this.tree.getSelectionModel().getSelectedItem();
				expandTreeView(selectedItem == null ? this.tree.getRoot() : selectedItem);
			}
		});
		this.tree.setOnDragDetected(e -> {
			ClipboardContent content = new ClipboardContent();
			TreeItem<Object> item = this.tree.getSelectionModel().getSelectedItem();
			if (item instanceof BeanTreeItem) {
				BeanDesc<?> beanDesc = ((BeanTreeItem) item).beanDesc;
				content.put(OperatorManager.isOperator(beanDesc.bean) ? OPERATOR_FORMAT : BEAN_FORMAT, beanDesc.getCompleteDescriptor());
				this.tree.startDragAndDrop(TransferMode.MOVE).setContent(content);
				e.consume();
			} else if (item instanceof OperatorClassTreeItem) {
				content.put(OPERATOR_CLASS_FORMAT, BeanManager.getDescriptorFromClass(((OperatorClassTreeItem<?>) item).type));
				this.tree.startDragAndDrop(TransferMode.MOVE).setContent(content);
				Log.info("dragg: " + ((OperatorClassTreeItem<?>) item).type);
				e.consume();
			}
		});
		// expandTreeView(); // a garder pour étendre l'arbre
		// for (TreeItem<Object> treeItem : rootNode.getChildren())
		// treeItem.setExpanded(true);

		this.beanList = FXCollections.observableArrayList();
		this.beanListView = new ListView<>();
		this.beanListView.setEditable(true);
		for (BeanDesc<?> beanDesc : BeanEditor.getAllBeans())
			this.beanList.add(beanDesc);
		SortedList<BeanDesc<?>> sortedBeanList = this.beanList.sorted((o1, o2) -> o1.name.compareTo(o2.name));
		this.beanListView.setCellFactory(e -> {
			return new ListCell<>() {

				private void commitEdit(TextField textField, BeanDesc<?> beanDesc) {
					cancelEdit();
					if (changeBeanName(beanDesc.bean, textField.getText()))
						getListView().refresh();
				}

				@Override
				public void startEdit() {
					if (!isEditable() || !getListView().isEditable())
						return;
					setText(null);
					BeanDesc<?> beanDesc = getItem();
					TextField textField = new TextField(beanDesc.name);
					textField.setPadding(new Insets(0));
					textField.setPrefHeight(0);
					textField.setStyle("-fx-background-color: -fx-control-inner-background;");
					textField.focusedProperty().addListener((a, oldValue, newValue) -> {
						if (!newValue)
							commitEdit(textField, beanDesc);
					});
					textField.setOnKeyPressed(e -> {
						if (e.getCode() == KeyCode.ENTER)
							commitEdit(textField, beanDesc);
					});
					HBox.setHgrow(textField, Priority.ALWAYS);
					setGraphic(new HBox(3, new ImageView(OperatorManager.isOperator(beanDesc.bean) ? OPERATOR_INST_ICON : BEAN_ICON), textField));
					textField.requestFocus();
				}

				@Override
				protected void updateItem(BeanDesc<?> item, boolean empty) {
					super.updateItem(item, empty);
					if (item == null || empty) {
						setText(null);
						setGraphic(null);
					} else {
						setText(item.name);
						setGraphic(new ImageView(OperatorManager.isOperator(item.bean) ? OPERATOR_INST_ICON : BEAN_ICON));
					}
				}
			};
		});
		this.beanListView.setItems(sortedBeanList);
		this.beanListView.getSelectionModel().selectedItemProperty().addListener((a, b, op) -> updatePanelOperator(op == null ? null : op.bean));
		this.beanListView.setOnDragDetected(e -> {
			BeanDesc<?> beanDesc = this.beanListView.getSelectionModel().getSelectedItem();
			if (beanDesc != null) {
				ClipboardContent content = new ClipboardContent();
				content.put(OperatorManager.isOperator(beanDesc.bean) ? OPERATOR_FORMAT : BEAN_FORMAT, beanDesc.getCompleteDescriptor());
				this.beanListView.startDragAndDrop(TransferMode.MOVE).setContent(content);
				e.consume();
			}
		});
		this.beanListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		this.beanListView.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.DELETE)
				for (BeanDesc<?> beanDesc : new ArrayList<>(this.beanListView.getSelectionModel().getSelectedItems()))
					removeBean(beanDesc, true, e.isShiftDown());
		});
		TextField searchField = new TextField();
		searchField.setPromptText("search");
		searchField.textProperty().addListener(e -> this.beanListView.setItems(sortedBeanList.filtered(a -> a.name.toLowerCase().contains(searchField.getText().toLowerCase()))));
		VBox.setVgrow(this.tree, Priority.ALWAYS);
		VBox.setVgrow(this.beanListView, Priority.ALWAYS);

		TextField searchFieldTree = new TextField();
		rootNode.predicateProperty().bind(Bindings.createObjectBinding(() -> {
			if (searchFieldTree.getText() == null || searchFieldTree.getText().isEmpty())
				return null;
			return TreeItemPredicate.create(actor -> actor.toString().toLowerCase().contains(searchFieldTree.getText().toLowerCase()));
		}, searchFieldTree.textProperty()));

		splitPane1.getItems().addAll(new VBox(searchFieldTree, this.tree), new VBox(searchField, this.beanListView));
		SplitPane splitPane2 = new SplitPane();
		splitPane2.setOrientation(Orientation.HORIZONTAL);
		splitPane2.setDividerPositions(0.3);
		splitPane2.setPrefSize(800, 600);
		this.operatorPane = new BorderPane();
		splitPane2.getItems().addAll(splitPane1, this.operatorPane);

		BeanEditor.addStrongRefBeanRegisterListener(this);
		BeanEditor.addStrongRefBeanUnregisterListener(this);
		BeanEditor.addStrongRefBeanRenameListener(this);
		BeanManager.addSubBeanChangeListener(this);
		return splitPane2;
	}

	private static boolean changeBeanName(Object bean, String newName) {
		BeanDesc<?> oldBeanDesc = BeanEditor.getBeanDesc(bean);
		if (oldBeanDesc != null) {
			String oldName = oldBeanDesc.name;
			if (!oldName.equals(newName))
				try {
					BeanEditor.renameBean(oldBeanDesc, newName);
					return true;
				} catch (IllegalArgumentException e) {
					Log.error(e.getMessage());
				}
		}
		return false;
	}

	private static void populateInternOperatorNode(FilterableTreeItem<Object> rootNode) {
		// Get a list of internal operators and a map of external classes for each module
		HashMap<Module, ArrayList<Class<?>>> externModules = new HashMap<>();
		ArrayList<Class<?>> internalOperators = new ArrayList<>();
		for (Class<?> operator : OperatorManager.getOperators())
			// Log.info(operator.getSimpleName() + "\t" + operator.getModule() + "\t" + (operator.getClassLoader() == dcl ? " is intern" : "isExtern"));
			if (!ModuleManager.isBuiltInModule(operator.getModule())) {
				Module module = operator.getModule();
				ArrayList<Class<?>> operators = externModules.get(operator.getModule());
				if (operators == null) {
					operators = new ArrayList<>();
					externModules.put(module, operators);
				}
				operators.add(operator);
			} else
				internalOperators.add(operator);
		rootNode.getInternalChildren().sort((r1, r2) -> r1.toString().compareTo(r2.toString()));
		// populate intern operators and extern modules
		populateOperatorsTreeItems(rootNode, internalOperators, operator -> {
			String className = operator.getName();
			if (className.contains(OPERATOR_PACKAGE_NAME))
				return className.substring(className.lastIndexOf(OPERATOR_PACKAGE_NAME) + OPERATOR_PACKAGE_NAME.length());
			int index1 = className.lastIndexOf(".") - 1;
			int index2 = className.lastIndexOf(".", index1);
			return className.substring((index2 != -1 ? index2 : index1 + 1) + 1); // attention index1 + 1 ajout + 1 car bug operator racine sinon
		});
		externModules.forEach((module, operators) -> rootNode.getInternalChildren().add(new ModuleTreeItem(module, operators)));
	}

	private void removeBean(BeanDesc<?> beanDescToRemove, boolean delete, boolean deleteSubBeans) {
		BeanManager.purgeBean(beanDescToRemove.bean, delete, deleteSubBeans);
		this.beanList.remove(beanDescToRemove);
		LinkedList<TreeItem<Object>> nodeToTest = new LinkedList<>();
		nodeToTest.add(this.tree.getRoot());
		while (!nodeToTest.isEmpty()) {
			FilterableTreeItem<Object> node = (FilterableTreeItem<Object>) nodeToTest.pop();
			node.getInternalChildren().removeIf(sonNode -> {
				boolean toRemove = sonNode instanceof BeanTreeItem && ((BeanTreeItem) sonNode).beanDesc == beanDescToRemove;
				if (!toRemove && !sonNode.isLeaf())
					nodeToTest.push(sonNode);
				return toRemove;
			});
		}
		if (this.beanManager != null && beanDescToRemove.bean == this.beanManager.getBean()) {
			this.beanManager = null;
			updatePanelOperator(null);
		}
		// Remove pk avant??? BeanManager.purgeBean le détruit déja via BeanManager.link...
		// new File(BeanManager.defaultDir + beanDescToRemove.bean.getClass().getSimpleName() + "-" + beanDescToRemove.name + BeanManager.SERIALIZEEXT).delete(); //Remove pk avant???
	}

	@Override
	public void subBeanChange(BeanDesc<?> owner) {
		LinkedList<FilterableTreeItem<Object>> nodeToTest = new LinkedList<>();
		nodeToTest.add((FilterableTreeItem<Object>) this.tree.getRoot());
		while (!nodeToTest.isEmpty()) {
			FilterableTreeItem<Object> node = nodeToTest.pop();
			for (TreeItem<Object> treeItem : node.getChildren()) {
				if (treeItem instanceof BeanTreeItem && ((BeanTreeItem) treeItem).beanDesc == owner)
					((BeanTreeItem) treeItem).reset();
				if (!treeItem.isLeaf())
					nodeToTest.push((FilterableTreeItem<Object>) treeItem);
			}
		}

	}

	private void updatePanelOperator(Object bean) {
		if (this.beanManager != null) {
			this.beanManager.saveIfChanged();
			this.beanManager = null;
		}
		if (bean != null) {
			this.beanManager = new BeanManager(bean, BeanManager.defaultDir);
			GridPane editor = this.beanManager.getEditor();
			ScrollPane sp = new ScrollPane(editor);
			sp.setFitToWidth(true);
			sp.setHbarPolicy(ScrollBarPolicy.NEVER);
			this.operatorPane.setCenter(sp);
			Button resetButton = new Button("Reset");
			resetButton.setOnAction(e -> {
				if (this.beanManager != null) {
					this.beanManager.reset();
					updatePanelOperator(this.beanManager.getBean());
				}
			});
			Button refreshButton = new Button("Refresh");
			refreshButton.setOnAction(e -> {
				if (this.beanManager != null)
					updatePanelOperator(this.beanManager.getBean());
			});
			Button saveButton = new Button("Save");
			saveButton.setOnAction(e -> {
				if (this.beanManager != null)
					this.beanManager.saveIfChanged();
			});
			Button cancelButton = new Button("Cancel");
			cancelButton.setOnAction(e -> {
				if (this.beanManager != null) {
					this.beanManager.load();
					this.beanManager.isEdited = false;
					BeanManager.createSubBeans(this.beanManager.getBean(), BeanManager.defaultDir);
					updatePanelOperator(this.beanManager.getBean());
				}
			});
			HBox vBox = new HBox(10, resetButton, refreshButton, saveButton, cancelButton);
			vBox.setAlignment(Pos.CENTER);
			vBox.setPadding(new Insets(3, 3, 3, 3));
			this.operatorPane.setBottom(vBox);
		} else
			this.operatorPane.setCenter(null);
	}

	@SuppressWarnings("unchecked")
	static void populateOperatorsTreeItems(FilterableTreeItem<Object> rootItem, List<Class<?>> operators, Function<Class<?>, String> nameProvider) {
		// Build a map of operators given the root package
		LinkedHashMap<String, Object> nodeMenuItemTools = new LinkedHashMap<>();
		for (Class<?> type : operators) {
			String typeName = nameProvider.apply(type);
			LinkedHashMap<String, Object> currentNode = nodeMenuItemTools;
			while (typeName.contains(".")) {
				int fi = typeName.indexOf(".");
				String parentName = typeName.substring(0, fi);
				parentName = parentName.replaceFirst(".", Character.toString(Character.toUpperCase(parentName.charAt(0))));
				typeName = typeName.substring(fi + 1);
				if (currentNode.get(parentName) == null) {
					LinkedHashMap<String, Object> sonNode = new LinkedHashMap<>();
					currentNode.put(parentName, sonNode);
					currentNode = sonNode;
				} else if (!(currentNode.get(parentName) instanceof HashMap)) {
					Log.error("An operator have the same name: " + parentName + " than an operator package: " + currentNode.get(parentName).toString());
					System.exit(-1);
				} else
					currentNode = (LinkedHashMap<String, Object>) currentNode.get(parentName);
			}
			currentNode.put(typeName, type);
		}
		// Get a list of all bean files
		ArrayList<String> beansPath = new ArrayList<>();
		for (File beanFile : new File(BeanManager.defaultDir).listFiles())
			if (beanFile.getName().endsWith(BeanManager.SERIALIZE_EXT))
				beansPath.add(beanFile.getName());
		// Build the tree of TreeItem and load files corresponding to operators
		LinkedList<HashMap<?, ?>> stTool = new LinkedList<>();
		LinkedList<ObservableList<TreeItem<Object>>> stDMTN = new LinkedList<>();
		stTool.push(nodeMenuItemTools);
		stDMTN.push(rootItem.getInternalChildren());
		while (!stTool.isEmpty()) {
			HashMap<?, ?> currToolNode = stTool.pop();
			ObservableList<TreeItem<Object>> currToolMenu = stDMTN.pop();
			currToolNode.forEach((tool, element) -> {
				if (element instanceof Class<?>) {
					currToolMenu.add(new OperatorClassTreeItem<>((Class<?>) element));
					loadSubBeanOperatorNode((Class<?>) element, beansPath);
				} else {
					FilterableTreeItem<Object> toolMenu = new FilterableTreeItem<>(tool, new ImageView(CLOSED_FOLDER_ICON));
					toolMenu.expandedProperty().addListener(e -> toolMenu.setGraphic(new ImageView(toolMenu.isExpanded() ? OPEN_FOLDER_ICON : CLOSED_FOLDER_ICON)));
					currToolMenu.add(toolMenu);
					stTool.push((HashMap<?, ?>) element);
					stDMTN.push(toolMenu.getInternalChildren());
				}
			});
		}

		// Sort tree to get firstly the package and then the operators
		LinkedList<FilterableTreeItem<Object>> todo = new LinkedList<>();
		todo.add(rootItem);
		while (!todo.isEmpty()) {
			FilterableTreeItem<Object> ti = todo.pop();
			ObservableList<TreeItem<Object>> children = ti.getInternalChildren();
			children.sort((r1, r2) -> {
				boolean isR1Op = r1 instanceof OperatorClassTreeItem;
				boolean isR2Op = r2 instanceof OperatorClassTreeItem;
				if (isR1Op && !isR2Op)
					return 1;
				else if (!isR1Op && isR2Op)
					return -1;
				return r1.toString().compareTo(r2.toString());
			});
			children.forEach(c -> todo.add((FilterableTreeItem<Object>) c));
		}
	}

	private static void loadSubBeanOperatorNode(Class<?> beanClass, ArrayList<String> beansPath) {
		String baseName = BeanDesc.getSimpleBaseDescriptor(beanClass);
		for (String fileName : beansPath)
			if (fileName.startsWith(baseName)) {
				String brotherName = fileName.substring(baseName.length());
				try {
					String brotherBeanName = brotherName.substring(0, brotherName.lastIndexOf("."));
					if (BeanEditor.getRegisterBean(beanClass, brotherBeanName) == null) {
						Object brotherBean = beanClass.getConstructor().newInstance();
						BeanDesc<?> beanDesc = BeanEditor.registerBean(brotherBean, brotherBeanName, BeanManager.defaultDir);
						if (!new BeanManager(brotherBean, BeanManager.defaultDir).load(new File(BeanManager.defaultDir + fileName)))
							BeanEditor.unregisterBean(beanDesc);
					}
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
			}
	}

	@Override
	public void loaded(Module module) {
		TreeView<Object> tree = this.tree;
		if (tree != null)
			FxUtils.runLaterIfNeeded(() -> ((FilterableTreeItem<Object>) tree.getRoot()).getInternalChildren()
					.add(new ModuleTreeItem(module, OperatorManager.getOperators().stream().filter(o -> o.getModule().equals(module)).collect(Collectors.toList()))));
	}

	@Override
	public Runnable modified(Module module) {
		return null;
	}

	@Override
	public void unloaded(Module module) {
		FxUtils.runLaterIfNeeded(() -> { // No Platform.runLater, the module must be removed immediately to release the jar file
			TreeView<Object> tree = this.tree;
			if (tree != null)
				((FilterableTreeItem<Object>) tree.getRoot()).getInternalChildren().removeIf(child -> child instanceof ModuleTreeItem && ((ModuleTreeItem) child).module.equals(module));
			// Bug javafx SelectedItemsReadOnlyObservableList itemsListChange = c; sert à rien et créer un leak quand je retire un module
			this.beanList.add(new BeanDesc<>(null, "", ""));
			this.beanList.remove(this.beanList.size() - 1);
		});
	}
}

class BeanTreeItem extends FilterableTreeItem<Object> {
	private boolean isFirstTime = true;
	public final BeanDesc<?> beanDesc;
	private boolean isResetting;

	@Override
	public ObservableList<TreeItem<Object>> getInternalChildren() {
		if (this.isFirstTime)
			populate();
		return super.getInternalChildren();
	}

	@Override
	public boolean isLeaf() {
		if (this.isFirstTime && !this.isResetting)
			populate();
		return super.isLeaf();
	}

	private void populate() {
		LinkedList<Object> bi = BeanManager.getSubBeans(this.beanDesc.bean, BeanManager.defaultDir);
		if (bi != null)
			for (Object subBean : bi) {
				TreeItem<Object> parent = this;
				boolean isInParents = false;
				try { // l'arbre peut étre en cours de chargement -> getBeanDesc renvoie null -> cancel
					BeanDesc<?> subBeanDesc = BeanEditor.getBeanDesc(subBean);
					while (parent instanceof BeanTreeItem) {
						if (((BeanTreeItem) parent).beanDesc == subBeanDesc) {
							isInParents = true;
							break;
						}
						parent = parent.getParent();
					}
					this.isFirstTime = false;
					if (!isInParents) {
						super.getInternalChildren().add(new BeanTreeItem(BeanEditor.getBeanDesc(subBean)));
						super.getInternalChildren().sort((r1, r2) -> r1.toString().compareTo(r2.toString()));
					}
				} catch (NullPointerException e) {
					this.isFirstTime = true;
					return;
				}
			}
	}

	public void reset() {
		this.isResetting = true;
		this.isFirstTime = true;
		super.getInternalChildren().clear();
		this.isResetting = false;
		// }
	}

	public BeanTreeItem(BeanDesc<?> beanDesc) {
		super(beanDesc, new ImageView(OperatorManager.isOperator(beanDesc.bean) ? Operators.OPERATOR_INST_ICON : Operators.BEAN_ICON));
		this.beanDesc = beanDesc;
	}
}

class OperatorClassTreeItem<T> extends FilterableTreeItem<Object> {
	private static final Image OPERATOR_CLASS_ICON = new Image(LoadPackageStream.getStream("/class.png"));
	public final Class<T> type;
	private boolean isFirstTime = true;
	private boolean isResetting;

	public OperatorClassTreeItem(Class<T> type) {
		super(type.getSimpleName(), new ImageView(OPERATOR_CLASS_ICON));
		this.type = type;
	}

	@Override
	public ObservableList<TreeItem<Object>> getInternalChildren() {
		if (this.isFirstTime)
			populate();
		return super.getInternalChildren();
	}

	@Override
	public boolean isLeaf() {
		if (this.isFirstTime && !this.isResetting)
			populate();
		return super.isLeaf();
	}

	private void populate() {
		this.isFirstTime = false;
		List<BeanDesc<T>> bi = BeanEditor.getBeanInstance(this.type);
		if (bi != null) {
			ArrayList<BeanTreeItem> beanTreeItems = new ArrayList<>();
			for (BeanDesc<T> beanDesc : bi)
				beanTreeItems.add(new BeanTreeItem(beanDesc));
			super.getInternalChildren().addAll(beanTreeItems);
			super.getInternalChildren().sort((r1, r2) -> r1.toString().compareTo(r2.toString()));
		}
	}

	public void reset() {
		this.isResetting = true;
		this.isFirstTime = true;
		super.getInternalChildren().clear();
		this.isResetting = false;
		// }
	}
}

class ModuleTreeItem extends FilterableTreeItem<Object> {
	private static final Image MODULECLASSICON = new Image(LoadPackageStream.getStream("/module.png"));
	public final Module module;

	public ModuleTreeItem(Module module, List<Class<?>> operators) {

		super(module.getName(), new ImageView(MODULECLASSICON));
		this.module = module;
		// Get the root package
		if (!operators.isEmpty()) {
			String name = operators.get(0).getName();
			name = name.substring(0, name.lastIndexOf("."));
			String[] operatorPath = name.split("\\.");
			for (int i = 1; i < operators.size(); i++) {
				String[] os = operators.get(i).getName().split("\\.");
				int max = Math.min(operatorPath.length, os.length);
				for (int j = 0; j < max; j++)
					if (!operatorPath[j].equals(os[j])) {
						String[] newOperatorPath = new String[j];
						System.arraycopy(operatorPath, 0, newOperatorPath, 0, newOperatorPath.length);
						operatorPath = newOperatorPath;
						break;
					}
			}

			int roorPackageNameSize = String.join(".", operatorPath).length() + 1;
			Operators.populateOperatorsTreeItems(this, operators, type -> type.getName().substring(roorPackageNameSize));
		}
	}
}

// Source: https://github.com/eclipse-efx/efxclipse-rt/blob/3.x/modules/ui/org.eclipse.fx.ui.controls/src/main/java/org/eclipse/fx/ui/controls/tree/FilterableTreeItem.java
class FilterableTreeItem<T> extends TreeItem<T> {
	private final ObservableList<TreeItem<T>> sourceList = FXCollections.observableArrayList();
	private final FilteredList<TreeItem<T>> filteredList = new FilteredList<>(this.sourceList);

	private final ObjectProperty<TreeItemPredicate<T>> predicate = new SimpleObjectProperty<>();

	public FilterableTreeItem(T value) {
		this(value, null);
	}

	public FilterableTreeItem(T value, Node graphic) {
		super(value, graphic);
		this.filteredList.predicateProperty().bind(Bindings.createObjectBinding(() -> {
			Predicate<TreeItem<T>> p = child -> {
				if (child instanceof FilterableTreeItem) {
					FilterableTreeItem<T> filterableChild = (FilterableTreeItem<T>) child;
					filterableChild.setPredicate(this.predicate.get());
				}
				if (this.predicate.get() == null)
					return true;
				if (child.getChildren().size() > 0)
					return true;
				return this.predicate.get().test(this, child.getValue());
			};
			return p;
		}, this.predicate));
		Bindings.bindContent(super.getChildren(), getBackingList());
	}

	@Override
	public ObservableList<TreeItem<T>> getChildren() {
		return super.getChildren();
	}

	protected ObservableList<TreeItem<T>> getBackingList() {
		return this.filteredList;
	}

	public ObservableList<TreeItem<T>> getInternalChildren() {
		return this.sourceList;
	}

	public final ObjectProperty<TreeItemPredicate<T>> predicateProperty() {
		return this.predicate;
	}

	public final TreeItemPredicate<T> getPredicate() {
		return this.predicate.get();
	}

	public final void setPredicate(TreeItemPredicate<T> predicate) {
		this.predicate.set(predicate);
	}
}

@FunctionalInterface
interface TreeItemPredicate<T> {
	boolean test(TreeItem<T> parent, T value);

	static <T> TreeItemPredicate<T> create(Predicate<T> predicate) {
		return (parent, value) -> predicate.test(value);
	}

}
