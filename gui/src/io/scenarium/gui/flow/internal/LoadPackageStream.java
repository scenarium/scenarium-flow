package io.scenarium.gui.flow.internal;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

public class LoadPackageStream {
	private LoadPackageStream() {}

	public static InputStream getStream(String resourceName) {
		Log.verbose("Load resource: '" + resourceName + "'");
		InputStream out = LoadPackageStream.class.getResourceAsStream(resourceName);
		if (out == null) {
			Log.error("Can not load resource: '" + resourceName + "'");
			/*
			try {
				URL tmp = LoadPackageStream.class.getResource("/module-info.class");
				Log.warning("    url = " + tmp);
				Log.warning("    url = " + tmp.toString());
				tmp = new URL(tmp.toString().substring(0, tmp.toString().lastIndexOf("/")));
				Log.warning("    url = " + tmp);
				Log.warning("list = " + LoadPackageStream.getResources(new URL(LoadPackageStream.class.getResource("/module-info.class").getPath())));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			*/
		}
		return out;
	}

	public static List<String> getResources(final URL element) {
		final ArrayList<String> retval = new ArrayList<>();

		if (element.getProtocol().contentEquals("file")) {
			final File file = new File(element.getPath());
			retval.addAll(getResourcesFromDirectory(file));
		} else if (element.getProtocol().contentEquals("jar")) {
			// final File file = new File(element.getPath());
			// Log.debug(" ---- " + getResourcesFromJarFile(file, pattern));

			URL fileUrl;
			String pathInJar = "/";
			try {
				String pathOfJar = element.getPath();
				int idSeparate = pathOfJar.indexOf('!');
				if (idSeparate != -1) {
					pathInJar = pathOfJar.substring(idSeparate + 1);
					Log.error("Path in JAR: " + pathInJar);
					while (pathInJar.startsWith("/"))
						pathInJar = pathInJar.substring(1);
					Log.error("Path in JAR: " + pathInJar);
					pathOfJar = pathOfJar.substring(0, idSeparate);
					Log.error("Path of JAR: " + pathOfJar);
				}
				Log.error("Path in JAR 2 : " + pathInJar);
				Log.error("Path of JAR 2 : " + pathOfJar);
				fileUrl = new URL(pathOfJar);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return retval;
			}
			List<String> tmpOut = null;
			if (fileUrl.getProtocol().contentEquals("file")) {
				final File file2 = new File(fileUrl.getPath());
				tmpOut = getResourcesFromJarFile(file2, pathInJar);
			}
			for (String elem : tmpOut)
				retval.add("jar:" + fileUrl.toString() + "!/" + elem);
		} else
			Log.warning("can not load resource for other thing than 'jar' or 'file'");
		return retval;
	}

	private static List<String> getResourcesFromJarFile(final File file, String basePath) {
		final ArrayList<String> retval = new ArrayList<>();
		ZipFile zf;
		try {
			zf = new ZipFile(file);
		} catch (final ZipException e) {
			throw new Error(e);
		} catch (final IOException e) {
			throw new Error(e);
		}
		final Enumeration<?> e = zf.entries();
		while (e.hasMoreElements()) {
			final ZipEntry ze = (ZipEntry) e.nextElement();
			final String fileName = ze.getName();
			// Log.error(" - check file : " + fileName);
			if (fileName.startsWith(basePath) && !fileName.endsWith("/"))
				retval.add(fileName);
		}
		try {
			zf.close();
		} catch (final IOException ex) {
			throw new Error(ex);
		}
		return retval;
	}

	private static List<String> getResourcesFromDirectory(final File directory) {
		final ArrayList<String> retval = new ArrayList<>();
		final File[] fileList = directory.listFiles();
		for (final File file : fileList)
			if (file.isFile())
				try {
					retval.add(file.getCanonicalPath());
				} catch (IOException e) {
					e.printStackTrace();
				}
		return retval;
	}

}
