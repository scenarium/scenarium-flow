package io.scenarium.gui.flow;

import java.util.List;

import io.beanmanager.PluginsBeanSupplier;
import io.beanmanager.consumer.ClassNameRedirectionConsumer;
import io.beanmanager.editors.container.BeanEditor;
import io.scenarium.flow.FlowDiagram;
import io.scenarium.flow.consumer.OperatorConsumer;
import io.scenarium.flow.consumer.PluginsFlowSupplier;
import io.scenarium.flow.operator.OperatorManager;
import io.scenarium.gui.core.PluginsGuiCoreSupplier;
import io.scenarium.gui.core.consumer.DrawersConsumer;
import io.scenarium.gui.core.consumer.ToolBarConsumer;
import io.scenarium.gui.core.display.ToolBarInfo;
import io.scenarium.gui.flow.display.drawer.DiagramDrawer;
import io.scenarium.gui.flow.display.toolBar.Operators;
import io.scenarium.gui.flow.internal.Log;
import io.scenarium.gui.flow.operator.player.SubFlowDiagram;
import io.scenarium.gui.flow.operator.viewer.FlowViewer;
import io.scenarium.gui.flow.operator.viewer.Oscilloscope;
import io.scenarium.gui.flow.operator.viewer.Viewer;
import io.scenarium.pluginManager.PluginsSupplier;

import javafx.scene.input.KeyCode;

public class Plugin implements PluginsSupplier, PluginsFlowSupplier, PluginsGuiCoreSupplier, PluginsBeanSupplier {

	@Override
	public void birth() {
		// TODO add a populate element in bean supplier'
		BeanEditor.setIconeProvider(obj -> OperatorManager.isOperator(obj) ? Operators.OPERATOR_INST_ICON : null);
	}

	@Override
	public void populateOperators(OperatorConsumer operatorConsumer) {
		operatorConsumer.accept(SubFlowDiagram.class); // Replace the original with this one
		operatorConsumer.accept(Viewer.class);
		operatorConsumer.accept(FlowViewer.class);
		operatorConsumer.accept(Oscilloscope.class);
	}

	@Override
	public void populateClassNameRedirection(ClassNameRedirectionConsumer classNameRedirectionConsumer) {
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.viewer.FlowViewer", "io.scenarium.gui.flow.operator.viewer.FlowViewer");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.viewer.Oscilloscope", "io.scenarium.gui.flow.operator.viewer.Oscilloscope");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.viewer.Viewer", "io.scenarium.gui.flow.operator.viewer.Viewer");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.viewer.conversion.ToCrossMarker", "io.scenarium.gui.flow.operator.viewer.conversion.ToCrossMarker");
	}

	@Override
	public void populateToolBars(ToolBarConsumer toolBarConsumer) {
		Log.warning("Load the operators tools");
		toolBarConsumer.accept(new ToolBarInfo(Operators.class, KeyCode.O, false, List.of(DiagramDrawer.class)));
	}

	@Override
	public void populateDrawers(DrawersConsumer drawersConsumer) {
		drawersConsumer.accept(FlowDiagram.class, DiagramDrawer.class);
	}
}
