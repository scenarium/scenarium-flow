/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.flow.operator.viewer;

import java.util.concurrent.ConcurrentLinkedQueue;

import io.beanmanager.editors.PropertyInfo;
import io.scenarium.flow.operator.EvolvedVarArgsOperator;

import javafx.scene.Scene;
import javafx.scene.control.TextArea;

public class FlowViewer extends AbstractViewer implements EvolvedVarArgsOperator {
	private TextArea ta;
	private final ConcurrentLinkedQueue<Object> pendingDatas = new ConcurrentLinkedQueue<>();
	@PropertyInfo(index = 0, info = "Maximum number of character in the text buffer before removing datas")
	private int maxBufferSize = 10000;

	@Override
	protected void closeViewer() {
		super.closeViewer();
		this.ta = null;
		this.pendingDatas.clear();
	}

	@Override
	public void process(Object... objs) {
		super.process(objs);
		ConcurrentLinkedQueue<Object> pd = this.pendingDatas;
		DrawingObjects tdd = this.toDrawDatas;
		if (pd != null && tdd != null)
			pd.offer(tdd.inputObjects[0]);
	}

	@Override
	protected void paintDatas(DrawingObjects toDrawDatas) {
		if (this.stage == null) {
			initStage();
			if (this.stage == null)
				return;
			this.ta = new TextArea();
			this.stage.setScene(new Scene(this.ta));
			this.stage.show();
		}
		Object object;
		StringBuilder taText = new StringBuilder(this.ta.getText());
		if (taText.length() != 0)
			taText.append("\n");
		while ((object = this.pendingDatas.poll()) != null)
			taText.append(object.toString() + "\n");
		taText.deleteCharAt(taText.length() - 1);
		int maxBufferSize = this.maxBufferSize;
		if (taText.length() > maxBufferSize) {
			int beginIndex = taText.length() - maxBufferSize;
			int newBeginIndex = taText.indexOf("\n", beginIndex);
			if (newBeginIndex != -1)
				beginIndex = newBeginIndex + 1;
			taText.delete(0, beginIndex);
		}
		this.ta.setText(taText.toString());
		this.ta.setScrollTop(Double.MAX_VALUE);
	}

	@Override
	public void death() {
		super.death();
	}

	public int getMaxBufferSize() {
		return this.maxBufferSize;
	}

	public void setMaxBufferSize(int maxBufferSize) {
		var oldValue = this.maxBufferSize;
		this.maxBufferSize = maxBufferSize;
		this.pcs.firePropertyChange("maxBufferSize", oldValue, maxBufferSize);
	}

	@Override
	public boolean canAddInput(Class<?>[] inputsType) {
		return false;
	}

	@Override
	public boolean isValidInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return additionalInput != null;
	}

}
