/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.gui.flow.operator.viewer;

import javax.vecmath.Point2i;

import io.beanmanager.editors.DynamicEnableBean;
import io.beanmanager.editors.PropertyEditor;
import io.beanmanager.editors.PropertyEditorManager;
import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.UpdatableViewBean;
import io.beanmanager.editors.container.BeanEditor;
import io.beanmanager.editors.container.BeanInfo;
import io.beanmanager.editors.container.DynamicBeanInfo;
import io.beanmanager.tools.FxUtils;
import io.scenarium.core.timescheduler.Scheduler;
//import org.scenicview.ScenicView;
import io.scenarium.flow.operator.EvolvedVarArgsOperator;
import io.scenarium.gui.core.display.RenderPane;
import io.scenarium.gui.core.display.ScenariumContainer;
import io.scenarium.gui.core.display.StackableDrawer;
import io.scenarium.gui.core.display.drawer.DrawerManager;
import io.scenarium.gui.core.display.drawer.PrimitiveDrawer;
import io.scenarium.gui.core.display.drawer.TheaterPanel;
import io.scenarium.gui.core.display.toolbarclass.Tool;
import io.scenarium.gui.flow.internal.Log;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputControl;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Text;
import javafx.stage.Stage;
//import javafx.stage.StageStyle;

public class Viewer extends AbstractViewer implements ScenariumContainer, EvolvedVarArgsOperator, DynamicEnableBean, UpdatableViewBean {
	private Region renderComponent;
	private Object editor;
	@PropertyInfo(index = 6, info = "Define if the viewer size is calculated automatically or defined manually by the user")
	private boolean autoSize = true;
	@PropertyInfo(index = 7, info = "Refresh on each new additional inputs")
	private boolean refreshOnNewAdditionalInputs = false;
	@PropertyInfo(index = 8, info = "Properties of the theater panel generated during the last run"/* , readOnly = true */)
	@BeanInfo(inline = true, alwaysExtend = true)
	@DynamicBeanInfo(possibleSubclassesMethodName = "getTypes")
	public TheaterPanel theaterPane;
	private Class<?> oldDrawClass;
	private Class<?>[] oldDrawedObjs = new Class<?>[0];

	@Override
	public void updateIOStructure() {
		super.updateIOStructure();
	}

	@Override
	public void paintDatas(DrawingObjects toDrawDatas) {
		Stage stage = this.stage;
		Region renderComponent = this.renderComponent;
		Class<?> oldDrawClass = this.oldDrawClass;
		// Object _editor = editor;
		Object[] objs = toDrawDatas.inputObjects;
		Object obj = objs[0];
		if (obj == null || objs.length == 0)
			throw new IllegalArgumentException("Cannot paint datas without the first data");
		Class<?> objClass = obj.getClass();
		boolean needToReset;
		if (oldDrawClass == null)
			needToReset = true;
		else if ((this.editor == null || this.editor instanceof PropertyEditor) && objs.length != 1 || this.editor instanceof PropertyEditor[] && objs.length == 1)
			needToReset = true;
		else {
			needToReset = !oldDrawClass.isAssignableFrom(objClass);
			if (needToReset)
				if (this.theaterPane != null && DrawerManager.getSpecializedDrawableObjectClass(this.oldDrawClass).isAssignableFrom(objClass)) // Inversion dans getRenderPanelParentObject
					needToReset = false;
				else if (this.editor instanceof PropertyEditor) {
					PropertyEditor<?> editor = PropertyEditorManager.findEditor(objClass, "");
					if (editor != null && editor.getClass() == this.editor.getClass())
						needToReset = false;
				} else {
					Class<? extends TheaterPanel> rpc = DrawerManager.getRenderPanelClass(objClass);
					if (this.theaterPane != null && rpc.equals(this.theaterPane.getClass()))
						needToReset = false;
				}
		}
		if (needToReset) {
			this.oldDrawedObjs = new Class<?>[0];
			this.editor = null;
			renderComponent = null;
			Class<? extends TheaterPanel> type = DrawerManager.getRenderPanelClass(objClass);
			if (type != null && !type.isAssignableFrom(PrimitiveDrawer.class)) { // Drawer view
				RenderPane renderpanel;
				try {
					renderpanel = this.theaterPane == null || this.theaterPane.getClass() != type ? new RenderPane((Scheduler) null, obj, Viewer.this, null, false, true)
							: new RenderPane((Scheduler) null, obj, Viewer.this, this.theaterPane, false, true);
				} catch (SecurityException | IllegalArgumentException e) {
					e.printStackTrace();
					if (!this.isDefaulting) {
						this.isDefaulting = true;
						Log.error("Cannot create a renderPanel for the View for the drawable element of type: " + objClass.getSimpleName());
					}
					return;
				}
				renderComponent = renderpanel.getPane();
				this.editor = renderpanel;
				if (this.theaterPane != renderpanel.getTheaterPane()) {
					this.theaterPane = renderpanel.getTheaterPane();
					this.propertyChanged = true;
				}
				this.theaterPane.updateTheaterFilter();
				if (objs.length > 1) {
					Object[] des = new Object[objs.length - 1];
					System.arraycopy(objs, 1, des, 0, des.length);
					this.theaterPane.setAdditionalDrawableElement(des);
				}
				this.theaterPane.setAnimated(this.animated);
				this.theaterPane.addTheaterFilterPropertyChangeListener(e -> this.propertyChanged = true);
			} else { // Property view
				if (objs.length == 1) { // Single property view
					PropertyEditor<?> propEditor = PropertyEditorManager.findEditor(objClass, "");
					if (propEditor != null && !(propEditor instanceof BeanEditor) && propEditor.hasCustomView()) {
						propEditor.setValueFromObj(obj);
						renderComponent = propEditor.getView();
						if (renderComponent instanceof TextInputControl)
							((TextInputControl) renderComponent).setEditable(false);
						this.editor = propEditor;
					}
					if (renderComponent == null) {
						renderComponent = new Label(obj.toString());
						renderComponent.setStyle("-fx-font-family: 'monospaced';");
						((Label)renderComponent).setAlignment(Pos.TOP_LEFT);
					}
				} else {
					GridPane gp = new GridPane();
					ColumnConstraints column3 = new ColumnConstraints();
					column3.setHgrow(Priority.ALWAYS);
					gp.getColumnConstraints().addAll(new ColumnConstraints(), new ColumnConstraints(), column3);
					gp.setAlignment(Pos.CENTER);
					updateGridPane(objs, toDrawDatas.inputNames, gp);
					renderComponent = gp;
				}
				this.theaterPane = null;
			}
			if (stage == null) {
				stage = initStage();
				if (stage == null)
					return;
				stage.setScene(new Scene(renderComponent));
			} else
				stage.getScene().setRoot(renderComponent);
			if (this.theaterPane == null)
				stage.getIcons().add(TheaterPanel.SCENARIUM_VIEWER_ICON);
			else
				stage.getIcons().add(this.theaterPane.getIcon());
			renderComponent.setMinWidth(170 + new Text(getBlockName()).getBoundsInLocal().getWidth());
			this.renderComponent = renderComponent;
			if (this.autoSize)
				stage.sizeToScene();
			// ScenicView.show(stage.getScene());
			this.stage = stage;
			if (this.theaterPane != null) {
				this.theaterPane.setIgnoreRepaint(false);
				this.theaterPane.repaint(false);
			}
			// renderComponent = _renderComponent; //Remonter sinon _stage.sizeToScene ne prend pas en compte la modif
			if (!stage.isShowing()) { // Relance une boucle de draw dans le draw..., c'est le bordel après si j'ai pas finis la boucle
				this.oldDrawClass = objClass;
				//stage.initStyle(StageStyle.UNDECORATED);
				//stage.initModality(Modality.NONE);
				stage.show();
				return;
			}
		} else if (this.editor instanceof RenderPane) {
			TheaterPanel tp = ((RenderPane) this.editor).getTheaterPane();
			if (tp.getDrawableElement() != obj)
				tp.setDrawableElement(obj);
			if (objs.length >= 1) {
				Object[] des = new Object[objs.length - 1];
				System.arraycopy(objs, 1, des, 0, des.length);
				tp.setAdditionalDrawableElement(des);
			}
			tp.paintImmediately(false); // si pas true, pas de rafraichissement des images si elles changent de taille
		} else if (this.editor instanceof PropertyEditor)
			((PropertyEditor<?>) this.editor).setValueFromObj(obj);
		else if (this.editor instanceof PropertyEditor[]) {
			if (updateGridPane(objs, toDrawDatas.inputNames, (GridPane) renderComponent) && this.autoSize)
				stage.sizeToScene();
		} else {
			Label label = (Label) renderComponent;
			String objAsString = obj.toString();
			label.setText(objAsString);
			if (label.getWidth() < new Text(objAsString).getLayoutBounds().getWidth() && this.autoSize)
				stage.sizeToScene();
		}
		this.oldDrawClass = objClass;
	}

	@Override
	protected boolean needInputsNames(Object[] objects) {
		if (objects.length != 1) {
			Class<? extends TheaterPanel> type = DrawerManager.getRenderPanelClass(objects[0].getClass());
			return type == null || type.isAssignableFrom(PrimitiveDrawer.class);
		}
		return false;
	}

	private boolean updateGridPane(Object[] objs, String[] inputsNames, GridPane gridPane) {
		boolean sizeToScene = false;
		// if (propertyNames.length - 1 != objs.length) // Arrive si les liens on changé entre temps. Il faudrait le faire quand on recoit les données mais on ne sait pas à ce moment le type de
		// drawer...
		// return false;
		PropertyEditor<?>[] editors = (PropertyEditor<?>[]) this.editor;
		if (this.oldDrawedObjs.length != objs.length) {
			this.oldDrawedObjs = new Class<?>[objs.length];
			editors = new PropertyEditor<?>[objs.length];
			this.editor = editors;
			gridPane.getChildren().clear();
			gridPane.getRowConstraints().clear();
			for (int i = 0; i < editors.length; i++) {
				gridPane.add(new Label(inputsNames[i]), 0, i);
				gridPane.add(new Label(": "), 1, i);
				gridPane.getRowConstraints().add(new RowConstraints());
			}
			sizeToScene = true;
		}
		for (int j = 0; j < editors.length; j++) {
			Object subObj = objs[j];
			if (subObj != null)
				if (this.oldDrawedObjs[j] != subObj.getClass()) { // changement de class
					Region subRenderComponent = null;
					PropertyEditor<?> propEditor = PropertyEditorManager.findEditor(subObj.getClass(), "");
					if (propEditor != null && !(propEditor instanceof BeanEditor) && propEditor.hasCustomView()) {
						propEditor.setValueFromObj(subObj);
						subRenderComponent = propEditor.getView();
						if (subRenderComponent instanceof TextInputControl)
							((TextInputControl) subRenderComponent).setEditable(false);
						editors[j] = propEditor;
						sizeToScene = true;
					}
					if (subRenderComponent == null)
						subRenderComponent = new Label(subObj.toString());
					Node renderComponent = getNodeFromGridPane(gridPane, 2, j);
					if (renderComponent != null)
						gridPane.getChildren().remove(renderComponent);
					gridPane.add(subRenderComponent, 2, j);
					gridPane.getRowConstraints().get(j).setVgrow(propEditor.canGrow() ? Priority.ALWAYS : Priority.NEVER);
					this.oldDrawedObjs[j] = subObj.getClass();
				} else {
					// if (true)
					// return false;
					PropertyEditor<?> subEditor = editors[j];
					Label subLabelPropertyName = (Label) getNodeFromGridPane(gridPane, 0, j);
					if (subLabelPropertyName.getText() != inputsNames[j])
						subLabelPropertyName.setText(inputsNames[j]);
					if (editors[j] == null) {
						Label subLabelValue = (Label) getNodeFromGridPane(gridPane, 2, j);
						String objAsString = subObj.toString();
						subLabelValue.setText(objAsString);
						if (subLabelValue.getWidth() < new Text(objAsString).getLayoutBounds().getWidth())
							sizeToScene = true;
					} else
						subEditor.setValueFromObj(subObj);
				}
		}
		return sizeToScene;
	}

	private static Node getNodeFromGridPane(GridPane gridPane, int col, int row) {
		for (Node node : gridPane.getChildren())
			if (GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row)
				return node;
		return null;
	}

	@Override
	protected void closeViewer() {
		Object editor = this.editor;
		this.editor = null;
		if (editor != null && editor instanceof RenderPane)
			((RenderPane) editor).close();
		this.oldDrawClass = null;
		this.renderComponent = null;
	}

	@Override
	public boolean canAddInput(Class<?>[] inputsType) {
		if (this.editor == null || this.editor instanceof PropertyEditor<?> || this.editor instanceof PropertyEditor<?>[]) {
			Class<? extends TheaterPanel> type = DrawerManager.getRenderPanelClass(inputsType[0]);
			if (type == PrimitiveDrawer.class)
				return true;
			if (type != null && StackableDrawer.class.isAssignableFrom(type))
				return TheaterPanel.canAddInputToRenderer(type.asSubclass(StackableDrawer.class), inputsType);
		}
		if (this.editor instanceof RenderPane) {
			TheaterPanel tp = ((RenderPane) this.editor).getTheaterPane();
			if (tp instanceof StackableDrawer)
				return ((StackableDrawer) tp).canAddInputToRenderer(inputsType);
		}
		return false;
	}

	@Override
	public boolean isValidInput(Class<?>[] inputsType, Class<?> additionalInput) {
		if (additionalInput == null)
			return false;
		if (inputsType.length == 0 || inputsType.length == 1 && inputsType[0] == null)
			return true;
		if (this.editor == null) {
			Class<? extends TheaterPanel> mainType = DrawerManager.getRenderPanelClass(inputsType[0]);
			if (mainType == PrimitiveDrawer.class)
				return DrawerManager.getRenderPanelClass(additionalInput) == PrimitiveDrawer.class;
			if (mainType != null && StackableDrawer.class.isAssignableFrom(mainType))
				return TheaterPanel.isValidAdditionalInput(mainType.asSubclass(StackableDrawer.class), inputsType, additionalInput);
		}
		if (this.editor instanceof RenderPane) {
			TheaterPanel tp = ((RenderPane) this.editor).getTheaterPane();
			if (tp instanceof StackableDrawer)
				return ((StackableDrawer) tp).isValidAdditionalInput(inputsType, additionalInput);
		}
		return true;
	}

	@Override
	protected boolean needToForceRefresh() {
		return this.theaterPane != null && this.theaterPane.isNeedToBeRefresh();
	}

	@Override
	protected boolean needToRefreshForNewAdditionalInputs() {
		return this.refreshOnNewAdditionalInputs;
	}

	@Override
	public void setAnimated(boolean animated) {
		super.setAnimated(animated);
		if (this.theaterPane != null)
			this.theaterPane.setAnimated(animated);
	}

	public boolean isAutoSize() {
		return this.autoSize;
	}

	public void setAutoSize(boolean autoSize) {
		var oldValue = this.autoSize;
		this.autoSize = autoSize;
		Stage stage = this.stage;
		FxUtils.runLaterIfNeeded(() -> {
			if (autoSize && stage != null)
				stage.sizeToScene();
			setEnable();
			updateView();
		});
		this.pcs.firePropertyChange("autoSize", oldValue, autoSize);
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "dimension", !this.autoSize);
	}

	public boolean isRefreshOnNewAdditionalInputs() {
		return this.refreshOnNewAdditionalInputs;
	}

	public void setRefreshOnNewAdditionalInputs(boolean refreshOnNewAdditionalInputs) {
		var oldValue = this.refreshOnNewAdditionalInputs;
		this.refreshOnNewAdditionalInputs = refreshOnNewAdditionalInputs;
		this.pcs.firePropertyChange("refreshOnNewAdditionalInputs", oldValue, refreshOnNewAdditionalInputs);
	}

	public TheaterPanel getTheaterPane() {
		return this.theaterPane;
	}

	// Introspection pour la liste des types possible de drawer
	public Class<?>[] getTypes() {
		return DrawerManager.getDrawers().toArray(Class[]::new);
	}

	public void setTheaterPane(TheaterPanel theaterPane) {
		this.theaterPane = theaterPane;
	}

	@Override
	public void updateView() {
		fireUpdateView(this, "dimension", false);
	}

	@Override
	public void adaptSizeToDrawableElement() {
		if (this.editor instanceof RenderPane)
			((RenderPane) this.editor).adaptSizeToDrawableElement();
		if (!this.autoSize) {
			this.autoSize = true;
			this.propertyChanged = true;
		}
	}

	@Override
	public Point2i getDefaultToolBarLocation(String simpleName) {
		return null;
	}

	@Override
	public Scheduler getScheduler() {
		return null;
	}

	@Override
	public int getSelectedElementFromTheaterEditor() {
		return 0;
	}

	@Override
	public boolean isDefaultToolBarAlwaysOnTop(String simpleName) {
		return false;
	}

	@Override
	public boolean isManagingAccelerator() {
		return false;
	}

	@Override
	public boolean isStatusBar() {
		return this.editor instanceof RenderPane ? ((RenderPane) this.editor).isStatusBar() : false;
	}

	@Override
	public void saveScenario() {}

	@Override
	public void showMessage(String message, boolean error) {}

	@Override
	public void updateStatusBar(String... infos) {
		if (this.editor instanceof RenderPane)
			((RenderPane) this.editor).updateStatusBar(infos);
	}

	@Override
	public void updateToolView(Class<? extends Tool> toolClass, boolean isVisible) {}

	@Override
	public void showTool(Class<? extends Tool> toolClass, boolean closeIfOpen) {}
}
