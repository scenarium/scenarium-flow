package io.scenarium.gui.flow.operator.player;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.function.BiFunction;
import java.util.function.Consumer;

import javax.vecmath.Point2i;

import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.container.BeanInfo;
import io.beanmanager.tools.FxUtils;
import io.scenarium.core.Scenarium;
import io.scenarium.core.filemanager.scenariomanager.ScenarioException;
import io.scenarium.core.timescheduler.Scheduler;
import io.scenarium.core.timescheduler.VisuableSchedulable;
import io.scenarium.core.tools.AnimationTimerConsumer;
import io.scenarium.gui.core.display.RenderPane;
import io.scenarium.gui.core.display.ScenarioFileChooserFx;
import io.scenarium.gui.core.display.ScenariumContainer;
import io.scenarium.gui.core.display.toolbarclass.Tool;
import io.scenarium.gui.flow.display.drawer.DiagramDrawer;
import io.scenarium.gui.flow.internal.LoadPackageStream;

import javafx.beans.InvalidationListener;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.Window;

public class SubFlowDiagram extends io.scenarium.flow.operator.player.SubFlowDiagram {
	@PropertyInfo(index = 4, hidden = true, info = "Properties of the theater panel generated during the last run")
	@BeanInfo(inline = true)
	private DiagramDrawer subViewDrawer;
	@PropertyInfo(index = 5, hidden = true, nullable = false, info = "Position of the viewer on the screen")
	private Point2i subViewPosition = new Point2i();
	@PropertyInfo(index = 6, hidden = true, nullable = false, info = "Dimension of the viewer on the screen")
	private Point2i subViewDimension = new Point2i();
	@PropertyInfo(index = 7, hidden = true, info = "Defines whether this windows is kept on top of other windows")
	private boolean subViewAlwaysOnTop = false;

	private SubFlowDiagramView subFlowDiagramView;
	private static BufferedImage buffImage;

	@Override
	public void setFile(File file) {
		super.setFile(file);
		if (this.subFlowDiagramView != null)
			this.subFlowDiagramView.setScenarioData(this.getSubDiagram().getScenarioData(), getSubFlowDiagramViewTitle());
	}

	@Override
	protected void onBlockUnbound() {
		super.onBlockUnbound();
		if (this.subFlowDiagramView != null) {
			this.subFlowDiagramView.dispose();
			this.subFlowDiagramView = null;
		}
	}

	class SubFlowDiagramView implements VisuableSchedulable {
		private Stage stage;
		private Consumer<VisuableSchedulable> unregisterMethod;

		public SubFlowDiagramView(Object scenarioData, String name, BiFunction<Window, Boolean, String> saveMethod) {
			class ViewSubPane {
				RenderPane renderPane;

				RenderPane show() {
					ScenariumContainer sc = new ScenariumContainer() {

						@Override
						public void adaptSizeToDrawableElement() {
							ViewSubPane.this.renderPane.adaptSizeToDrawableElement();
						}

						@Override
						public Point2i getDefaultToolBarLocation(String simpleName) {
							return null;
						}

						@Override
						public Scheduler getScheduler() {
							return null;
						}

						@Override
						public int getSelectedElementFromTheaterEditor() {
							return -1;
						}

						@Override
						public boolean isDefaultToolBarAlwaysOnTop(String simpleName) {
							return false;
						}

						@Override
						public boolean isManagingAccelerator() {
							return false;
						}

						@Override
						public boolean isStatusBar() {
							return ViewSubPane.this.renderPane.isStatusBar();
						}

						@Override
						public void saveScenario() {
							saveMethod.apply(SubFlowDiagramView.this.stage, false);
						}

						@Override
						public void showMessage(String message, boolean error) {
							ViewSubPane.this.renderPane.showMessage(message, error);
						}

						@Override
						public void updateStatusBar(String... infos) {
							ViewSubPane.this.renderPane.updateStatusBar(infos);
						}

						@Override
						public void showTool(Class<? extends Tool> toolClass, boolean closeIfOpen) {
							ViewSubPane.this.renderPane.showTool(toolClass, closeIfOpen);
						}

						@Override
						public void updateToolView(Class<? extends Tool> toolClass, boolean isVisible) {}
					};
					this.renderPane = SubFlowDiagram.this.subViewDrawer == null ? new RenderPane((Scheduler) null, scenarioData, sc, null, false, true, true)
							: new RenderPane((Scheduler) null, scenarioData, sc, SubFlowDiagram.this.subViewDrawer, false, true, true);
					return this.renderPane;
				}
			}
			RenderPane renderPane = new ViewSubPane().show();
			if (SubFlowDiagram.this.subViewDrawer != renderPane.getTheaterPane()) {
				SubFlowDiagram.this.subViewDrawer = (DiagramDrawer) renderPane.getTheaterPane();
				SubFlowDiagram.this.propertyChanged = true;
			}
			SubFlowDiagram.this.subViewDrawer.updateTheaterFilter();
			Stage stage = new Stage();
			stage.setTitle(name);
			stage.getIcons().add(new Image(LoadPackageStream.getStream("/scenarium_icon_diagram.png")));
			SubFlowDiagram.this.subViewDrawer = (DiagramDrawer) renderPane.getTheaterPane();
			SubFlowDiagram.this.subViewDrawer.setIgnoreRepaint(false);
			SubFlowDiagram.this.subViewDrawer.repaint(false);
			StackPane pane = renderPane.getPane();
			pane.setOnKeyPressed(e1 -> {
				if (e1.isControlDown() && e1.getCode() == KeyCode.S) {
					String newName = saveMethod.apply(stage, e1.isShiftDown());
					if (newName != null)
						stage.setTitle(newName);
					e1.consume();
				}
			});
			SubFlowDiagram.this.subViewDrawer.prefWidthProperty().bind(pane.widthProperty());
			SubFlowDiagram.this.subViewDrawer.prefHeightProperty().bind(pane.heightProperty());
			SubFlowDiagram.this.subViewDrawer.addTheaterFilterPropertyChangeListener(e -> SubFlowDiagram.this.propertyChanged = true);
			Scene scene = new Scene(pane, -1, -1);
			stage.setScene(scene);
			stage.setOnCloseRequest(e1 -> {
				dispose();
				setAnimationTimer(null);
				this.stage = null;
				renderPane.close();
				SubFlowDiagram.this.subFlowDiagramView = null;
			});

			if (SubFlowDiagram.this.subViewPosition != null
					&& FxUtils.isLocationInScreenBounds(new Point2D(Math.max(SubFlowDiagram.this.subViewPosition.x, 0), Math.max(SubFlowDiagram.this.subViewPosition.y, 0)))) {
				stage.setX(SubFlowDiagram.this.subViewPosition.getX());
				stage.setY(SubFlowDiagram.this.subViewPosition.getY());
			}
			if (SubFlowDiagram.this.subViewDimension != null) {
				stage.setWidth(SubFlowDiagram.this.subViewDimension.x);
				stage.setHeight(SubFlowDiagram.this.subViewDimension.y);
			} else
				stage.sizeToScene();
			stage.setAlwaysOnTop(SubFlowDiagram.this.subViewAlwaysOnTop);
			stage.alwaysOnTopProperty().addListener(e -> {
				Stage s = stage;
				if (s != null && s.isAlwaysOnTop() != SubFlowDiagram.this.subViewAlwaysOnTop) {
					SubFlowDiagram.this.subViewAlwaysOnTop = s.isAlwaysOnTop();
					SubFlowDiagram.this.propertyChanged = true;
				}
			});
			stage.xProperty().addListener(e -> {
				Stage s = stage;
				if (s != null && s.getX() != SubFlowDiagram.this.subViewPosition.getX()) {
					SubFlowDiagram.this.subViewPosition.setX((int) s.getX());
					SubFlowDiagram.this.propertyChanged = true;
				}
			});
			stage.yProperty().addListener(e -> {
				Stage s = stage;
				if (s != null && s.getY() != SubFlowDiagram.this.subViewPosition.getY()) {
					SubFlowDiagram.this.subViewPosition.setY((int) s.getY());
					SubFlowDiagram.this.propertyChanged = true;
				}
			});
			stage.widthProperty().addListener(e -> {
				Stage s = stage;
				if (s != null && s.getWidth() != SubFlowDiagram.this.subViewDimension.getX()) {
					SubFlowDiagram.this.subViewDimension.setX((int) s.getWidth());
					SubFlowDiagram.this.propertyChanged = true;
				}
			});
			stage.heightProperty().addListener(e -> {
				Stage s = stage;
				if (s != null && s.getHeight() != SubFlowDiagram.this.subViewDimension.getY()) {
					SubFlowDiagram.this.subViewDimension.setY((int) s.getHeight());
					SubFlowDiagram.this.propertyChanged = true;
				}
			});
			this.stage = stage;
			stage.show();
		}

		public void dispose() {
			setAnimationTimer(null);
			FxUtils.runLaterIfNeeded(() -> {
				this.stage.close();
			});
		}

		public void setScenarioData(Object scenarioData, String title) {
			// Platform.runLater(() -> { // Avoid ordering problems
			SubFlowDiagram.this.subViewDrawer.setDrawableElement(scenarioData);
			SubFlowDiagram.this.subViewDrawer.repaint(false);
			// SubDiagram.this.fileChangeDone.release();
			this.stage.setTitle(title);
			// });
		}

		public void setAnimationTimer(AnimationTimerConsumer animationTimerConsumer) {
			if (animationTimerConsumer != null) {
				SubFlowDiagram.this.subViewDrawer.setAnimationTimerConsumer(animationTimerConsumer);
				animationTimerConsumer.register(this);
				this.unregisterMethod = animationTimerConsumer::unRegister;
			} else {
				SubFlowDiagram.this.subViewDrawer.setAnimationTimerConsumer(null);
				if (this.unregisterMethod != null) {
					this.unregisterMethod.accept(this);
					this.unregisterMethod = null;
				}
			}
		}

		@Override
		public void paint() {
			if (SubFlowDiagram.this.subViewDrawer.isNeedToBeRefresh())
				SubFlowDiagram.this.subViewDrawer.paintImmediately(true);
		}

		public void requestFocus() {
			this.stage.requestFocus();
		}

		@Override
		public void setAnimated(boolean animated) {}

	}

	@Override
	public Region getNode() {
		ImageView iv = new ImageView();
		StackPane sp = new StackPane(iv);
		sp.setPickOnBounds(false);
		iv.setOnMouseClicked(e -> {
			if (e.isConsumed())
				return;
			if (e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 2) {
				e.consume();
				if (this.subFlowDiagramView != null) {
					this.subFlowDiagramView.requestFocus();
					return;
				}
				File file = getFile();
				if (getSubDiagram().getScenarioData() == null)
					try {
						getSubDiagram().load(file, false);
					} catch (IOException | ScenarioException ex) {
						ex.printStackTrace();
					}
				Object sd = getSubDiagram().getScenarioData();
				if (sd != null) {
					this.subFlowDiagramView = new SubFlowDiagramView(sd, getSubFlowDiagramViewTitle(), this::saveDiagramAs);
					this.subFlowDiagramView.setAnimationTimer(this.animationTimerConsumer);
				}
			}
		});
		InvalidationListener il = e -> updateImage(sp, iv);
		sp.heightProperty().addListener(il);
		sp.widthProperty().addListener(il);
		return sp;
	}

	private String saveDiagramAs(Window window, boolean saveAs) {
		File file = null;
		if (!saveAs)
			file = getFile();
		if (file == null || !file.exists())
			file = ScenarioFileChooserFx.showSaveDialog(window, getSubDiagram(), new File(Scenarium.getWorkspace()));
		if (file != null) {
			// this.subDiagram.setFile(file);
			getSubDiagram().saveDiagram(file);
			return file.getName();
		}
		return null;
	}

	private static void updateImage(StackPane sp, ImageView iv) {
		int width = (int) (sp.getPrefWidth() * 0.7);
		int height = (int) (sp.getPrefWidth() * 0.7);
		iv.setFitHeight(height);
		iv.setFitWidth(width);
		if (width == 0 || height == 0)
			iv.setImage(null);
		else {
			createBufferedImage(width, height);
			iv.setImage(SwingFXUtils.toFXImage(buffImage, null));
		}
	}

	private static void createBufferedImage(int width, int height) {
		if (buffImage == null || buffImage.getWidth() != width || buffImage.getHeight() != height) {
			buffImage = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
			Graphics2D g = buffImage.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setColor(new Color(0, 0, 0, 20));
			int roundRect = width / 8;
			g.fillRoundRect(width / 10, height / 20, 8 * width / 10, 9 * height / 10, roundRect * 2, roundRect * 2);
			int rectWidth = 3 * width / 15;
			int rectHeight = 4 * width / 15;
			g.setColor(new Color(112, 200, 216, 80));
			g.setStroke(new BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g.drawLine(3 * width / 15 + rectWidth, 2 * height / 15 + rectHeight / 2, 8 * width / 15, 3 * height / 15 + rectHeight / 2);
			g.drawLine(3 * width / 15 + rectWidth / 2, 2 * height / 15 + rectHeight, 4 * width / 15 + rectWidth / 2, 8 * height / 15);
			g.drawLine(8 * width / 15 + rectWidth / 2, 3 * height / 15 + rectHeight, 9 * width / 15 + rectWidth / 2, 9 * height / 15);
			g.drawLine(4 * width / 15 + rectWidth, 8 * height / 15 + rectHeight / 2, 9 * width / 15, 9 * height / 15 + rectHeight / 2);
			g.setStroke(new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g.setColor(new Color(48, 29, 7, 80));
			g.drawRoundRect(3 * width / 15, 2 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
			g.drawRoundRect(8 * width / 15, 3 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
			g.drawRoundRect(4 * width / 15, 8 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
			g.drawRoundRect(9 * width / 15, 9 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
			g.setColor(new Color(112, 200, 216, 80));
			g.fillRoundRect(3 * width / 15, 2 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
			g.fillRoundRect(8 * width / 15, 3 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
			g.fillRoundRect(4 * width / 15, 8 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
			g.fillRoundRect(9 * width / 15, 9 * height / 15, rectWidth, rectHeight, roundRect, roundRect);
		}
	}

	private String getSubFlowDiagramViewTitle() {
		File file = getFile();
		return file == null || file.getPath().isBlank() ? "New Diagram" : file.getName();
	}

	@Override
	public void setAnimationTimer(AnimationTimerConsumer animationTimerConsumer) {
		super.setAnimationTimer(animationTimerConsumer);
		if (this.subFlowDiagramView != null)
			this.subFlowDiagramView.setAnimationTimer(animationTimerConsumer);
	}

	public DiagramDrawer getSubViewDrawer() {
		return this.subViewDrawer;
	}

	public void setSubViewDrawer(DiagramDrawer subViewDrawer) {
		this.subViewDrawer = subViewDrawer;
	}

	public Point2i getSubViewPosition() {
		return this.subViewPosition;
	}

	public void setSubViewPosition(Point2i subViewPosition) {
		this.subViewPosition = subViewPosition;
	}

	public Point2i getSubViewDimension() {
		return this.subViewDimension;
	}

	public void setSubViewDimension(Point2i subViewDimension) {
		this.subViewDimension = subViewDimension;
	}

	public boolean isSubViewAlwaysOnTop() {
		return this.subViewAlwaysOnTop;
	}

	public void setSubViewAlwaysOnTop(boolean subViewAlwaysOnTop) {
		this.subViewAlwaysOnTop = subViewAlwaysOnTop;
	}
}
