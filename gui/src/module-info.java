import io.scenarium.gui.flow.Plugin;
import io.scenarium.pluginManager.PluginsSupplier;

open module io.scenarium.gui.flow {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive io.scenarium.gui.core;
	requires transitive io.scenarium.flow;
	requires javafx.swing;
	requires javafx.controls;
	requires javafx.graphics;

	exports io.scenarium.gui.flow;
	exports io.scenarium.gui.flow.operator.viewer;

	requires javafx.web;

}
